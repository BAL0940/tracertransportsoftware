!*****************************************************************
!
! MODULE NAME:
!	PAR_sysstop
! FUNCTION:
!	implementation specific part of the parallelization
!	system stop for error handling
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	parsys_stop
! FUNCTION:
!	gracefully finish parallelization
! SYNTAX:
!	CALL parsys_stop
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	parsys_stop
! COMMENTS:
!	this is for the MPI-1 standard
! USES:
!
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version	j. behrens	1/98
!
!*****************************************************************
	MODULE PAR_sysstop

	USE MISC_globalparam

!---------- include the mpi definitions

	INCLUDE "mpif.h"

	PRIVATE
	PUBLIC :: parsys_stop

	CONTAINS
!*****************************************************************
	SUBROUTINE parsys_stop

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER              :: i_err

!---------- this is the implementation dependent finalize command

	CALL mpi_finalize(i_err)

	IF(i_err /= MPI_SUCCESS) THEN
	  WRITE(0,*) 'ERROR: [parsys_stop] Finalize routine has been executad unsuccessfully'
	  WRITE(0,*) 'ERROR: [parsys_stop] parallel error code:',i_err
	END IF

	RETURN
	END SUBROUTINE parsys_stop

	END MODULE PAR_sysstop
