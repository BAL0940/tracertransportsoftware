!*****************************************************************
!
! MODULE NAME:
!	PAR_datatype
! FUNCTION:
!	define the data structures for communication
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	parsys_makeinttype
! FUNCTION:
!	create integer data type for communication (of derived types)
! SYNTAX:
!	CALL parsys_makeinttype(int, int, int.arr)
! ON INPUT:
!	i_mpipnt:   pointer to type in array i_mpitypes	INTEGER
!	i_mpilen:   length of array i_mpitypes		INTEGER
! ON OUTPUT:
!	i_mpitypes: array containig data type def.s	INTEGER
! CALLS:
!	print_error
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_makerealtype
! FUNCTION:
!	create real data type for communication (of derived types)
! SYNTAX:
!	CALL parsys_makerealtype(int, int, int.arr)
! ON INPUT:
!	i_mpipnt:   pointer to type in array i_mpitypes	INTEGER
!	i_mpilen:   length of array i_mpitypes		INTEGER
! ON OUTPUT:
!	i_mpitypes: array containig data type def.s	INTEGER
! CALLS:
!	print_error
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_makeelmttype
! FUNCTION:
!	create element derived data type for communication
! SYNTAX:
!	CALL parsys_makeelmttype(int, int, int.arr)
! ON INPUT:
!	i_mpipnt:   pointer to type in array i_mpitypes	INTEGER
!	i_mpilen:   length of array i_mpitypes		INTEGER
! ON OUTPUT:
!	i_mpitypes: array containig data type def.s	INTEGER
! CALLS:
!	print_error, mpi_address, mpi_type_struct, mpi_type_commit
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_makeedgetype
! FUNCTION:
!	create edge derived data type for communication
! SYNTAX:
!	CALL parsys_makeedgetype(int, int, int.arr)
! ON INPUT:
!	i_mpipnt:   pointer to type in array i_mpitypes	INTEGER
!	i_mpilen:   length of array i_mpitypes		INTEGER
! ON OUTPUT:
!	i_mpitypes: array containig data type def.s	INTEGER
! CALLS:
!	print_error, mpi_address, mpi_type_struct, mpi_type_commit
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_makeinttype
! FUNCTION:
!	create node derived data type for communication
! SYNTAX:
!	CALL parsys_makenodetype(int, int, int.arr)
! ON INPUT:
!	i_mpipnt:   pointer to type in array i_mpitypes	INTEGER
!	i_mpilen:   length of array i_mpitypes		INTEGER
! ON OUTPUT:
!	i_mpitypes: array containig data type def.s	INTEGER
! CALLS:
!	print_error, mpi_address, mpi_type_struct, mpi_type_commit
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_makeinfotype
! FUNCTION:
!	create info derived data type for communication
! SYNTAX:
!	CALL parsys_makeinfotype(int, int, int.arr)
! ON INPUT:
!	i_mpipnt:   pointer to type in array i_mpitypes	INTEGER
!	i_mpilen:   length of array i_mpitypes		INTEGER
! ON OUTPUT:
!	i_mpitypes: array containig data type def.s	INTEGER
! CALLS:
!	print_error, mpi_address, mpi_type_struct, mpi_type_commit
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!	CAUTION: when changing the type definition in FEM_define,
!	         the definitions have to be changed here as well!
!
! USES:
!
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version	j. behrens	4/98
!
!*****************************************************************
	MODULE PAR_datatype

	USE MISC_globalparam
	USE MISC_error
	USE FEM_define
	USE PAR_define

!---------- include the mpi definitions

	INCLUDE "mpif.h"

	CONTAINS
!*****************************************************************
	SUBROUTINE parsys_makeinttype(i_mpipnt, i_mpilen, i_mpitypes)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                 :: i_mpipnt, i_mpilen
	INTEGER, DIMENSION(DEF_numtypes)    :: i_mpitypes
	INTEGER                             :: i_err

!---------- check length

	IF(i_mpipnt > DEF_numtypes) CALL print_error(199)

!---------- this is the normal integer type

	IF(i_mpipnt == DEF_inttype) THEN
	  i_mpitypes(i_mpipnt) = MPI_INTEGER
	ELSE
	  CALL print_error(199)
	END IF

	RETURN
	END SUBROUTINE parsys_makeinttype
!*****************************************************************
	SUBROUTINE parsys_makerealtype(i_mpipnt, i_mpilen, i_mpitypes)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                 :: i_mpipnt, i_mpilen
	INTEGER, DIMENSION(DEF_numtypes)    :: i_mpitypes
	INTEGER                             :: i_err

!---------- check length

	IF(i_mpipnt > DEF_numtypes) CALL print_error(199)

!---------- this is the normal integer type

	IF(i_mpipnt == DEF_realtype) THEN
	  i_mpitypes(i_mpipnt) = MPI_REAL
	ELSE
	  CALL print_error(199)
	END IF

	RETURN
	END SUBROUTINE parsys_makerealtype
!*****************************************************************
	SUBROUTINE parsys_makeelmttype(i_mpipnt, i_mpilen, i_mpitypes)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                 :: i_mpipnt, i_mpilen
	INTEGER, DIMENSION(i_mpilen)        :: i_mpitypes
	INTEGER                             :: i_err
	INTEGER                             :: i_mpitmp, i_cnt
	INTEGER, PARAMETER                  :: i_mpie= 13
	INTEGER, DIMENSION(i_mpie)          :: i_mpiecnt, i_mpietyp, i_mpiedis
	TYPE (elmt)                         :: p_etmp

!---------- behave differently according to the different data types

	i_err= MPI_SUCCESS

!---------- check length

	IF(i_mpipnt > DEF_numtypes) CALL print_error(199)

!---------- this is the element type

	elmt_typ: IF(i_mpipnt ==  DEF_elmttype) THEN

!---------- conter array

	  i_mpiecnt= (/ 1, DEF_elnodes, DEF_eledges, 3, DEF_timesteps, DEF_timesteps, &
	                DEF_evalsize, DEF_timesteps, DEF_elchild* DEF_timesteps, &
	                3, 1, DEF_timesteps, DEF_elchild* DEF_timesteps  /)
!----------- types array

	  i_mpietyp= (/ MPI_INTEGER, MPI_INTEGER, MPI_INTEGER, MPI_INTEGER, &
	                MPI_INTEGER, MPI_INTEGER, MPI_REAL, MPI_INTEGER, MPI_INTEGER, &
	                MPI_INTEGER, MPI_REAL, MPI_INTEGER, MPI_INTEGER /)

!---------- address displacements array

	  CALL mpi_address(p_etmp%def%i_indx, i_mpiedis(1), i_err)
	  CALL mpi_address(p_etmp%def%p_node(1), i_mpiedis(2), i_err)
	  CALL mpi_address(p_etmp%def%p_edge(1), i_mpiedis(3), i_err)
	  CALL mpi_address(p_etmp%att%i_time, i_mpiedis(4), i_err)
	  CALL mpi_address(p_etmp%att%i_edge(1), i_mpiedis(5), i_err)
	  CALL mpi_address(p_etmp%att%i_stat(1), i_mpiedis(6), i_err)
	  CALL mpi_address(p_etmp%att%r_vals(1), i_mpiedis(7), i_err)
	  CALL mpi_address(p_etmp%lnk%p_prnt(1), i_mpiedis(8), i_err)
	  CALL mpi_address(p_etmp%lnk%p_chil(1,1), i_mpiedis(9), i_err)
	  CALL mpi_address(p_etmp%par%i_proc, i_mpiedis(10), i_err)
	  CALL mpi_address(p_etmp%par%r_load, i_mpiedis(11), i_err)
	  CALL mpi_address(p_etmp%par%i_prnt(1), i_mpiedis(12), i_err)
	  CALL mpi_address(p_etmp%par%i_chil(1,1), i_mpiedis(13), i_err)
	  i_mpitmp= i_mpiedis(1)
	  i_mpiedis= i_mpiedis- i_mpitmp

!---------- type definition

	  CALL mpi_type_struct(i_mpie, i_mpiecnt, i_mpiedis, i_mpietyp, i_mpitmp, i_err)
	  CALL mpi_type_commit(i_mpitmp, i_err)
	  i_mpitypes(i_mpipnt) = i_mpitmp

	  IF(i_err /= MPI_SUCCESS) THEN
	    IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	    CALL print_error(160)
	  END IF	    

	END IF elmt_typ
	    
	RETURN
	END SUBROUTINE parsys_makeelmttype
!*****************************************************************
	SUBROUTINE parsys_makeedgetype(i_mpipnt, i_mpilen, i_mpitypes)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                 :: i_mpipnt, i_mpilen
	INTEGER, DIMENSION(i_mpilen)        :: i_mpitypes
	INTEGER                             :: i_err
	INTEGER                             :: i_mpitmp, i_cnt
	INTEGER, PARAMETER                  :: i_mpig= 16
	INTEGER, DIMENSION(i_mpig)          :: i_mpigcnt, i_mpigtyp, i_mpigdis
	TYPE (edge)                         :: p_gtmp

!---------- behave differently according to the different data types

	i_err= MPI_SUCCESS

!---------- check length

	IF(i_mpipnt > DEF_numtypes) CALL print_error(199)

!---------- this is the element type

	edge_typ: IF(i_mpipnt ==  DEF_edgetype) THEN

!---------- conter array

	  i_mpigcnt= (/ 1, DEF_egnodes, 1, DEF_egelems, DEF_timesteps, 1, DEF_timesteps, &
	                DEF_egchild* DEF_timesteps, 1, 3, DEF_egparcopy, DEF_egparcopy, &
	                DEF_timesteps, DEF_egelems, DEF_egchild*DEF_timesteps, 1 /)

!---------- types array

	  i_mpigtyp= MPI_INTEGER

!---------- address displacements array

	  CALL mpi_address(p_gtmp%def%i_indx, i_mpigdis(1), i_err)
	  CALL mpi_address(p_gtmp%def%p_node(1), i_mpigdis(2), i_err)
	  CALL mpi_address(p_gtmp%att%i_time, i_mpigdis(3), i_err)
	  CALL mpi_address(p_gtmp%att%p_elem(1), i_mpigdis(4), i_err)
	  CALL mpi_address(p_gtmp%att%i_node(1), i_mpigdis(5), i_err)
	  CALL mpi_address(p_gtmp%att%i_boun, i_mpigdis(6), i_err)
	  CALL mpi_address(p_gtmp%att%i_stat(1), i_mpigdis(7), i_err)
	  CALL mpi_address(p_gtmp%lnk%p_chil(1,1), i_mpigdis(8), i_err)
	  CALL mpi_address(p_gtmp%lnk%p_peri, i_mpigdis(9), i_err)
	  CALL mpi_address(p_gtmp%par%i_proc, i_mpigdis(10), i_err)
	  CALL mpi_address(p_gtmp%par%i_cppr(1), i_mpigdis(11), i_err)
	  CALL mpi_address(p_gtmp%par%i_cpid(1), i_mpigdis(12), i_err)
	  CALL mpi_address(p_gtmp%par%i_node(1), i_mpigdis(13), i_err)
	  CALL mpi_address(p_gtmp%par%i_elem(1), i_mpigdis(14), i_err)
	  CALL mpi_address(p_gtmp%par%i_chil(1,1), i_mpigdis(15), i_err)
	  CALL mpi_address(p_gtmp%par%i_peri, i_mpigdis(16), i_err)
	  i_mpitmp= i_mpigdis(1)
	  i_mpigdis= i_mpigdis- i_mpitmp

!---------- type definition

	  CALL mpi_type_struct(i_mpig, i_mpigcnt, i_mpigdis, i_mpigtyp, i_mpitmp, i_err)
	  CALL mpi_type_commit(i_mpitmp, i_err)
	  i_mpitypes(i_mpipnt) = i_mpitmp

	  IF(i_err /= MPI_SUCCESS) THEN
	    IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	    CALL print_error(160)
	  END IF

	END IF edge_typ
	    
	RETURN
	END SUBROUTINE parsys_makeedgetype
!*****************************************************************
	SUBROUTINE parsys_makenodetype(i_mpipnt, i_mpilen, i_mpitypes)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                 :: i_mpipnt, i_mpilen
	INTEGER, DIMENSION(i_mpilen)        :: i_mpitypes
	INTEGER                             :: i_err
	INTEGER                             :: i_mpitmp, i_cnt
	INTEGER, PARAMETER                  :: i_mpin= 14
	INTEGER, DIMENSION(i_mpin)          :: i_mpincnt, i_mpintyp, i_mpindis
	TYPE (node)                         :: p_ntmp

!---------- behave differently according to the different data types

	i_err= MPI_SUCCESS

!---------- check length

	IF(i_mpipnt > DEF_numtypes) CALL print_error(199)

!---------- this is the element type

	node_typ: IF(i_mpipnt ==  DEF_nodetype) THEN

!---------- conter array

	  i_mpincnt= (/ 1, DEF_dimension, 2, DEF_timesteps, DEF_timesteps, &
	                DEF_ndpatch* DEF_timesteps, DEF_nvalsize* DEF_timesteps, &
	                1, 3, DEF_ndparcopy, DEF_ndparcopy, 1, &
	                DEF_ndpatch* DEF_timesteps, 1 /)

!---------- types array

	  i_mpintyp= (/ MPI_INTEGER, MPI_REAL, MPI_INTEGER, MPI_INTEGER, &
	                MPI_INTEGER, MPI_INTEGER, MPI_REAL, MPI_INTEGER, &
	                MPI_INTEGER, MPI_INTEGER, MPI_INTEGER, MPI_INTEGER, &
	                MPI_INTEGER, MPI_INTEGER /)

!---------- address displacements array

	  CALL mpi_address(p_ntmp%def%i_indx, i_mpindis(1), i_err)
	  CALL mpi_address(p_ntmp%def%r_coor(1), i_mpindis(2), i_err)
	  CALL mpi_address(p_ntmp%att%i_time, i_mpindis(3), i_err)
	  CALL mpi_address(p_ntmp%att%i_ptch(1), i_mpindis(4), i_err)
	  CALL mpi_address(p_ntmp%att%i_stat(1), i_mpindis(5), i_err)
	  CALL mpi_address(p_ntmp%att%p_ptch(1,1), i_mpindis(6), i_err)
	  CALL mpi_address(p_ntmp%att%r_vals(1,1), i_mpindis(7), i_err)
	  CALL mpi_address(p_ntmp%lnk%p_peri, i_mpindis(8), i_err)
	  CALL mpi_address(p_ntmp%par%i_proc, i_mpindis(9), i_err)
	  CALL mpi_address(p_ntmp%par%i_cppr(1), i_mpindis(10), i_err)
	  CALL mpi_address(p_ntmp%par%i_cpid(1), i_mpindis(11), i_err)
	  CALL mpi_address(p_ntmp%par%i_edge, i_mpindis(12), i_err)
	  CALL mpi_address(p_ntmp%par%i_ptcp(1,1), i_mpindis(13), i_err)
	  CALL mpi_address(p_ntmp%par%i_peri, i_mpindis(14), i_err)
	  i_mpitmp= i_mpindis(1)
	  i_mpindis= i_mpindis- i_mpitmp

!---------- type definition

	  CALL mpi_type_struct(i_mpin, i_mpincnt, i_mpindis, i_mpintyp, i_mpitmp, i_err)
	  CALL mpi_type_commit(i_mpitmp, i_err)
	  i_mpitypes(i_mpipnt) = i_mpitmp

	  IF(i_err /= MPI_SUCCESS) THEN
	    IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	    CALL print_error(160)
	  END IF

	END IF node_typ
	    
	RETURN
	END SUBROUTINE parsys_makenodetype
!*****************************************************************
	SUBROUTINE parsys_makeinfotype(i_mpipnt, i_mpilen, i_mpitypes)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                 :: i_mpipnt, i_mpilen
	INTEGER, DIMENSION(i_mpilen)        :: i_mpitypes
	INTEGER                             :: i_err
	INTEGER                             :: i_mpitmp, i_cnt
	INTEGER, PARAMETER                  :: i_mpii= 5
	INTEGER, DIMENSION(i_mpii)          :: i_mpiicnt, i_mpiityp, i_mpiidis
	TYPE (grid_glob)                    :: p_itmp

!---------- behave differently according to the different data types

	i_err= MPI_SUCCESS

!---------- check length

	IF(i_mpipnt > DEF_numtypes) CALL print_error(199)

!---------- this is the element type

	info_typ: IF(i_mpipnt ==  DEF_infotype) THEN

!---------- conter array

	  i_mpiicnt= (/ 1, 1, 1, 1, 1 /)

!---------- types array

	  i_mpiityp= MPI_INTEGER

!---------- address displacements array

	  CALL mpi_address(p_itmp%i_totalelements, i_mpiidis(1), i_err)
	  CALL mpi_address(p_itmp%i_fineelements, i_mpiidis(2), i_err)
	  CALL mpi_address(p_itmp%i_totaledges, i_mpiidis(3), i_err)
	  CALL mpi_address(p_itmp%i_fineedges, i_mpiidis(4), i_err)
	  CALL mpi_address(p_itmp%i_totalnodes, i_mpiidis(5), i_err)
	  i_mpitmp= i_mpiidis(1)
	  i_mpiidis= i_mpiidis- i_mpitmp

!---------- type definition

	  CALL mpi_type_struct(i_mpii, i_mpiicnt, i_mpiidis, i_mpiityp, i_mpitmp, i_err)
	  CALL mpi_type_commit(i_mpitmp, i_err)
	  i_mpitypes(i_mpipnt) = i_mpitmp

	  IF(i_err /= MPI_SUCCESS) THEN
	    IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	    CALL print_error(160)
	  END IF	    

	END IF info_typ
	    
	RETURN
	END SUBROUTINE parsys_makeinfotype

	END MODULE PAR_datatype
