!*****************************************************************
!
! MODULE NAME:
!	PAR_system
! FUNCTION:
!	implementation specific part of the parallelization
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	parsys_singlesided
! FUNCTION:
!	test if singlesided communication is available
! SYNTAX:
!	logical= parsys_singlesided()
! ON INPUT:
!
! ON OUTPUT:
!	l_sing:	true if single sided comm. available	LOGICAL
! CALLS:
!
! COMMENTS:
!	this has to be set by hand
!-----------------------------------------------------------------
!
! NAME:
!	parsys_usertypes
! FUNCTION:
!	test if user defined (derived) types are supported
! SYNTAX:
!	logical= parsys_usertypes()
! ON INPUT:
!
! ON OUTPUT:
!	l_types: true if user types are supported	LOGICAL
! CALLS:
!
! COMMENTS:
!	this has to be set by hand
!-----------------------------------------------------------------
!
! NAME:
!	parsys_init
! FUNCTION:
!	initialize the parallelization library
! SYNTAX:
!	CALL parsys_init
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_totprocs
! FUNCTION:
!	total number of processors
! SYNTAX:
!	CALL parsys_totprocs(int)
! ON INPUT:
!
! ON OUTPUT:
!	i_num: total number of procs	INTEGER
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_mynumber
! FUNCTION:
!	the local processor id
! SYNTAX:
!	CALL parsys_mynumber(int)
! ON INPUT:
!
! ON OUTPUT:
!	i_num: my local process id	INTEGER
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_quit
! FUNCTION:
!	gracefully finish parallelization
! SYNTAX:
!	CALL parsys_quit
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_barrier
! FUNCTION:
!	calling interface to barrier routine
! SYNTAX:
!	CALL parsys_barrier
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_bcast
! FUNCTION:
!	broadcast data items to all other processes
! SYNTAX:
!	CALL parsys_bcast(int, int, int, *.arr)
! ON INPUT:
!	i_rootid: sender's process id		INTEGER
!	i_length: length of array		INTEGER
!	i_type:   data type handle		INTEGER
!	i_arr:    integer array (optional)	INTEGER
!	r_arr:    real array (optional)		REAL
!	e_arr:    element array (optional)	TYPE (elmt)
!	g_arr:    edge array (optional)		TYPE (edge)
!	n_arr:    node array (optional)		TYPE (node)
!	t_arr:    info array (optional)		TYPE (grid_glob)
! ON OUTPUT:
!	i_arr:    integer array (optional)	INTEGER
!	r_arr:    real array (optional)		REAL
!	e_arr:    element array (optional)	TYPE (elmt)
!	g_arr:    edge array (optional)		TYPE (edge)
!	n_arr:    node array (optional)		TYPE (node)
!	t_arr:    info array (optional)		TYPE (grid_glob)
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_send
! FUNCTION:
!	send data items to a remote process
! SYNTAX:
!	CALL parsys_send(int, int, int, int, *.arr)
! ON INPUT:
!	i_local: local process id (sender)	INTEGER
!	i_remot: remote process id (recipient)	INTEGER
!	i_length: length of array		INTEGER
!	i_type:   data type handle		INTEGER
!	i_arr:    integer array (optional)	INTEGER
!	r_arr:    real array (optional)		REAL
!	e_arr:    element array (optional)	TYPE (elmt)
!	g_arr:    edge array (optional)		TYPE (edge)
!	n_arr:    node array (optional)		TYPE (node)
!	t_arr:    info array (optional)		TYPE (grid_glob)
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!	it is (in this release) assumed that we can use non-blocking
!	send/recieve constructs
!-----------------------------------------------------------------
!
! NAME:
!	parsys_recv
! FUNCTION:
!	recieve data items from a remote process
! SYNTAX:
!	CALL parsys_recv(int, int, int, int, *.arr)
! ON INPUT:
!	i_local: local process id (sender)	INTEGER
!	i_remot: remote process id (recipient)	INTEGER
!	i_length: length of array		INTEGER
!	i_type:   data type handle		INTEGER
! ON OUTPUT:
!	i_arr:    integer array (optional)	INTEGER
!	r_arr:    real array (optional)		REAL
!	e_arr:    element array (optional)	TYPE (elmt)
!	g_arr:    edge array (optional)		TYPE (edge)
!	n_arr:    node array (optional)		TYPE (node)
!	t_arr:    info array (optional)		TYPE (grid_glob)
! CALLS:
!
! COMMENTS:
!	it is (in this release) assumed that we can use non-blocking
!	send/recieve constructs
!-----------------------------------------------------------------
!
! NAME:
!	parsys_waitallrecv
! FUNCTION:
!	wait until all nonblocking recieves are finished
! SYNTAX:
!	CALL parsys_waitallrecv
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!	uses a globally defined (but private in this module) array
!
!	CAUTION: the counter for request counting is a buggy thing:
!	when not waiting after each series of non-blocking recieves,
!	it will potentially produce deadlocks, because it is set to
!	a wrong number ...
!-----------------------------------------------------------------
!
! NAME:
!	parsys_waitallrecvinit
! FUNCTION:
!	initialize waiting after nonblocking recieves
! SYNTAX:
!	CALL parsys_waitallrecvinit
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_procsum
! FUNCTION:
!	sum up a scalar over all processors and distribute results to all
! SYNTAX:
!	CALL parsys_procsum(int, int, real, real)
! ON INPUT:
!	i_local: local variable (optional)	INTEGER
!	r_local: local variable (optional)	REAL
! ON OUTPUT:
!	i_global: global variable (optional)	INTEGER
!	r_global: global variable (optional)	REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_procmin
! FUNCTION:
!	global minimum local scalars of over all processors
! SYNTAX:
!	CALL parsys_procmin(int, int, real, real)
! ON INPUT:
!	i_local: local variable (optional)	INTEGER
!	r_local: local variable (optional)	REAL
! ON OUTPUT:
!	i_global: global variable (optional)	INTEGER
!	r_global: global variable (optional)	REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_procmax
! FUNCTION:
!	global maximum local scalars of over all processors
! SYNTAX:
!	CALL parsys_procmax(int, int, real, real)
! ON INPUT:
!	i_local: local variable (optional)	INTEGER
!	r_local: local variable (optional)	REAL
! ON OUTPUT:
!	i_global: global variable (optional)	INTEGER
!	r_global: global variable (optional)	REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	parsys_maketypes
! FUNCTION:
!	create data types for communication of derived types
! SYNTAX:
!	CALL parsys_maketypes
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!	there is an array i_mpitypes, that holds data type info
!
!-----------------------------------------------------------------
!
! NAME:
!
! FUNCTION:
!
! SYNTAX:
!
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	parsys_singlesided, parsys_usertypes, parsys_init,
!	parsys_totprocs, parsys_mynumber, parsys_quit, parsys_bcast,
!	parsys_barrier, parsys_send, parsys_recv, parsys_waitallrecv,
!	parsys_maketypes
! COMMENTS:
!	this is for the MPI-1 standard
!
!	most of the routines in this module are just translations of the
!	mpi naming into a local library-independent naming. however, there
!	are a few more complex routines, which concern the derived data
!	types used in the mesh generation and finite-element-packages, this
!	set of routines is made for.
! USES:
!	MISC_globalparam, MISC_error, PAR_define
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version	j. behrens	8/97
!
!*****************************************************************
	MODULE PAR_system

	USE MISC_globalparam
	USE MISC_error
	USE PAR_define
	USE PAR_datatype

!---------- include the mpi definitions

	INCLUDE "mpif.h"

	PRIVATE
	PUBLIC :: parsys_singlesided, parsys_usertypes, parsys_init, &
	          parsys_totprocs, parsys_mynumber, parsys_quit, parsys_bcast, &
	          parsys_maketypes, parsys_barrier, parsys_send, parsys_recv, &
	          parsys_waitallrecv

!---------- this is for user defined data types
	INTEGER, DIMENSION(DEF_numtypes) :: i_mpitypes
!---------- these are the tag and status array for sending/recieving
	INTEGER                          :: i_mpitag=17
	INTEGER, DIMENSION(MPI_STATUS_SIZE) :: i_mpistat
!---------- this is for non-blocking recieves (used in wait)
	INTEGER, DIMENSION(:), ALLOCATABLE :: i_mpirecvreq, i_mpisendreq
	INTEGER, DIMENSION(:,:), ALLOCATABLE :: i_mpistats
	INTEGER                            :: i_mpirecvreqcount, i_mpisendreqcount
	CONTAINS
!*****************************************************************
	FUNCTION parsys_singlesided() RESULT (l_side)

	IMPLICIT NONE

!---------- local declarations

	LOGICAL :: l_side

!---------- this value is set manually (false for MPI)

	l_side= .FALSE.

	RETURN
	END FUNCTION parsys_singlesided

!*****************************************************************
	FUNCTION parsys_usertypes() RESULT (l_types)

	IMPLICIT NONE

!---------- local declarations

	LOGICAL :: l_types

!---------- this value is set manually (true for MPI)

	l_types= .TRUE.

	RETURN
	END FUNCTION parsys_usertypes

!*****************************************************************
	SUBROUTINE parsys_init

	IMPLICIT NONE

!---------- local declarations

	INTEGER :: i_err, i_proc
	LOGICAL :: l_ini

!---------- check if initialization has already been performed

	CALL mpi_initialized(i_ini, i_err)
	IF(i_ini) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'WARNING: [parsys_init] Parallel execution environment already initialized!'
	  RETURN
	END IF

!---------- this is the implementation dependent initialization

	CALL mpi_init(i_err)

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

!---------- allocate data for nonblocking communications

	CALL mpi_comm_size(MPI_COMM_WORLD, i_proc, i_err)
	ALLOCATE(i_mpirecvreq(i_proc), i_mpisendreq(i_proc), stat= i_err)
	IF(i_err /= 0) THEN
	  CALL print_error(159)
	END IF
	i_mpisendreqcount= 0
	i_mpirecvreqcount= 0
	ALLOCATE(i_mpistats(MPI_STATUS_SIZE,i_proc), stat= i_err)
	IF(i_err /= 0) THEN
	  CALL print_error(159)
	END IF

	RETURN
	END SUBROUTINE parsys_init

!*****************************************************************
	SUBROUTINE parsys_totprocs(i_num)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(out) :: i_num
	INTEGER              :: i_err

!---------- this is the implementation dependent processor number

	CALL mpi_comm_size(MPI_COMM_WORLD, i_num, i_err)

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_totprocs

!*****************************************************************
	SUBROUTINE parsys_mynumber(i_num)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(out) :: i_num
	INTEGER              :: i_err

!---------- this is the implementation dependent processor id

	CALL mpi_comm_rank(MPI_COMM_WORLD, i_num, i_err)

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

!---------- first process id is 1 (for fortran array compatibility)

	i_num= i_num+ 1

	RETURN
	END SUBROUTINE parsys_mynumber

!*****************************************************************
	SUBROUTINE parsys_quit

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER              :: i_err
	LOGICAL              :: l_ini

!---------- check if parallel exec. environment is active

	CALL mpi_initialized(l_ini, i_err)
	IF(.NOT. l_ini) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'WARNING: [parsys_quit] Parallel execution environment not active!'
	  RETURN
	END IF

!---------- this is the implementation dependent finalize command

	CALL mpi_finalize(i_err)

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  WRITE(i_ioerr,*) 'ERROR: [parsys_quit] Finalize routine has been executad unsuccessfully'
	END IF

!---------- deallocate data for nonblocking communications

	IF(allocated(i_mpirecvreq)) DEALLOCATE(i_mpirecvreq)
	IF(allocated(i_mpisendreq)) DEALLOCATE(i_mpisendreq)
	IF(allocated(i_mpistats))   DEALLOCATE(i_mpistats)

	RETURN
	END SUBROUTINE parsys_quit

!*****************************************************************
	SUBROUTINE parsys_bcast(i_rootid, i_length, i_type, i_arr, r_arr, e_arr, &
	                        g_arr, n_arr, t_arr)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                             :: i_rootid, i_length, i_type
	INTEGER, DIMENSION(i_length), OPTIONAL          :: i_arr
	REAL, DIMENSION(i_length), OPTIONAL             :: r_arr
	TYPE (elmt), DIMENSION(i_length), OPTIONAL      :: e_arr
	TYPE (edge), DIMENSION(i_length), OPTIONAL      :: g_arr
	TYPE (node), DIMENSION(i_length), OPTIONAL      :: n_arr
	TYPE (grid_glob), DIMENSION(i_length), OPTIONAL :: t_arr
	INTEGER                                         :: i_err
	INTEGER                                         :: i_mpitmp, i_mpiroot


!---------- do the broadcasting using the type identifier given

	i_mpitmp= i_mpitypes(i_type)
	i_mpiroot= i_rootid- 1
	i_err   = MPI_SUCCESS

!---------- select type

	type_case: SELECT CASE (i_type)
	  CASE(DEF_inttype) type_case
	    IF(.NOT. present(i_arr)) THEN
	      CALL print_error(155)
	    END IF
	    CALL mpi_bcast(i_arr, i_length, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	  CASE(DEF_realtype) type_case
	    IF(.NOT. present(r_arr)) THEN
	      CALL print_error(155)
	    END IF
	    CALL mpi_bcast(r_arr, i_length, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	  CASE(DEF_elmttype) type_case
	    IF(.NOT. present(e_arr)) THEN
	      CALL print_error(155)
	    END IF
	    CALL mpi_bcast(e_arr, i_length, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	  CASE(DEF_edgetype) type_case
	    IF(.NOT. present(g_arr)) THEN
	      CALL print_error(155)
	    END IF
	    CALL mpi_bcast(g_arr, i_length, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	  CASE(DEF_nodetype) type_case
	    IF(.NOT. present(n_arr)) THEN
	      CALL print_error(155)
	    END IF
	    CALL mpi_bcast(n_arr, i_length, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	  CASE(DEF_infotype) type_case
	    IF(.NOT. present(t_arr)) THEN
	      CALL print_error(155)
	    END IF
	    CALL mpi_bcast(t_arr, i_length, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	  CASE DEFAULT type_case
	    CALL print_error(154)
	END SELECT type_case

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_bcast

!*****************************************************************
	SUBROUTINE parsys_gather(i_rootid, i_length, i_type, i_scl, i_arr, r_scl, r_arr)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                             :: i_rootid, i_length, i_type
	INTEGER                                         :: i_scl
	INTEGER, DIMENSION(i_length), OPTIONAL          :: i_arr
	REAL                                            :: r_scl
	REAL, DIMENSION(i_length), OPTIONAL             :: r_arr
	INTEGER                                         :: i_err
	INTEGER                                         :: i_mpitmp, i_mpiroot


!---------- do the broadcasting using the type identifier given

	i_mpitmp= i_mpitypes(i_type)
	i_mpiroot= i_rootid- 1
	i_err   = MPI_SUCCESS

!---------- select type

	type_case: SELECT CASE (i_type)
	  CASE(DEF_inttype) type_case
	    IF(present(i_arr) .AND. present(i_scl)) THEN
	      CALL mpi_gather(i_scl, 1, i_mpitmp, i_arr, 1, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	    ELSE
	      CALL print_error(155)
	    END IF
	  CASE(DEF_realtype) type_case
	    IF(present(r_arr) .AND. present(r_scl)) THEN
	      CALL mpi_gather(r_scl, 1, i_mpitmp, r_arr, 1, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	    ELSE
	      CALL print_error(155)
	    END IF
	  CASE DEFAULT type_case
	    CALL print_error(154)
	END SELECT type_case

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_gather

!*****************************************************************
	SUBROUTINE parsys_scatter(i_rootid, i_length, i_type, i_scl, i_arr, r_scl, r_arr)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                             :: i_rootid, i_length, i_type
	INTEGER, OPTIONAL                               :: i_scl
	INTEGER, DIMENSION(i_length), OPTIONAL          :: i_arr
	REAL, OPTIONAL                                  :: r_scl
	REAL, DIMENSION(i_length), OPTIONAL             :: r_arr
	INTEGER                                         :: i_err
	INTEGER                                         :: i_mpitmp, i_mpiroot


!---------- do the broadcasting using the type identifier given

	i_mpitmp= i_mpitypes(i_type)
	i_mpiroot= i_rootid- 1
	i_err   = MPI_SUCCESS

!---------- select type

	type_case: SELECT CASE (i_type)
	  CASE(DEF_inttype) type_case
	    IF(present(i_arr) .AND. present(i_scl)) THEN
	      CALL mpi_scatter(i_arr, 1, i_mpitmp, i_scl, 1, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	    ELSE
	      CALL print_error(155)
	    END IF
	  CASE(DEF_realtype) type_case
	    IF(present(r_arr) .AND. present(r_scl)) THEN
	      CALL mpi_scatter(r_arr, 1, i_mpitmp, r_scl, 1, i_mpitmp, i_mpiroot, MPI_COMM_WORLD, i_err)
	    ELSE
	      CALL print_error(155)
	    END IF
	  CASE DEFAULT type_case
	    CALL print_error(154)
	END SELECT type_case

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_scatter

!*****************************************************************
	SUBROUTINE parsys_barrier

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER              :: i_err

!---------- this is the implementation dependent processor number

	CALL mpi_barrier(MPI_COMM_WORLD, i_err)

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_barrier

!*****************************************************************
	SUBROUTINE parsys_send(i_local, i_remot, i_length, i_type, i_arr, &
	                       r_arr, e_arr, g_arr, n_arr, t_arr)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                             :: i_local, i_remot, i_length, i_type
	INTEGER, DIMENSION(i_length), OPTIONAL          :: i_arr
	REAL, DIMENSION(i_length), OPTIONAL             :: r_arr
	TYPE (elmt), DIMENSION(i_length), OPTIONAL      :: e_arr
	TYPE (edge), DIMENSION(i_length), OPTIONAL      :: g_arr
	TYPE (node), DIMENSION(i_length), OPTIONAL      :: n_arr
	TYPE (grid_glob), DIMENSION(i_length), OPTIONAL :: t_arr
	INTEGER                                         :: i_err
	INTEGER                                         :: i_mpitmp, i_mpiremot, i_mpilocal


!---------- do the broadcasting using the type identifier given

	i_mpitmp= i_mpitypes(i_type)
	i_err   = MPI_SUCCESS
	i_mpisendreqcount= i_mpisendreqcount+ 1
	i_mpilocal= i_local- 1
	i_mpiremot= i_remot- 1

!---------- select type

	type_case: SELECT CASE (i_type)
	  CASE(DEF_inttype) type_case
	    IF(.NOT. present(i_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_isend(i_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpisendreq(i_mpisendreqcount), i_err)
	  CASE(DEF_realtype) type_case
	    IF(.NOT. present(r_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_isend(r_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpisendreq(i_mpisendreqcount), i_err)
	  CASE(DEF_elmttype) type_case
	    IF(.NOT. present(e_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_isend(e_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpisendreq(i_mpisendreqcount), i_err)
	  CASE(DEF_edgetype) type_case
	    IF(.NOT. present(g_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_isend(g_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpisendreq(i_mpisendreqcount), i_err)
	  CASE(DEF_nodetype) type_case
	    IF(.NOT. present(n_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_isend(n_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpisendreq(i_mpisendreqcount), i_err)
	  CASE(DEF_infotype) type_case
	    IF(.NOT. present(t_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_isend(t_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpisendreq(i_mpisendreqcount), i_err)
	  CASE DEFAULT type_case
	    CALL print_error(154)
	END SELECT type_case

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_send

!*****************************************************************
	SUBROUTINE parsys_recv(i_local, i_remot, i_length, i_type, i_arr, &
	                       r_arr, e_arr, g_arr, n_arr, t_arr)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in)                 :: i_local, i_remot, i_type, i_length
	INTEGER, DIMENSION(i_length), OPTIONAL     :: i_arr
	REAL, DIMENSION(i_length), OPTIONAL        :: r_arr
	TYPE (elmt), DIMENSION(i_length), OPTIONAL :: e_arr
	TYPE (edge), DIMENSION(i_length), OPTIONAL :: g_arr
	TYPE (node), DIMENSION(i_length), OPTIONAL :: n_arr
	TYPE (grid_glob), DIMENSION(i_length), OPTIONAL :: t_arr
	INTEGER                             :: i_err
	INTEGER                             :: i_mpitmp, i_mpiremot, i_mpilocal


!---------- do the broadcasting using the type identifier given

	i_mpitmp= i_mpitypes(i_type)
	i_err   = MPI_SUCCESS
	i_mpirecvreqcount= i_mpirecvreqcount+ 1
	i_mpilocal= i_local- 1
	i_mpiremot= i_remot- 1

!---------- select type

	type_case: SELECT CASE (i_type)
	  CASE(DEF_inttype) type_case
	    IF(.NOT. present(i_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_irecv(i_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpirecvreq(i_mpirecvreqcount), i_err)
	  CASE(DEF_realtype) type_case
	    IF(.NOT. present(r_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_irecv(r_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpirecvreq(i_mpirecvreqcount), i_err)
	  CASE(DEF_elmttype) type_case
	    IF(.NOT. present(e_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_irecv(e_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpirecvreq(i_mpirecvreqcount), i_err)
	  CASE(DEF_edgetype) type_case
	    IF(.NOT. present(g_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_irecv(g_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpirecvreq(i_mpirecvreqcount), i_err)
	  CASE(DEF_nodetype) type_case
	    IF(.NOT. present(n_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_irecv(n_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpirecvreq(i_mpirecvreqcount), i_err)
	  CASE(DEF_infotype) type_case
	    IF(.NOT. present(t_arr)) THEN
	      CALL print_error(157)
	    END IF
	    CALL mpi_irecv(t_arr, i_length, i_mpitmp, i_mpiremot, i_mpitag, &
	                  MPI_COMM_WORLD, i_mpirecvreq(i_mpirecvreqcount), i_err)
	  CASE DEFAULT type_case
	    CALL print_error(154)
	END SELECT type_case

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_recv

!*****************************************************************
	SUBROUTINE parsys_waitallrecv

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER :: i_mpimaxcount, i_err

!---------- derive the number of requests

	i_mpimaxcount= i_mpirecvreqcount

!---------- call wait

	CALL mpi_waitall(i_mpimaxcount, i_mpirecvreq, i_mpistats, i_err)

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_waitallrecv

!*****************************************************************
	SUBROUTINE parsys_waitallrecvinit

	IMPLICIT NONE
	
!---------- local declarations

	i_mpirecvreqcount= 0
	i_mpirecvreq     = 0

	RETURN
	END SUBROUTINE parsys_waitallrecvinit

!*****************************************************************
	SUBROUTINE parsys_procsum(i_local, i_global, r_local, r_global)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in), OPTIONAL  :: i_local
	INTEGER, INTENT(out), OPTIONAL :: i_global
	REAL, INTENT(in), OPTIONAL     :: r_local
	REAL, INTENT(out), OPTIONAL    :: r_global
	INTEGER                        :: i_err

!---------- act accordint to input

	in_type: IF(present(i_local)) THEN
	  IF(.NOT. present(i_global)) THEN
	    CALL print_error(161)
	  END IF
	  CALL mpi_allreduce(i_local, i_global, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, i_err)
	ELSE IF((present(r_local)) THEN in_type
	  IF(.NOT. present(r_global)) THEN
	    CALL print_error(161)
	  END IF
	  CALL mpi_allreduce(r_local, r_global, 1, MPI_REAL, MPI_SUM, MPI_COMM_WORLD, i_err)
	ELSE in_type
	  CALL print_error(161)
	END IF in_type

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_procsum

!*****************************************************************
	SUBROUTINE parsys_procmin(i_local, i_global, r_local, r_global)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in), OPTIONAL  :: i_local
	INTEGER, INTENT(out), OPTIONAL :: i_global
	REAL, INTENT(in), OPTIONAL     :: r_local
	REAL, INTENT(out), OPTIONAL    :: r_global
	INTEGER                        :: i_err

!---------- act accordint to input

	in_type: IF(present(i_local)) THEN
	  IF(.NOT. present(i_global)) THEN
	    CALL print_error(161)
	  END IF
	  CALL mpi_allreduce(i_local, i_global, 1, MPI_INTEGER, MPI_MIN, MPI_COMM_WORLD, i_err)
	ELSE IF((present(r_local)) THEN in_type
	  IF(.NOT. present(r_global)) THEN
	    CALL print_error(161)
	  END IF
	  CALL mpi_allreduce(r_local, r_global, 1, MPI_REAL, MPI_MIN, MPI_COMM_WORLD, i_err)
	ELSE in_type
	  CALL print_error(161)
	END IF in_type

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_procmin

!*****************************************************************
	SUBROUTINE parsys_procmax(i_local, i_global, r_local, r_global)

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER, INTENT(in), OPTIONAL  :: i_local
	INTEGER, INTENT(out), OPTIONAL :: i_global
	REAL, INTENT(in), OPTIONAL     :: r_local
	REAL, INTENT(out), OPTIONAL    :: r_global
	INTEGER                        :: i_err

!---------- act accordint to input

	in_type: IF(present(i_local)) THEN
	  IF(.NOT. present(i_global)) THEN
	    CALL print_error(161)
	  END IF
	  CALL mpi_allreduce(i_local, i_global, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, i_err)
	ELSE IF((present(r_local)) THEN in_type
	  IF(.NOT. present(r_global)) THEN
	    CALL print_error(161)
	  END IF
	  CALL mpi_allreduce(r_local, r_global, 1, MPI_REAL, MPI_MAX, MPI_COMM_WORLD, i_err)
	ELSE in_type
	  CALL print_error(161)
	END IF in_type

	IF(i_err /= MPI_SUCCESS) THEN
	  IF(i_iolog > 0) WRITE(i_iolog,*) 'ERROR: parallel error code:',i_err
	  CALL print_error(160)
	END IF

	RETURN
	END SUBROUTINE parsys_procmax

!*****************************************************************
	SUBROUTINE parsys_maketypes

	IMPLICIT NONE
	
!---------- local declarations

	INTEGER                             :: i_cnt

!---------- behave differently according to the different data types

	all_types: DO i_cnt= 1, DEF_numtypes

!---------- tis is the normal integer type

	  IF(i_cnt == DEF_inttype) THEN

	    CALL parsys_makeintype(i_cnt, DEF_numtypes, i_mpitypes)

!---------- tis is the normal real type

	  ELSE IF(i_cnt == DEF_realtype) THEN

	    CALL parsys_makerealype(i_cnt, DEF_numtypes, i_mpitypes)

!---------- here we define the element type

	  ELSE IF(i_cnt == DEF_elmttype) THEN

	    CALL parsys_makeelmtype(i_cnt, DEF_numtypes, i_mpitypes)

!---------- here we define the edge type

	  ELSE IF(i_cnt == DEF_edgetype) THEN

	    CALL parsys_makeedgetype(i_cnt, DEF_numtypes, i_mpitypes)

!---------- here we define the node type

	  ELSE IF(i_cnt == DEF_nodetype) THEN

	    CALL parsys_makenodetype(i_cnt, DEF_numtypes, i_mpitypes)

!---------- here we define the info type

	  ELSE IF(i_cnt == DEF_infotype) THEN

	    CALL parsys_makeinfotype(i_cnt, DEF_numtypes, i_mpitypes)

	  END IF
	END DO all_types
	    
	RETURN
	END SUBROUTINE parsys_maketypes

	END MODULE PAR_system
