!*****************************************************************
!
! MODULE NAME:
!    ADV_semilagrange
! FUNCTION:
!    perform semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!    slm_adapt
! FUNCTION:
!    adapt the grid according to an error estimate
! SYNTAX:
!    CALL slm_adapt(grid, param, logical)
! ON INPUT:
!    p_ghand:   handle for the grid        TYPE (grid_handle)
!    p_param:   global parameter structure    TYPE (global_param)
! ON OUTPUT:
!    l_changed: flag for changed grid    LOGICAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!    slm_initialize
! FUNCTION:
!    initialize the advection problem
! SYNTAX:
!    CALL slm_initialize(grid, param)
! ON INPUT:
!    p_param: parameter data structure    TYPE (global_param)
! ON OUTPUT:
!    p_ghand: grid handling data structure    TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!    slm_finish
! FUNCTION:
!    terminate slm (free dynamically alloc. memory, ...)
! SYNTAX:
!    CALL slm_finish(grid, param)
! ON INPUT:
!    p_ghand: grid handling data structure    TYPE (grid_handle)
!    p_param: parameter data structure    TYPE (global_param)
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!    slm_timestepping
! FUNCTION:
!    perform the timestepping in the slm
! SYNTAX:
!    CALL slm_timestepping(grid, param, cmd)
! ON INPUT:
!    p_ghand: grid handling data structure    TYPE (grid_handle)
!    p_param: parameter data structure    TYPE (global_param)
!    p_cmdln: command line argument struct.    TYPE (cmdline)
! ON OUTPUT:
!    p_ghand: grid handling data structure    TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!    slm_displace, slm_update, slm_upstream
!    slm_initialize, slm_finish, slm_timestepping
! COMMENTS:
!
! USES:
!    MISC_globalparam, MISC_error, FEM_handle
!    FEM_errorestimate, FEM_param
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!    1. original version            j. behrens    10/96
!    2. several improvements/fixes  j. behrens    11/96-1/97
!    3. nodal values time depend.   j. behrens    1/97
!    4. stop_watch removed, plot    j. behrens    1/97
!       (position) changed, inner
!       iteration counter added
!    5. slm_adapt changed           j. behrens    2/97
!    6. slm_adapt changed to hide
!       grid data structures        j. behrens    7/97
!    7. control data structure      j. behrens    12/97
!    8. non-homog. rhs added        j. behrens    2/98
!    9. compliant to amatos 1.0     j. behrens    12/2000
!    10. compliant to amatos 1.2    j. behrens    3/2002
!    11. compliant to amatos 2.0    j. behrens    7/2003
!    12. added visnetplot           f. klaschka   12/2003
!
!*****************************************************************
    MODULE ADV_semilagrange
      USE FLASH_parameters
      USE MISC_timing
      USE IO_vtuplot
#ifndef NO_NETCDF
      USE IO_netcdfplot
#endif
      USE IO_utils
      USE GRID_api
      USE SLM_errorestimate
      USE SLM_initial
      USE SLM_simple
      USE ADV_wind
      USE ADV_rhs
      PRIVATE
      PUBLIC  :: slm_initialize, slm_finish, slm_timestepping

!      USE SLM_advanced

      CONTAINS
!*****************************************************************
      SUBROUTINE slm_adapt(p_ghand, p_param, l_changed, l_water)


!---------- local declarations

      IMPLICIT NONE
      TYPE (grid_handle), INTENT(inout)      :: p_ghand
      TYPE (control_struct), INTENT(in)      :: p_param
      LOGICAL, INTENT(out)                   :: l_changed
      LOGICAL, OPTIONAL, INTENT(in)          :: l_water
      LOGICAL                                :: l_switch
      REAL (KIND = GRID_SR)                                   :: r_errmx, &
        r_refcrit, r_crscrit, r_fac
      INTEGER                                :: i_size, &
        i_manyc, i_manyr, i_alct, i_cnt
      REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE        :: r_aux1
      INTEGER, DIMENSION(:), ALLOCATABLE     :: i_aux1, i_aux2, i_aux3
      LOGICAL                                :: l_ref, l_crs

!---------- initialize refinement flag

      l_changed= .FALSE.

!---------- handle watermark switch

      wat_present: IF(present(l_water)) THEN
        l_switch= l_water
      ELSE wat_present
        l_switch= .TRUE.
      END IF wat_present

!---------- allocate work arrays

      i_size= p_ghand%i_enumfine
      allocate(r_aux1(i_size), i_aux1(i_size), i_aux2(i_size), &
                   i_aux3(i_size), stat=i_alct)
      not_alloc: IF(i_alct /= 0) THEN
        CALL grid_error(35)
      END IF not_alloc

!---------- estimate the local error

      CALL slm_errorest(p_ghand, i_size, r_aux1)

!---------- set coarsening/refinement criterion

      r_errmx= maxval(r_aux1(1:i_size))
      r_crscrit= r_errmx* p_param%num%r_crstolerance
      r_refcrit= r_errmx* p_param%num%r_reftolerance

!---------- get level information and set up flags for refinement/coarsening

      CALL grid_getinfo(p_ghand, l_finelevel= .TRUE., i_elementlevel= i_aux1, &
                        i_elementstatus= i_aux3)
      DO i_cnt=1,i_size
        i_aux2(i_cnt)= 0
        IF((i_aux1(i_cnt) > p_param%num%i_crslevel) .AND. &
           (r_aux1(i_cnt) < r_crscrit)) i_aux2(i_cnt)= GRID_pleasecoarse
        IF((i_aux1(i_cnt) < p_param%num%i_reflevel) .AND. &
           (r_aux1(i_cnt) > r_refcrit)) i_aux2(i_cnt)= GRID_pleaserefine
      END DO

!---------- determine if there is enough to be done (this can be
!           switched off by l_water=.FALSE.)

      IF(l_switch) THEN
        i_manyr= count(i_aux2 == GRID_pleaserefine)
        r_fac= real(i_manyr,GRID_SR)/ real(i_size,GRID_SR)
        enough_ref: IF(r_fac > p_param%num%r_refwatermark) THEN
          l_ref= .TRUE.
        ELSE
          l_ref= .FALSE.
        END IF enough_ref

        i_manyc= count(i_aux2 == GRID_pleasecoarse)
        r_fac= real(i_manyc,GRID_SR)/ real(i_size,GRID_SR)
        enough_crs: IF(r_fac > p_param%num%r_crswatermark) THEN
          l_crs= .TRUE.
        ELSE
          l_crs= .FALSE.
        END IF enough_crs
      ELSE
        l_ref= .TRUE.
        l_crs= .TRUE.
      END IF

!---------- update grid flags

      update: IF(l_ref .OR. l_crs) THEN
        IF(l_ref) i_aux3= merge(i_aux2, i_aux3, i_aux2==GRID_pleaserefine)
        IF(l_crs) i_aux3= merge(i_aux2, i_aux3, i_aux2==GRID_pleasecoarse)
        CALL grid_putinfo(p_ghand, l_finelevel= .TRUE., i_elementstatus= i_aux3)
      END IF update

!---------- deallocate work arrays

      deallocate(r_aux1, i_aux1, i_aux2, i_aux3)

!---------- adapt the grid

      CALL grid_adapt(p_ghand, l_changed)

      RETURN
      END SUBROUTINE slm_adapt

!*****************************************************************
      SUBROUTINE slm_diagnostics(p_ghand, p_param, p_tinfo, c_action)

!---------- local declarations

      IMPLICIT NONE
      TYPE (grid_handle), INTENT(in)           :: p_ghand
      TYPE (control_struct), INTENT(inout)     :: p_param
      TYPE (rt_info), INTENT(in)               :: p_tinfo
      CHARACTER (len=4), INTENT(in), OPTIONAL  :: c_action
      INTEGER, SAVE                            :: i_iodiag
      CHARACTER (len=32)                       :: c_file
      CHARACTER (len=28)                       :: c_tmp
      REAL (KIND = GRID_SR), PARAMETER                          :: r_1o3= (1./3.)
      INTEGER                                  :: i_fst, i_tmp, &
        i_size, i_alct, i_1, i_2, i_3, i_4, i_5, i_6
      REAL (KIND = GRID_SR), SAVE                               :: r_rfm0, r_rsm0
      REAL (KIND = GRID_SR)                                     :: r_dispn, r_rfm, r_rsm, &
        r_ts, r_calci, r_calcs, r_mxnrm, r_l2nrm, r_max, r_min, r_diffn, &
        r_medln
      REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE          :: r_aux1, r_aux2, &
        r_aux3, r_aux4
      REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE        :: r_auxx
      INTEGER, DIMENSION(1)                    :: i_valind

!---------- action init

      present_act: IF(present(c_action)) THEN
        action_type: IF(c_action == 'init') THEN

!---------- open file for diagnostic output

          i_iodiag= 9
          i_tmp   = p_param%num%i_experiment
          write(c_tmp,*) trim(GRID_parameters%program_name), '_diag.'
          write(c_file,1010) trim(c_tmp), i_tmp
          c_file= adjustl(c_file)
          open(i_iodiag, file= c_file, action= 'write', form= 'formatted', &
               iostat= i_fst)
          not_opened: IF(i_fst /= 0) THEN
            CALL grid_error(36)
          END IF not_opened
          IF(GRID_parameters%iolog > 0) &
            write(GRID_parameters%iolog,*) 'INFO: Filename: ', c_file, ' opened on unit: ', i_iodiag

!---------- allocate workspace

          i_size= p_ghand%i_nnumber
          allocate(r_aux1(i_size), r_aux2(i_size), r_aux3(i_size), &
                   r_aux4(i_size), r_auxx(1,i_size), stat=i_alct)
          not_alloc: IF(i_alct /= 0) THEN
            CALL grid_error(37)
          END IF not_alloc 
          r_aux1= 0.0; r_aux2= 0.0; r_aux3= 0.0; r_aux4= 0.0

!---------- get minimum edge length

          CALL grid_edgelength(p_ghand, r_min=r_medln)

!---------- calculate reference values, ... extract actual calculated concentration

          i_valind= (/ GRID_tracer /)
          CALL grid_getinfo(p_ghand, i_arraypoint=i_valind, &
                            r_nodevalues= r_auxx)
          r_aux1(:)= r_auxx(1,:)
          DEALLOCATE(r_auxx)

!---------- calculate area pieces for each node

          CALL grid_nodearea(p_ghand, i_size, r_aux2)

!---------- calculate analytical solution

          r_ts= p_param%num%r_deltatime* float(p_tinfo%i_step)
          CALL slm_analyticsolution(p_ghand, r_ts, i_size, r_aux3)

!---------- now the integral of the concentration (mass) is

          r_calci= dot_product(r_aux1, r_aux2)
          r_rfm0 = r_calci

!---------- the integral of the squared concentration ("entropy"(?)) is

          r_aux4 = r_aux1* r_aux1
          r_calcs= dot_product(r_aux4, r_aux2)
          r_rsm0 = r_calcs

!---------- the maximum-norm of the error is

          r_aux4 = abs(r_aux1- r_aux3)
          r_mxnrm= maxval(r_aux4)

!---------- the l2-norm of the error is

          r_aux4 = r_aux4* r_aux4
          r_l2nrm= dot_product(r_aux4, r_aux2)

!---------- maximum and minimum

          r_max  = maxval(r_aux1)
          r_min  = minval(r_aux1)

!---------- diffusion and dispersion (not yet implemented)

          r_diffn= 0.0
          r_dispn= 0.0

!---------- print it

          r_rfm= r_calci/r_rfm0
          r_rsm= r_calcs/r_rsm0
          write(i_iodiag,1100) GRID_parameters%program_name, GRID_parameters%version, &
                               GRID_parameters%subversion, GRID_parameters%patchversion
          i_1= p_tinfo%i_step
          i_2= p_ghand%i_enumber
          i_3= p_ghand%i_enumfine
          i_4= p_ghand%i_gnumber
          i_5= p_ghand%i_gnumfine
          i_6= p_ghand%i_nnumber
          write(i_iodiag,1000) i_1, i_2, i_3, i_4, i_5, i_6, r_min, r_max, &
                               r_rfm, r_rsm, r_mxnrm, r_l2nrm, r_diffn, &
                   r_dispn, r_medln

!---------- deallocate workspace

          deallocate(r_aux1, r_aux2, r_aux3, r_aux4)

!---------- initialization done

          RETURN

!---------- action quit

        ELSE IF(c_action == 'quit') THEN action_type

!---------- close diagnostic output file

          close(i_iodiag)
           IF(GRID_parameters%iolog > 0) &
            write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_iodiag

!---------- action quit done

          RETURN
        END IF action_type
      END IF present_act

!---------- action diag (default): allocate workspace

      i_size= p_ghand%i_nnumber
      allocate(r_aux1(i_size), r_aux2(i_size), r_aux3(i_size), &
               r_aux4(i_size), r_auxx(1,i_size), stat=i_alct)
      not_allc: IF(i_alct /= 0) THEN
        CALL grid_error(37)
      END IF not_allc 
      r_aux1= 0.0; r_aux2= 0.0; r_aux3= 0.0; r_aux4= 0.0

!---------- get minimum edge length

      CALL grid_edgelength(p_ghand, r_min=r_medln)

!---------- calculate reference values, ... extract actual calculated concentration

      i_valind= (/ GRID_tracer /)
      CALL grid_getinfo(p_ghand, i_arraypoint=i_valind, &
                 r_nodevalues= r_auxx)
      r_aux1(:)= r_auxx(1,:)
      DEALLOCATE(r_auxx)

!---------- calculate area pieces for each node

      CALL grid_nodearea(p_ghand, i_size, r_aux2)

!---------- calculate analytical solution

      r_ts= p_param%num%r_deltatime* float(p_tinfo%i_step)
      CALL slm_analyticsolution(p_ghand, r_ts, i_size, r_aux3)

!---------- now the integral of the concentration (mass) is

      r_calci= dot_product(r_aux1, r_aux2)

!---------- the integral of the squared concentration ("entropy"(?)) is

      r_aux4 = r_aux1* r_aux1
      r_calcs= dot_product(r_aux4, r_aux2)

!---------- the maximum-norm of the error is

      r_aux4 = abs(r_aux1- r_aux3)
      r_mxnrm= maxval(r_aux4)

!---------- the l2-norm of the error is

      r_aux4 = r_aux4* r_aux4
      r_l2nrm= dot_product(r_aux4, r_aux2)

!---------- maximum and minimum

      r_max  = maxval(r_aux1)
      r_min  = minval(r_aux1)

!---------- diffusion and dispersion (not yet implemented)

      r_diffn= 0.0
      r_dispn= 0.0

!---------- print it

      r_rfm= r_calci/r_rfm0
      r_rsm= r_calcs/r_rsm0
      i_1= p_tinfo%i_step
      i_2= p_ghand%i_enumber
      i_3= p_ghand%i_enumfine
      i_4= p_ghand%i_gnumber
      i_5= p_ghand%i_gnumfine
      i_6= p_ghand%i_nnumber
      write(i_iodiag,1000) i_1, i_2, i_3, i_4, i_5, i_6, r_min, r_max, &
                           r_rfm, r_rsm, r_mxnrm, r_l2nrm, r_diffn, &
                           r_dispn, r_medln

!---------- deallocate workspace

      deallocate(r_aux1, r_aux2, r_aux3, r_aux4)

      RETURN
 1000      FORMAT(1x, i10, 1x, i10, 1x, i10, 1x, i10, 1x, i10, 1x, i10, &
             1x, e15.8, 1x, e15.8, 1x, e15.8, 1x, e15.8,&
             1x, e15.8, 1x, e15.8, 1x, e15.8, 1x, e15.8,&
             1x, e15.8)
 1010      FORMAT(a28,i4.4)
 1100      FORMAT(1x,'*******************************************', &
                '*******************************************', &
                '*******************************************', &
                '*******************************************', &
                '*************************************',/ &
             1x,'***** PROGRAM: ',a15,174x,'*****',/ &
             1x,'***** VERSION: ',i2.2,'.',i2.2,'.',i2.2,181x,'*****',/ &
             1x,'***** Diagnostic output ',180x,'*****',/ &
             1x,'*******************************************', &
                '*******************************************', &
                '*******************************************', &
                '*******************************************', &
                '*************************************',/ &
             1x,'* timestep ','  elements ','  fine el. ','     edges ', &
                '  fine ed. ','     nodes ','        minimum ', &
                '        maximum ','            RFM ','            RSM ', &
                    '       max-norm ','        l2-norm ','      diffusion ', &
                '     dispersion ',' min.edge len.*',/ &
             1x,'*******************************************', &
                '*******************************************', &
                '*******************************************', &
                '******************************************', &
                '**************************************')
      END SUBROUTINE slm_diagnostics

!*****************************************************************
      SUBROUTINE slm_initialize(p_ghand, p_param)

!---------- local declarations

      IMPLICIT NONE

      TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(out) :: p_ghand
      TYPE (control_struct), INTENT(inout)          :: p_param

      INTEGER                                     :: i_steps
      CHARACTER (len=32)                          :: c_file
      CHARACTER (len=28)                          :: c_tmp
      INTEGER                                     :: i_tmp, i_cnt
      LOGICAL                                     :: l_refined
      INTEGER                                     :: i_vertnum, i_alct
      REAL (KIND = GRID_SR), DIMENSION(:,:), POINTER               :: r_vertinit

!---------- decide whether a new experiment is startet or an old one is continued

      new_experiment: IF(p_param%num%i_experiment <= 0) THEN

!---------- reset timesteps (start with 1 in any case)

        time_one: IF(p_param%num%i_frsttimestep /= 1) THEN
          IF(GRID_parameters%iolog > 0) &
            write(GRID_parameters%iolog,*) 'WARNING      : Timestep counters reset due to new experiment'
          i_steps= p_param%num%i_lasttimestep- p_param%num%i_frsttimestep
          p_param%num%i_frsttimestep= 1
          p_param%num%i_lasttimestep= p_param%num%i_frsttimestep+ i_steps
        END IF time_one

!---------- initialize variables for stacked layers

        i_stcnumlayers = p_param%tst%tst_int(1,1)
        i_stctracer = grid_registerfemvar(1,i_length=i_stcnumlayers)
        i_stcuv     = grid_registerfemvar(1,i_length=(i_stcnumlayers*3))
        allocate(r_stclayerpressure(i_stcnumlayers), stat=i_alct)
        not_allc: IF(i_alct /= 0) THEN
            CALL grid_error(c_error='[slm_initialize]: Could not allocate layers pressure array')
        END IF not_allc 
        

!---------- initialize grid parameters

        CALL grid_setparameter(p_ghand, i_coarselevel= p_param%num%i_crslevel, &
                               i_finelevel= p_param%num%i_reflevel)

!---------- define domain, first read data from file (compiled here)

        CALL grid_readdomain(i_vertnum, r_vertinit, c_readfile=p_param%io%c_domainfile)
        CALL grid_definegeometry(i_vertnum, r_vertexarr= r_vertinit)

!---------- create initial triangulation

        CALL grid_createinitial(p_ghand, c_filename=p_param%io%c_triangfile)

!---------- initialize wind field calculation

        CALL slm_windinit(p_param)

!---------- initialize grid and adapt at steep gradients

        i_cnt= 0
        l_refined= .TRUE.
        refine_loop: DO WHILE (l_refined)
          CALL slm_initialvalues(p_ghand(i_timeplus))
          CALL slm_adapt(p_ghand(i_timeplus), p_param, l_refined, &
                         l_water=.FALSE.)
        END DO refine_loop

!---------- duplicate grid (old time)

        CALL grid_timeduplicate(p_ghand(i_timeplus), p_ghand(i_time))

!---------- if an old experiment is to be continued from stored data:

      ELSE new_experiment

!---------- create grid from saveset, first compile filename

        i_tmp= p_param%num%i_experiment- 1
        write(c_tmp,*) trim(GRID_parameters%program_name), '_save.'
        write(c_file,1010) trim(c_tmp), i_tmp
        c_file= adjustl(c_file)

        CALL grid_readinitial(p_ghand, c_file)

!---------- initialize wind field calculation

        CALL slm_windinit(p_param)

      END IF new_experiment

      RETURN
 1010      FORMAT(a28,i4.4)
      END SUBROUTINE slm_initialize

!*****************************************************************
      SUBROUTINE slm_finish(p_ghand, p_param)

!---------- local declarations

      IMPLICIT NONE

      TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
      TYPE (control_struct), INTENT(in)                          :: p_param
      CHARACTER (len=32)                                       :: c_file
      CHARACTER (len=28)                                       :: c_tmp
      INTEGER                                                  :: i_tmp

!---------- open and write saveset, if required

      save_req: IF(p_param%io%i_savelast /= 0) THEN

        i_tmp= p_param%num%i_experiment
        write(c_tmp,*) trim(GRID_parameters%program_name), '_save.'
        write(c_file,1010) trim(c_tmp), i_tmp
        c_file= adjustl(c_file)
        CALL grid_writesaveset(c_file, p_ghand)

!---------- write parameter file for next experiment

        CALL io_putinputfile(p_param)
      END IF save_req

!---------- gracefully terminate wind field calculations

      CALL slm_windquit

      RETURN
 1010      FORMAT(a28,i4.4)
      END SUBROUTINE slm_finish

!*****************************************************************
      SUBROUTINE slm_timestepping(p_ghand, p_param)

!---------- local declarations

      IMPLICIT NONE

      INTEGER (KIND = GRID_SI), PARAMETER                 :: i_innermax=15
      TYPE (grid_handle), DIMENSION(GRID_timesteps), &
                          INTENT(inout)                   :: p_ghand
      TYPE (control_struct), INTENT(inout)                :: p_param
      INTEGER (KIND = GRID_SI)                            :: i_timecount
      TYPE (sw_info)                                      :: p_time, p_timeaux
      LOGICAL                                             :: l_refined
      REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE    :: r_tracer
      REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_coord, r_aux
      REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_3dtracer
      CHARACTER (len=32)                                  :: c_file, c_matfile
      CHARACTER (len=28)                                  :: c_tmp
      INTEGER (KIND = GRID_SI)                            :: i_tmp, &
        i_size, i_alct, i_tst, i_fst, i_lay, i_start
      INTEGER (KIND = GRID_SI)                            :: i_iomatl=21
      REAL (KIND = GRID_SR)                               :: r_modtime
      INTEGER (KIND = GRID_SI)                            :: i_newlen, i_ndep
      INTEGER (KIND = GRID_SI)                            :: i_layerid
      INTEGER (KIND = GRID_SI), DIMENSION(i_stcnumlayers) :: i_valind

!---------- initialize constant

      i_ndep = 1_GRID_SI

!---------- initialize timestep info structure

      p_timestepinfo%i_step       = 0
      p_timestepinfo%i_adapit     = 0
      p_timestepinfo%l_ploted     = .FALSE.
      p_timestepinfo%l_saved      = .FALSE.
      p_timestepinfo%r_modeltime  = 0.0

!---------- initialize timing structure

      p_time%p_tim%r_tim   = 0.0
      p_time%p_tim%r_lap   = 0.0
      p_time%p_tim%c_tim   = '                '
      p_timeaux%p_tim%r_tim= 0.0
      p_timeaux%p_tim%r_lap= 0.0
      p_timeaux%p_tim%c_tim= '                '

!---------- initialize stop watches

      CALL stop_watch_init(1,(/'total time      '/),p_timeaux)
      CALL stop_watch_init(8,(/'plotting        ', 'grid duplication', &
                               'trajectory calc.', 'right hand side ', &
                               'grid value updt.', 'grid adaption   ', &
                               'diagnostics     ', 'whole timestep  '/), p_time)
                               
!---------- if diagnostics are demanded, initialize diagnostical output

      IF(p_param%io%l_diagnostics) THEN
        p_timestepinfo%i_step= 0
        CALL slm_diagnostics(p_grid(i_timeplus), p_param, p_timestepinfo, c_action='init')
      END IF

!---------- plot initial data

      i_timecount= 0
#ifndef NO_NETCDF
      IF(p_param%io%l_netcdf) THEN
        CALL plot_netcdf(p_ghand(i_timeplus), i_time=i_timecount)
      END IF
#endif
      IF(p_param%io%l_vtu) THEN
        CALL generate_vtu(p_ghand(i_timeplus), i_time=i_timecount)
      END IF
!---------- put out initial information

      CALL io_putruntimeinfo(p_ghand(i_timeplus), p_timestepinfo, p_time)

!---------- timestep loop

      CALL stop_watch('start',1,p_timeaux)
      CALL stop_watch('start',8,p_time)
      i_timecount = 0_GRID_SI
      p_timestepinfo%r_modeltime = p_param%num%r_starttime
      time_loop: DO WHILE (p_timestepinfo%r_modeltime < p_param%num%r_finaltime - p_param%num%r_deltatime)
        i_timecount                = i_timecount+ 1_GRID_SI
        p_timestepinfo%i_step      = i_timecount
        p_timestepinfo%r_modeltime = p_timestepinfo%r_modeltime + p_param%num%r_deltatime
        p_timestepinfo%i_adapit    = 0_GRID_SI

!---------- duplicate old grid, use it as first guess for new grid

        CALL stop_watch('start',2,p_time)
        CALL grid_timeduplicate(p_ghand(i_time), p_ghand(i_timeplus))
        CALL stop_watch('stop ',2,p_time)

!---------- adaptive (inner) loop

        l_refined= .TRUE.
        adap_loop: DO WHILE(l_refined .AND. p_timestepinfo%i_adapit < i_innermax)
          p_timestepinfo%i_adapit= p_timestepinfo%i_adapit+ 1

!---------- allocate and extract working arrays
!---------- use amatos 1.2 functionality to calculate only new nodes

          i_size= p_ghand(i_timeplus)%i_nnumber
          allocate(r_aux(GRID_dimension,i_size), stat=i_alct)
          not_alloc: IF(i_alct /= 0) THEN
            CALL grid_error(38)
          END IF not_alloc

!-SLM--------- do the following SLM calculations in arrays (grid-point-wise)

          CALL grid_getinfo(p_ghand(i_timeplus), r_nodecoordinates=r_aux, &
                            i_newsdepth= 1, i_nlength= i_newlen)
          allocate(r_tracer(i_newlen), r_coord(GRID_dimension,i_newlen), stat=i_alct)
          not_alloc0: IF(i_alct /= 0) THEN
            CALL grid_error(38)
          END IF not_alloc0
          r_coord(:,1:i_newlen)= r_aux(:,1:i_newlen)
          deallocate(r_aux)

!-SLM--------- allocate 3d tracer field

          allocate(r_3dtracer(i_stcnumlayers,i_newlen), stat=i_alct)
          not_alloc1: IF(i_alct /= 0) THEN
            CALL grid_error(38)
          END IF not_alloc1

!-SLM--------- loop over stacked layers

          layer_loop: DO i_lay=1,i_stcnumlayers

!-SLM--------- call the SLM step

            r_modtime= p_timestepinfo%r_modeltime- p_param%num%r_deltatime
            CALL slm_step(p_ghand, p_param, p_time, r_modtime, i_newlen, &
                          r_coord, r_tracer, i_ndep, i_lay)

!-SLM--------- store layer data in 3D array

            r_3dtracer(i_lay,:)= r_tracer(:)

          END DO layer_loop

!-SLM--------- call vertical advection step

          CALL slm_verticalstep(p_ghand, p_param, p_time, r_modtime, i_newlen, &
                                r_coord, r_3dtracer, i_ndep)

!-SLM--------- update grid data structure and deallocate work arrays
!-SLM--------- change back from (grid-point)arrays to grid data structure

          i_valind= (/ ((i_stctracer-1+i_lay), i_lay=1,i_stcnumlayers) /)
          CALL grid_putinfo(p_ghand(i_timeplus), i_arraypoint= i_valind, &
                            i_newsdepth= 1, r_nodevalues= r_3dtracer)
          deallocate(r_coord, r_tracer, r_3dtracer)

!-SLM--------- adapt the grid corresponding to an error estimate

          CALL stop_watch('start',6,p_time)
          CALL slm_adapt(p_ghand(i_timeplus), p_param, l_refined)
          CALL stop_watch('stop ',6,p_time)

        END DO adap_loop

!---------- diagnostics, if requested

        IF(p_param%io%l_diagnostics) THEN
          CALL stop_watch('start',7,p_time)
          CALL slm_diagnostics(p_grid(i_timeplus), p_param, p_timestepinfo, c_action='diag')
          CALL stop_watch('stop ',7,p_time)
        END IF

!---------- plot data (every [i_plotoffset]th timestep)

        CALL stop_watch('start',1,p_time)
        plot_step: IF(mod(i_timecount, p_param%io%i_plotoffset) == 0) THEN
#ifndef NO_NETCDF
          IF(p_param%io%l_netcdf) THEN
            CALL plot_netcdf(p_ghand(i_timeplus), i_time=i_timecount)
            p_timestepinfo%l_ploted= .TRUE.
          END IF
#endif
          IF(p_param%io%l_vtu) THEN
            CALL generate_vtu(p_ghand(i_timeplus), i_time=i_timecount)
            p_timestepinfo%l_ploted= .TRUE.
          END IF
        END IF plot_step
        CALL stop_watch('stop ',1,p_time)

!---------- put a saveset to disc every ... timesteps

        save_step: IF((mod(i_timecount, p_param%io%i_saveoffset) == 0) .AND. &
                      (i_timecount > 1)) THEN
          i_tmp= p_param%num%i_experiment
           write(c_tmp,*) trim(GRID_parameters%program_name), '_save.'
          write(c_file,1010) trim(c_tmp), i_tmp
          c_file= adjustl(c_file)
          CALL grid_writesaveset(c_file,p_ghand)
          p_timestepinfo%l_saved= .TRUE.
        END IF save_step

!---------- runtime information output

        CALL stop_watch('stop ',8,p_time)
        CALL io_putruntimeinfo(p_ghand(i_timeplus), p_timestepinfo, p_time)
        CALL stop_watch_init(8,(/'plotting        ', 'grid duplication', &
                                 'trajectory calc.', 'right hand side ', &
                                 'grid value updt.', 'grid adaption   ', &
                                 'diagnostics     ', 'whole timestep  '/), p_time)
        CALL stop_watch('start',8,p_time)

!---------- remove obsolecent grid items

        CALL grid_sweep

!---------- toggle time handles for next step if gfx-proces has not exited

        CALL grid_timetoggle

      END DO time_loop
      CALL stop_watch('stop ',1,p_timeaux)

!---------- print total time

       write(GRID_parameters%ioout,1005)
      write(GRID_parameters%ioout,1003) p_timeaux%p_tim(1)%r_tim
      write(GRID_parameters%ioout,1004)
      IF(GRID_parameters%iolog > 0) THEN
        write(GRID_parameters%iolog,1003) p_timeaux%p_tim(1)%r_tim
      END IF

!---------- terminate diagnostics

      IF(p_param%io%l_diagnostics) THEN
        CALL slm_diagnostics(p_grid(i_timeplus), p_param, p_timestepinfo, c_action='quit')
      END IF

      RETURN
 1000      FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/ &
             1x,'*****            Runtime Information Output           *****',/ &
             1x,'***** ----- ----- ----- ----- ----- ----- ----- ----- *****')
 1003      FORMAT(1x,'***** Total time for timesteps ',10x,e12.4,' *****')
 1004      FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/)
 1005      FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/ &
             1x,'*****             Final Information Output            *****',/ &
             1x,'***** ----- ----- ----- ----- ----- ----- ----- ----- *****')
 1010      FORMAT(a28,i4.4)
      END SUBROUTINE slm_timestepping

    END MODULE ADV_semilagrange
