!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize slotted cylinder for semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initslot
! FUNCTION:
!	initialize a grid with values of slotted cylinder test case
! SYNTAX:
!	CALL slm_initslot(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initcylndr
! FUNCTION:
!	initialize a grid with values of a cylinder test case
! SYNTAX:
!	CALL slm_initcylndr(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_centr: coordinates of cyl. centre	REAL
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	in_side
! FUNCTION:
!	checks, if a given point (x,y) lies within a given triangle
! SYNTAX:
!	logical= in_side(real.arr, real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord: coordinate array		REAL
!	r_node1: node1 of triangle		REAL
!	r_node2: node2 of triangle		REAL
!	r_node3: node3 of triangle		REAL
! ON OUTPUT:
!	l_in:    .true. if r_coord \in p_elem	logical
! CALLS:
!
! COMMENTS:
!	this routine decides whether a point lies in or out of a
!	triangle. this is done with the following approach:
!	calculate the area for the given triangle and for the 
!	three triangles resulting from drawing lines from the given
!	point to all three triangle nodes.
!	if the sum of the areas of those three trianlges exceeds
!	the area of the given trianlge, the the point lies outside
!	of it.
!	for calculation of the areas following formula is used:
!	(Herons formula(?))
!
!	A = sqrt(s* (s- a)* (s- b)* (s- c)),
!
!	where s= 1/2* (a+ b+ c)
!	      a, b, c sides.
!
!	in order to calculate the sidelengths |x-y|= sqrt(x**2-y**2)
!	the complex absolute value intrinsic function is used.
!	hopefully this fuction is faster than using sqrt and
!	power-of-two instead.
!
!	internally double precision is used in this function, because
!	machine precision is crucial when a coordinate pair appears
!	near the edge or corner of an element.
!
!-----------------------------------------------------------------
!
! NAME:
!	dc_area
! FUNCTION:
!	calculate area of a triangle (in a plain) in double precision
! SYNTAX:
!	real= dc_area(real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord1: node1 of triangle			REAL
!	r_coord2: node2 of triangle			REAL
!	r_coord3: node3 of triangle			REAL
! ON OUTPUT:
!	r_area:   area of triangle			REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	7/97
!	2. names changed		j. behrens	7/97
!	3. changed to use GRID_api	j. behrens	11/97
!	4. changed interfaces		j. behrens	12/97
!	5. compliant to amatos 1.0	j. behrens	12/2000
!	6. compliant to amatos 1.2	j. behrens	3/2002
!       7. compliant to amatos 2.0      f. klaschka     8/2006
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
	  USE ADV_wind
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
! 	  REAL, DIMENSION(GRID_dimension) :: r_cntr=(/ -0.25, 0.0 /)
! 	  REAL                            :: r_hgt=4.0
! 	  REAL                            :: r_srd=0.15
! 	  REAL                            :: r_sln=0.22
! 	  REAL                            :: r_swd=0.06
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand
	  INTEGER                                    :: i_lev= 6

!---------- initialize some constant for the slotted cylinder

	  CALL slm_initcoshill(p_ghand)

	  RETURN
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                               :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_time
	  INTEGER, INTENT(in)                                          :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)       :: r_array
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_centr
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)          :: r_sentr=(/ 0.0, 0.0 /)
	  REAL (KIND = GRID_SR)                                        :: r_rds, r_dpt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_tmp
	  INTEGER                                                      :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE           :: r_coo
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)          :: r_spher

!---------- calculate center ! CAUTION: THIS IS NOT IMPLEMENTED YET !!!

!	  r_centr= grid_geokart(r_sentr)

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(55)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- check array length

	  IF(i_arlen < i_num) THEN
	    IF(GRID_parameters%iolog > 0) THEN
	      write(GRID_parameters%iolog,*) '[slm_analyticsolution]: Array sizes did not match, adjusting to the smaller size!'
	    END IF
	    i_num= i_arlen
	  END IF

!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_spher = grid_kartgeo(r_coo(:,i_count))
	    r_array(i_count)= coshill(r_spher, r_sentr)
	  END DO node_loop

!---------- deallocate workspace

	  DEALLOCATE(r_coo)

	  RETURN
	  END SUBROUTINE slm_analyticsolution

!*****************************************************************
	  SUBROUTINE slm_initcoshill(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                      :: p_ghand
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)    :: r_centr
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical) :: r_sentr=(/ 0.0, 0.0 /)
	  REAL (KIND = GRID_SR)                               :: r_rds, r_dpt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)    :: r_tmp
	  INTEGER (KIND = GRID_SI)                            :: i_count, i_num, i_alct, &
	    i_lay, i_ind, i_start, i_cnt
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_coo
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_uvw
      INTEGER (KIND = GRID_SI), DIMENSION(GRID_dimension) :: i_uvp
	  INTEGER (KIND = GRID_SI), DIMENSION(1)              :: i_valind
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical) :: r_spher

!---------- get center

!	  r_centr= grid_geokart(r_sentr)

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber

	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), &
	           r_uvw(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(55)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- loop over layers

      layer_loop: DO i_lay= 1, i_stcnumlayers
      
!---------- compute radii of initial ball-type cosine hill density...

         i_ind = max(4 - abs(floor(0.5*i_stcnumlayers)-i_lay),0)
         r_rds = (1.0/ 6.0)*i_ind/4

        r_aux = 0.
        r_uvw = 0.
        IF ((i_lay .GE. 2) .AND. (i_lay .LE. i_stcnumlayers -2)) then
!---------- loop over the nodes

	    node_loop: DO i_count= 1, i_num
	      r_spher = grid_kartgeo(r_coo(:,i_count))
	      r_aux(1,i_count)= coshill(r_spher, r_sentr, r_radius=r_rds)
	      r_uvw(:,i_count)= slm_windfield(r_coo(:,i_count), i_lay)
	    END DO node_loop
	    END IF

!---------- update grid information

	    i_valind= (/ i_stctracer-1+i_lay /)
	    CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)
        i_start= (i_stcuv-1)+ GRID_dimension*(i_lay-1)
        i_uvp = (/ ((i_start+i_cnt), i_cnt=1,GRID_dimension) /)
        CALL grid_putinfo(p_ghand, i_arraypoint=i_uvp, r_nodevalues=r_uvw(1:GRID_dimension,:))
	  END DO layer_loop

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo, r_uvw)

	  RETURN
	  END SUBROUTINE slm_initcoshill

!*****************************************************************
	  FUNCTION coshill(r_coor, r_centr, r_radius) RESULT (r_hill)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)    :: r_coor
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)    :: r_centr
	  REAL (KIND = GRID_SR), OPTIONAL                     :: r_radius
	  REAL (KIND = GRID_SR)                               :: r_hill
	  REAL (KIND = GRID_SR)                               :: r_maxheight=1.0
	  REAL (KIND = GRID_SR)                               :: r_maxrad
	  REAL (KIND = GRID_SR)                               :: r_dist, r_tmp
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)    :: r_xy

!---------- watch out for optional argument

      opt_arg: IF(present(r_radius)) THEN
        r_maxrad = r_radius
      ELSE opt_arg
!---------- initialize maximal radius with default values
        r_maxrad= GRID_RADIUS/ 6.0
      END IF opt_arg

!---------- initialize r_hill

	  r_hill= 0.0

!---------- calculate distance to center

	  r_xy= r_centr- r_coor
	  r_dist= DOT_PRODUCT(r_xy,r_xy)

!---------- calculate inner values

	  IF(r_dist < r_maxrad) THEN
	    r_tmp = (GRID_PI* r_dist)/r_maxrad
	    r_hill= r_maxheight *(1.0+ cos(r_tmp))
	  END IF

	  RETURN
	  END FUNCTION coshill

!*****************************************************************

	END MODULE SLM_initial




