!*****************************************************************
!
! MODULE NAME:
!	SLM_errorestimate
! FUNCTION:
!	provide an error estimator for the adaptive SLM scheme
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_error
! FUNCTION:
!	estimate the error elementwise
! SYNTAX:
!	real= slm_error(real.arr)
! ON INPUT:
!	r_v:       values at element nodes	REAL
! ON OUTPUT:
!	r_esterr:  estimated error		REAL
! CALLS:
!
! COMMENTS:
!	this is only a simple estimator based on local gradients
!-----------------------------------------------------------------
!
! NAME:
!	slm_errorest
! FUNCTION:
!	this hides the loop structure from the advection part
! SYNTAX:
!	CALL slm_errorest(grid, int, real.arr)
! ON INPUT:
!	p_ghand: handle for the grid		TYPE (grid_handle)
!	i_siz:   size of array for local errors	INTEGER
! ON OUTPUT:
!	i_siz:   size of array for local errors	INTEGER
!	r_arr:   estimated error		REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_errorest
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	11/96
!	2. nodal values time depend.	j. behrens	1/97
!	3. FEM_handle added		j. behrens	7/97
!	4. version with hashing		j. behrens	7/97
!	5. changed to use GRID_api	j. behrens	11/97
!	6. compliant to amatos 1.0	j. behrens	12/2000
!	7. compliant to amatos 1.2	j. behrens	3/2002
!       8. compliant to amatos 2.0      f. klaschka     8/2006
!
!*****************************************************************
	MODULE SLM_errorestimate
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC :: slm_errorest
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_error(r_v) RESULT (r_esterr)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes) :: r_v
	  REAL (KIND = GRID_SR)                               :: r_esterr
	  REAL (KIND = GRID_SR), PARAMETER                    :: r_1o3= (1.0/3.0)
	  REAL (KIND = GRID_SR)                               :: r_d1, r_d2, r_d3

!---------- calculate differences

	  r_d1  =  abs(r_v(1)- r_v(2))
	  r_d2  =  abs(r_v(2)- r_v(3))
	  r_d3  =  abs(r_v(3)- r_v(1))

!---------- this is the estimated error

	  r_esterr= (r_d1+ r_d2+ r_d3)* r_1o3

	  RETURN
	  END FUNCTION slm_error

!*****************************************************************
	  SUBROUTINE slm_errorest(p_ghand, i_siz, r_arr)

!---------- local declarations

	  IMPLICIT NONE
	  TYPE (grid_handle), INTENT(in)  :: p_ghand
	  INTEGER, INTENT(in)             :: i_siz
	  REAL (KIND = GRID_SR), DIMENSION(:), INTENT(out)    :: r_arr
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_aux
	  INTEGER, DIMENSION(:,:), ALLOCATABLE                :: i_enods
	  INTEGER                         :: i_cnt, i_size, i_alct, i_nnum, i_lay, i_start
	  INTEGER, DIMENSION(1)           :: i_valind
	  REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes) :: r_tmp

!---------- allocate auxilliary array

	  i_size= p_ghand%i_enumfine
	  i_nnum= p_ghand%i_nnumber
	  IF(i_siz /= i_size) THEN
	    CALL grid_error(48)
	  END IF

	  ALLOCATE(r_aux(1,i_nnum), &
	           i_enods(GRID_elementnodes,i_size), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(49)
	  END IF

!---------- initialize r_arr, since we want to sum over all layers

      r_arr= 0._GRID_SR

!---------- loop over stacked layers

      i_start = i_stctracer - 1
      layer_loop: DO i_lay=1,i_stcnumlayers
      
!---------- get values

	    i_valind= (/ i_start + i_lay /)
	    CALL grid_getinfo(p_ghand, l_finelevel= .TRUE., i_arraypoint=i_valind, &
	                      r_nodevalues= r_aux, i_elementnodes=i_enods)

!---------- loop through all elements of finest loop

	    elmt_loop: DO i_cnt=1,i_siz
	      r_tmp(1:GRID_elementnodes)= r_aux(1,i_enods(1:GRID_elementnodes,i_cnt))
	      r_arr(i_cnt)= r_arr(i_cnt) + slm_error(r_tmp)
	    END DO elmt_loop
	  END DO layer_loop

!---------- deallocate and exit

	  DEALLOCATE(r_aux, i_enods)

	  RETURN
	  END SUBROUTINE slm_errorest

	END MODULE SLM_errorestimate
