!*******************************************************************************
!
!> @file  FLASH_metadata.f90
!> @brief contains metadata definitions for FLASH_parameters
!
!*******************************************************************************
! MODULE DESCRIPTION:
!> @brief   This is a meta data structure that contains info for the 
!>          FLASH_parameters physical parameters, related to different
!>          Test cases.
!> @author  J. Behrens
!> @version 1.0
!> @date    5/2016
!*****************************************************************
    MODULE FLASH_metadata

    IMPLICIT NONE

!---------- character length for comparison

    INTEGER, PARAMETER                          :: i_comparlen=12 

!---------- meta data for logical parameters 

    LOGICAL, PARAMETER                          :: l_logused= .FALSE.
    INTEGER, PARAMETER                          :: i_lognum=1
    CHARACTER (len=i_comparlen), DIMENSION(i_lognum)  :: c_logkeywds= &
                                                (/ '            ' /)

!---------- meta data for integer parameters 

    LOGICAL, PARAMETER                          :: l_intused= .TRUE.
    INTEGER, PARAMETER                          :: i_intnum=4
    INTEGER, DIMENSION(i_intnum)                :: i_intsizes= (/ 1, 1, 1, 1 /)
    CHARACTER (len=i_comparlen), DIMENSION(i_intnum)  :: c_intkeywds= &
                                                (/ 'NUM_VERT_LAY', &
                                                   'START_TIME_Y', &
                                                   'START_TIME_M', &
                                                   'START_TIME_D' /)

!---------- meta data for character parameters 

    LOGICAL, PARAMETER                          :: l_charused= .TRUE.
    INTEGER, PARAMETER                          :: i_charnum=3
    INTEGER, PARAMETER                          :: i_charlength= 64
    CHARACTER (len=i_comparlen), DIMENSION(i_charnum) :: c_charkeywds= &
                                                (/ 'UWIND_FILE_N', &
                                                   'VWIND_FILE_N', &
                                                   'OMEGA_FILE_N' /)

!---------- meta data for real parameters 

    LOGICAL, PARAMETER                          :: l_realused= .TRUE.
    INTEGER, PARAMETER                          :: i_realnum=5
    INTEGER, DIMENSION(i_realnum)               :: i_realsizes= (/ 1, 1, 1, 1, 1 /)
    CHARACTER (len=i_comparlen), DIMENSION(i_realnum) :: c_realkeywds= &
                                                (/ 'SEA_LEVEL_PR', &
                                                   'SEA_LEVEL_TE', &
                                                   'TEMP_LAPSE_R', &
                                                   'IDEAL_GAS_CO', &
                                                   'DRY_AIR_MOLA' /)

!---------- convenience pointers

    INTEGER, PARAMETER                          :: PARAM_SL_PRESS= 1
    INTEGER, PARAMETER                          :: PARAM_SL_TMPTR= 2
    INTEGER, PARAMETER                          :: PARAM_TEMPLAPS= 3
    INTEGER, PARAMETER                          :: PARAM_IDEALGAS= 4
    INTEGER, PARAMETER                          :: PARAM_DRYAIRML= 5
    INTEGER, PARAMETER                          :: PARAM_UWINDFIL= 1
    INTEGER, PARAMETER                          :: PARAM_VWINDFIL= 2
    INTEGER, PARAMETER                          :: PARAM_OWINDFIL= 3
    INTEGER, PARAMETER                          :: PARAM_NUMLAYER= 1

	END MODULE FLASH_metadata
