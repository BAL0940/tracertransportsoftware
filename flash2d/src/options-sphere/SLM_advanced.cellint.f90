!*****************************************************************
!
! MODULE NAME:
!	SLM_advanced
! FUNCTION:
!	provide advanced semi-Lagrangian routines
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_step
! FUNCTION:
!	one step of the basic SLM algorithm
! SYNTAX:
!	CALL slm_step(int, real.arr, real.arr)
! ON INPUT:
!	...
! ON OUTPUT:
!	r_tracer: array with tracer values	real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_displace
! FUNCTION:
!	extrapolate the alpha, values for the displacements of the upstream
!	points from the gridpoints
! SYNTAX:
!	CALL slm_displace(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_coord: real array of xy-coordinates		real
! ON OUTPUT:
!	r_alpha: displacement vectors to each point	real
! CALLS:
!	wind_field
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_update
! FUNCTION:
!	calculate the update to the velocity
! SYNTAX:
!	CALL slm_update(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_rside: array with right hand side values	real
! ON OUTPUT:
!	r_udate: array with new (updated) gid values	real
! CALLS:
!
! COMMENTS:
!	this routine is trivial for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_upstream
! FUNCTION:
!	calculate right hand side of the equation (upstream values)
! SYNTAX:
!	CALL slm_upstream(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_alpha: displacement vectors to each point	real
! ON OUTPUT:
!	r_rside: array with right hand side values	real
! CALLS:
!
! COMMENTS:
!	this routine is just interpolation for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_interpolate
! FUNCTION:
!	do the interpolation
! SYNTAX:
!	CALL slm_interpolate(grid, int, real, real.arr, real.arr, real.arr)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
!	r_fac:   factor at which point to interpolate	REAL
!	i_arlen: array length for the following arrays	INTEGER
!	r_coord: coordinate array (new grid)		REAL
!	r_alpha: displacement array (corr. to r_coord)	REAL
!	r_value: values on the old grid (array)		REAL
! ON OUTPUT:
!	r_rside: right hand side (interpolated)		REAL
! CALLS:
!
! COMMENTS:
!	this one is plain bi-cubic spline interpolation
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_interpolinit
! FUNCTION:
!	initialize the interpolation (conservative interpolation)
! SYNTAX:
!	CALL slm_interpolate(grid)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!	we set a global value present in this module:
!	r_conservation: conservation value		REAL
!
!-----------------------------------------------------------------
!
! NAME:
!	triang_ar
! FUNCTION:
!	calculate triangle area given by three coordinates
! SYNTAX:
!
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_displace, slm_update, slm_upstream
! COMMENTS:
!
! USES:
!	FLASH_parameters, GRID_api, SLM_interpolation, ADV_wind, ADV_rhs
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!   1. original version		j. behrens	4/2002
!   2. compliant to amatos 2.0      f. klaschka     8/2006
!
!*****************************************************************
	MODULE SLM_advanced
	  USE FLASH_parameters
	  USE MISC_timing
	  USE GRID_api
	  USE ADV_wind
	  USE ADV_rhs
	  PRIVATE
	  PUBLIC  :: slm_astep
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_astep(p_ghand, p_param, p_time, r_modtime, i_size, &
	                       r_coord, r_tracer, i_newsdepth)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
	  TYPE (control_struct), INTENT(in)                         :: p_param
	  TYPE (sw_info), INTENT(inout)                             :: p_time
	  REAL (KIND = GRID_SR), INTENT(in)                         :: r_modtime
	  INTEGER (KIND = GRID_SI), INTENT(in)                      :: i_size
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_size), INTENT(out)     :: r_tracer
	  INTEGER (KIND = GRID_SI), OPTIONAL, INTENT(in)            :: i_newsdepth
	  
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE          :: r_newvl
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE        :: r_alpha
	  INTEGER (KIND = GRID_SI)                                  :: i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), POINTER            :: r_dualcoordinates
	  INTEGER (KIND = GRID_SI), DIMENSION(:,:,:), POINTER       :: i_dualedges
	  INTEGER (KIND = GRID_SI)                                  :: i_dual
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE        :: r_dualalpha

!---------- check size!

	  IF(i_size <= 0) THEN
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'INFO [slm_astep]: Zero step size, returning to calling routine'
	    RETURN
	  END IF

!---------- check newslevel (Not supported for Cell-integrated scheme due to dual)

	  IF(present(i_newsdepth)) THEN
	    CALL grid_error(c_error='[slm_astep]: no newslevel in Cell-Integrated Scheme supported')
	  END IF
	  IF(i_size /= p_ghand(i_timeplus)%i_nnumber) THEN
	    CALL grid_error(c_error='[slm_astep]: incompatible array size')
	  END IF	  

!---------- allocate auxiliary arrays

	  allocate(r_newvl(i_size), r_alpha(GRID_dimension,i_size), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(38)
	  END IF not_alloc

!---------- create dual mesh

	  CALL grid_createdual_donald(p_ghand(i_timeplus), i_dual, i_dualedges, &
	                       r_dualcoordinates)

!---------- allocate array for dual displacements

	  allocate(r_dualalpha(GRID_dimension,i_dual), stat=i_alct)
	  not_allocdual: IF(i_alct /= 0) THEN
	    CALL grid_error(40)
	  END IF not_allocdual

!-SLM--------- calculate trajectory pieces (displacements)

	  CALL stop_watch('start',3,p_time)
	  CALL slm_adisplace(p_param, i_size, r_coord, r_alpha, &
	  		    i_dual, r_dualcoordinates, r_dualalpha, r_time=r_modtime)
	  CALL stop_watch('stop ',3,p_time)

!-SLM--------- calculate right hand side

	  CALL stop_watch('start',4,p_time)
	  CALL slm_aupstream(p_ghand, i_size, r_coord, r_alpha, &
	  		    i_dual, r_dualcoordinates, r_dualalpha, &
	  		    i_dualedges, r_newvl)
	  CALL stop_watch('stop ',4,p_time)

!-SLM--------- calculate new grid values

	  CALL stop_watch('start',5,p_time)
	  CALL slm_aupdate(p_param, i_size, r_coord, r_newvl, r_tracer, r_time=r_modtime)
	  CALL stop_watch('stop ',5,p_time)

!---------- destroy dual mesh

	  CALL grid_destroydual(i_dual, i_dualedges, r_dualcoordinates)

!-SLM--------- put alpha values to u and v field entries

	  r_alpha= -r_alpha
	  CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	                    i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))

!-SLM--------- deallocate work arrays

	  deallocate(r_dualalpha, r_alpha, r_newvl)

	  RETURN
	  END SUBROUTINE slm_astep

!*****************************************************************
	  SUBROUTINE slm_adisplace(p_param, i_arlen, r_coord, r_alpha, &
	                          i_darlen, r_dcoord, r_dalpha, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                      :: p_param
	  INTEGER (KIND = GRID_SI), INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_alpha
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                           :: r_time
	  INTEGER (KIND = GRID_SI), INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)  :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(out) :: r_dalpha
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_fac, r_caf, &
	    r_axy, r_xyc, r_sxy
	  REAL (KIND = GRID_SR)                                                 :: r_dt0, r_dt1, &
	    r_dt2, r_tim
	  INTEGER (KIND = GRID_SI)                                              :: i_cnt1, i_cnt2

!---------- set constants

	  r_dt0= p_param%num%r_deltatime
	  r_dt1= 0.5_GRID_SR* p_param%num%r_deltatime
	  r_dt2= 1.5_GRID_SR* p_param%num%r_deltatime
	  r_fac= 0.5_GRID_SR
	  r_caf= 2.0_GRID_SR
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- calculate in an iteration process the displacements

	  unknown_loop: DO i_cnt1=1,i_arlen
	    r_axy= 0.0_GRID_SR

	    iter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_coord(:,i_cnt1)- r_fac* r_axy
	      r_sxy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	      r_axy= sphere_correct(r_coord(:,i_cnt1),r_sxy)
	    END DO iter_loop

	    r_alpha(:,i_cnt1)= r_axy
	  END DO unknown_loop

!---------- the same for the dual grid

	  dual_loop: DO i_cnt1=1,i_darlen
	    r_axy= 0.0_GRID_SR

	    diter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_dcoord(:,i_cnt1)- r_fac* r_axy
              r_sxy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
              r_axy= sphere_correct(r_dcoord(:,i_cnt1),r_sxy)
	    END DO diter_loop

	    r_dalpha(:,i_cnt1)= r_axy
	  END DO dual_loop

	  RETURN
	  END SUBROUTINE slm_adisplace

!*****************************************************************
	  SUBROUTINE slm_aupdate(p_param, i_arlen, r_coord, r_rside, r_udate, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                                    :: p_param
	  INTEGER (KIND = GRID_SI), INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_udate
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                          :: r_time
	  INTEGER (KIND = GRID_SI)                                             :: i_cnt
	  REAL (KIND = GRID_SR)                                                :: r_dt, r_tim

!---------- in the linear advection case and with f90 this is just

!	  r_udate= r_rside

!---------- including a non-zero right hand side, we have

	  r_dt= p_param%num%r_deltatime
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

	  main_loop: DO i_cnt=1, i_arlen
	    r_udate(i_cnt)= r_rside(i_cnt)+ r_dt* slm_righthand(r_coord(:,i_cnt))
	  END DO main_loop

	  RETURN
	  END SUBROUTINE slm_aupdate

!*****************************************************************
	  SUBROUTINE slm_aupstream(p_mesh, i_arlen, r_coord, r_alpha, &
	                          i_darlen, r_dcoord, r_dalpha, &
	                          i_dedge, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)                         :: p_mesh
	  INTEGER (KIND = GRID_SI), INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_alpha
	  INTEGER (KIND = GRID_SI), INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dalpha
	  INTEGER (KIND = GRID_SI), DIMENSION(2,2*GRID_patchelements,i_arlen), INTENT(in) :: i_dedge
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)                :: r_rside
	  INTEGER (KIND = GRID_SI)                                              :: i_alct, i_cnt, &
             j_cnt, i_tnc, i_oarlen, i_fill, i_len, i_tmp
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_1, r_2, r_3
	  REAL (KIND = GRID_SR)                                                 :: r_oarea, r_narea
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE                    :: r_aux1
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE                    :: r_aux2
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE                      :: r_conc
	  REAL (KIND = GRID_SR)                                                 :: r_onethird= 1._GRID_SR/3._GRID_SR
	  INTEGER (KIND = GRID_SI), DIMENSION(:,:), ALLOCATABLE                 :: i_enods
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical,GRID_elementnodes) :: r_vertx
	  REAL (KIND = GRID_SR)                                                 :: r_part, r_isarea
	  REAL (KIND = GRID_SR), EXTERNAL                                       :: triatria_isectar
	  REAL (KIND = GRID_SR)                                                 :: r_xmax, r_ymax, r_xmin, r_ymin
	  REAL (KIND = GRID_SR)                                                 :: r_maxx, r_maxy, r_minx, r_miny
	  REAL (KIND = GRID_SR)                                                 :: r_mass
	  INTEGER (KIND = GRID_SI), DIMENSION(:), POINTER                       :: i_triang
	  REAL (KIND = GRID_SR), DIMENSION(:), POINTER                          :: r_area
	  INTEGER (KIND = GRID_SI), DIMENSION(1)                                :: i_valind
	  
	  	  
!---------- allocate auxilliary arrays

	  i_oarlen= p_mesh(i_time)%i_enumfine
	  allocate(r_aux1(GRID_dimension,i_oarlen), &
               r_aux2(1,i_oarlen), i_enods(GRID_elementnodes,i_oarlen), &
               r_conc(i_oarlen), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(39)
	  END IF not_alloc
	  i_valind= (/ GRID_tracer /)

!---------- get info from old grid to calculate mass elements

	  CALL grid_getinfo(p_mesh(i_time), r_nodecoordinates= r_aux1, &
	                    i_elementnodes=i_enods, i_arraypoint=i_valind, &
	                    r_nodevalues= r_aux2, l_finelevel=.TRUE.)

!---------- calculate concentration in elements

	  conc_calc: DO i_cnt=1,i_oarlen
	    r_conc(i_cnt)= r_onethird* (r_aux2(1,i_enods(1,i_cnt))+ &
	                   r_aux2(1,i_enods(2,i_cnt))+ r_aux2(1,i_enods(3,i_cnt)))				   		
	  END DO conc_calc
	  
!---------- now the main loop over the nodes of the new grid

	  node_loop: DO i_cnt=1,i_arlen

	    r_mass= 0.0_GRID_SR
	    r_narea= 0.0_GRID_SR
	    r_vertx= 0.0_GRID_SR

!---------- the upstream dual element's center node

	    r_vertx(:,1)= grid_kartgeo(r_coord(:,i_cnt)- r_alpha(:,i_cnt))

!---------- now calculate the interference of the voronoi thing with elements from old mesh

	    dual_ptch_loop: DO i_tnc= 1, 2*GRID_patchelements
	    IF(i_dedge(1,i_tnc,i_cnt) == 0) THEN
	      EXIT dual_ptch_loop
	    ELSE

!---------- nullify pointer arrays

	      NULLIFY(i_triang,r_area)

!---------- the upstream dual element's outer nodes

	      r_vertx(:,2)= grid_kartgeo(r_dcoord(:,i_dedge(1,i_tnc,i_cnt))- r_dalpha(:,i_dedge(1,i_tnc,i_cnt)))
	      r_vertx(:,3)= grid_kartgeo(r_dcoord(:,i_dedge(2,i_tnc,i_cnt))- r_dalpha(:,i_dedge(2,i_tnc,i_cnt)))

!---------- calculate downstream area around primal node

	      r_narea= r_narea+ triang_area(r_coord(:,i_cnt), &
	                                    r_dcoord(:,i_dedge(1,i_tnc,i_cnt)), &
	                                    r_dcoord(:,i_dedge(2,i_tnc,i_cnt)))

!---------- calculate intersection

	      CALL grid_polygridintersect(p_mesh(i_time),GRID_elementnodes,r_vertx,&
	                                  i_len, i_triang, r_area, .TRUE.)
									  
	      DO j_cnt=1,i_len
	        i_tmp= i_triang(j_cnt)
	        r_mass= r_mass + r_area(j_cnt)* r_conc(i_tmp)
	      END DO

	      IF(ASSOCIATED(i_triang)) THEN
	        DEALLOCATE(i_triang)
	        NULLIFY(i_triang)
	      END IF
	      IF(ASSOCIATED(r_area)) THEN
	        DEALLOCATE(r_area)
	        NULLIFY(r_area)
	      END IF
	    END IF
	    END DO dual_ptch_loop

!---------- now calculate concentration
        
	    r_rside(i_cnt)= r_mass/r_narea
		
	  END DO node_loop

!---------- deallocate auxilliary arrays

	  deallocate(r_aux1, r_aux2, r_conc, i_enods)

	  RETURN
	  END SUBROUTINE slm_aupstream

!*****************************************************************
	  FUNCTION triang_area(r_coord1, r_coord2, r_coord3) RESULT (r_area)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord1, r_coord2, r_coord3
	  REAL (KIND = GRID_SR)                                        :: r_area

	  REAL (KIND = GRID_SR)                                        :: r_sum, r_hav, &
	    r_a, r_b, r_c, r_s, r_t, r_e
	  REAL (KIND = GRID_SR)                                        :: r_halfpi
	  REAL (KIND = GRID_SR), PARAMETER                             :: r_degree=57.295779513082320876798 ! degrees per radian
	  REAL (KIND = GRID_SR)                                        :: r_cosb1, r_cosb2, r_cosb3
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)          :: r_lam1, r_lam2, r_lam3

!---------- initialization

	  r_sum= 0.0_GRID_SR
	  r_halfpi=GRID_PI*0.5_GRID_SR

!---------- calculate spherical coordinates

	  r_lam1= grid_kartgeo(r_coord1)
	  r_lam2= grid_kartgeo(r_coord2)
	  r_lam3= grid_kartgeo(r_coord3)

!---------- calculate cosines

	  r_cosb1= cos(r_lam1(2))
	  r_cosb2= cos(r_lam2(2))
	  r_cosb3= cos(r_lam3(2))
	  
!---------- for all three pairs do the excess calculation

	  IF(r_lam1(1) /= r_lam2(1)) THEN
	    r_hav= hav(r_lam2(2)- r_lam1(2))+ r_cosb1*r_cosb2*hav(r_lam2(1)- r_lam1(1))
	    r_a  = 2.0_GRID_SR* asin(sqrt(r_hav))
	    r_b  = r_halfpi- r_lam2(2)
	    r_c  = r_halfpi- r_lam1(2)
	    r_s  = 0.5_GRID_SR* (r_a+ r_b+ r_c);
	    r_t  = tan(r_s* 0.5_GRID_SR)* tan((r_s-r_a)*0.5_GRID_SR)* tan((r_s-r_b)*0.5_GRID_SR)* tan((r_s-r_c)*0.5_GRID_SR)
	    r_e  = abs(4.0_GRID_SR* atan(sqrt(abs(r_t))))* r_degree
	    IF(r_lam1(1) > r_lam2(1)) r_e= -r_e
	    
	    r_sum= r_sum+ r_e
	  END IF

	  IF(r_lam2(1) /= r_lam3(1)) THEN
	    r_hav= hav(r_lam3(2)- r_lam2(2))+ r_cosb2*r_cosb3*hav(r_lam3(1)- r_lam2(1))
	    r_a  = 2.0_GRID_SR* asin(sqrt(r_hav))
	    r_b  = r_halfpi- r_lam3(2)
	    r_c  = r_halfpi- r_lam2(2)
	    r_s  = 0.5_GRID_SR* (r_a+ r_b+ r_c);
	    r_t  = tan(r_s* 0.5_GRID_SR)* tan((r_s-r_a)*0.5_GRID_SR)* tan((r_s-r_b)*0.5_GRID_SR)* tan((r_s-r_c)*0.5_GRID_SR)
	    r_e  = abs(4.0_GRID_SR* atan(sqrt(abs(r_t))))* r_degree
	    IF(r_lam2(1) > r_lam3(1)) r_e= -r_e
	    
	    r_sum= r_sum+ r_e
	  END IF

	  IF(r_lam3(1) /= r_lam1(1)) THEN
	    r_hav= hav(r_lam1(2)- r_lam3(2))+ r_cosb3*r_cosb1*hav(r_lam1(1)- r_lam3(1))
	    r_a  = 2.0_GRID_SR* asin(sqrt(r_hav))
	    r_b  = r_halfpi- r_lam1(2)
	    r_c  = r_halfpi- r_lam3(2)
	    r_s  = 0.5_GRID_SR* (r_a+ r_b+ r_c);
	    r_t  = tan(r_s* 0.5_GRID_SR)* tan((r_s-r_a)*0.5_GRID_SR)* tan((r_s-r_b)*0.5_GRID_SR)* tan((r_s-r_c)*0.5_GRID_SR)
	    r_e  = abs(4.0_GRID_SR* atan(sqrt(abs(r_t))))* r_degree
	    IF(r_lam3(1) > r_lam1(1)) r_e= -r_e
	    
	    r_sum= r_sum+ r_e
	  END IF

	  r_area= abs(r_sum)

	  RETURN
	  END FUNCTION triang_area

!*****************************************************************
	  FUNCTION hav(r_in) RESULT (r_out)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_in
	  REAL (KIND = GRID_SR)                                        :: r_out

!---------- calculate haversine function

	  r_out= (1.0_GRID_SR - cos(r_in))* 0.5_GRID_SR

	  RETURN
	  END FUNCTION hav

!*****************************************************************
	  FUNCTION sphere_correct(r_coord, r_displ) RESULT (r_corct)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_coord, r_displ
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_corct

	  REAL (KIND = GRID_SR)                            :: r_e, r_rat
	  REAL (KIND = GRID_SR)                            :: r_c

!---------- calculate Euklidean norm

	  r_e  = eukl_norm(r_displ)
	  r_rat= r_e/ GRID_RADIUS

!---------- calculate correction

	  r_c  = tan(r_rat)/r_rat
	  r_corct= r_displ* r_c

	  RETURN
	  END FUNCTION sphere_correct

!*****************************************************************
	  FUNCTION eukl_norm(r_vec) RESULT(r_rst)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_vec
	  REAL (KIND = GRID_SR)                            :: r_rst, r_tmp

!---------- calculate vector crossproduct, 3D only

	  r_tmp= dot_product(r_vec, r_vec)
	  r_rst= sqrt(r_tmp)

	  END FUNCTION eukl_norm

	END MODULE SLM_advanced

