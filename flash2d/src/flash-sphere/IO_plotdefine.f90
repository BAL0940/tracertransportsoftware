!*****************************************************************
!
! MODULE NAME:
!	IO_plotdefine
! FUNCTION:
!	define common variables for the plotting routines
! CONTAINS:
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version	j. behrens	9/96
!	2. added tracergrid	j. behrens	12/97
!
!*****************************************************************
	MODULE IO_plotdefine

!---------- some common definitions

	  INTEGER, PARAMETER              :: i_plotuplain=     1  ! plot u as scalar field
	  INTEGER, PARAMETER              :: i_plotvplain=     2  ! plot v as scalar field
	  INTEGER, PARAMETER              :: i_plotphiplain=   3  ! plot phi as scalar
	  INTEGER, PARAMETER              :: i_plotugrid =     4  ! plot u as scalar field
	  INTEGER, PARAMETER              :: i_plotvgrid =     5  ! plot v as scalar field
	  INTEGER, PARAMETER              :: i_plotphigrid=    6  ! plot phi as scalar
	  INTEGER, PARAMETER              :: i_plotgrid=       7  ! plot grid
	  INTEGER, PARAMETER              :: i_plotuvvector=   8  ! plot (u,v)-vectors
	  INTEGER, PARAMETER              :: i_plotvorticity=  9  ! plot vorticity as scalar
	  INTEGER, PARAMETER              :: i_plottracer=     10 ! plot tracer as scalar
	  INTEGER, PARAMETER              :: i_plottracergrid= 11 ! plot tracer plus grid

	END MODULE IO_plotdefine
