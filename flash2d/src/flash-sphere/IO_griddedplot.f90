!*****************************************************************
!
! MODULE NAME:
!	IO_griddedplot
! FUNCTION:
!	perform rendering into file
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	plot_gridded
! FUNCTION:
!	create output for visualization by gnuplot and other grid oriented
!	mapping tools
! SYNTAX:
!	call plot_gridded(grid, int, int, char)
! ON INPUT:
!	p_handle:grid handle for the linked lists   (required)	type (grid_handle)
!	i_time: time stamp for file naming          (optional)	integer
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	plot_gridded
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, IO_plotdefine, GRID_api
! LIBRARIES:
!
! REFERENCES:
!	this module is based on the original implementations in the
!	splash/fe project (fortran 77 version), but wildly modified!
! VERSION(S):
!	1. original version		j. behrens	1/2000
!	2. compliant to amatos 1.0	j. behrens	12/2000
!	3. compliant to amatos 1.2	j. behrens	3/2002
!
!*****************************************************************
	MODULE IO_griddedplot
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  INTEGER, SAVE :: i_timecounter=0
	  PUBLIC :: plot_gridded
	  CONTAINS
!*****************************************************************
	  SUBROUTINE plot_gridded(p_handle, i_time)

!---------- local declarations

	  IMPLICIT NONE
	  TYPE (grid_handle), INTENT(in)       :: p_handle
	  INTEGER, OPTIONAL, INTENT(in)        :: i_time
	  INTEGER                              :: i_io1, i_cnt, j_cnt, i_out, &
	    i_tim, i_alct, i_gridpoints, i_tcnt, i_fst
	  REAL, DIMENSION(:,:), ALLOCATABLE    :: r_xytmp
	  REAL, DIMENSION(GRID_dimension)      :: r_sw, r_ne, r_coo
	  REAL                                 :: r_tmp, r_xstep, r_ystep
	  CHARACTER (len=32)                   :: c_matfile
	  CHARACTER (len=28)                   :: c_tmp

!---------- file handling (open)

	  IF(present(i_time)) THEN
	    i_tcnt= i_time
	    i_timecounter= i_timecounter+1
	  ELSE
	    i_tcnt= i_timecounter
	    i_timecounter= i_timecounter+1
	  END IF
	  write(c_tmp,*) trim(GRID_parameters%program_name), '_gridded.'
	  write(c_matfile,1020) trim(c_tmp), i_tcnt
	  c_matfile= adjustl(c_matfile)
	  i_io1= 15
	  OPEN(i_io1, file= c_matfile, form= 'formatted', iostat= i_fst)
	  IF(i_fst /= 0) THEN
	    RETURN
	  END IF

!---------- deternime maximum extent of domain, allocate temporary array

	  i_cnt= p_handle%i_nnumber
	  allocate(r_xytmp(GRID_dimension,i_cnt), stat=i_alct)
	  not_alloc1: IF(i_alct /= 0) THEN
	    CALL grid_error(45)
	  END IF not_alloc1

!---------- get array with boundary polygonal line

	  CALL grid_getpolyline(p_handle, GRID_boundary, i_cnt, j_cnt, r_xytmp)

!---------- determine minimum/maximum

	  r_sw= minval(r_xytmp(:,1:j_cnt), dim=2)
	  r_ne= maxval(r_xytmp(:,1:j_cnt), dim=2)
	  deallocate(r_xytmp)

!---------- determine number of required grid-points

	  r_tmp= (1.+ p_handle%i_maxlvl)* 0.5
	  i_cnt= ceiling(r_tmp)
	  i_gridpoints= 2**i_cnt
	  write(i_io1,1010) i_gridpoints

!---------- determine coordinate step size (only 2D!)

	  r_xstep= (r_ne(1)- r_sw(1))/(i_gridpoints- 1)
	  r_ystep= (r_ne(2)- r_sw(2))/(i_gridpoints- 1)

!---------- now the nested loop (only 2D!)

	  outer_loop: DO j_cnt=0,i_gridpoints-1
	    r_coo(2)= r_sw(2)+ j_cnt* r_ystep
	    inner_loop: DO i_cnt=0,i_gridpoints-1
	      r_coo(1)= r_sw(1)+ i_cnt* r_xstep
	      r_tmp= grid_coordvalue(p_handle, r_coo, &
	             i_interpolorder=GRID_loworder, i_valpoint=GRID_tracer)
	      write(i_io1,1000) r_tmp
!gnupl	      write(i_io1,1000) r_tmp
	    END DO inner_loop
!gnupl	    write(i_io1,1010)
	  END DO outer_loop

!---------- close file

	  CLOSE(i_io1)

	  RETURN
 1000	  FORMAT(e16.6)
 1010	  FORMAT(i8)
!gnupl 1000	  FORMAT(e12.5)
!gnupl 1010	  FORMAT(O012)
 1020	  FORMAT(a28,i4.4)
	  END SUBROUTINE plot_gridded

	END MODULE IO_griddedplot
