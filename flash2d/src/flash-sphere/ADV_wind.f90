!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	12/97
!	2. compliant to amatos 1.0	j. behrens	12/2000
!	3. compliant to amatos 1.2	j. behrens	3/2002
!       4. compliant to amatos 2.0      f. klaschka     8/2006
!
!*****************************************************************
	MODULE ADV_wind
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC :: slm_windfield, slm_windinit, slm_windquit
	  REAL  (KIND = GRID_SR)  :: r_u0
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coord, r_time, r_lamphi) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical), OPTIONAL :: r_lamphi
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                   :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)              :: r_field
	  REAL (KIND = GRID_SR)                                         :: r_angl=0.0
	  REAL (KIND = GRID_SR)                                         :: r_tim
	  REAL (KIND = GRID_SR)                                         :: r_tmp
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)           :: r_lp, r_zetet, r_gup
! 	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)              :: r_kup

!---------- set time

	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0
	  END IF

!---------- calculate spherical coordinates

	  IF(present(r_lamphi)) THEN
	    r_lp= r_lamphi
	  ELSE
	    r_lp= grid_kartgeo(r_coord)
	  END IF

!---------- calculate wind vector in spherical coordinates

	  r_tmp= sin(r_angl)
	  r_zetet(1)= r_u0* (cos(r_lp(2))* cos(r_angl) + &
	              sin(r_lp(2))* cos(r_lp(1))* r_tmp)
	  r_zetet(2)=  -r_u0* sin(r_lp(1))* r_tmp

!---------- calculate wind vector in kartesian coordinates

! 	  r_gup= r_lp- r_zetet
! 	  r_kup=grid_geokart(r_gup)
! 	  r_field= r_kup- r_coord
	  r_field= kartwind(r_zetet,r_lp)
	
	  RETURN
	  END FUNCTION slm_windfield

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE
	  TYPE (control_struct)     :: p_control

!---------- calculate velocity factor (one revolution after 12 days/
!           576 time steps of half an hour)

	  r_u0= (2.0* GRID_PI* GRID_RADIUS)/1.0368e6

	  RETURN
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
	  FUNCTION kartwind(r_lamphi, r_lpcoor) RESULT (r_kart)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical) :: r_lamphi, r_lpcoor
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)    :: r_kart
	  REAL (KIND = GRID_SR)                               :: r_sl, r_cl, r_sp, r_cp

!---------- calculate constants

	  r_sl= sin(r_lpcoor(1))
	  r_cl= cos(r_lpcoor(1))
	  r_sp= sin(r_lpcoor(2))
	  r_cp= cos(r_lpcoor(2))

!---------- calculate 3D kartesian components

	  r_kart(1)= -r_sl* r_lamphi(1)- r_sp* r_cl* r_lamphi(2)
	  r_kart(2)=  r_cl* r_lamphi(1)- r_sp* r_sl* r_lamphi(2)
	  r_kart(3)=                     r_cp*       r_lamphi(2)

	  RETURN
	  END FUNCTION kartwind

!*****************************************************************
	END MODULE ADV_wind


