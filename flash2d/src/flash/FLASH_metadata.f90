!*******************************************************************************
!
!> @file  FLASH_metadata.f90
!> @brief contains metadata definitions for FLASH_parameters
!
!*******************************************************************************
! MODULE DESCRIPTION:
!> @brief   This is a meta data structure that contains info for the 
!>          FLASH_parameters physical parameters, related to different
!>          Test cases.
!> @author  J. Behrens
!> @version 1.0
!> @date    5/2016
!*****************************************************************
    MODULE FLASH_metadata

    IMPLICIT NONE

!---------- character length for comparison

    INTEGER, PARAMETER                          :: i_comparlen=12 

!---------- meta data for logical parameters 

    LOGICAL, PARAMETER                          :: l_logused= .FALSE.
    INTEGER, PARAMETER                          :: i_lognum=1
    CHARACTER (len=i_comparlen), DIMENSION(i_lognum)  :: c_logkeywds= &
                                                (/ '            ' /)

!---------- meta data for integer parameters 

    LOGICAL, PARAMETER                          :: l_intused= .FALSE.
    INTEGER, PARAMETER                          :: i_intnum=1
    INTEGER, DIMENSION(i_intnum)                :: i_intsizes= (/ 0 /)
    CHARACTER (len=i_comparlen), DIMENSION(i_intnum)  :: c_intkeywds= &
                                                (/ '            ' /)

!---------- meta data for character parameters 

    LOGICAL, PARAMETER                          :: l_charused= .TRUE.
    INTEGER, PARAMETER                          :: i_charnum=1
    INTEGER, PARAMETER                          :: i_charlength= 64
    CHARACTER (len=i_comparlen), DIMENSION(i_charnum) :: c_charkeywds= &
                                                (/ 'WIND_FILE_NA' /)

!---------- meta data for real parameters 

    LOGICAL, PARAMETER                          :: l_realused= .TRUE.
    INTEGER, PARAMETER                          :: i_realnum=3
    INTEGER, DIMENSION(i_realnum)               :: i_realsizes= (/ 1, 1, 1 /)
    CHARACTER (len=i_comparlen), DIMENSION(i_realnum) :: c_realkeywds= &
                                                (/ 'DIFFUSION_CO', &
                                                   'WIND_COEFFIC', &
                                                   'TOTAL_MASS_P' /)

!---------- convenience pointers

    INTEGER, PARAMETER                          :: PARAM_DIFFCOEF= 1
    INTEGER, PARAMETER                          :: PARAM_WINDCOEF= 2
    INTEGER, PARAMETER                          :: PARAM_TOTALMASS= 3
    INTEGER, PARAMETER                          :: PARAM_WINDFILE= 1

	END MODULE FLASH_metadata
