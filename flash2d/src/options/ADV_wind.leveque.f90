!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	12/97
!	2. compliant to amatos 1.0	j. behrens	12/2000
!	3. done some work for 2d	l. mentrup	2003
!
!*****************************************************************
	MODULE ADV_wind
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC :: slm_windfield, slm_windinit, slm_windquit
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coord, r_time) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                  :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_field
	  REAL (KIND = GRID_SR)                                        :: r_fac=.363610260832151995e-4_GRID_SR
	  REAL (KIND = GRID_SR)                                        :: r_end=1.5_GRID_SR
	  REAL (KIND = GRID_SR)					      :: r_diag= .1e-3_GRID_SR
	  REAL (KIND = GRID_SR)                                        :: r_tim
	  REAL (KIND = GRID_SR)					      :: r_tmp
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), PARAMETER  :: r_cntr=(/ 0._GRID_SR, 0._GRID_SR /)
	  REAL (KIND = GRID_SR)                                        :: r_px, r_py, r_pt, &
	    r_2px, r_2py

!---------- set time

	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0
	  END IF

!---------- calculate the advection at (x,y,z) 
!---------- circulating wind in x,y-plane with increasing velocity 
!---------- wrt. the distance to r_cntr

	  r_px= GRID_PI* r_coord(1)
	  r_py= GRID_PI* r_coord(2)
	  r_2px= 2_GRID_SR* GRID_PI* r_coord(1)
	  r_2py= 2_GRID_SR* GRID_PI* r_coord(2)
	  r_pt= (GRID_PI* r_tim)/ r_end
      r_tmp= sin(r_px)* sin(r_px)* sin(r_2py)* cos(r_pt)
	  r_field(1)= r_tmp
	  r_tmp= -(sin(r_py)* sin(r_py))* sin(r_2px)* cos(r_pt)
	  r_field(2)= r_tmp

	  RETURN
	  END FUNCTION slm_windfield

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE
	  TYPE (control_struct)     :: p_control

	  RETURN
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
	END MODULE ADV_wind
