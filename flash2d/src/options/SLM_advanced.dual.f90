!*****************************************************************
!
! MODULE NAME:
!	SLM_advanced
! FUNCTION:
!	provide advanced semi-Lagrangian routines
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_step
! FUNCTION:
!	one step of the basic SLM algorithm
! SYNTAX:
!	CALL slm_step(int, real.arr, real.arr)
! ON INPUT:
!	...
! ON OUTPUT:
!	r_tracer: array with tracer values	real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_displace
! FUNCTION:
!	extrapolate the alpha, values for the displacements of the upstream
!	points from the gridpoints
! SYNTAX:
!	CALL slm_displace(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_coord: real array of xy-coordinates		real
! ON OUTPUT:
!	r_alpha: displacement vectors to each point	real
! CALLS:
!	wind_field
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_update
! FUNCTION:
!	calculate the update to the velocity
! SYNTAX:
!	CALL slm_update(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_rside: array with right hand side values	real
! ON OUTPUT:
!	r_udate: array with new (updated) gid values	real
! CALLS:
!
! COMMENTS:
!	this routine is trivial for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_upstream
! FUNCTION:
!	calculate right hand side of the equation (upstream values)
! SYNTAX:
!	CALL slm_upstream(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_alpha: displacement vectors to each point	real
! ON OUTPUT:
!	r_rside: array with right hand side values	real
! CALLS:
!
! COMMENTS:
!	this routine is just interpolation for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	triang_area
! FUNCTION:
!	calculate triangle area given by three coordinates
! SYNTAX:
!
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_displace, slm_update, slm_upstream
! COMMENTS:
!
! USES:
!	FLASH_parameters, GRID_api, ADV_wind, ADV_rhs
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	4/2002
!	2. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_advanced
	  USE FLASH_parameters
	  USE MISC_timing
	  USE GRID_api
	  USE ADV_wind
	  USE ADV_rhs
	  PRIVATE
	  PUBLIC  :: slm_astep
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_astep(p_ghand, p_param, p_time, r_modtime, i_size, &
	                       r_coord, r_tracer, i_newsdepth)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
	  TYPE (control_struct), INTENT(in)                         :: p_param
	  TYPE (sw_info), INTENT(inout)                             :: p_time
	  REAL (KIND = GRID_SR), INTENT(in)                                          :: r_modtime
	  INTEGER, INTENT(in)                                       :: i_size
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size), INTENT(in)        :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_size), INTENT(out)                      :: r_tracer
	  INTEGER, OPTIONAL, INTENT(in)                             :: i_newsdepth
	  
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE      :: r_newvl
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    :: r_alpha
	  INTEGER                              :: i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), POINTER        :: r_dualcoordinates
	  INTEGER, DIMENSION(:,:,:), POINTER   :: i_dualedges
	  INTEGER                              :: i_dual
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    :: r_dualalpha

!---------- check size!

	  IF(i_size <= 0) THEN
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'INFO [slm_astep]: Zero step size, returning to calling routine'
	    RETURN
	  END IF

!---------- allocate auxiliary arrays

	  allocate(r_newvl(i_size), r_alpha(GRID_dimension,i_size), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(38)
	  END IF not_alloc

!---------- create dual mesh

	  IF(present(i_newsdepth)) THEN
	    CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
	                         r_dualcoordinates, i_newsdepth= i_newsdepth, i_newlength=i_size)
	    IF(i_alct /= i_size) CALL grid_error(c_error='[slm_astep]: incompatible new lengths...')
	  ELSE
	    CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
	                         r_dualcoordinates)
	  END IF

!---------- allocate array for dual displacements

	  allocate(r_dualalpha(GRID_dimension,i_dual), stat=i_alct)
	  not_allocdual: IF(i_alct /= 0) THEN
	    CALL grid_error(40)
	  END IF not_allocdual

!-SLM--------- calculate trajectory pieces (displacements)

	  CALL stop_watch('start',3,p_time)
	  CALL slm_adisplace(p_param, i_size, r_coord, r_alpha, &
	  		    i_dual, r_dualcoordinates, r_dualalpha, r_time=r_modtime)
	  CALL stop_watch('stop ',3,p_time)

!-SLM--------- calculate right hand side

	  CALL stop_watch('start',4,p_time)
	  CALL slm_aupstream(p_ghand, i_size, r_coord, r_alpha, &
	  		    i_dual, r_dualcoordinates, r_dualalpha, &
	  		    i_dualedges, r_newvl)
	  CALL stop_watch('stop ',4,p_time)

!-SLM--------- calculate new grid values

	  CALL stop_watch('start',5,p_time)
	  CALL slm_aupdate(p_param, i_size, r_coord, r_newvl, r_tracer, r_time=r_modtime)
	  CALL stop_watch('stop ',5,p_time)

!---------- destroy dual mesh

	  CALL grid_destroydual(i_dual, i_dualedges, r_dualcoordinates)

!-SLM--------- put alpha values to u and v field entries

	  r_alpha= -r_alpha
	  IF(present(i_newsdepth)) THEN
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_newsdepth=i_newsdepth, i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  ELSE
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  END IF

!-SLM--------- deallocate work arrays

	  deallocate(r_dualalpha, r_alpha, r_newvl)

	  RETURN
	  END SUBROUTINE slm_astep

!*****************************************************************
	  SUBROUTINE slm_adisplace(p_param, i_arlen, r_coord, r_alpha, &
	                          i_darlen, r_dcoord, r_dalpha, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                      :: p_param
	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_alpha
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                           :: r_time
	  INTEGER, INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)  :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(out) :: r_dalpha
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_fac, r_caf, &
	    r_axy, r_xyc
	  REAL (KIND = GRID_SR)                                                 :: r_dt0, r_dt1, &
	    r_dt2, r_tim
	  INTEGER                                              :: i_cnt1, i_cnt2

!---------- set constants

	  r_dt0= p_param%num%r_deltatime
	  r_dt1= 0.5_GRID_SR* p_param%num%r_deltatime
	  r_dt2= 1.5_GRID_SR* p_param%num%r_deltatime
	  r_fac= 0.5_GRID_SR
	  r_caf= 2.0_GRID_SR
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- calculate in an iteration process the displacements

	  unknown_loop: DO i_cnt1=1,i_arlen
	    r_axy= 0.0_GRID_SR

	    iter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_coord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO iter_loop

	    r_alpha(:,i_cnt1)= r_axy
	  END DO unknown_loop

!---------- the same for the dual grid

	  dual_loop: DO i_cnt1=1,i_darlen
	    r_axy= 0.0_GRID_SR

	    diter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_dcoord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO diter_loop

	    r_dalpha(:,i_cnt1)= r_axy
	  END DO dual_loop

	  RETURN
	  END SUBROUTINE slm_adisplace

!*****************************************************************
	  SUBROUTINE slm_aupdate(p_param, i_arlen, r_coord, r_rside, r_udate, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                   :: p_param
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_udate
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                          :: r_time
	  INTEGER                                             :: i_cnt
	  REAL (KIND = GRID_SR)                                                :: r_dt, r_tim

!---------- in the linear advection case and with f90 this is just

!	  r_udate= r_rside

!---------- including a non-zero right hand side, we have

	  r_dt= p_param%num%r_deltatime
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

	  main_loop: DO i_cnt=1, i_arlen
	    r_udate(i_cnt)= r_rside(i_cnt)+ r_dt* slm_righthand(r_coord(:,i_cnt))
	  END DO main_loop

	  RETURN
	  END SUBROUTINE slm_aupdate

!*****************************************************************
	  SUBROUTINE slm_aupstream(p_mesh, i_arlen, r_coord, r_alpha, &
	                          i_darlen, r_dcoord, r_dalpha, &
	                          i_dedge, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)       :: p_mesh
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_alpha
	  INTEGER, INTENT(in)                                          :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)         :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)         :: r_dalpha
	  INTEGER, DIMENSION(2,GRID_patchelements,i_arlen), INTENT(in) :: i_dedge
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                     :: r_fac
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE                     :: r_aux, &
	    r_uparea, r_dwarea
	  INTEGER                                             :: i_alct, i_cnt, j_cnt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                     :: r_1, r_2, r_3
	  REAL (KIND = GRID_SR)                                                :: r_oarea, r_narea

!---------- set factor (at which point of trajectory shall i interpolate)

	  r_fac= 1.0_GRID_SR

!---------- allocate auxilliary arrays

	  allocate(r_aux(i_arlen), &
	           r_uparea(i_arlen), r_dwarea(i_arlen), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(39)
	  END IF not_alloc

!---------- interpolation of node values

	  CALL slm_ainterpolate(p_mesh, r_fac, i_arlen, r_coord, &
	                        r_alpha, r_aux)

!---------- loop over nodes

	  node_loop: DO i_cnt= 1, i_arlen
	    r_oarea= 0.
	    r_narea= 0.
	    ptch_loop: DO j_cnt= 1, GRID_patchelements
	      IF(i_dedge(1,j_cnt,i_cnt) == 0) THEN
	        EXIT ptch_loop
	      ELSE

!---------- the upstream dual element

	        r_1= r_coord(:,i_cnt)- r_alpha(:,i_cnt)
	        r_2= r_dcoord(:,i_dedge(1,j_cnt,i_cnt))- r_dalpha(:,i_dedge(1,j_cnt,i_cnt))
	        r_3= r_dcoord(:,i_dedge(2,j_cnt,i_cnt))- r_dalpha(:,i_dedge(2,j_cnt,i_cnt))
	        r_oarea= r_oarea+ triang_area(r_1, r_2, r_3)

!---------- the downstream dual element

	        r_1= r_coord(:,i_cnt)
	        r_2= r_dcoord(:,i_dedge(1,j_cnt,i_cnt))
	        r_3= r_dcoord(:,i_dedge(2,j_cnt,i_cnt))
	        r_narea= r_narea+ triang_area(r_1, r_2, r_3)
	      END IF
	    END DO ptch_loop
	    r_uparea(i_cnt)= r_oarea
	    IF(r_narea /= 0._GRID_SR) THEN
	      r_dwarea(i_cnt)= 1._GRID_SR/ r_narea
	    ELSE
	      r_dwarea(i_cnt)= 0._GRID_SR
	    END IF
	  END DO node_loop

!---------- now calculate new values using a (nonexact) interpolation formula

	  r_rside= r_aux* r_uparea* r_dwarea

!---------- deallocate auxilliary arrays

	  deallocate(r_aux, r_uparea, r_dwarea)

	  RETURN
	  END SUBROUTINE slm_aupstream

!*****************************************************************
	  SUBROUTINE slm_ainterpolate(p_mesh, r_fac, i_arlen, &
	                             r_coord, r_alpha, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)       :: p_mesh
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)         :: r_fac
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_alpha
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE                   :: r_upstr
	  REAL (KIND = GRID_SR), DIMENSION(GRID_nodevalues)                    :: r_tval
	  INTEGER, DIMENSION(GRID_elementnodes)               :: i_ttmp
	  REAL (KIND = GRID_SR)                                                :: r_eps, r_max, r_min, r_tmp
	  INTEGER                                             :: i_cnt, i_alct, i_val
	  INTEGER                                             :: i_ind, i_tim, &
	    j_cnt, i_out, i_stat

!---------- initialize constant

	  i_val= GRID_tracer
	  r_eps= GRID_EPS
	  i_tim= p_mesh(i_time)%i_timetag

!---------- allocate work array

	  ALLOCATE(r_upstr(GRID_dimension,i_arlen), stat=i_alct)
	  not_allocated: IF(i_alct /= 0) THEN
	    CALL grid_error(60)
	  END IF not_allocated

!---------- calculate upstream coordinates

	  dim_loop: DO i_cnt=1, GRID_dimension
	    r_upstr(i_cnt,:) = r_coord(i_cnt,:)- r_fac(i_cnt)* r_alpha(i_cnt,:)
	  END DO dim_loop

!---------- loop over nodes: find element containing upstream point

	  node_loop: DO i_cnt=1, i_arlen

!---------- check if upstream value is outside of the domain

	    i_out= grid_domaincheck(p_mesh(i_time), r_upstr(:,i_cnt))

!---------- take the intersection of the trajectory with the boundary as new upstream point

	    out_domain: IF(i_out /= 0) then
	       r_upstr(:,i_cnt)= grid_boundintersect(p_mesh(i_time), &
	                        r_coord(:,i_cnt), r_upstr(:,i_cnt), i_info=i_stat)
               no_intersect: IF(i_stat /= 0) THEN
                  r_rside(i_cnt)= 0.0
                  CYCLE node_loop
               END IF no_intersect
	    END IF out_domain

!---------- interpolate

	    r_tmp= grid_coordvalue(p_mesh(i_time), r_upstr(:,i_cnt), &
	           i_interpolorder=GRID_highorder, i_valpoint=i_val, &
                i_index=i_ind, i_domaincheck=0, l_relative=.FALSE., l_sfcorder=.FALSE.)


	    index_valid: IF(i_ind > 0) THEN

!---------- get nodes of element surrounding r_upstr

	       CALL grid_getiteminfo(i_ind, 'elmt', i_arrlen=GRID_elementnodes, i_nodes=i_ttmp)

!---------- get values at nodes of element surrounding r_upstr, and obtain min/max

               r_min= 0.0_GRID_SR; r_max= 0.0_GRID_SR
               elmt_loop: DO j_cnt=1,GRID_elementnodes
                  CALL grid_getiteminfo(i_ttmp(j_cnt), 'node', i_arrlen=GRID_nodevalues, &
                                        r_values= r_tval, i_time=i_tim)
                  r_min= MIN(r_min, r_tval(i_val))
                  r_max= MAX(r_max, r_tval(i_val))	      
               END DO elmt_loop

!---------- clip value

               IF(r_tmp > r_max) THEN
                  r_tmp= r_max
               ELSE IF(r_tmp < r_min) THEN
                  r_tmp= r_min
               END IF
	    END IF index_valid

!---------- set interpolation value

	    r_rside(i_cnt)= r_tmp
	 
	    small_val: IF(abs(r_rside(i_cnt)) < r_eps) THEN
               r_rside(i_cnt)= 0.0_GRID_SR
	    END IF small_val

	  END DO node_loop

!---------- deallocate work array

	  DEALLOCATE(r_upstr)

	  RETURN
	  END SUBROUTINE slm_ainterpolate

!*****************************************************************
	  FUNCTION triang_area(r_coord1, r_coord2, r_coord3) RESULT (r_area)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord1, r_coord2, r_coord3
	  REAL (KIND = GRID_SR)                                        :: r_area
	  REAL (KIND = GRID_SR)                                        :: r_c
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_a, r_b

!---------- calculate vector components

	  r_a= r_coord2- r_coord1
	  r_b= r_coord3- r_coord1

!---------- calculate components (a,b,c) of cross product vector

	  r_c= (r_a(1)* r_b(2)- r_a(2)* r_b(1))

!---------- calculate area

	  r_area= abs(r_c)* 0.5_GRID_SR

	  RETURN
	  END FUNCTION triang_area

	END MODULE SLM_advanced
