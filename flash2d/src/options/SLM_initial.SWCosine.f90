!*****************************************************************
!
! MODULE NAME:
!   SLM_initial
! FUNCTION:
!   initialize a cosinus hill with zero base velocity in the center of the area
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!   slm_initialvalues
! FUNCTION:
!   initialize a grid with values
! SYNTAX:
!   CALL slm_initialvalues(grid)
! ON INPUT:
!   p_ghand: grid handle            TYPE (grid_handle)
! ON OUTPUT:
!   p_ghand: grid handle            TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!   the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_initwaves
! FUNCTION:
!   initialize the gridpoint values with the initial values for
!    (u,v),  h  and (hu, hv)
! SYNTAX:
!   CALL slm_initbar(grid)
! ON INPUT:
!   p_ghand: grid handle            TYPE (grid_handle)
! ON OUTPUT:
!   p_ghand: grid handle            TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!   the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_analyticsolution
! FUNCTION:
!   calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!   CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!   p_ghand: grid handle            TYPE (grid_handle)
!   r_time:  model time         REAL
!   i_arlen: array length for values array  INTEGER
! ON OUTPUT:
!   r_array: values at gridpoints       REAL
! CALLS:
!
! COMMENTS:
!   the routine is made for two dimensions only. It calculates the analytical solution for h and u
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_initvelo
! FUNCTION:
!   calculates the initival velocity
! SYNTAX:
!   CALL slm_initvelo(real.arr, int, real.arr)
! ON INPUT:
!   r_coord: coordinates of gridpoints   REAL
!   i_arlen: array length for values array  INTEGER
! ON OUTPUT:
!  r_initialV: values at gridpoints       REAL
! CALLS:
!
! COMMENTS:
!   the routine is made for two dimensions only. It calculates just the u at the moment
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_initheight
! FUNCTION:
!   calculates the initival height
! SYNTAX:
!   CALL slm_initvelo(real.arr, int, real.arr)
! ON INPUT:
!   r_coord: coordinates of gridpoints   REAL
!   i_arlen: array length for values array  INTEGER
! ON OUTPUT:
!  r_initialH: values at gridpoints       REAL
! CALLS:
!
! COMMENTS:
!   the routine is made for two dimensions only. It calculates the initial height
!
!-----------------------------------------------------------------
!
! PUBLIC:
!   slm_initialvalues, slm_analyticsolution, slm_initvelo
! COMMENTS:
!
! USES:
!   MISC_globalparam, MISC_error, GRID_api, FLASH_Parameters
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!   1. original version     j. behrens  7/97
!   2. names changed        j. behrens  7/97
!   3. changed to use GRID_api  j. behrens  11/97
!   4. changed interfaces       j. behrens  12/97
!   5. compliant to amatos 1.0  j. behrens  12/2000
!   6. compliant to amatos 1.2  j. behrens  3/2002
!   7. compliant to amatos 2.0  j. behrens  7/2003
!   8. adjusted to Cosinus Hill s.paruszewski 1/2015
!
!*****************************************************************
    MODULE SLM_initial
      USE FLASH_parameters
!      USE IO_plot_vtu
      USE GRID_api
      PRIVATE
      PUBLIC slm_initialvalues, slm_analyticsolution, slm_initvelo, slm_initGrad, add_grid_params, getAvrgH
      REAL (KIND = GRID_SR)                            :: r_avrgH=8.0
      REAL (KIND = GRID_SR)                            :: r_varH=8.0
      REAL (KIND = GRID_SR)                            :: r_centerPoint=12500.0
      REAL (KIND = GRID_SR)                            :: r_radius=6000.0
                                                              
      CONTAINS
!*****************************************************************
      
      SUBROUTINE getAvrgH(r_Height)

!---------- local declarations

      IMPLICIT NONE

      REAL (KIND = GRID_SR), INTENT(out)              :: r_Height
   
      r_Height= r_avrgH

      RETURN
      END SUBROUTINE getAvrgH

!*****************************************************************
      SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

      IMPLICIT NONE

      TYPE (grid_handle), INTENT(in)             :: p_ghand

    
      CALL slm_initwaves(p_ghand)

      RETURN
      END SUBROUTINE slm_initialvalues
!*****************************************************************
      SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_num, r_iniOutHeight, r_iniMom, r_iniVelo)
!----  A T T E N T I O N     T H I S   I S   N O T   A N   A N A L Y T I C A L    S O L U T I O N !!!!!!!!!!!!!
!----  THAT MEANS ALSO:  NOT EVERY VALUE IN THE DIAGNOSTIC FILE IS USEFUL
!---------- local declarations

      IMPLICIT NONE

      TYPE (grid_handle), INTENT(in)                         :: p_ghand
      REAL (KIND = GRID_SR), INTENT(in)                      :: r_time
      INTEGER, INTENT(in)                                    :: i_num
      REAL (KIND = GRID_SR), DIMENSION(i_num), INTENT(out) :: r_iniOutHeight
      INTEGER                                                :: i_count , i_alct
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_num), INTENT(out) :: r_iniVelo, r_iniMom
      REAL (KIND = GRID_SR), DIMENSION(1,i_num)::    r_iniHeight
      REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
      REAL (KIND = GRID_SR)                              :: r_x, r_y
!---------- allocate workspace

      ALLOCATE( r_coo(GRID_dimension,i_num), stat= i_alct)
      IF(i_alct /= 0) THEN
        CALL grid_error(51)
      END IF

!---------- get coordinate information

      CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- calculate initial velocity
      CALL slm_initVelo(r_coo, i_num, r_iniVelo)  
!---------- calculate initial height
      CALL slm_initHeight(r_coo, i_num, r_iniHeight)
       r_iniOutHeight=r_iniHeight(1,:)
!---------- loop over the nodes

      node_loop: DO i_count= 1, i_num

!----------- Calculate momentum (hu)  by using h*u
          
          r_iniMom(1,i_count)= r_iniVelo(1,i_count)*r_iniHeight(1,i_count)
          r_iniMom(2,i_count)= r_iniVelo(2,i_count)*r_iniHeight(1,i_count)

          IF(abs(r_iniMom(1,i_count))<GRID_eps) THEN
            r_iniMom(1,i_count)=0.0_GRID_SR
          END IF
          IF(abs(r_iniMom(2,i_count))<GRID_eps) THEN
            r_iniMom(2,i_count)=0.0_GRID_SR
          END IF

  
       END DO node_loop
       
       DEALLOCATE( r_coo)

      
      RETURN
      END SUBROUTINE slm_analyticsolution

!*****************************************************************
      SUBROUTINE slm_initwaves(p_ghand)

!---------- local declarations

      IMPLICIT NONE

      TYPE (grid_handle), INTENT(in)                     :: p_ghand
      INTEGER                                            :: i_count, i_num, i_alct
      REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_iniVelo, r_iniMom, r_iniHeight
      REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo

!---------- allocate workspace

      i_num= p_ghand%i_nnumber

      ALLOCATE(r_iniHeight(1,i_num),r_iniMom(GRID_dimension,i_num),r_iniVelo(GRID_dimension,i_num), &
                r_coo(GRID_dimension,i_num), stat= i_alct)
      IF(i_alct /= 0) THEN
        CALL grid_error(51)
      END IF

!---------- get coordinate information

      CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- calculate initial velocity
      CALL slm_initVelo(r_coo, i_num, r_iniVelo)  
!---------- calculate initial height
      CALL slm_initHeight(r_coo, i_num, r_iniHeight)

!---------- loop over the nodes

      node_loop: DO i_count= 1, i_num

!----------- Calculate momentum (hu)  by using h*u
          
          r_iniMom(1,i_count)=r_iniVelo(1,i_count)*r_iniHeight(1,i_count)
          r_iniMom(2,i_count)=r_iniVelo(2,i_count)*r_iniHeight(1,i_count)

          IF(abs(r_iniMom(1,i_count))<GRID_eps) THEN
            r_iniMom(1,i_count)=0.0_GRID_SR
          END IF
          IF(abs(r_iniMom(2,i_count))<GRID_eps) THEN
            r_iniMom(2,i_count)=0.0_GRID_SR
          END IF


       END DO node_loop

    CALL grid_putinfo(p_ghand, i_arraypoint=(/ GRID_ucomp, GRID_vcomp /), r_nodevalues= r_iniVelo)
    CALL grid_putinfo(p_ghand, i_arraypoint=(/ GRID_h /), r_nodevalues= r_iniHeight)
    CALL grid_putinfo(p_ghand, i_arraypoint=(/ GRID_hu, GRID_hv /), r_nodevalues= r_iniMom)
   

!---------- deallocate workspace

      DEALLOCATE(r_iniHeight,r_iniMom,r_iniVelo, r_coo)

      RETURN
      END SUBROUTINE slm_initwaves
      
!*****************************************************************
      SUBROUTINE slm_initvelo(r_coord, i_arlen, r_initialV)

!---------- local declarations

      IMPLICIT NONE

      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
      INTEGER, INTENT(in)                                                   :: i_arlen
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_initialV
      INTEGER                                                               :: i_count
      LOGICAL                                                               :: l_isOutside

      node_loop: DO i_count= 1, i_arlen
      
      CALL isOutsideCalculationArea(r_coord(:, i_count), l_isOutside)
     
        IF(l_isOutside) THEN
        
        r_initialV(1,i_count)=0.0_GRID_SR
        r_initialV(2,i_count)=0.0_GRID_SR
        
        ELSE
      
        r_initialV(1,i_count)=0.0_GRID_SR
        r_initialV(2,i_count)=0.0_GRID_SR
        
        END IF
      END DO node_loop

      RETURN
      END SUBROUTINE slm_initvelo
!*****************************************************************    
      SUBROUTINE slm_initHeight(r_coord,i_arlen, r_iniH)

      !---------- local declarations

      IMPLICIT NONE

      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
      INTEGER, INTENT(in)                                                   :: i_arlen
      REAL (KIND = GRID_SR), DIMENSION(1,i_arlen), INTENT(out)              :: r_iniH
      INTEGER                                                               :: i_cnt
      LOGICAL                                                               :: l_isOutside

       DO i_cnt=1, i_arlen
       
       CALL isOutsideCalculationArea(r_coord(:, i_cnt), l_isOutside)
     
        IF(l_isOutside) THEN 
          r_iniH(1,i_cnt)=r_avrgH 
        ELSE
       
          r_iniH(1,i_cnt)= r_avrgH+r_varH/r_radius*sqrt(r_radius*r_radius-(r_coord(1,i_cnt)- r_centerPoint)*(r_coord(1,i_cnt)&
                 - r_centerPoint)-(r_coord(2,i_cnt)- r_centerPoint)*(r_coord(2,i_cnt)- r_centerPoint))

          IF(abs(r_iniH(1,i_cnt))<GRID_eps) THEN
            r_iniH(1,i_cnt)=0.0_GRID_SR
          END IF
        END IF
       END DO


      END SUBROUTINE slm_initHeight
      
      
!*****************************************************************    

    SUBROUTINE isOutsideCalculationArea(r_coordPoint ,l_isOutside)
    
    LOGICAL, INTENT(out)                                                :: l_isOutside
    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,1), INTENT(in)      :: r_coordPoint
    REAL (KIND = GRID_SR)                                               :: r_distToCenterPoint
    
    
    r_distToCenterPoint =sqrt( (r_centerPoint-r_coordPoint(1,1))*(r_centerPoint-r_coordPoint(1,1)) + &
    (r_centerPoint-r_coordPoint(2,1))*(r_centerPoint-r_coordPoint(2,1)))
   
    IF( r_distToCenterPoint >= r_radius) THEN
       l_isOutside= .true.
    ELSE
        l_isOutside= .false.
    END IF
     
    
    END SUBROUTINE

!*******************************************************************
      
     SUBROUTINE add_grid_params()


       GRID_h= grid_registerfemvar(1)
       GRID_hu= grid_registerfemvar(1,i_length=2)
       GRID_hv= GRID_hu+1

     END SUBROUTINE add_grid_params
    END MODULE SLM_initial
