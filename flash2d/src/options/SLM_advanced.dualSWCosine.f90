!*****************************************************************
!
! MODULE NAME:
!   SLM_advanced
! FUNCTION:
!   provide advanced semi-Lagrangian routines
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!   slm_astep
! FUNCTION:
!   one step of the cell-integrated SLM algorithm
! SYNTAX:
!   CALL slm_step(int, real.arr, real.arr)
! ON INPUT:
!   ...
! ON OUTPUT:
! 
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_displace
! FUNCTION:
!   extrapolate the alpha, values for the displacements of the upstream
!   points from the gridpoints
! SYNTAX:
!   CALL slm_displace(int, real.arr, real.arr)
! ON INPUT:
!   i_arlen: array length for the real arrays   integer
!   r_coord: real array of xy-coordinates       real
! ON OUTPUT:
!   r_alpha: displacement vectors to each point real
! CALLS:
!   wind_field
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_update
! FUNCTION:
!   calculate the update to the velocity
! SYNTAX:
!   CALL slm_update(int, real.arr, real.arr)
! ON INPUT:
!   i_arlen: array length for the real arrays   integer
!   r_rside: array with right hand side values  real
! ON OUTPUT:
!   r_udate: array with new (updated) gid values    real
! CALLS:
!
! COMMENTS:
!   this routine is trivial for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_upstream
! FUNCTION:
!   calculate right hand side of the equation (upstream values)
! SYNTAX:
!   CALL slm_upstream(int, real.arr, real.arr)
! ON INPUT:
!   i_arlen: array length for the real arrays   integer
!   r_alpha: displacement vectors to each point real
! ON OUTPUT:
!   r_rside: array with right hand side values  real
! CALLS:
!
! COMMENTS:
!   this routine is just interpolation for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_interpolate
! FUNCTION:
!   do the interpolation
! SYNTAX:
!   CALL slm_interpolate(grid, int, real, real.arr, real.arr, real.arr)
! ON INPUT:
!   p_ogrid: grid handle to old grid (with data)    TYPE (grid_handle)
!   r_fac:   factor at which point to interpolate   REAL
!   i_arlen: array length for the following arrays  INTEGER
!   r_coord: coordinate array (new grid)        REAL
!   r_alpha: displacement array (corr. to r_coord)  REAL
!   r_value: values on the old grid (array)     REAL
! ON OUTPUT:
!   r_rside: right hand side (interpolated)     REAL
! CALLS:
!
! COMMENTS:
!   this one is plain bi-cubic spline interpolation
!
!-----------------------------------------------------------------
!
! NAME:
!   slm_interpolinit
! FUNCTION:
!   initialize the interpolation (conservative interpolation)
! SYNTAX:
!   CALL slm_interpolate(grid)
! ON INPUT:
!   p_ogrid: grid handle to old grid (with data)    TYPE (grid_handle)
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!   we set a global value present in this module:
!   r_conservation: conservation value      REAL
!
!-----------------------------------------------------------------
!
! NAME:
!   triang_ar
! FUNCTION:
!   calculate triangle area given by three coordinates
! SYNTAX:
!
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!   slm_displace, slm_update, slm_upstream
! COMMENTS:
!
! USES:
!   FLASH_parameters, GRID_api, SLM_interpolation, ADV_wind, ADV_rhs
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!   1. original version     j. behrens  4/2002
!   2. compliant to amatos 2.0  j. behrens  7/2003
!   3. adapted to quasi-conservative SL  s.paruszewski 03/2015
!
!*****************************************************************
    MODULE SLM_advanced
      USE FLASH_parameters
      USE MISC_timing
      USE GRID_api
      USE ADV_wind
      USE ADV_rhs
      USE SLM_initial
!      USE IO_plot_vtu
!      USE F95_LAPACK, ONLY:la_gels
      PRIVATE
      PUBLIC  :: slm_astep
      !---------- Which type of boundary condition?
!---------- 0 - dirichlet   1 - periodic   2 - reflecting

      INTEGER ::i_boundMod = 2
      CONTAINS
!*****************************************************************
      SUBROUTINE slm_astep(p_ghand, p_param, p_time, r_modtime, i_size, &
                           r_coord, i_newsdepth)

!---------- local declarations

      IMPLICIT NONE

      TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
      TYPE (control_struct), INTENT(in)                         :: p_param
      TYPE (sw_info), INTENT(inout)                             :: p_time
      REAL (KIND = GRID_SR), INTENT(in)                                          :: r_modtime
      INTEGER, INTENT(in)                                       :: i_size
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size), INTENT(in)        :: r_coord
      INTEGER, OPTIONAL, INTENT(in)                             :: i_newsdepth
      
      
      INTEGER                                              ::  i_alct, i_dual 
      REAL (KIND = GRID_SR), DIMENSION(i_size)              :: r_newArea, r_oldArea, r_upHeight, r_height, r_iniHeight
      REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    ::  r_dualalpha, r_dualvelocity, r_dualhtmp
      REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE    ::  r_dualalph, r_dualh
      REAL (KIND = GRID_SR), DIMENSION(:,:), POINTER        :: r_dualcoordinates
      INTEGER, DIMENSION(:,:,:), POINTER   :: i_dualedges
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size)   :: r_upMomentum,   r_putHeight, r_alpha, r_iniGrad, r_oldHeight
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size)   :: r_momentum, r_updatedvl, r_newdHeight
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size)   :: r_velocity,  r_iniVelo, r_iniMom, r_upHeightTemp
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)           :: r_fac
      INTEGER (KIND = GRID_SI)                                 :: i_valPoint, i_count, i_timecnt=0, i_cnt, j_cnt
      INTEGER (KIND = GRID_SI), DIMENSION(GRID_patchelements, i_size) :: i_nodepatch
      REAL (KIND = GRID_SR) ::     r_deltaT

!---------- check size!

      IF(i_size <= 0) THEN
        IF(GRID_parameters%iolog > 0) &
          write(GRID_parameters%iolog,*) 'INFO [slm_step]: Zero step size, returning to calling routine'
        RETURN
      END IF

      
        r_deltaT=p_param%num%r_deltatime
        r_fac = 1.0_GRID_SR
  
!---------- create dual mesh

      IF(present(i_newsdepth)) THEN
        CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
                             r_dualcoordinates, i_newsdepth= i_newsdepth, i_newlength=i_size)
        IF(i_alct /= i_size) CALL grid_error(c_error='[slm_step]: incompatible new lengths...')
      ELSE
        CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
                             r_dualcoordinates)
      END IF


!---------- allocate arrays for dual displacements
      allocate(r_dualalpha(GRID_dimension,i_dual),r_dualhtmp(GRID_dimension,i_dual),&
               r_dualh(i_dual), r_dualvelocity(GRID_dimension,i_dual), stat=i_alct)
      not_allocdual: IF(i_alct /= 0) THEN
        CALL grid_error(40)
      END IF not_allocdual

!---------- get old velocities of grid points
     CALL grid_getinfo(p_ghand(i_timeplus), r_nodevalues = r_velocity,&
        i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))


!--- just for debugging
 !    CALL grid_getinfo(p_ghand(i_timeplus), i_nodepatch = i_nodepatch)
 !   DO i_cnt= 1, i_size 
 !       DO j_cnt=1, GRID_patchelements
 !     WRITE(*,*) r_coord(:,i_cnt), 'Patchelement', i_nodepatch(j_cnt, i_cnt)
 !       END DO
 !   END DO

!-SLM--------- calculate (interpolate) old (->OLD TIME) velocities of dual points      
!---TODO--- Check for a better way to test if it is initial time
    IF(r_modtime/=0.0_GRID_SR) THEN
          CALL  slm_ainterpolate(p_ghand, r_fac, i_time, i_dual, i_size, GRID_ucomp, &
                                r_coord, r_dualcoordinates, r_dualvelocity(1,:))
          CALL  slm_ainterpolate(p_ghand, r_fac, i_time, i_dual, i_size, GRID_vcomp, &
                                r_coord, r_dualcoordinates, r_dualvelocity(2,:))        
                                                                      
    ELSE
          CALL  slm_initvelo(r_dualcoordinates, i_dual, r_dualvelocity)
    END IF
              
  
!-SLM--------- calculate trajectory pieces (displacements) alphas
      CALL slm_adisplace(p_param, i_size, r_coord, r_velocity, r_alpha, &
                         i_dual, r_dualcoordinates,r_dualvelocity, r_dualalpha, &
                         r_time=r_modtime)

!-SLM--------- calculate upstream values for velocity, momentum and height (also area calculations)  
      CALL slm_aupstream(p_ghand, i_time, i_size, r_coord, r_alpha, &
                                  i_dual, r_dualcoordinates, r_dualalpha, &
                i_dualedges, r_upHeight, r_upMomentum, r_oldArea, r_newArea)
     

!-SLM--------Solving the 2D shallow water equations in conservativ form

!-SLM--------- calculate new grid values for h 
 
   ! Classic version
   CALL slm_calculateH(i_size, r_oldArea, r_newArea, r_upHeight, r_height)
         
!-SLM--------- interpolate h values in the dual cell (for derivative calculations)
      CALL  slm_ainterpolate(p_ghand, r_fac, i_time, i_dual, i_size, GRID_h, &
                                r_coord, r_dualcoordinates, r_dualhtmp(1,:))
       r_dualh=r_dualhtmp(1,:)
 
!-SLM--------- calculate derivative in space of h (needed in the momentum eq)   
     CALL calcDrvtMeanPatch(i_size, r_coord, i_dual, r_dualcoordinates,  i_dualedges,r_height, &
                              r_dualh, r_newdHeight)

    
      ! For debugging using the initial analytical gradient
   !    CALL slm_initGrad(r_coord,i_size, r_iniGrad)              
    !     r_newdHeight=r_iniGrad     
                               
      
!-SLM--------- calculate new grid values for momentum
    ! Classic version
    CALL slm_calculateMom(i_size, r_oldArea, r_newArea, r_upMomentum, r_upHeight, r_deltaT, r_height,&
        r_newdHeight, r_momentum)

!-SLM--------- recalculate velocity with hu/h     
     CALL slm_recalculateVelocity(i_size, r_height, r_momentum, r_updatedvl)


      r_putHeight(1,:)=r_height        
   
        
!---------- destroy dual mesh
      CALL grid_destroydual(i_dual, i_dualedges, r_dualcoordinates)

!-SLM--------- write new values for velocity, momentum and height in grid-variables

      CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_updatedvl, &
                     i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
      CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_momentum, &
                     i_arraypoint=(/ GRID_hu, GRID_hv /))
      CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_putHeight, &
                     i_arraypoint=(/ GRID_h /))


!-SLM--------- deallocate work arrays

      deallocate(r_dualalpha, r_dualhtmp, r_dualh, r_dualvelocity)
      
        i_timecnt=i_timecnt+1
    
      END SUBROUTINE slm_astep

!*****************************************************************
      SUBROUTINE slm_adisplace(p_param, i_arlen, r_coord, r_velocity, r_alpha, &
                              i_darlen, r_dcoord,r_dvelocity, r_dalpha, r_time)

!---------- local declarations

      IMPLICIT NONE

      TYPE (control_struct), INTENT(in)                                       :: p_param
      INTEGER, INTENT(in)                                                     :: i_arlen
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)    :: r_coord, r_velocity
      INTEGER, INTENT(in)                                                     :: i_darlen
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)   :: r_dcoord, r_dvelocity
      REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                             :: r_time
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(out)  :: r_dalpha
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out)   :: r_alpha
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                        :: r_axy
      REAL (KIND = GRID_SR)                                                   :: r_dt0
      INTEGER                                                                 :: i_cnt1

!---------- set constants

      r_dt0= p_param%num%r_deltatime

!---------- calculate in an iteration process the displacements

      unknown_loop: DO i_cnt1=1,i_arlen
        r_axy= 0.0_GRID_SR

!---------- instead windfield, we use the updated velocities by hv/v
          r_axy= r_dt0* r_velocity(:, i_cnt1)
           r_alpha(:,i_cnt1)= r_axy
      END DO unknown_loop
       

!---------- calculate the displacements for the dual nodes

      dual_loop: DO i_cnt1=1,i_darlen
        r_axy= 0.0_GRID_SR

           r_axy= r_dt0* r_dvelocity(:, i_cnt1)

        r_dalpha(:,i_cnt1)= r_axy
      END DO dual_loop

      RETURN
      END SUBROUTINE slm_adisplace

!*****************************************************************
      SUBROUTINE slm_aupdate(p_param, i_arlen, r_coord, r_rside, r_udate, r_time)

!---------- local declarations

      IMPLICIT NONE

      TYPE (control_struct), INTENT(in)                   :: p_param
      INTEGER, INTENT(in)                                 :: i_arlen
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                :: r_rside
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_udate
      REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                          :: r_time
      INTEGER                                             :: i_cnt
      REAL (KIND = GRID_SR)                                                :: r_dt

!---------- including a non-zero right hand side, we have

      r_dt= p_param%num%r_deltatime

      main_loop: DO i_cnt=1, i_arlen
        r_udate(i_cnt)= r_rside(i_cnt)+ r_dt* slm_righthand(r_coord(:,i_cnt))
      END DO main_loop

      RETURN
      END SUBROUTINE slm_aupdate

!*****************************************************************
      SUBROUTINE slm_aupstream(p_mesh, i_time, i_arlen, r_coord, r_alpha, &
                                 i_darlen, r_dcoord, r_dalpha, &
                                 i_dedge, r_upHeight, r_upMomentum, r_uparea, r_dwarea)

!---------- local declarations

      IMPLICIT NONE

      INTEGER, INTENT(in)                                                   :: i_arlen, i_time
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_alpha
      INTEGER, INTENT(in)                                                   :: i_darlen
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dcoord
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dalpha
      INTEGER, DIMENSION(2,GRID_patchelements,i_arlen), INTENT(in)          :: i_dedge
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)                :: r_uparea, r_dwarea
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_upMomentum
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)                :: r_upHeight
      TYPE (grid_handle), DIMENSION(GRID_timesteps)                         :: p_mesh
      INTEGER                                                               :: i_cnt, j_cnt, i_tnc, i_out
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_1, r_2, r_3, r_fac
      REAL (KIND = GRID_SR)                                                 :: r_oarea, r_narea
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,GRID_elementnodes)    :: r_vertx
      REAL (KIND = GRID_SR)                                                 :: r_val, r_tmp

!---------- set factor (at which point of trajectory shall i interpolate)

      r_fac= 1.0_GRID_SR
 
!---------- interpolation of node values on upstream position

      CALL slm_ainterpolateUpstrm(p_mesh, r_fac, i_time, i_arlen, GRID_hu, r_coord, &
                            r_alpha, r_upMomentum(1,:))
      CALL slm_ainterpolateUpstrm(p_mesh, r_fac, i_time, i_arlen, GRID_hv, r_coord, &
                            r_alpha, r_upMomentum(2,:))
      CALL slm_ainterpolateUpstrm(p_mesh, r_fac, i_time, i_arlen, GRID_h, r_coord, &
                            r_alpha, r_upHeight)

!---------- loop over nodes

       node_loop: DO i_cnt= 1, i_arlen
        r_oarea= 0.
        r_narea= 0.
        ptch_loop: DO j_cnt= 1, GRID_patchelements
          IF(i_dedge(1,j_cnt,i_cnt) == 0) THEN
            EXIT ptch_loop
          ELSE

!---------- the upstream dual element

            r_1= r_coord(:,i_cnt)- r_alpha(:,i_cnt)
            r_2= r_dcoord(:,i_dedge(1,j_cnt,i_cnt))- r_dalpha(:,i_dedge(1,j_cnt,i_cnt))
            r_3= r_dcoord(:,i_dedge(2,j_cnt,i_cnt))- r_dalpha(:,i_dedge(2,j_cnt,i_cnt))
            r_oarea= r_oarea+ triang_ar(r_1, r_2, r_3)

  !  WRITE(*,*) i_cnt, r_1(1), r_1(2), r_2(1), r_2(2), r_3(1), r_3(2), r_oarea
!---------- the downstream dual element

            r_1= r_coord(:,i_cnt)
            r_2= r_dcoord(:,i_dedge(1,j_cnt,i_cnt))
            r_3= r_dcoord(:,i_dedge(2,j_cnt,i_cnt))
            r_narea= r_narea+ triang_ar(r_1, r_2, r_3)
            
  !  WRITE(*,*) 'Downstream: ', r_narea , 'Upstream:', r_oarea, 'Verhältnis', (r_narea/r_oarea)
           
           
            
          END IF
        END DO ptch_loop
        
        IF(abs((r_narea/r_oarea)-1.0)>0.1) THEN
       ! WRITE(*,*) 'kritisches Flächenverhältnis: ', (r_narea/r_oarea), 'zu Knoten Nummer: ' , i_cnt
        END IF
!--------- sum up the total area for each dualcell        
        r_uparea(i_cnt)= r_oarea
        r_dwarea(i_cnt)= r_narea
      END DO node_loop

      RETURN
      END SUBROUTINE slm_aupstream

!*****************************************************************
      SUBROUTINE slm_ainterpolateUpstrm(p_mesh, r_fac, i_timept, i_arlen, i_val, &
                                 r_coord, r_alpha, r_rside)

!---------- local declarations

      IMPLICIT NONE

      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)         :: r_fac
      INTEGER (KIND = GRID_SI), INTENT(in)                                 :: i_timept
      INTEGER (KIND = GRID_SI), INTENT(in)                                 :: i_arlen
      INTEGER (KIND = GRID_SI), INTENT(in)                                 :: i_val
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_alpha
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_rside
      REAL (KIND = GRID_SR), DIMENSION(i_arlen)                            :: r_reflectHackX, r_reflectHackY
      TYPE (grid_handle), DIMENSION(GRID_timesteps)                        :: p_mesh
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen)             :: r_upstr
      REAL (KIND = GRID_SR), DIMENSION(GRID_nodevalues)                    :: r_tval
      INTEGER, DIMENSION(GRID_elementnodes)                                :: i_ttmp
      REAL (KIND = GRID_SR)                                                :: r_eps, r_max, r_min, r_tmp, &
      r_areaHeight, r_areaWidth, r_boundCoordRight, r_boundCoordLeft, r_boundCoordTop, r_boundCoordBottom
      INTEGER                                                              :: i_cnt, i_alct
      INTEGER                                                              :: i_ind, i_tim, &
        j_cnt, i_out, i_stat
        
!---------- initialize constant

      r_eps= GRID_EPS
      i_tim= p_mesh(i_timept)%i_timetag


    

!---------- calculate upstream coordinates

      dim_loop: DO i_cnt=1, GRID_dimension
        r_upstr(i_cnt,:) = r_coord(i_cnt,:)- r_fac(i_cnt)* r_alpha(i_cnt,:)
      END DO dim_loop

    IF( i_boundMod > 0) THEN
!-------- calculation area is an envelope rectangular 
      r_boundCoordRight = 25000.0_GRID_SR
       r_boundCoordTop = r_boundCoordRight
       r_boundCoordLeft= 0000000.0_GRID_SR
       r_boundCoordBottom = r_boundCoordLeft
       r_areaHeight = r_boundCoordTop - r_boundCoordBottom
       r_areaWidth = r_boundCoordRight - r_boundCoordLeft
    END IF
    
    IF(i_boundMod == 1) THEN  
!----------  P E R I O D I C  B O U N D A R I E S !!! THIS IS A DIRTY HACK!    
     DO i_cnt=1, i_arlen    
             
        IF(r_upstr(1,i_cnt) >= r_boundCoordRight) THEN
          r_upstr(1,i_cnt)= r_upstr(1,i_cnt)- r_areaWidth
        END IF
        IF(r_upstr(1,i_cnt) < r_boundCoordLeft) THEN
        r_upstr(1,i_cnt)= r_upstr(1,i_cnt)+ r_areaWidth
        END IF
        IF(r_upstr(2,i_cnt) >= r_boundCoordTop) THEN
        r_upstr(2,i_cnt)= r_upstr(2,i_cnt)- r_areaHeight
        END IF
        IF(r_upstr(2,i_cnt) < r_boundCoordBottom) THEN
        r_upstr(2,i_cnt)= r_upstr(2,i_cnt)+ r_areaHeight
        END IF
     END DO
     
    ELSE IF (i_boundMod == 2) THEN
!----------  R E F L E C T I N G  B O U N D A R I E S !!! THIS IS A DIRTY HACK!    
     DO i_cnt=1, i_arlen
     
        r_reflectHackX(i_cnt)=1.0
        r_reflectHackY(i_cnt)=1.0
        IF(r_upstr(1,i_cnt) > r_boundCoordRight) THEN
        r_upstr(1,i_cnt)= r_boundCoordRight-abs(r_upstr(1,i_cnt)-r_boundCoordRight)
        r_reflectHackX(i_cnt)=-1.0
        END IF
        IF(r_upstr(1,i_cnt) < r_boundCoordLeft) THEN
        r_upstr(1,i_cnt)= r_boundCoordLeft+abs(r_boundCoordLeft-r_upstr(1,i_cnt)) 
        r_reflectHackX(i_cnt)=-1.0
        END IF
        IF(r_upstr(2,i_cnt) > r_boundCoordRight) THEN
        r_upstr(2,i_cnt)=r_boundCoordRight-abs(r_upstr(2,i_cnt)-r_boundCoordRight) 
        r_reflectHackY(i_cnt)=-1.0 
        END IF
        IF(r_upstr(2,i_cnt) < r_boundCoordLeft) THEN
        r_upstr(2,i_cnt)= r_boundCoordLeft+abs(r_boundCoordLeft-r_upstr(2,i_cnt)) 
        r_reflectHackY(i_cnt)=-1.0
        END IF
     END DO
    
    END IF

!---------- loop over nodes: find element containing upstream point

      node_loop: DO i_cnt=1, i_arlen

  
  
   IF(i_boundMod == 0) THEN
   
!---------- check if upstream value is outside of the domain (-1) or inside (0)

        i_out= grid_domaincheck(p_mesh(i_timept), r_upstr(:,i_cnt))

!---------- take the intersection of the trajectory with the boundary as new upstream point

        out_domain: IF(i_out /= 0) then
           r_upstr(:,i_cnt)= grid_boundintersect(p_mesh(i_timept), &
                            r_coord(:,i_cnt), r_upstr(:,i_cnt), i_info=i_stat)
               no_intersect: IF(i_stat /= 0) THEN
                  r_rside(i_cnt)= 0.0
                  CYCLE node_loop
               END IF no_intersect
        END IF out_domain
  
!   ELSE 
 !       r_rside(i_cnt)= 0.0
   END IF


!---------- interpolate
        r_tmp= grid_coordvalue(p_mesh(i_timept), r_upstr(:,i_cnt), &
               i_interpolorder=GRID_highorder, i_valpoint=i_val, &
                i_index=i_ind, i_domaincheck=0, l_relative=.FALSE., l_sfcorder=.FALSE.)


        index_valid: IF(i_ind > 0) THEN

!---------- get nodes of element surrounding r_upstr

           CALL grid_getiteminfo(i_ind, 'elmt', i_arrlen=GRID_elementnodes, i_nodes=i_ttmp)

!---------- get values at nodes of element surrounding r_upstr, and obtain min/max

               r_min= 0.0_GRID_SR; r_max= 0.0_GRID_SR
               elmt_loop: DO j_cnt=1,GRID_elementnodes
                  CALL grid_getiteminfo(i_ttmp(j_cnt), 'node', i_arrlen=GRID_nodevalues, &
                                        r_values= r_tval, i_time=i_tim)
                 
                 
                  r_min= MIN(r_min, r_tval(i_val))
                  r_max= MAX(r_max, r_tval(i_val))        
               END DO elmt_loop

!---------- clip value

               IF(r_tmp > r_max) THEN
                  r_tmp= r_max
               ELSE IF(r_tmp < r_min) THEN
                  r_tmp= r_min
               END IF
        END IF index_valid

!---------- set interpolation value

        r_rside(i_cnt)= r_tmp
     
        small_val: IF(abs(r_rside(i_cnt)) < r_eps) THEN
               r_rside(i_cnt)= 0.0_GRID_SR
        END IF small_val

      END DO node_loop

!--- Tausche Vorzeichen bei der Geschwindigkeit und dem Impuls
    IF(i_boundMod == 2)THEN
    
         IF (i_val.EQ.GRID_hv) THEN 
           r_rside(:)=r_reflectHackY(:)*r_rside(:)
           END IF
 
         IF (i_val.EQ.GRID_hu )THEN 
             r_rside(:)=r_reflectHackX(:)*r_rside(:)
         END IF
 
    END IF
    
      RETURN
      END SUBROUTINE slm_ainterpolateUpstrm



!*****************************************************************


SUBROUTINE slm_ainterpolate(p_mesh, r_fac, i_timept, i_arlen, i_length, i_val, &
                                r_coord, r_nodeOriginal, r_rside)

!---------- local declarations

      IMPLICIT NONE

      TYPE (grid_handle), DIMENSION(GRID_timesteps)                        :: p_mesh
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)         :: r_fac
      INTEGER (KIND = GRID_SI), INTENT(in)                                 :: i_timept
      INTEGER (KIND = GRID_SI), INTENT(in)                                 :: i_arlen, i_length
      INTEGER (KIND = GRID_SI), INTENT(in)         :: i_val
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_nodeOriginal
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_length), INTENT(in) :: r_coord
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen)             :: r_node
      REAL (KIND = GRID_SR), DIMENSION(i_arlen)                            :: r_reflectHackX, r_reflectHackY
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_rside
      REAL (KIND = GRID_SR), DIMENSION(GRID_nodevalues)                    :: r_tval
      INTEGER, DIMENSION(GRID_elementnodes)               :: i_ttmp
      REAL (KIND = GRID_SR)                                                :: r_eps, r_max, r_min, r_tmp, &
        r_boundCoordRight, r_boundCoordLeft, r_areaHeight, r_areaWidth, r_boundCoordBottom, r_boundCoordTop
      INTEGER                                             :: i_cnt, i_alct
      INTEGER                                             :: i_ind, i_tim, &
        j_cnt, i_out, i_stat

!---------- initialize constant

      r_eps= GRID_EPS
      i_tim= p_mesh(i_timept)%i_timetag

!---------- loop over nodes: find element containing point
    

    IF(i_boundMod > 0) THEN
       !-------- calculation area is an envelope rectangular 
  !     r_boundCoordRight = 2000000.0_GRID_SR
  !     r_boundCoordRight = 6.283185_GRID_SR
        r_boundCoordRight = 25000.0_GRID_SR
       r_boundCoordTop = r_boundCoordRight
       r_boundCoordLeft= 0000000.0_GRID_SR
       r_boundCoordBottom = r_boundCoordLeft
       r_areaHeight = r_boundCoordTop - r_boundCoordBottom
       r_areaWidth = r_boundCoordRight - r_boundCoordLeft
    END IF       

      node_loop: DO i_cnt=1, i_arlen

            r_node(:,i_cnt)=r_nodeOriginal(:,i_cnt)

    IF(i_boundMod == 1) THEN
    
    !----------  P E R I O D I C  B O U N D A R I E S !!! THIS IS A DIRTY HACK!         
        IF(r_node(1,i_cnt) >= r_boundCoordRight) THEN
          r_node(1,i_cnt)= r_node(1,i_cnt)- r_areaWidth
         !WRITE(*,*) r_node(1,i_cnt)
        END IF
        IF(r_node(1,i_cnt) < r_boundCoordLeft) THEN
        r_node(1,i_cnt)= r_node(1,i_cnt)+ r_areaWidth
         !WRITE(*,*) r_node(1,i_cnt)
        END IF
        IF(r_node(2,i_cnt) >= r_boundCoordTop) THEN
        r_node(2,i_cnt)= r_node(2,i_cnt)- r_areaHeight
        END IF
        IF(r_node(2,i_cnt) < r_boundCoordBottom) THEN
        r_node(2,i_cnt)= r_node(2,i_cnt)+ r_areaHeight
        END IF
    
   END IF

    IF(i_boundMod == 2) THEN
!----------  R E F L E C T I N G  B O U N D A R I E S !!! THIS IS A DIRTY HACK!   
        r_reflectHackX(i_cnt)=1.0
        r_reflectHackY(i_cnt)=1.0
        IF(r_node(1,i_cnt) > r_boundCoordRight) THEN
        r_node(1,i_cnt)= r_boundCoordRight-abs(r_node(1,i_cnt)-r_boundCoordRight)
        r_reflectHackX(i_cnt)=-1.0
        END IF
        IF(r_node(1,i_cnt) < r_boundCoordLeft) THEN
        r_node(1,i_cnt)= r_boundCoordLeft+abs(r_boundCoordLeft-r_node(1,i_cnt)) 
        r_reflectHackX(i_cnt)=-1.0
        END IF
        IF(r_node(2,i_cnt) > r_boundCoordRight) THEN
        r_node(2,i_cnt)=r_boundCoordRight-abs(r_node(2,i_cnt)-r_boundCoordRight)
        r_reflectHackY(i_cnt)=-1.0 
        END IF
        IF(r_node(2,i_cnt) < r_boundCoordLeft) THEN 
        r_node(2,i_cnt)= r_boundCoordLeft+abs(r_boundCoordLeft-r_node(2,i_cnt)) 
        r_reflectHackY(i_cnt)=-1.0
        END IF
     END IF
        
    

    IF(i_boundMod == 0) THEN
!---------- check if upstream value is outside of the domain
        i_out= grid_domaincheck(p_mesh(i_timept), r_node(:,i_cnt))

!---------- take the intersection of the trajectory with the boundary as new upstream point

        out_domain: IF(i_out /= 0) then
           r_node(:,i_cnt)= grid_boundintersect(p_mesh(i_timept), &
                            r_coord(:,i_cnt), r_node(:,i_cnt), i_info=i_stat)
               no_intersect: IF(i_stat /= 0) THEN
                  r_rside(i_cnt)= 0.0_GRID_SR
                  CYCLE node_loop
               END IF no_intersect
        END IF out_domain
 !   ELSE 
    
 !        r_rside(i_cnt)= 0.0_GRID_SR
    
    END IF

!---------- interpolate



        r_tmp= grid_coordvalue(p_mesh(i_timept), r_node(:,i_cnt), &
               i_interpolorder=GRID_highorder, i_valpoint=i_val, &
                i_index=i_ind, i_domaincheck=0, l_relative=.FALSE., l_sfcorder=.FALSE.)


        index_valid: IF(i_ind > 0) THEN

!---------- get nodes of element surrounding r_node

           CALL grid_getiteminfo(i_ind, 'elmt', i_arrlen=GRID_elementnodes, i_nodes=i_ttmp)

!---------- get values at nodes of element surrounding r_node, and obtain min/max

               r_min= 0.0_GRID_SR; r_max= 0.0_GRID_SR
               elmt_loop: DO j_cnt=1,GRID_elementnodes
                  CALL grid_getiteminfo(i_ttmp(j_cnt), 'node', i_arrlen=GRID_nodevalues, &
                                        r_values= r_tval, i_time=i_tim)
                  r_min= MIN(r_min, r_tval(i_val))
                  r_max= MAX(r_max, r_tval(i_val))        
               END DO elmt_loop

!---------- clip value

               IF(r_tmp > r_max) THEN
                  r_tmp= r_max
               ELSE IF(r_tmp < r_min) THEN
                  r_tmp= r_min
               END IF
        END IF index_valid

!---------- set interpolation value

        r_rside(i_cnt)= r_tmp
     
        small_val: IF(abs(r_rside(i_cnt)) < r_eps) THEN
               r_rside(i_cnt)= 0.0_GRID_SR
        END IF small_val

      END DO node_loop

   IF((i_boundMod == 2))THEN
      IF (i_val.EQ.GRID_vcomp ) THEN 
       r_rside(:)=r_reflectHackY(:)*r_rside(:)
       END IF
 
         IF (i_val.EQ.GRID_ucomp )THEN 
             r_rside(:)=r_reflectHackX(:)*r_rside(:)
         END IF
 
       END IF

      RETURN
      END SUBROUTINE slm_ainterpolate
      
    
      
     
!*****************************************************************
      
      
      FUNCTION triang_ar(r_coord1, r_coord2, r_coord3) RESULT (r_area)

!---------- local declarations

      IMPLICIT NONE

      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord1, r_coord2, r_coord3
      REAL (KIND = GRID_SR)                                        :: r_area
      REAL (KIND = GRID_SR)                                        :: r_c
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_a, r_b

!---------- calculate vector components

      r_a= r_coord2- r_coord1
      r_b= r_coord3- r_coord1

!---------- calculate components (a,b,c) of cross product vector

      r_c= (r_a(1)* r_b(2)- r_a(2)* r_b(1))

!---------- calculate area

      r_area= abs(r_c)* 0.5_GRID_SR

      RETURN
      END FUNCTION triang_ar


!*****************************************************************
     SUBROUTINE slm_recalculateVelocity(i_arlen, r_height, r_momentum, r_updatedvl)


!---------- local declarations

      IMPLICIT NONE

      INTEGER, INTENT(in)                                                    :: i_arlen
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)   :: r_momentum
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                  :: r_height
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out)  :: r_updatedvl
      INTEGER                                                                :: i_count, i_countDim
      REAL (KIND = GRID_SR), DIMENSION(i_arlen)                             :: r_corrHeight
          
    DO i_count=1, i_arlen
        DO i_countDim=1, GRID_dimension
        
            IF(abs(r_height(i_count))< GRID_eps) THEN
              WRITE(*,*) 'Warning in slm_recalculateVelocity: Height goes to zero for element'
              WRITE(*,*) i_count
              r_updatedvl(i_countDim,i_count)=0.0_GRID_SR
            ELSE
             r_corrHeight(i_count)=r_height(i_count)
             r_updatedvl(i_countDim,i_count)=r_momentum(i_countDim,i_count)/r_corrHeight(i_count)
            END IF


            IF(abs(r_updatedvl(i_countDim,i_count))<GRID_eps) THEN
            r_updatedvl(i_countDim,i_count)=0.0_GRID_SR
            END IF


        END DO
    END DO

    END SUBROUTINE slm_recalculateVelocity

!*****************************************************************
     SUBROUTINE slm_calculateH(i_arlen, r_oldArea, r_newArea, r_upHeight, r_newHeight)


!---------- local declarations

      IMPLICIT NONE

      INTEGER, INTENT(in)                                                    :: i_arlen
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                  :: r_upHeight, r_oldArea, r_newArea
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)                 :: r_newHeight
      INTEGER                                                                :: i_count

    DO i_count=1, i_arlen
    
        IF(abs(r_newArea(i_count)).lt.GRID_eps)THEN
          !  WRITE(*,*) (r_newArea(i_count))
            WRITE(*,*) 'Warning your initial mesh is too fine for the machine precision'
        END IF


          r_newHeight(i_count)= abs(r_oldArea(i_count))/abs(r_newArea(i_count)) * r_upHeight(i_count)
  
            IF(abs(r_newHeight(i_count))<GRID_eps) THEN
                 r_newHeight(i_count)=0.0_GRID_SR
            END IF

    END DO

    END SUBROUTINE slm_calculateH


!*****************************************************************
    SUBROUTINE slm_calculateMom(i_arlen, r_oldArea, r_newArea, r_upMomentum, r_upHeight, &
         r_deltaT, r_newHeight, r_newHeightDvt, r_momentum)


!---------- local declarations

    IMPLICIT NONE

      INTEGER, INTENT(in)                                                    :: i_arlen
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)   :: r_newHeightDvt, r_upMomentum
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                  :: r_upHeight, r_newHeight
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out)  :: r_momentum
       REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)                :: r_oldArea, r_newArea
      INTEGER                                                                :: i_count, i_countDim
      REAL (KIND= GRID_SR), INTENT(in) ::   r_deltaT
    

    DO i_count=1, i_arlen
        Do i_countDim=1, GRID_dimension

            IF(abs(r_newArea(i_count)).lt.GRID_eps)THEN
             WRITE(*,*) 'Warning your initial mesh is to fine for the machine precision'
            END IF

            
             r_momentum(i_countDim,i_count) = abs(r_oldArea(i_count))/abs(r_newArea(i_count)) * &
                  r_upMomentum(i_countDim,i_count)- r_deltaT*GRID_grav*r_newHeight(i_count)*&
                   r_newHeightDvt(i_countDim, i_count)
                   

             IF(abs(r_momentum(i_countDim,i_count))<GRID_eps) THEN
                r_momentum(i_countDim,i_count)=0.0_GRID_SR
             END IF
        END DO
    END DO


    END SUBROUTINE slm_calculateMom
    
    
!***************************************************************** 
!2 try mit der Methode der kleinsten Quadrate LAPACK95 needed
!   SUBROUTINE  calculateDerivative(i_arlen, r_coord, i_darlen, r_dcoord, i_dedge, r_dwnHeight, &
!                                 r_dHeight, r_newHeightDvt)
!---------- local declarations
! 
!      IMPLICIT NONE
!     INTEGER, INTENT(in)                                                     ::  i_arlen, i_darlen                      
!     REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out)   ::  r_newHeightDvt  
!     REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                   ::  r_dwnHeight 
!      REAL (KIND = GRID_SR), DIMENSION(i_darlen), INTENT(in)                  :: r_dHeight
!     REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)    :: r_coord
!     REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)   :: r_dcoord
!     REAL (KIND = GRID_SR)                                                   :: r_hxtmp, r_hytmp, r_leftH, r_rightH,  &
!                                                                               r_area, r_globalarea
!     REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                       :: r_1, r_2, r_3 , r_centr
!     REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,2*GRID_patchelements) :: r_dualnod
!     REAL (KIND = GRID_SR), DIMENSION(2*GRID_patchelements) :: r_vals
!     INTEGER, DIMENSION(:,:,:), POINTER, INTENT(in)                                  :: i_dedge
!     INTEGER, DIMENSION(2*GRID_patchelements)                                  :: i_dualnodNe
!     INTEGER                                                                :: i_cnt, j_cnt, i_cntt, i_nneigb                
!     LOGICAL                                                                :: l_inside1, l_inside2
!    
!        
!---------- loop over nodes (and their dual cells)
!      node_loop: DO i_cnt= 1, i_arlen
!          i_nneigb=0
!          r_dualnod=0.0
!          r_vals=0.0
!          i_dualnodNe=0
!          r_centr=0.0
!          l_inside1=.false.
!          l_inside2=.false.
!          r_area=0.0
!          r_globalarea=0.0
!         
!---------- loop over ther patches of the node's dual cell   
!       ptch_loop: DO j_cnt= 1, GRID_patchelements
!---------- if there is no edge entry, there is no further patch for that dualcell         
!         IF(i_dedge(1,j_cnt,i_cnt) == 0) THEN
!           EXIT ptch_loop
!         ELSE
!---------- the coordinates of the points that form the dual cell (+ r_coord(i_cnt))
!           r_1= r_dcoord(:,i_dedge(1,j_cnt,i_cnt))
!           r_2= r_dcoord(:,i_dedge(2,j_cnt,i_cnt))
!           r_3= r_coord(:,i_cnt)
!           r_area= triang_ar(r_3, r_1, r_2)
!           r_globalarea=r_globalarea+r_area
!    
!---------- have a look if the node is already known and if not, put it into the list of neighbnodes
!     
!         IF(i_nneigb==0)THEN
!            i_nneigb  =  i_nneigb + 1_GRID_SI
!            i_dualnodNe(i_nneigb) = i_dedge(1,j_cnt,i_cnt)
!            r_dualnod(:,i_nneigb) = r_1   
!            i_nneigb  =  i_nneigb + 1_GRID_SI
!            i_dualnodNe(i_nneigb) = i_dedge(1,j_cnt,i_cnt)
!            r_dualnod(:,i_nneigb) = r_2 
!         ELSE
!       
!       
!           knownneigb: DO i_cntt=1, i_nneigb
!              IF (.NOT. l_inside1) THEN
!                IF (.NOT.(( r_dualnod(1,i_cntt).eq.r_1(1)).AND.( r_dualnod(2,i_cntt).eq.r_1(2)) ))THEN
!                  i_nneigb  =  i_nneigb + 1_GRID_SI
!                  i_dualnodNe(i_nneigb) = i_dedge(1,j_cnt,i_cnt)
!                  r_dualnod(:,i_nneigb) = r_1   
!                  l_inside1=.true.
!                END IF
!              END IF
!              IF(.NOT.l_inside2)THEN  
!                IF(.NOT.(( r_dualnod(1,i_cntt).eq.r_2(1)).AND.( r_dualnod(2,i_cntt).eq.r_2(2)) ))THEN
!                   i_nneigb  =  i_nneigb + 1_GRID_SI
!                   i_dualnodNe(i_nneigb) = i_dedge(1,j_cnt,i_cnt)
!                   r_dualnod(:,i_nneigb) = r_2   
!                   l_inside2=.true.
!                END IF
!              END IF
!           END DO knownneigb
!         END IF
!     
!          
!        END IF     
!       END DO ptch_loop
!       
! 
!       r_centr = SUM(r_dualnod,DIM=2)/i_nneigb
!       DO i_cntt=1,i_nneigb
!          r_dualnod(:,i_cntt)= r_dualnod(:,i_cntt) - r_centr(:)
!          r_vals(i_cntt)      = r_dHeight(i_dualnodNe(i_cntt))
!       END DO
!       
!       
!     !  r_newHeightDvt(:,i_cnt)= solve_lsq_problem(i_nneigb, r_dualnod, r_vals)
!       
!       r_newHeightDvt(1,i_cnt)= r_newHeightDvt(1,i_cnt)/r_globalarea
!       r_newHeightDvt(2,i_cnt)= r_newHeightDvt(2,i_cnt)/r_globalarea
!       IF(abs(r_newHeightDvt(1,i_cnt))<GRID_eps) THEN
!               r_newHeightDvt(1,i_cnt)=0.0_GRID_SR
!       END IF
!       IF(abs(r_newHeightDvt(2,i_cnt))<GRID_eps) THEN
!               r_newHeightDvt(2,i_cnt)=0.0_GRID_SR
!       END IF
!       
!       
!     END DO node_loop
!  
!  
!      
!                                 
!   END SUBROUTINE calculateDerivative


!***************************************************************** 
!  hx der einzelnen patches bilden und mitteln
    SUBROUTINE  calcDrvtMeanPatch(i_arlen, r_coord, i_darlen, r_dcoord, i_dedge, r_dwnHeight, &
                                  r_dHeight, r_newHeightDvt)

!---------- local declarations
  
       IMPLICIT NONE

      INTEGER, INTENT(in)                                                     ::  i_arlen, i_darlen                      
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out)   ::  r_newHeightDvt  
      REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                   ::  r_dwnHeight 
       REAL (KIND = GRID_SR), DIMENSION(i_darlen), INTENT(in)                  :: r_dHeight
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)    :: r_coord
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)   :: r_dcoord
      REAL (KIND = GRID_SR)                                                   :: r_hxtmp, r_hytmp, r_leftH, r_rightH,  &
                                                                                   r_h1,  r_h2,  r_h3, r_area, r_globalarea
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                       :: r_1, r_2, r_3 , r_centr, r_a, r_b, r_c, &
                                                                                r_tmpGradSingle
      REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,2*GRID_patchelements) :: r_dualnod
      REAL (KIND = GRID_SR), DIMENSION(2*GRID_patchelements) :: r_vals
      INTEGER, DIMENSION(:,:,:), POINTER, INTENT(in)                                  :: i_dedge
      INTEGER, DIMENSION(2*GRID_patchelements)                                  :: i_dualnodNe
      INTEGER                                                                :: i_cnt, j_cnt, i_cntt, i_nneigb                
      LOGICAl                                                               :: l_isOnBound
     
!--- Strategie: Benutze die Dualen Koordinaten und suche dort (rechtsoben rechtsunten) (linksoebn linksunten)-(  
  
    
   !---------- loop over nodes (und somit dualen Elementen

       node_loop: DO i_cnt= 1, i_arlen
           i_nneigb=0
           r_dualnod=0.0
           r_vals=0.0
           i_dualnodNe=0
           r_centr=0.0
           r_area=0.0
           r_hxtmp=0.0
           r_hytmp=0.0
           r_globalarea=0.0

   !---- HACK  Setze an den Raendern die initialen Gradienten
   
    CALL isOnBoundary(r_coord(:,i_cnt),l_isOnBound)
    
!    IF(l_isOnBound) THEN
!    CALL slm_getGradForCoordinate(r_coord(:,i_cnt),r_tmpGradSingle)
!    r_newHeightDvt(:,i_cnt) = r_tmpGradSingle(:)
!    ELSE

        ptch_loop: DO j_cnt= 1, GRID_patchelements
         
         
          IF(i_dedge(1,j_cnt,i_cnt) == 0) THEN
       ! WRITE(*,*) 'Node: ', r_coord(:,i_cnt), 'Number of Neighbours', j_cnt
            EXIT ptch_loop
          ELSE

!---------- the downstream dual element
            r_1= r_coord(:,i_cnt)
            r_2= r_dcoord(:,i_dedge(1,j_cnt,i_cnt))
            r_3= r_dcoord(:,i_dedge(2,j_cnt,i_cnt))
            r_area= triang_ar(r_1, r_2, r_3)
            r_globalarea=r_globalarea+r_area
!---------- calculate the hx at echt triangle of the patch element of dual element
            
            
            r_a= r_2- r_1
            r_b= r_3- r_1

!---------- calculate components (a,b,c) of cross product vector

             r_c= r_a(1)* r_b(2)- r_a(2)* r_b(1)
            r_h1=r_dwnHeight(i_cnt)
            r_h3=r_dHeight(i_dedge(2,j_cnt,i_cnt))
            r_h2=r_dHeight(i_dedge(1,j_cnt,i_cnt))
!---------- calculate area

         

            r_hxtmp= r_hxtmp + r_area* &
                (   ( (r_h2-r_h3) *(r_1(2)-r_3(2)) - (r_h1-r_h3)*(r_2(2)-r_3(2))  )  /&
                    ( (r_2(1)-r_3(1))*(r_1(2)-r_3(2)) - (r_1(1)-r_3(1))*(r_2(2)-r_3(2)))   )
      
      
      
            r_hytmp=r_hytmp+r_area* ( ( (r_h2-r_h3)*(r_1(1)-r_3(1)) - (r_h1-r_h3)*(r_2(1)-r_3(1))  )  /&
                    ((r_2(2)-r_3(2))*(r_1(1)-r_3(1)) - (r_1(2)-r_3(2))*(r_2(1)-r_3(1)))) 
!--------- mean values of the patch elements to build the 
      
    

             END IF
            END DO ptch_loop
           r_newHeightDvt(1,i_cnt)= r_hxtmp/r_globalarea

           r_newHeightDvt(2,i_cnt)= r_hytmp/r_globalarea
  ! END IF


            IF(abs(r_newHeightDvt(1,i_cnt))<GRID_eps) THEN
                    r_newHeightDvt(1,i_cnt)=0.0_GRID_SR
            END IF
            IF(abs(r_newHeightDvt(2,i_cnt))<GRID_eps) THEN
                    r_newHeightDvt(2,i_cnt)=0.0_GRID_SR
            END IF

        
      END DO node_loop
                                  
    END SUBROUTINE calcDrvtMeanPatch
   
 !***************************************************************** 

    SUBROUTINE  isOnBoundary(r_coordSingle, l_isOnBoundary)

    LOGICAL,   INTENT(out)                                              :: l_isOnBoundary
    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) , INTENT(in)       :: r_coordSingle
  
    
    l_isOnBoundary = .FALSE.
    
    IF ((r_coordSingle(1) == 0.0_GRID_SR ).OR.(r_coordSingle(2) == 0.0_GRID_SR ).OR.&
    (r_coordSingle(1) == 25000.0_GRID_SR ).OR.(r_coordSingle(2) == 25000.0_GRID_SR )) THEN
    
    l_isOnBoundary = .TRUE.
    
    END IF
    
    
    END SUBROUTINE
    
    END MODULE SLM_advanced
