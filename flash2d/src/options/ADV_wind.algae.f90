!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	12/97
!	2. bug fix concerning interp.	j. behrens	2/98
!	3. compliant to amatos 1.0	j. behrens	12/2000
!	4. compliant to amatos 1.2	j. behrens	3/2002
!	5. new version for course tracer transport	j. behrens 01/2012
!
!*****************************************************************
	MODULE ADV_wind
	  USE GRID_api
	  USE FLASH_parameters
	  PRIVATE
	  INTEGER, PARAMETER        :: i_ioerr=0
	  REAL (KIND = GRID_SR)     :: r_intervallen, r_readlast
	  REAL (KIND = GRID_SR)     :: r_ncupdatelast, r_secperstep
	  CHARACTER (LEN=io_fillen)                            :: c_windpath
	  CHARACTER (LEN=io_fillen), DIMENSION(:), ALLOCATABLE :: c_windfile
	  CHARACTER (LEN=io_fillen)                            :: c_tempfile
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE :: r_flowx
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE :: r_flowy
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE   :: r_inittemp
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE     :: r_lat, r_vlat
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE     :: r_lon, r_ulon
      INTEGER (KIND = GRID_SI)  :: i_lon, i_lat, i_timesteps, i_level, i_numfiles
	  INTEGER (KIND = GRID_SI)  :: i_pointsx, i_pointsy
	  INTEGER (KIND = GRID_SI)  :: i_timeinterval, i_intervalnum, i_stepcounter
	  LOGICAL                   :: l_lonlatread
	  
	  PUBLIC                    :: slm_windfield, slm_windinit, slm_windquit
	  PUBLIC                    :: slm_tempfield
	  
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coord, r_time) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_field
	  REAL (KIND = GRID_SR)                       :: r_tim
      CHARACTER (LEN=2*io_fillen)                 :: c_ncfile
	  INTEGER (KIND = GRID_SI)                    :: i_iost, i_cnt, j_cnt

!---------- set time

	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- decide, if data has to be read

	  data_read: IF(r_readlast <= r_tim) THEN

!---------- update values for next open

	    r_readlast    = r_readlast+ r_intervallen
	    i_timeinterval= i_timeinterval+ 1
	    IF(i_timeinterval > i_numfiles) THEN
	      CALL grid_error(i_error=1,c_error='[slm_windfield]: no more current files in list, fixing last one')
          i_timeinterval = i_numfiles
        END IF
        i_stepcounter = 0_GRID_SI

!---------- create filenames

	    write(c_ncfile,*) trim(c_windpath),c_windfile(i_timeinterval)
	    c_ncfile= adjustl(c_ncfile)

!---------- read current data from NetCDF file

	    CALL read_netcdf_currents(c_ncfile)

	  END IF data_read

!---------- decide, if counter needs to be updated

      update_nccount: IF(r_ncupdatelast <= r_tim) THEN
        i_stepcounter= i_stepcounter+1
        IF(i_stepcounter > i_timesteps) &
          CALL grid_error(c_error='[slm_windfield]: step counter overflow!')
        r_ncupdatelast= r_ncupdatelast+ r_secperstep
      END IF update_nccount

!---------- interpolate to coordinate

	  r_field= wind_interpol(r_coord,i_timeinterval)

	  RETURN
	  END FUNCTION slm_windfield

!*****************************************************************
	  FUNCTION slm_tempfield(r_coord) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR)                                        :: r_field
	  
	  INTEGER (KIND = GRID_SI)                         :: i_lox, i_hix, i_loy, i_hiy
	  INTEGER (KIND = GRID_SI)                         :: i_cnt
	  REAL (KIND = GRID_SR)                            :: r_dx, r_dy, r_dxi, r_dyi
	  REAL (KIND = GRID_SR)                            :: r_lx, r_hx, r_ly, r_hy
	  REAL (KIND = GRID_SR)                            :: r_l1, r_h1
	  REAL (KIND = GRID_SR)                            :: r_scalx, r_scaly
	  REAL (KIND = GRID_SR)                            :: r_deg, r_radians
	  REAL (KIND = GRID_SR), PARAMETER                 :: r_earth=6.371e6_GRID_SR ! earth's radius

!---------- initialize radians

	  r_radians= GRID_PI/180._GRID_SR
	  r_deg= r_radians* r_earth

!---------- find u-component wind box corresponding to coordinate

	  i_lox=1_GRID_SI
	  i_hix=2_GRID_SI
	  i_loy=1_GRID_SI
	  i_hiy=2_GRID_SI

	  determine_lon: DO i_cnt=1, i_lon-1
	    IF(r_lon(i_cnt) .LT. r_coord(1)) THEN
	      IF(r_lon(i_cnt +1) .GE. r_coord(1)) THEN
	        i_lox= i_cnt
	        i_hix= i_cnt+1
	        exit determine_lon
	      END IF
	    END IF
	  END DO determine_lon
	  IF(r_lon(i_lon) .LT. r_coord(1)) THEN
	    i_lox = i_lon-1
	    i_hix = i_lon
	  END IF

	  determine_lat: DO i_cnt=1, i_lat-1
	    IF(r_lat(i_cnt) .GE. r_coord(2)) THEN
	      IF(r_lat(i_cnt +1) .LT. r_coord(2)) THEN
	        i_loy= i_cnt
	        i_hiy= i_cnt+1
	        exit determine_lat
	      END IF
	    END IF
	  END DO determine_lat
	  IF(r_lat(i_lat) .GE. r_coord(2)) THEN
	    i_loy = i_lat-1
	    i_hiy = i_lat
	  END IF

!---------- calculate weights for bilinear interpolation

	  r_dx= r_lon(i_hix)- r_lon(i_lox)
	  r_dy= r_lat(i_loy)- r_lat(i_hiy) ! note that the coordinate direction is inverted
	  r_lx= r_coord(1) - r_lon(i_lox)
	  r_ly= r_coord(2) - r_lat(i_hiy)
	  r_hx= r_lon(i_hix) - r_coord(1)
	  r_hy= r_lat(i_loy) - r_coord(2)
	  r_scalx=1._GRID_SR/(r_deg*cos(r_radians*r_lat(i_hiy))) !r_coord(2)))
	  r_scaly=1._GRID_SR/(r_deg)

!---------- linear interpolation in x-direction

	  IF(r_dx /= 0.0_GRID_SR) THEN
	    r_dxi= 1._GRID_SR/r_dx
	    r_l1= (r_hx* r_inittemp(i_lox, i_loy)+ &
	           r_lx* r_inittemp(i_hix, i_loy))* r_dxi
	    r_h1= (r_hx* r_inittemp(i_lox, i_hiy)+ &
	           r_lx* r_inittemp(i_hix, i_hiy))* r_dxi
	  ELSE
	    r_l1= r_inittemp(i_lox, i_loy)
	    r_h1= r_inittemp(i_lox, i_hiy)
	  END IF

!---------- linear interpolation in y-direction

	  IF(r_dy /= 0.0_GRID_SR) THEN
	    r_dyi= 1._GRID_SR/r_dy
	    r_field= (r_hy* r_l1+ r_ly* r_h1)* r_dyi !* r_scalx
	  ELSE
	    r_field= r_l1 !* r_scalx
	  END IF

	  RETURN
	  END FUNCTION slm_tempfield

!*****************************************************************
	  SUBROUTINE read_netcdf_currents(c_filename)

!---------- local declarations

	  IMPLICIT NONE
      INCLUDE "netcdf.inc"
      
!---------- input parameters      
	  CHARACTER (LEN=2*io_fillen), INTENT(in)  :: c_filename
	  
!---------- local variables
	  INTEGER (KIND = GRID_SI)        :: i_alct, i_ncstat
      INTEGER (KIND = GRID_SI)        :: i_fileuvid
      INTEGER (KIND = GRID_SI)        :: i_dimid, i_varid
      INTEGER (KIND = GRID_SI)        :: i_tm, i_ln, i_lt
      INTEGER (KIND = GRID_SI)        :: i_vlat, i_ulon, i_windlon, i_windlat, &
                                         i_windlevel
      INTEGER, DIMENSION(:,:,:,:), ALLOCATABLE :: i_uwind, i_vwind

!---------- initialize diagnostic variable

      i_alct= 0_GRID_SI

!---------- open current file

      IF(GRID_parameters%iolog > 0) &
        write(GRID_parameters%iolog,*) 'INFO: Opening NetCDF file ',c_filename
      i_ncstat= nf_open(c_filename,NF_NOWRITE,i_fileuvid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not open currents data file')
    
!---------- determine lon/lat/time dimension sizes (grid size of currents field)

      i_ncstat= nf_inq_dimid(i_fileuvid, 'xtno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify lon dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_windlon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lon dimension')
      
      i_ncstat= nf_inq_dimid(i_fileuvid, 'ytno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify lat dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_windlat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lat dimension')

      i_ncstat= nf_inq_dimid(i_fileuvid, 'time', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify time dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_timesteps)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read time dimension')
 
      i_ncstat= nf_inq_dimid(i_fileuvid, 'zlevno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify level dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_windlevel)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read level dimension')

!---------- check for the staggered dimensions

      lonlatfirstread: IF(l_lonlatread) THEN
        IF((i_lat /= i_windlat) .OR. (i_lon /= i_windlon) .OR. &
           (i_level /= i_windlevel)) & 
	       CALL grid_error(c_error='[read_netcdf_currents]: dimensions incompatible to temperature data')
      ELSE lonlatfirstread
        i_lat= i_windlat
        i_lon= i_windlon
        i_level= i_windlevel
    
!---------- allocate latitude and longitude coordinate arrays

        IF((.NOT. allocated(r_lat)) .AND. (.NOT. allocated(r_lon))) &
          ALLOCATE(r_lat(i_lat), r_lon(i_lon), stat= i_alct)
        IF(i_alct /= 0) &
          CALL grid_error(c_error='[read_netcdf_currents]: could not allocate lat/lon field')

!---------- read latitude and longitude coordinate values

        i_ncstat= nf_inq_varid(i_fileuvid, 'xtno', i_varid)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_currents]: could not determine lon varid')
        i_ncstat= nf_get_var_double(i_fileuvid, i_varid, r_lon)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_currents]: could not read lon data')

        i_ncstat= nf_inq_varid(i_fileuvid, 'ytno', i_varid)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_currents]: could not determine lat varid')
        i_ncstat= nf_get_var_double(i_fileuvid, i_varid, r_lat)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_currents]: could not read lat data')
	    
	    l_lonlatread= .TRUE.
	    
      END IF lonlatfirstread

!---------- check for the staggered dimensions

      i_ncstat= nf_inq_dimid(i_fileuvid, 'xuno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify staggered lon dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_ulon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read staggered lon dimension')
      
      i_ncstat= nf_inq_dimid(i_fileuvid, 'yvno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify staggered lat dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_vlat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read staggered lat dimension')

      IF((i_lat /= i_vlat) .OR. (i_lon /= i_ulon)) & 
	    CALL grid_error(c_error='[read_netcdf_currents]: staggered dimensions not matching')
    
!---------- allocate latitude and longitude coordinate arrays

      IF ((.NOT. allocated(r_vlat)) .AND. (.NOT. allocated(r_ulon))) &
	    ALLOCATE(r_vlat(i_lat), r_ulon(i_lon), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not allocate lat/lon field')

!---------- read staggered latitude and longitude coordinate values

      i_ncstat= nf_inq_varid(i_fileuvid, 'xuno', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine staggered lon varid')
      i_ncstat= nf_get_var_double(i_fileuvid, i_varid, r_ulon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read staggered lon data')

      i_ncstat= nf_inq_varid(i_fileuvid, 'yvno', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine staggered lat varid')
      i_ncstat= nf_get_var_double(i_fileuvid, i_varid, r_vlat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read staggered lat data')
    
!---------- allocate current data arrays - take r_flowx as indicator for allocated state of all arrays...

      IF(.NOT. allocated(r_flowx)) &
	    ALLOCATE(r_flowx(i_lon, i_lat, i_timesteps), r_flowy(i_lon, i_lat, i_timesteps), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not allocate currents fields')
      IF((.NOT. allocated(i_uwind)) .AND. (.NOT. allocated(i_vwind))) &
	    ALLOCATE(i_uwind(i_lon, i_lat, i_level, i_timesteps), i_vwind(i_lon, i_lat, i_level, i_timesteps), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not allocate auxiliary currents fields')

!---------- read x-/y-direction data of currents

      i_ncstat= nf_inq_varid(i_fileuvid, 'uno', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine varid of uno')
      i_ncstat= nf_get_var_int(i_fileuvid, i_varid, i_uwind)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read uno data')

      i_ncstat= nf_inq_varid(i_fileuvid, 'vno', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine varid of vno')
      i_ncstat= nf_get_var_int(i_fileuvid, i_varid, i_vwind)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read vno data')
    
!---------- Convert data and fix mask values

	  DO i_tm=1,i_timesteps
	  	DO i_lt= 1,i_lat
	  	  DO i_ln= 1,i_lon
	  	    r_flowx(i_ln, i_lt, i_tm)= 0.001_GRID_SR* REAL(i_uwind(i_ln, i_lt, 1, i_tm), GRID_SR)
	  	    IF(i_uwind(i_ln, i_lt, 1, i_tm) <= -11111) r_flowx(i_ln, i_lt, i_tm)= 0._GRID_SR
	  	    r_flowy(i_ln, i_lt, i_tm)= 0.001_GRID_SR* REAL(i_vwind(i_ln, i_lt, 1, i_tm), GRID_SR)
	  	    IF(i_vwind(i_ln, i_lt, 1, i_tm) <= -11111) r_flowy(i_ln, i_lt, i_tm)= 0._GRID_SR
	  	  END DO
	  	END DO
	  END DO

!---------- close currents file

	  i_ncstat= nf_close(i_fileuvid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not close currents data file')

!---------- deallocate aux arrays

      DEALLOCATE(i_uwind, i_vwind)
	    
	  END SUBROUTINE read_netcdf_currents

!*****************************************************************
	  SUBROUTINE read_netcdf_temperature(c_filename)

!---------- local declarations

	  IMPLICIT NONE
      INCLUDE "netcdf.inc"
      
!---------- input parameters      
	  CHARACTER (LEN=2*io_fillen), INTENT(in)  :: c_filename
	  
!---------- local variables
	  INTEGER (KIND = GRID_SI)        :: i_alct, i_ncstat
      INTEGER (KIND = GRID_SI)        :: i_fileuvid
      INTEGER (KIND = GRID_SI)        :: i_dimid, i_varid
      INTEGER (KIND = GRID_SI)        :: i_tm, i_ln, i_lt
      INTEGER (KIND = GRID_SI)        :: i_templat, i_templon, i_templevel, i_temptime
      INTEGER, DIMENSION(:,:,:,:), ALLOCATABLE :: i_itemp

!---------- open current file

      IF(GRID_parameters%iolog > 0) &
        write(GRID_parameters%iolog,*) 'INFO: Opening NetCDF file ',c_filename
      i_ncstat= nf_open(c_filename,NF_NOWRITE,i_fileuvid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not open temperature data file')
    
!---------- determine lon/lat/time dimension sizes (grid size of temperature field)

      i_ncstat= nf_inq_dimid(i_fileuvid, 'xtno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not identify lon dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_templon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not read lon dimension')
      
      i_ncstat= nf_inq_dimid(i_fileuvid, 'ytno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not identify lat dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_templat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not read lat dimension')

      i_ncstat= nf_inq_dimid(i_fileuvid, 'time', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not identify time dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_temptime)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not read time dimension')
 
      i_ncstat= nf_inq_dimid(i_fileuvid, 'zlevno', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not identify level dimension')
      i_ncstat= nf_inq_dimlen(i_fileuvid, i_dimid, i_templevel)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not read level dimension')
   
!---------- check for consistency with currents file

      lonlatfirstread: IF(l_lonlatread) THEN
        IF((i_lat /= i_templat) .OR. (i_lon /= i_templon) .OR. &
           (i_level /= i_templevel)) & 
	       CALL grid_error(c_error='[read_netcdf_temperature]: dimensions incompatible to currents data')
      ELSE lonlatfirstread
        i_lat= i_templat
        i_lon= i_templon
        i_level= i_templevel
    
!---------- allocate latitude and longitude coordinate arrays

	    ALLOCATE(r_lat(i_lat), r_lon(i_lon), stat= i_alct)
        IF(i_alct /= 0) &
	      CALL grid_error(c_error='[read_netcdf_temperature]: could not allocate lat/lon field')

!---------- read latitude and longitude coordinate values

        i_ncstat= nf_inq_varid(i_fileuvid, 'xtno', i_varid)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_temperature]: could not determine lon varid')
        i_ncstat= nf_get_var_double(i_fileuvid, i_varid, r_lon)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_temperature]: could not read lon data')

        i_ncstat= nf_inq_varid(i_fileuvid, 'ytno', i_varid)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_temperature]: could not determine lat varid')
        i_ncstat= nf_get_var_double(i_fileuvid, i_varid, r_lat)
	    IF(i_ncstat /= NF_NOERR) &
	      CALL grid_error(c_error='[read_netcdf_temperature]: could not read lat data')
	    
	    l_lonlatread= .TRUE.
	    
      END IF lonlatfirstread

!---------- allocate current data arrays

	  ALLOCATE(r_inittemp(i_lon, i_lat), i_itemp(i_lon, i_lat, i_level, i_temptime), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not allocate temperature fields')

!---------- read x-/y-direction data of currents

      i_ncstat= nf_inq_varid(i_fileuvid, 'tno', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not determine varid of tno')
      i_ncstat= nf_get_var_int(i_fileuvid, i_varid, i_itemp)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not read tno data')

!---------- Convert data and fix mask values

      DO i_lt= 1,i_lat
        DO i_ln= 1,i_lon
          r_inittemp(i_ln, i_lt)= 0.01_GRID_SR* REAL(i_itemp(i_ln, i_lt, 1, 1), GRID_SR)
          IF(i_itemp(i_ln, i_lt, 1, 1) <= -11111) r_inittemp(i_ln, i_lt)= 0._GRID_SR
        END DO
      END DO

!---------- close currents file

	  i_ncstat= nf_close(i_fileuvid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_temperature]: could not close currents data file')

!---------- deallocate aux arrays

      DEALLOCATE(i_itemp)
	    
	  END SUBROUTINE read_netcdf_temperature

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct)                  :: p_control
	  INTEGER                                :: i_iofil= 19
	  CHARACTER (len=i_charlength+io_fillen) :: c_name
	  CHARACTER (len=128)                    :: a_filrow
	  INTEGER                                :: i_iost, i_ioend, i_alct, i_cnt
	  CHARACTER (LEN=2*io_fillen)            :: c_ncfile

!---------- initialize

	  WRITE(c_name,*) trim(p_control%cmd%c_directory),p_control%tst%tst_char(1)
	  c_name= adjustl(c_name)
	  c_windpath= p_control%cmd%c_directory
      i_numfiles= -1

!---------- open wind parameter file

	  open(unit= i_iofil, file= c_name, status= 'OLD', action= 'READ', iostat= i_iost)
	  file_notopen: IF(i_iost /= 0) THEN
	    write(i_ioerr,*) 'ERROR: Filename: ', c_name
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'ERROR: Filename: ', c_name
	    CALL grid_error(c_error='[slm_windinit]: could not open wind parameters file')
	  END IF file_notopen
	  IF(GRID_parameters%iolog > 0) &
	    write(GRID_parameters%iolog,*) 'INFO: Opened file on unit: ', i_iofil

!---------- read wind parameter file

	  read_loop: DO
	    read(i_iofil,2000,iostat=i_ioend) a_filrow

!---------- if file ended

	    file_end: IF(i_ioend /= 0) THEN
	      close(i_iofil)
	      IF(GRID_parameters%iolog > 0) &
	        write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_iofil
	      EXIT read_loop
	    ELSE file_end

!---------- decide what to DO with line according to first character

	    comment_line: IF(a_filrow(1:1) == '#' .or. a_filrow(1:1) == '!') THEN
	      CONTINUE
	    ELSE IF(a_filrow(1:8) == 'ABS_PATH') THEN comment_line
	      read(i_iofil,2000) c_windpath
	    ELSE IF(a_filrow(1:8) == 'SEC_PER_') THEN comment_line
	      read(i_iofil,*) r_secperstep
	    ELSE IF(a_filrow(1:8) == 'STEPS_PE') THEN comment_line
	      read(i_iofil,*) i_timesteps
	    ELSE IF(a_filrow(1:8) == 'INFILE_N') THEN comment_line
	      read(i_iofil,*) i_numfiles
          ALLOCATE(c_windfile(i_numfiles), stat=i_alct)
          IF(i_alct /= 0) &
            CALL grid_error(c_error='[slm_windinit]: could not allocate windname array')
	    ELSE IF(a_filrow(1:8) == 'WIND_FIL') THEN comment_line
	      IF(i_numfiles < 1) &
	        CALL grid_error(c_error='[slm_windinit]: number of wind files unknown')
          DO i_cnt=1,i_numfiles
            c_windfile(i_cnt)= '                                                                '
	        read(i_iofil,2010) c_windfile(i_cnt)
	      END DO
	    ELSE IF(a_filrow(1:8) == 'TEMP_FIL') THEN comment_line
	      c_tempfile= '                                                                '
	      read(i_iofil,2010) c_tempfile
	    END IF comment_line
          
	    END IF file_end
	  END DO read_loop

!---------- initialize some values

	  r_readlast     = 0.0_GRID_SR
	  r_ncupdatelast = 0.0_GRID_SR
	  i_timeinterval = 0_GRID_SI
	  i_stepcounter  = 0_GRID_SI
	  r_intervallen  = r_secperstep* i_timesteps
	  l_lonlatread   = .FALSE.

!---------- read temperature data (wind data are read in slm_windfield)

      write(c_ncfile,*) trim(c_windpath),c_tempfile
      c_ncfile= adjustl(c_ncfile)

      CALL read_netcdf_temperature(c_ncfile)

	  RETURN
 2000 FORMAT(a128)
 2010 FORMAT(a64)
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

!---------- deallocate wind data arrays

	  DEALLOCATE(r_flowx, r_flowy, r_inittemp)
	  DEALLOCATE(r_lon, r_lat, r_ulon, r_vlat)

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
	  FUNCTION wind_interpol(r_coord, i_tim) RESULT (r_inter)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_coord
	  INTEGER (KIND = GRID_SI)                         :: i_tim
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_inter
	  
	  INTEGER (KIND = GRID_SI)                         :: i_ulox, i_uhix, i_uloy, i_uhiy
	  INTEGER (KIND = GRID_SI)                         :: i_vlox, i_vhix, i_vloy, i_vhiy
	  INTEGER (KIND = GRID_SI)                         :: i_cnt
	  REAL (KIND = GRID_SR)                            :: r_dx, r_dy, r_dxi, r_dyi
	  REAL (KIND = GRID_SR)                            :: r_ulx, r_uhx, r_uly, r_uhy
	  REAL (KIND = GRID_SR)                            :: r_vlx, r_vhx, r_vly, r_vhy
	  REAL (KIND = GRID_SR)                            :: r_l1, r_h1, r_l2, r_h2
	  REAL (KIND = GRID_SR)                            :: r_scalx, r_scaly
	  REAL (KIND = GRID_SR)                            :: r_deg, r_radians
	  REAL (KIND = GRID_SR), PARAMETER                 :: r_earth=6.371e6_GRID_SR ! earth's radius

!---------- initialize radians

	  r_radians= GRID_PI/180._GRID_SR
	  r_deg= r_radians* r_earth

!---------- find u-component wind box corresponding to coordinate

	  i_ulox=1_GRID_SI
	  i_uhix=2_GRID_SI
	  i_uloy=1_GRID_SI
	  i_uhiy=2_GRID_SI

	  determine_ulon: DO i_cnt=1, i_lon-1 ! r_lon goes from small to large values...
	    IF(r_ulon(i_cnt) .LT. r_coord(1)) THEN
	      IF(r_ulon(i_cnt +1) .GE. r_coord(1)) THEN
	        i_ulox= i_cnt
	        i_uhix= i_cnt+1
	        exit determine_ulon
	      END IF
	    END IF
	  END DO determine_ulon
	  IF(r_ulon(i_lon) .LT. r_coord(1)) THEN
	    i_ulox = i_lon-1
	    i_uhix = i_lon
	  END IF

	  determine_ulat: DO i_cnt=1, i_lat-1 ! r_lat goes from large to small values... 
	    IF(r_lat(i_cnt) .GE. r_coord(2)) THEN
	      IF(r_lat(i_cnt +1) .LT. r_coord(2)) THEN
	        i_uloy= i_cnt
	        i_uhiy= i_cnt+1
	        exit determine_ulat
	      END IF
	    END IF
	  END DO determine_ulat
	  IF(r_lat(i_lat) .GT. r_coord(2)) THEN
	    i_uloy = i_lat-1
	    i_uhiy = i_lat
	  END IF

!---------- find v-component wind box corresponding to coordinate

	  i_vlox=1_GRID_SI
	  i_vhix=2_GRID_SI
	  i_vloy=1_GRID_SI
	  i_vhiy=2_GRID_SI

	  determine_vlon: DO i_cnt=1, i_lon-1
	    IF(r_lon(i_cnt) .LT. r_coord(1)) THEN
	      IF(r_lon(i_cnt +1) .GE. r_coord(1)) THEN
	        i_vlox= i_cnt
	        i_vhix= i_cnt+1
	        exit determine_vlon
	      END IF
	    END IF
	  END DO determine_vlon
	  IF(r_lon(i_lon) .LT. r_coord(1)) THEN
	    i_vlox = i_lon-1
	    i_vhix = i_lon
	  END IF

	  determine_vlat: DO i_cnt=1, i_lat-1
	    IF(r_vlat(i_cnt) .GE. r_coord(2)) THEN
	      IF(r_vlat(i_cnt +1) .LT. r_coord(2)) THEN
	        i_vloy= i_cnt
	        i_vhiy= i_cnt+1
	        exit determine_vlat
	      END IF
	    END IF
	  END DO determine_vlat
	  IF(r_vlat(i_lat) .GE. r_coord(2)) THEN
	    i_vloy = i_lat-1
	    i_vhiy = i_lat
	  END IF
    
!---------- calculate weights for bilinear interpolation

	  r_dx= r_lon(i_vhix)- r_lon(i_vlox)
	  r_dy= r_lat(i_uhiy)- r_lat(i_uloy)
	  r_ulx= r_coord(1) - r_ulon(i_ulox)
	  r_uly= r_coord(2) - r_lat(i_uloy)
	  r_uhx= r_ulon(i_uhix) - r_coord(1)
	  r_uhy= r_lat(i_uhiy) - r_coord(2)
	  r_vlx= r_coord(1) - r_lon(i_vlox)
	  r_vly= r_coord(2) - r_vlat(i_vloy)
	  r_vhx= r_lon(i_vhix) - r_coord(1)
	  r_vhy= r_vlat(i_vhiy) - r_coord(2)
	  r_scalx=1._GRID_SR/(r_deg*cos(r_radians*r_lat(i_uhiy))) !r_coord(2)))
	  r_scaly=1._GRID_SR/(r_deg)

!---------- linear interpolation in x-direction

	  IF(r_dx /= 0.0_GRID_SR) THEN
	    r_dxi= 1._GRID_SR/r_dx
	    r_l1= (r_uhx* r_flowx(i_ulox, i_uloy, i_tim)+ &
	           r_ulx* r_flowx(i_uhix, i_uloy, i_tim))* r_dxi
	    r_h1= (r_uhx* r_flowx(i_ulox, i_uhiy, i_tim)+ &
	           r_ulx* r_flowx(i_uhix, i_uhiy, i_tim))* r_dxi
	    r_l2= (r_vhx* r_flowy(i_vlox, i_vloy, i_tim)+ &
	           r_vlx* r_flowy(i_vhix, i_vloy, i_tim))* r_dxi
	    r_h2= (r_vhx* r_flowy(i_vlox, i_vhiy, i_tim)+ &
	           r_vlx* r_flowy(i_vhix, i_vhiy, i_tim))* r_dxi
	  ELSE
	    r_l1= r_flowx(i_ulox, i_uloy, i_tim)
	    r_h1= r_flowx(i_ulox, i_uhiy, i_tim)
	    r_l2= r_flowy(i_vlox, i_vloy, i_tim)
	    r_h2= r_flowy(i_vlox, i_vhiy, i_tim)
	  END IF

!---------- linear interpolation in y-direction

	  IF(r_dy /= 0.0_GRID_SR) THEN
	    r_dyi= 1._GRID_SR/r_dy
	    r_inter(1)= (r_uhy* r_l1+ r_uly* r_h1)* r_dyi* r_scalx
	    r_inter(2)= (r_vhy* r_l2+ r_vly* r_h2)* r_dyi* r_scaly
	  ELSE
	    r_inter(1)= r_l1* r_scalx
	    r_inter(2)= r_l2* r_scaly
	  END IF

	  RETURN
	  END FUNCTION wind_interpol
!*****************************************************************
	END MODULE ADV_wind
