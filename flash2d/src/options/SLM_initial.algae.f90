!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize algae test case for semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initalgae
! FUNCTION:
!	initialize a grid with values of a algae concentration
! SYNTAX:
!	CALL slm_initalgae(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initaltemp
! FUNCTION:
!	initialize a grid with values of a temperature distribution
! SYNTAX:
!	CALL slm_initaltemp(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	7/97
!	2. names changed		j. behrens	7/97
!	3. changed to use GRID_api	j. behrens	11/97
!	4. changed interfaces		j. behrens	12/97
!	5. compliant to amatos 1.0	j. behrens	12/2000
!	6. compliant to amatos 1.2	j. behrens	3/2002
!	7. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_cntr=(/ -0.25, 0.0 /)
	  REAL (KIND = GRID_SR)                            :: r_hgt=0.1
	  REAL (KIND = GRID_SR)                            :: r_srd=0.15
	  REAL (KIND = GRID_SR)                            :: r_sln=0.22
	  REAL (KIND = GRID_SR)                            :: r_swd=0.06
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand

!---------- initialize some constant for the slotted cylinder

	  CALL slm_initalgae(p_ghand)
	  CALL slm_inittemp(p_ghand)

	  RETURN
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                         :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                      :: r_time
	  INTEGER, INTENT(in)                                    :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out) :: r_array

!---------- dummy routine

	  r_array= 0.0_GRID_SR

	  RETURN
	  END SUBROUTINE slm_analyticsolution

!*****************************************************************
	  SUBROUTINE slm_initalgae(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                     :: p_ghand
	  INTEGER                                            :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  INTEGER, DIMENSION(1)                              :: i_valind


!---------- allocate workspace

	  i_num= p_ghand%i_nnumber

	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(51)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- initial values

!	  CALL compute_hump(i_num, r_coo, r_aux(1,:))
	  CALL compute_cylndr(i_num, r_coo, r_aux(1,:))

!---------- update grid information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_initalgae

!*****************************************************************
    SUBROUTINE compute_hump(i_num, r_coo, r_aux)

!---------- local declarations

    IMPLICIT NONE

    INTEGER                                          :: i_num
    REAL (KIND = GRID_SR), DIMENSION(:)              :: r_aux
    REAL (KIND = GRID_SR), DIMENSION(:,:)            :: r_coo

    INTEGER                                          :: i_count
    REAL (KIND = GRID_SR)                            :: r_rds, r_dpt, r_rad, r_pr
    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_centr, r_tmp

!---------- initialize some constant for the slotted cylinder

    r_srd= 0.25_GRID_SR
    r_rds= r_srd*r_srd
    r_centr= (/ 0.0_GRID_SR, 0.0_GRID_SR /)
        
!---------- loop over the nodes

    node_loop: DO i_count= 1, i_num
        r_tmp(:)      = r_coo(:,i_count)- r_centr(:)
        r_dpt= dot_product(r_tmp, r_tmp)
        r_aux(i_count)= 0.0_GRID_SR
        inside: IF(r_dpt < r_rds) THEN
            r_rad= min(sqrt(r_dpt),r_srd)/r_srd
            r_pr= GRID_PI* r_rad
            r_aux(i_count)= 0.25_GRID_SR* (1+ cos(r_pr))
        END IF inside
    END DO node_loop

    RETURN
    END SUBROUTINE compute_hump

!*****************************************************************
	  SUBROUTINE compute_cylndr(i_num, r_coo, r_aux)

!---------- local declarations

	  IMPLICIT NONE

	  INTEGER                                          :: i_num
	  REAL (KIND = GRID_SR), DIMENSION(:)              :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:)            :: r_coo

	  INTEGER                                          :: i_count
	  REAL (KIND = GRID_SR)                            :: r_rds, r_ras, r_rad, &
	    r_dpt, r_a, r_fac
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_tmp, &
	    r_nw, r_sw, r_ne, r_se, r_centr
	  LOGICAL                                          :: l_aux

!---------- initialize some constants for the slotted cylinder

    r_srd= 0.25_GRID_SR
    r_rds= r_srd*r_srd
    r_centr= (/ 0.0_GRID_SR, 0.0_GRID_SR /)
    r_a= r_srd
    r_fac= 2._GRID_SR
	  
	
!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_aux(i_count)= 0.0_GRID_SR
	    r_tmp(:)      = r_coo(:,i_count)- r_centr(:)
	    r_dpt= dot_product(r_tmp, r_tmp)
	    inside: IF(r_dpt < r_rds) THEN
	      r_aux(i_count)= (r_hgt-0.025_GRID_SR) + ((r_coo(1,i_count)- r_a)*r_fac)* (0.05_GRID_SR)
!	      r_aux(i_count)= r_hgt
	    END IF inside
	  END DO node_loop

	  RETURN
	  END SUBROUTINE compute_cylndr


!*****************************************************************
	  SUBROUTINE slm_inittemp(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                     :: p_ghand
	  REAL (KIND = GRID_SR)                              :: r_meantemp, r_deltatemp, r_maxtemp, r_mintemp
	  INTEGER                                            :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  INTEGER, DIMENSION(1)                              :: i_valind
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_diff, r_centr

    REAL (KIND = GRID_SR) :: r_mumax, r_Topt, r_Tslope, r_Cmort, r_mu, r_tmp, r_T2, r_a, r_b, r_fac, r_dpt

!---------- set constants CAUTION: Make sure that these are consistent with ADV_rhs!

 !   r_mumax  = 0.5_GRID_SR  ! [per day]
    r_mumax  = 0.5_GRID_SR/86400._GRID_SR  ! [per s]
    r_Topt   = 22.0_GRID_SR ! [°C]
    r_Tslope = 5.0_GRID_SR  ! [°C]
!    r_Cmort  = 0.05_GRID_SR ! [per day]
    r_Cmort  = 0.05_GRID_SR/86400._GRID_SR ! [per s]

!---------- initialize some constants for the circular algae distribution

    r_T2       = r_Tslope*r_Tslope
    r_meantemp = r_Topt - sqrt((r_T2* (log(r_mumax)-log(r_Cmort))))
    r_deltatemp= 0.002_GRID_SR
    r_maxtemp  = r_meantemp+ r_deltatemp
    r_mintemp  = r_meantemp- r_deltatemp

    r_a= 0.5_GRID_SR*0.5_GRID_SR
    r_centr= (/ 0.0_GRID_SR, 0.0_GRID_SR /)

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber

	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(51)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- determine min/max x-coordinates

!    r_a= minval(r_coo(1,:))
!    r_b= maxval(r_coo(1,:))
!    r_fac= 1._GRID_SR/(r_b-r_a)

!---------- initial values

	  node_loop: DO i_count= 1, i_num
	    r_aux(1,i_count)= 0.0_GRID_SR
	    r_diff(:)      = r_coo(:,i_count)- r_centr(:)
	    r_dpt= dot_product(r_diff, r_diff)
	    inside: IF(r_dpt < r_a) THEN
!	      r_aux(1,i_count)= (r_hgt-0.025_GRID_SR) + ((r_coo(1,i_count)- r_a)*r_fac)* (0.05_GRID_SR)
	      r_aux(1,i_count)= r_meantemp
	    END IF inside
	  END DO node_loop

!    node_loop: DO i_count= 1, i_num
!        r_aux(1,i_count)= r_mintemp + ((r_coo(1,i_count)- r_a)*r_fac)* (r_maxtemp-r_mintemp)
!    END DO node_loop
!    r_aux= r_meantemp

!---------- update grid information

	  i_valind= (/ GRID_phi /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_inittemp

	END MODULE SLM_initial
