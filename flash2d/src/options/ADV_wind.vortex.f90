!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
!	according to vortex test case (cf. Nair 2006)
!	but with modified coefficients (l. mentrup)
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force field for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		    real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		l. mentrup	12/2006
!
!*****************************************************************
	MODULE ADV_wind
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC :: slm_windfield, slm_windinit, slm_windquit
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coo, r_time) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coo
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                  :: r_time
		
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_rvec, r_vvec, r_field
		
		REAL (KIND = GRID_SR)                                        :: r_dlt   = 0.05_GRID_SR			! controls the width of the decay from value 2 to value 0
		REAL (KIND = GRID_SR)                                        :: r_vnot  = 2.598_GRID_SR 		! tangential velocity such that omega < 1.
		REAL (KIND = GRID_SR)                                        :: r_scale = 86400._GRID_SR		! scale on one day	
		REAL (KIND = GRID_SR)                                        :: r_eps   = 1.E-6_GRID_SR

	  REAL (KIND = GRID_SR)                                        :: r_t
 	  REAL (KIND = GRID_SR)                                        :: r_r
	
		REAL (KIND = GRID_SR)                                        :: r_vt, r_tanh
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), PARAMETER  :: r_ctr=(/ 0._GRID_SR, 0._GRID_SR /)
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_tmp

!---------- initialize

!---------- set time

	  IF(present(r_time)) THEN
	    r_t= r_time
	  ELSE
	    r_t= 0.0_GRID_SR
	  END IF

!---------- compute the windfield at (x,y) 
!---------- vortex wind in x,y-plane

	  r_rvec = (r_coo - r_ctr) 
		r_r    = twonorm(r_rvec) * GRID_PI * 2._GRID_SR					! scale the domain from [-0.5,0,5]to [-Pi, Pi]
		
		IF (r_r <= r_eps) THEN
		  r_field = 0._GRID_SR
		ELSE
		  r_tanh = tanh(r_r) 
		  r_vt   = (1._GRID_SR - r_tanh*r_tanh) * r_tanh * r_vnot * GRID_PI / r_scale
		  r_vvec  = (/ -r_rvec(2), r_rvec(1) /) / r_r
			r_field = r_vt * r_vvec
		END IF 
		 
	
	  RETURN
	  END FUNCTION slm_windfield

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE
	  TYPE (control_struct)     :: p_control

	  RETURN
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
    FUNCTION twonorm(r_vector) RESULT(r_nrm)
		
		IMPLICIT NONE

		REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)           :: r_vector
		REAL (KIND = GRID_SR)                                      :: r_nrm
		
		INTEGER                                                    :: i_cnt
		
		r_nrm = 0._GRID_SR
		r_nrm = dot_product(r_vector, r_vector)
		r_nrm = sqrt(r_nrm)
		
		END FUNCTION twonorm

	END MODULE ADV_wind
