!*****************************************************************
!
! MODULE NAME:
!	SLM_advanced
! FUNCTION:
!	provide advanced semi-Lagrangian routines
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_step
! FUNCTION:
!	one step of the basic SLM algorithm
! SYNTAX:
!	CALL slm_step(int, real.arr, real.arr)
! ON INPUT:
!	...
! ON OUTPUT:
!	r_tracer: array with tracer values	real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_displace
! FUNCTION:
!	extrapolate the alpha, values for the displacements of the upstream
!	points from the gridpoints
! SYNTAX:
!	CALL slm_displace(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_coord: real array of xy-coordinates		real
! ON OUTPUT:
!	r_alpha: displacement vectors to each point	real
! CALLS:
!	wind_field
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_update
! FUNCTION:
!	calculate the update to the velocity
! SYNTAX:
!	CALL slm_update(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_rside: array with right hand side values	real
! ON OUTPUT:
!	r_udate: array with new (updated) gid values	real
! CALLS:
!
! COMMENTS:
!	this routine is trivial for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_upstream
! FUNCTION:
!	calculate right hand side of the equation (upstream values)
! SYNTAX:
!	CALL slm_upstream(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_alpha: displacement vectors to each point	real
! ON OUTPUT:
!	r_rside: array with right hand side values	real
! CALLS:
!
! COMMENTS:
!	this routine is just interpolation for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_interpolate
! FUNCTION:
!	do the interpolation
! SYNTAX:
!	CALL slm_interpolate(grid, int, real, real.arr, real.arr, real.arr)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
!	r_fac:   factor at which point to interpolate	REAL
!	i_arlen: array length for the following arrays	INTEGER
!	r_coord: coordinate array (new grid)		REAL
!	r_alpha: displacement array (corr. to r_coord)	REAL
!	r_value: values on the old grid (array)		REAL
! ON OUTPUT:
!	r_rside: right hand side (interpolated)		REAL
! CALLS:
!
! COMMENTS:
!	this one is plain bi-cubic spline interpolation
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_interpolinit
! FUNCTION:
!	initialize the interpolation (conservative interpolation)
! SYNTAX:
!	CALL slm_interpolate(grid)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!	we set a global value present in this module:
!	r_conservation: conservation value		REAL
!
!-----------------------------------------------------------------
!
! NAME:
!	triang_ar
! FUNCTION:
!	calculate triangle area given by three coordinates
! SYNTAX:
!
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_displace, slm_update, slm_upstream
! COMMENTS:
!
! USES:
!	FLASH_parameters, GRID_api, SLM_interpolation, ADV_wind, ADV_rhs
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	4/2002
!	2. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_advanced
	  USE FLASH_parameters
	  USE MISC_timing
	  USE GRID_api
	  USE ADV_wind
	  USE ADV_rhs
	  PRIVATE
	  PUBLIC  :: slm_astep
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_astep(p_ghand, p_param, p_time, r_modtime, i_size, &
	                       r_coord, r_tracer, i_newsdepth)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
	  TYPE (control_struct), INTENT(in)                         :: p_param
	  TYPE (sw_info), INTENT(inout)                             :: p_time
	  REAL (KIND = GRID_SR), INTENT(in)                                          :: r_modtime
	  INTEGER, INTENT(in)                                       :: i_size
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size), INTENT(in)        :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_size), INTENT(out)                      :: r_tracer
	  INTEGER, OPTIONAL, INTENT(in)                             :: i_newsdepth

	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE      :: r_newvl
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    :: r_alpha
	  INTEGER                              :: i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), POINTER        :: r_dualcoordinates
	  INTEGER, DIMENSION(:,:,:), POINTER   :: i_dualedges
	  INTEGER                              :: i_dual
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    :: r_dualalpha

!---------- check size!

	  IF(i_size <= 0) THEN
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'INFO [slm_astep]: Zero step size, returning to calling routine'
	    RETURN
	  END IF

!---------- allocate auxiliary arrays

	  allocate(r_newvl(i_size), r_alpha(GRID_dimension,i_size), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(38)
	  END IF not_alloc

!---------- create dual mesh

	  IF(present(i_newsdepth)) THEN
!	    CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
!	                         r_dualcoordinates, i_selectlength=i_size, i_selectindex=i_index)
	  ELSE
	    CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
	                         r_dualcoordinates)
	  END IF

!---------- allocate array for dual displacements

	  allocate(r_dualalpha(GRID_dimension,i_dual), stat=i_alct)
	  not_allocdual: IF(i_alct /= 0) THEN
	    CALL grid_error(40)
	  END IF not_allocdual

!-SLM--------- calculate trajectory pieces (displacements)

	  CALL stop_watch('start',3,p_time)
	  CALL slm_adisplace(p_param, i_size, r_coord, r_alpha, &
	  		    i_dual, r_dualcoordinates, r_dualalpha, r_time=r_modtime)
	  CALL stop_watch('stop ',3,p_time)

!-SLM--------- calculate right hand side

	  CALL stop_watch('start',4,p_time)
	  CALL slm_aupstream(p_ghand, i_size, r_coord, r_alpha, &
	  		    i_dual, r_dualcoordinates, r_dualalpha, &
	  		    i_dualedges, r_newvl)
	  CALL stop_watch('stop ',4,p_time)

!-SLM--------- calculate new grid values

	  CALL stop_watch('start',5,p_time)
	  CALL slm_aupdate(p_param, i_size, r_coord, r_newvl, r_tracer, r_time=r_modtime)
	  CALL stop_watch('stop ',5,p_time)

!---------- destroy dual mesh

	  CALL grid_destroydual(i_dual, i_dualedges, r_dualcoordinates)

!-SLM--------- put alpha values to u and v field entries

	  r_alpha= -r_alpha
	  IF(present(i_newsdepth)) THEN
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_newsdepth=i_newsdepth, i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  ELSE
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  END IF

!-SLM--------- deallocate work arrays

	  deallocate(r_dualalpha, r_alpha, r_newvl)

	  RETURN
	  END SUBROUTINE slm_astep

!*****************************************************************
	  SUBROUTINE slm_adisplace(p_param, i_arlen, r_coord, r_alpha, &
	                          i_darlen, r_dcoord, r_dalpha, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                      :: p_param
	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_alpha
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                           :: r_time
	  INTEGER, INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)  :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(out) :: r_dalpha
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_fac, r_caf, &
	    r_axy, r_xyc
	  REAL (KIND = GRID_SR)                                                 :: r_dt0, r_dt1, &
	    r_dt2, r_tim
	  INTEGER                                              :: i_cnt1, i_cnt2

!---------- set constants

	  r_dt0= p_param%num%r_deltatime
	  r_dt1= 0.5_GRID_SR* p_param%num%r_deltatime
	  r_dt2= 1.5_GRID_SR* p_param%num%r_deltatime
	  r_fac= 0.5_GRID_SR
	  r_caf= 2.0_GRID_SR
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- calculate in an iteration process the displacements

	  unknown_loop: DO i_cnt1=1,i_arlen
	    r_axy= 0.0_GRID_SR

	    iter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_coord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO iter_loop

	    r_alpha(:,i_cnt1)= r_axy
	  END DO unknown_loop

!---------- the same for the dual grid

	  dual_loop: DO i_cnt1=1,i_darlen
	    r_axy= 0.0_GRID_SR

	    diter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_dcoord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO diter_loop

	    r_dalpha(:,i_cnt1)= r_axy
	  END DO dual_loop

	  RETURN
	  END SUBROUTINE slm_adisplace

!*****************************************************************
	  SUBROUTINE slm_aupdate(p_param, i_arlen, r_coord, r_rside, r_udate, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                   :: p_param
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_udate
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                          :: r_time
	  INTEGER                                             :: i_cnt
	  REAL (KIND = GRID_SR)                                                :: r_dt, r_tim

!---------- in the linear advection case and with f90 this is just

!	  r_udate= r_rside

!---------- including a non-zero right hand side, we have

	  r_dt= p_param%num%r_deltatime
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

	  main_loop: DO i_cnt=1, i_arlen
	    r_udate(i_cnt)= r_rside(i_cnt)+ r_dt* slm_righthand(r_coord(:,i_cnt))
	  END DO main_loop

	  RETURN
	  END SUBROUTINE slm_aupdate

!*****************************************************************
	  SUBROUTINE slm_aupstream(p_mesh, i_arlen, r_coord, r_alpha, &
	                           i_darlen, r_dcoord, r_dalpha, &
	                           i_dedge, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)        :: p_mesh
	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_alpha
	  INTEGER, INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dalpha
	  INTEGER, DIMENSION(2,GRID_patchelements,i_arlen), INTENT(in) :: i_dedge
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)                :: r_rside
	  INTEGER                                              :: i_alct, i_cnt, &
	    j_cnt, i_tnc, i_oarlen, i_fill, i_len, i_tmp
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_1, r_2, r_3
	  REAL (KIND = GRID_SR)                                                 :: r_oarea, r_narea
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE                  :: r_aux1
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE                    :: r_aux2
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE                      :: r_mass
	  REAL (KIND = GRID_SR)                                                 :: r_onethird=1._GRID_SR/3._GRID_SR
	  INTEGER, DIMENSION(GRID_patchelements)               :: i_polynod
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,GRID_elementnodes)    :: r_vertx
	  REAL (KIND = GRID_SR)                                                 :: r_part, r_isarea
	  REAL (KIND = GRID_SR), EXTERNAL                                       :: triatria_isectar
	  REAL (KIND = GRID_SR)                                                 :: r_xmax, r_ymax, r_xmin, r_ymin
	  REAL (KIND = GRID_SR)                                                 :: r_maxx, r_maxy, r_minx, r_miny
	  REAL (KIND = GRID_SR)                                                 :: r_val, r_intgrl
	  INTEGER, DIMENSION(:), POINTER                       :: i_triang
	  REAL (KIND = GRID_SR), DIMENSION(:), POINTER                          :: r_area


!---------- main loop over the nodes of the new grid

	  node_loop: DO i_cnt=1,i_arlen

	    r_val= 0.0_GRID_SR
	    r_narea= 0.0_GRID_SR
	    r_vertx= 0.0_GRID_SR

!---------- the upstream dual element's center node

	    r_vertx(1,1)= r_coord(1,i_cnt)- r_alpha(1,i_cnt)
	    r_vertx(2,1)= r_coord(2,i_cnt)- r_alpha(2,i_cnt)

!---------- now calculate the interference of the voronoi thing with elements from old mesh

	    ptch_loop: DO i_tnc= 1, GRID_patchelements
	    IF(i_dedge(1,i_tnc,i_cnt) == 0) THEN
	      EXIT ptch_loop
	    ELSE

!---------- calculate area

	      r_narea= r_narea+ triang_ar(r_coord(:,i_cnt), &
	                                    r_dcoord(:,i_dedge(1,i_tnc,i_cnt)), &
	                                    r_dcoord(:,i_dedge(2,i_tnc,i_cnt)))

!---------- the upstream dual element's outer nodes

	      r_vertx(1,2)= r_dcoord(1,i_dedge(1,i_tnc,i_cnt))- r_dalpha(1,i_dedge(1,i_tnc,i_cnt))
	      r_vertx(2,2)= r_dcoord(2,i_dedge(1,i_tnc,i_cnt))- r_dalpha(2,i_dedge(1,i_tnc,i_cnt))
	      r_vertx(1,3)= r_dcoord(1,i_dedge(2,i_tnc,i_cnt))- r_dalpha(1,i_dedge(2,i_tnc,i_cnt))
	      r_vertx(2,3)= r_dcoord(2,i_dedge(2,i_tnc,i_cnt))- r_dalpha(2,i_dedge(2,i_tnc,i_cnt))

!---------- calculate integral with quadrature rule

	      CALL triang_quadrature(p_mesh(i_time), r_vertx, r_intgrl)
	      r_val= r_val+ r_intgrl

	    END IF
	    END DO ptch_loop

!---------- now calculate concentration

	    r_rside(i_cnt)= r_val/r_narea

	  END DO node_loop

	  RETURN
	  END SUBROUTINE slm_aupstream
!*****************************************************************
	  FUNCTION triang_ar(r_coord1, r_coord2, r_coord3) RESULT (r_area)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord1, r_coord2, r_coord3
	  REAL (KIND = GRID_SR)                                        :: r_area
	  REAL (KIND = GRID_SR)                                        :: r_c
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_a, r_b

!---------- calculate vector components

	  r_a= r_coord2- r_coord1
	  r_b= r_coord3- r_coord1

!---------- calculate components (a,b,c) of cross product vector

	  r_c= (r_a(1)* r_b(2)- r_a(2)* r_b(1))

!---------- calculate area

	  r_area= abs(r_c)* 0.5_GRID_SR

	  RETURN
	  END FUNCTION triang_ar

!*****************************************************************
	  SUBROUTINE triang_quadrature(p_mesh, r_vertcoo, r_intgrl)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle)                            :: p_mesh
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,3), INTENT(in) :: r_vertcoo
	  REAL (KIND = GRID_SR), INTENT(out)                             :: r_intgrl

	  REAL (KIND = GRID_SR), PARAMETER                               :: r_onethird=1._GRID_SR/3._GRID_SR
	  REAL (KIND = GRID_SR), PARAMETER                               :: r_one20=1._GRID_SR/20._GRID_SR
	  REAL (KIND = GRID_SR), PARAMETER                               :: r_two15=2._GRID_SR/15._GRID_SR
	  REAL (KIND = GRID_SR), PARAMETER                               :: r_nine20=9._GRID_SR/20._GRID_SR
	  REAL (KIND = GRID_SR)                                          :: r_narea
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)               :: r_quadpoint
	  REAL (KIND = GRID_SR), DIMENSION(7)                            :: r_vals
	  REAL (KIND = GRID_SR), DIMENSION(3)                            :: r_weight

!---------- calculate area and weights

	  r_narea= triang_ar(r_vertcoo(:,1), r_vertcoo(:,2), r_vertcoo(:,3))
	  r_weight(1)= r_narea* r_one20
	  r_weight(2)= r_narea* r_two15
	  r_weight(3)= r_narea* r_nine20
	  

!---------- calculate interpolation values at quadpoints

	  r_quadpoint=r_vertcoo(:,1)
	  r_vals(1)= grid_coordvalue(p_mesh, r_quadpoint, &
	                             i_interpolorder=GRID_highorder)
	  r_quadpoint=r_vertcoo(:,2)
	  r_vals(2)= grid_coordvalue(p_mesh, r_quadpoint, &
	                             i_interpolorder=GRID_highorder)
	  r_quadpoint=r_vertcoo(:,3)
	  r_vals(3)= grid_coordvalue(p_mesh, r_quadpoint, &
	                             i_interpolorder=GRID_highorder)

	  r_quadpoint=(r_vertcoo(:,1)+ r_vertcoo(:,2))* 0.5_GRID_SR
	  r_vals(4)= grid_coordvalue(p_mesh, r_quadpoint, &
	                             i_interpolorder=GRID_highorder)
	  r_quadpoint=(r_vertcoo(:,2)+ r_vertcoo(:,3))* 0.5_GRID_SR
	  r_vals(5)= grid_coordvalue(p_mesh, r_quadpoint, &
	                             i_interpolorder=GRID_highorder)
	  r_quadpoint=(r_vertcoo(:,3)+ r_vertcoo(:,1))* 0.5_GRID_SR
	  r_vals(6)= grid_coordvalue(p_mesh, r_quadpoint, &
	                             i_interpolorder=GRID_highorder)

	  r_quadpoint=(r_vertcoo(:,1)+ r_vertcoo(:,2)+ r_vertcoo(:,3))* r_onethird
	  r_vals(7)= grid_coordvalue(p_mesh, r_quadpoint, &
	                             i_interpolorder=GRID_highorder)

!---------- calculate integral

	  r_intgrl= SUM(r_vals(1:3))* r_weight(1)+ &
	            SUM(r_vals(4:6))* r_weight(2)+ &
		    r_vals(7)* r_weight(3)

	  RETURN
	  END SUBROUTINE triang_quadrature

	END MODULE SLM_advanced
