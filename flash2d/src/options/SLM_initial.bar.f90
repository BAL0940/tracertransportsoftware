!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize slotted cylinder for semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initbar
! FUNCTION:
!	initialize a grid with values of a bar
! SYNTAX:
!	CALL slm_initbar(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	7/97
!	2. names changed		j. behrens	7/97
!	3. changed to use GRID_api	j. behrens	11/97
!	4. changed interfaces		j. behrens	12/97
!	5. compliant to amatos 1.0	j. behrens	12/2000
!	6. compliant to amatos 1.2	j. behrens	3/2002
!	7. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_cntr=(/ -0.25, 0.0 /)
	  REAL (KIND = GRID_SR)                            :: r_hgt=4.0
	  REAL (KIND = GRID_SR)                            :: r_srd=0.15
	  REAL (KIND = GRID_SR)                            :: r_sln=0.22
	  REAL (KIND = GRID_SR)                            :: r_swd=0.06
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand

!---------- initialize some constant for the slotted cylinder

	  CALL slm_initbar(p_ghand)

	  RETURN
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                         :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                      :: r_time
	  INTEGER, INTENT(in)                                    :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out) :: r_array

!---------- dummy routine

	  r_array= 0.0_GRID_SR

	  RETURN
	  END SUBROUTINE slm_analyticsolution

!*****************************************************************
	  SUBROUTINE slm_initbar(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                     :: p_ghand
	  REAL (KIND = GRID_SR)                              :: r_xsw, r_ysw, r_xne, r_yne
	  INTEGER                                            :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  INTEGER, DIMENSION(1)                              :: i_valind

!---------- initialize some constants for the bar

	  r_xsw= -0.375_GRID_SR
	  r_xne= -0.125_GRID_SR
	  r_ysw= -0.05_GRID_SR
	  r_yne= 0.05_GRID_SR

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber

	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(51)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_aux(1,i_count)= 0.0_GRID_SR
	    inside: IF((r_coo(1,i_count)>r_xsw .AND. r_coo(1,i_count)<r_xne) .AND. &
	               (r_coo(2,i_count)>r_ysw .AND. r_coo(2,i_count)<r_yne)) THEN
	      r_aux(1,i_count)= r_hgt
	    END IF inside
	  END DO node_loop

!---------- update grid information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_initbar

	END MODULE SLM_initial
