!*****************************************************************
!
! MODULE NAME:
!	SLM_advanced
! FUNCTION:
!	
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_astep
! FUNCTION:
!	one step of the advanced SLM algorithm
! SYNTAX:
!	CALL slm_step(int, real.arr, real.arr)
! ON INPUT:
!	...
! ON OUTPUT:
!	r_tracer: array with tracer values	real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_adisplace
! FUNCTION:
!	extrapolate the alpha, values for the displacements of the upstream
!	points from the gridpoints
! SYNTAX:
!	CALL slm_adisplace(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_coord: real array of xy-coordinates		real
! ON OUTPUT:
!	r_alpha: displacement vectors to each point	real
! CALLS:
!	wind_field
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_aupdate
! FUNCTION:
!	calculate the update to the velocity
! SYNTAX:
!	CALL slm_aupdate(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_rside: array with right hand side values	real
! ON OUTPUT:
!	r_udate: array with new (updated) gid values	real
! CALLS:
!
! COMMENTS:
!	this routine is trivial for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_aupstream
! FUNCTION:
!	calculate right hand side of the equation (upstream values)
! SYNTAX:
!	CALL slm_aupstream(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_alpha: displacement vectors to each point	real
! ON OUTPUT:
!	r_rside: array with right hand side values	real
! CALLS:
!
! COMMENTS:
!	this routine is just interpolation for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_ainterpolate
! FUNCTION:
!	do the MPSLM scheme (no interpolation is done!)
! SYNTAX:
!	CALL slm_MPscheme(grid, int, real, real.arr, real.arr, real.arr)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
!	r_fac:   factor at which point to interpolate	REAL
!	i_arlen: array length for the following arrays	INTEGER
!	r_coord: coordinate array (new grid)		REAL
!	r_alpha: displacement array (corr. to r_coord)	REAL
!	r_value: values on the old grid (array)		REAL
! ON OUTPUT:
!	r_rside: right hand side (interpolated)		REAL
! CALLS:
!
! COMMENTS:
!	this is the 2D implementation of Mass Packet SLM
!
!-----------------------------------------------------------------
!
! NAME:
!   generate_MP_coeff
! FUNCTION:
!   generate mass packet coefficients to divide a single 
!   triangle in parts
! SYNTAX:
!   CALL generate_MP_coeff()
! ON INPUT:
!   
! ON OUTPUT:
!   
! CALLS:
!
! COMMENTS:
!   
!
!-----------------------------------------------------------------
!
! NAME:
!   remove_MP_coeff
! FUNCTION:
!   remove the MP_coeff from memory
! SYNTAX:
!   CALL generate_MP_coeff()
! ON INPUT:
!   
! ON OUTPUT:
!   
! CALLS:
!
! COMMENTS:
!   
!
!-----------------------------------------------------------------
!
! 
! COMMENTS:
!
! USES:
!   MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
!
!*****************************************************************
! PUBLIC:
!	slm_step
! COMMENTS:
!
! USES:
!	FLASH_parameters, GRID_api, ADV_wind, ADV_rhs
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	04/2002
!	2. original MPSLM version     	l. mentrup  	02/2003
!	3. compliant to amatos 2.0	j. behrens	07/2003
!   	4. adapted 2D version   	l. mentrup  	11/2003
!	5. bugs fixed			j. behrens	12/2003
!
!*****************************************************************
	MODULE SLM_advanced
	  USE FLASH_parameters
	  USE MISC_timing
	  USE GRID_api
	  USE ADV_wind
	  USE ADV_rhs
	  

!*****************************************************************

	IMPLICIT NONE

	INTEGER, SAVE                   	:: MAX_REFLVL

!---------- Begin Triangle table

	TYPE conservation_entry
	  INTEGER                 		:: i_MPperTri
	  INTEGER, POINTER			:: i_MPindex(:)            	! index array of the MP evoked by a triangle
	  INTEGER            			:: i_refinelvl          	! refinement level
	  LOGICAL       	      		:: l_massassigned
	  LOGICAL   	          		:: l_searchtri
	  LOGICAL             			:: l_refine
	  REAL (KIND = GRID_SR)                			:: r_area
	  REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)    :: r_mass          		! mass per corner point of the triangle
	END TYPE conservation_entry


	TYPE conservation_entry_ptr
	  TYPE(conservation_entry), POINTER   	:: cp
	END TYPE conservation_entry_ptr


	TYPE (conservation_entry_ptr), DIMENSION(:,:), ALLOCATABLE, SAVE :: p_ttable  
	
!---------- End Triangle table


!---------- Begin Mass Packet Coefficient table

	!------ coefficient container to generate one mass packet
	TYPE MP_coeff                   
	  INTEGER               		:: i_origin 	! index of the originating triangle node
	  INTEGER, DIMENSION(GRID_dimension)  	:: i_prvdir 	! prev direction of the tree
	  INTEGER               		:: i_celltype   ! 1: @corner, 2: @edge, 3: in triangle
	  REAL (KIND = GRID_SR)                  		:: r_area 	! ratio of the mass packets area to the area of the triangle
	  REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)  	:: r_coeff  	! coefficients to compute the coordinates of the mass packet (dimensionless)
	  REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)  	:: r_mass   	! defines how much mass per node will be assigned (ratio)
	  LOGICAL               		:: l_end    	! end of tree? if TRUE, then create new branches in next refinement level
	END TYPE MP_coeff


	! array of MassPacket coefficients, to generate Mass Packets of a triangle for a certain reflvl
	TYPE MP_coeff_arr                   
	  TYPE (MP_coeff), POINTER      	:: mpc(:)   	!
	END TYPE MP_coeff_arr


	TYPE MP_coeff_arr_ptr
	  INTEGER               		:: i_cnum   	! number of cells/mass packets, each cell is a mass packet
	  TYPE (MP_coeff_arr), POINTER      	:: p_mpc    	! pointer to the mass packet coefficient array
	END TYPE MP_coeff_arr_ptr


	TYPE (MP_coeff_arr_ptr), DIMENSION(:), ALLOCATABLE, SAVE    :: p_MPCtable   ! pointer to the coefficient array of each reflvl
	 
	   
!----------
      
      
      
!---------- Begin Mass packet table  
   
	TYPE MP_entry
	  INTEGER                       	:: i_MPinTri
	  INTEGER                       	:: i_MPbyTri
	  REAL (KIND = GRID_SR)                          	:: r_mass
	  REAL (KIND = GRID_SR)                              	:: r_area
	  REAL (KIND = GRID_SR), DIMENSION (GRID_dimension)	:: r_coord
	  LOGICAL                       	:: l_searchMP
 	  LOGICAL                       	:: l_assignmass
	END TYPE MP_entry


	TYPE MP_entry_ptr
	  TYPE (MP_entry), POINTER         	:: MP
	END TYPE MP_entry_ptr 
	  
!---------- End Mass packet table  


!*****************************************************************

	  PRIVATE
	  PUBLIC  :: slm_astep, slm_interpolinit, slm_interpolquit
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_astep(p_ghand, p_param, p_time, r_modtime, i_size, &
	                       r_coord, r_tracer, i_newsdepth)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
	  TYPE (control_struct), INTENT(in)                         :: p_param
	  TYPE (sw_info), INTENT(inout)                             :: p_time
	  REAL (KIND = GRID_SR), INTENT(in)                                          :: r_modtime
	  INTEGER, INTENT(in)                                       :: i_size
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size), INTENT(in)        :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_size), INTENT(out)                      :: r_tracer
	  INTEGER, OPTIONAL, INTENT(in)                             :: i_newsdepth
	  
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE      :: r_newvl
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    :: r_alpha
	  INTEGER                              :: i_alct

!---------- check size!

	  IF(i_size <= 0) THEN
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'INFO [slm_astep]: Zero step size, returning to calling routine'
	    RETURN
	  END IF

!---------- allocate auxiliary arrays

	  allocate(r_newvl(i_size), r_alpha(GRID_dimension,i_size), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(38)
	  END IF not_alloc

!-SLM--------- calculate trajectory pieces (displacements)

	  CALL stop_watch('start',3,p_time)
	  CALL slm_adisplace(p_param, i_size, r_coord, r_alpha, r_time=r_modtime)
	  CALL stop_watch('stop ',3,p_time)

!-SLM--------- calculate right hand side

	  CALL stop_watch('start',4,p_time)
	  CALL slm_aupstream(p_ghand, i_size, r_coord, r_alpha, r_newvl)
	  CALL stop_watch('stop ',4,p_time)

!-SLM--------- calculate new grid values

	  CALL stop_watch('start',5,p_time)
	  CALL slm_aupdate(p_param, i_size, r_coord, r_newvl, r_tracer, r_time=r_modtime)
	  CALL stop_watch('stop ',5,p_time)

!-SLM--------- put alpha values to u and v field entries

	  r_alpha= -r_alpha
	  IF(present(i_newsdepth)) THEN
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_newsdepth=i_newsdepth, i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  ELSE
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  END IF

!-SLM--------- deallocate work arrays

	  deallocate(r_alpha, r_newvl)

	  RETURN
	  END SUBROUTINE slm_astep

!*****************************************************************
	  SUBROUTINE slm_adisplace(p_param, i_arlen, r_coord, r_alpha, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                      :: p_param
	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_alpha
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL				:: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)			:: r_fac, r_caf, &
	    r_axy, r_xyc
	  REAL (KIND = GRID_SR)                                                 :: r_dt0, r_dt1, &
	    r_dt2, r_tim
	  INTEGER                                              :: i_cnt1, i_cnt2
          
!---------- set constants

	  r_dt0= p_param%num%r_deltatime
	  r_dt1= 0.5_GRID_SR* p_param%num%r_deltatime
	  r_dt2= 1.5_GRID_SR* p_param%num%r_deltatime
	  r_fac= 0.5_GRID_SR
	  r_caf= 2.0_GRID_SR
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- calculate in an iteration process the displacements

	  unknown_loop: DO i_cnt1=1,i_arlen
	    r_axy= 0.0_GRID_SR

	    iter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_coord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO iter_loop

	    r_alpha(:,i_cnt1)= r_axy
	  END DO unknown_loop

	  RETURN
	  END SUBROUTINE slm_adisplace

!*****************************************************************
	  SUBROUTINE slm_aupdate(p_param, i_arlen, r_coord, r_rside, r_udate, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                   :: p_param
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_udate
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                          :: r_time
	  INTEGER                                             :: i_cnt
	  REAL (KIND = GRID_SR)                                                :: r_dt, r_tim

!---------- in the linear advection case and with f90 this is just

!	  r_udate= r_rside

!---------- including a non-zero right hand side, we have

	  r_dt= p_param%num%r_deltatime
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

	  main_loop: DO i_cnt=1, i_arlen
	    r_udate(i_cnt)= r_rside(i_cnt)+ r_dt* slm_righthand(r_coord(:,i_cnt))
	  END DO main_loop

	  RETURN
	  END SUBROUTINE slm_aupdate

!*****************************************************************
	  SUBROUTINE slm_aupstream(p_mesh, i_arlen, r_coord, &
	                          r_alpha, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)       :: p_mesh
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_alpha
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)	       :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)		       :: r_fac

!---------- set factor (at which point of trajectory shall i interpolate)

	  r_fac= 1.0_GRID_SR

!---------- in the linear advection case this is just interpolation

	  CALL slm_ainterpolate(p_mesh, r_fac, i_arlen, r_coord, &
	                       r_alpha, r_rside)

	  RETURN
	  END SUBROUTINE slm_aupstream
	  
!*****************************************************************

      SUBROUTINE slm_ainterpolate(p_mesh, r_fac, i_arlen, &
                                 r_coord, r_alpha, r_rside)

!---------- local declarations

	IMPLICIT NONE

	TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_mesh
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)         	:: r_fac
	INTEGER, INTENT(in)                                 	:: i_arlen
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) 	:: r_coord
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) 	:: r_alpha
	REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               	:: r_rside
     
      
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension, GRID_edgenodes) 	:: r_gn_coor
	REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE           		:: r_en_coor_old
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             		:: r_ref 
      
	REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE              		:: r_upstr
	REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE             		:: r_elmtarea_up
      
	REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE             		:: r_totalarea
          
	REAL (KIND = GRID_SR), DIMENSION(i_arlen)                    		:: r_nmass, r_narea

	REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)            		:: r_mass_tmp
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension, GRID_elementnodes)  	:: r_en_coor				! elementnodes coordinates
	
	!------- vars. for a priori determination of inner refinement level
	REAL (KIND = GRID_SR)							:: r_beta				! see info below
	REAL (KIND = GRID_SR)							:: r_elmtmass				! current tetra mass
	REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE				:: r_elmtconc				! triangle (elmt) concentration array
	REAL (KIND = GRID_SR)							:: r_elmtconc_max, r_elmtconc_min	! extrema of concentration in triangle (elmt)
	REAL (KIND = GRID_SR)							:: r_relconc, r_relreflvl		! relative conc w.r.t. extrema (0..1)				

	!------- control mass conservation
	REAL (KIND = GRID_SR)                            			:: r_mass_before, r_mass_inside, r_mass_outside

	!------- control inner adaptivity
	INTEGER				 			:: MAX_LOOPS
	REAL (KIND = GRID_SR)				    			:: r_gamma, r_eps
	REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE				:: r_arearatio
	REAL (KIND = GRID_SR)							:: r_arearatio_max, r_arearatio_min, r_arearatio_mean
      
	INTEGER				 			:: i_loopnum

	
	INTEGER, DIMENSION(:), ALLOCATABLE		  	:: i_mps_in_tri
	INTEGER				 			:: i_mpInTri
	INTEGER				 			:: i_MPidx
!	INTEGER				 			:: i_elmtindex
	INTEGER, DIMENSION(:,:), ALLOCATABLE			:: i_gnodes 
	INTEGER, DIMENSION(:,:), ALLOCATABLE			:: i_elmtedges   
	INTEGER, DIMENSION(:,:), ALLOCATABLE			:: i_enodes, i_enodes_old
	INTEGER, DIMENSION(:,:), ALLOCATABLE			:: i_eneighbours
	INTEGER, DIMENSION(:), ALLOCATABLE		  	:: i_elmtlevel	 	! level of refinement for each	triangle (finite element of triangular form)
	INTEGER, DIMENSION(:), ALLOCATABLE			:: i_maxlvl
	INTEGER				 			:: i_alct, i_siz
	INTEGER							:: i_enum_f, i_enum_f_old, i_gnum,  i_nnum, i_nnum_old, i_MPnum, i_enum_mass
	INTEGER				 			:: i_elmt_cnt, i_node_cnt, j_cnt, i_triangle_cnt, i_MP_cnt, i_cnt, i_dim
	INTEGER				 			:: i_tim, i_timplus
	INTEGER				 			:: i_massassigned, i_areaoverload, i_savedMP, i_totalMP
	
	INTEGER							:: i_reflvl_startincr, i_reflvl_incr, i_clvlbnd, i_rlvlbnd
	
	

	TYPE (MP_entry_ptr), DIMENSION(:), ALLOCATABLE   	:: p_MPtable		! Mass	packet table
	TYPE (MP_entry_ptr), DIMENSION(:), ALLOCATABLE   	:: p_MPtable_tmp	! temp. mass packet table, for initialization and reallocation purposes
	TYPE (MP_entry), POINTER		    		:: p_MP			! temp. single mass packet entry
	
	TYPE (conservation_entry_ptr), DIMENSION(:), ALLOCATABLE  :: p_ttable_tmp	! temp. triangle table, for initialization and reallocation purposes
	TYPE (conservation_entry), POINTER		 	:: p_ce			! temp. single conservation entry 
	INTEGER, POINTER					:: i_MPidx_tmp(:)	! temp. mass packet index array to reallocate mass packets per triangle dynamically
!-----------	
	
	
	
!----------- initialize
	i_enum_f		= p_mesh(i_timeplus)%i_enumfine
	i_enum_f_old  		= p_mesh(i_time)%i_enumfine
	i_gnum	  		= p_mesh(i_timeplus)%i_gnumber
	i_nnum_old		= p_mesh(i_time)%i_nnumber
	i_nnum			= p_mesh(i_timeplus)%i_nnumber     
	i_tim     		= p_mesh(i_time)%i_timetag
	i_timplus 		= p_mesh(i_timeplus)%i_timetag
	i_clvlbnd		= p_mesh(i_tim)%i_crslvlbnd	! coarsest refinement level
	i_rlvlbnd		= p_mesh(i_tim)%i_reflvlbnd	! finest refinement level


!----------- a priori refinement control
   	r_beta			= 0.5   	! this controls how much the concentration is weighted against the reflvl of the triangle for 
						! the a priori choice of refinement level

!----------- a posteriori controls for inner adaptivity
	MAX_LOOPS 		= 1		! set maximum inner iterations
	i_reflvl_startincr	= 5		! starting increment for inner refinement
	i_reflvl_incr 		= 3 		! increment for inner refinement

	r_gamma 		= 1.1
!----------- r_gamma controls the mass assignment
!----------- if the total volume of all triangle which, via the barycenter, assigned mass to a triangle in the new mesh is greater than r_gamma*area(triangle), 
!----------- the old mesh is coarse (w.r.t. volume) to be downstreamed without refining locally. Danger of mass concentration!	
						
	r_eps			= 1.e-5		! epsilon greather than zero...
!-----------	

  
!---------- allocate work array
!-------------- 'old mesh' arrays
	
	ALLOCATE(i_elmtlevel(i_enum_f_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')

	ALLOCATE(i_maxlvl(i_enum_f_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	    
	ALLOCATE(i_enodes_old(GRID_elementnodes,i_enum_f_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(r_en_coor_old(GRID_dimension, i_nnum_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(r_elmtconc(i_enum_f_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')

!-------------- 'new mesh' arrays     
	
	ALLOCATE(r_upstr(GRID_dimension, i_nnum), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
     
	ALLOCATE(r_elmtarea_up(i_enum_f), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(r_totalarea(i_enum_f), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(r_arearatio(i_enum_f), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(i_mps_in_tri(i_enum_f), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')    
	
	ALLOCATE(i_gnodes(GRID_edgenodes,i_gnum), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(i_enodes(GRID_elementnodes,i_enum_f), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(i_elmtedges(GRID_elementedges,i_enum_f), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	
	ALLOCATE(i_eneighbours(GRID_elementedges, i_enum_f), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')

!----------

	
!---------- preinitialize
	
	i_elmtlevel= 0.
	i_enodes_old= 0
	r_en_coor_old= 0.
	r_elmtconc=0.
	
	r_upstr= 0.
	r_elmtarea_up= 0.
	r_totalarea= 0.
	i_mps_in_tri= 0
	i_gnodes= 0
	i_enodes= 0
	i_elmtedges= 0
	i_eneighbours= 0
	
	r_nmass= 0.
	r_narea= 0.
	r_rside= 0.
	
!----------
	

!---------- initialize Tetra table	
!---------- ATTENTION> values of i_timeplus and i_time are alternating after each timestep. thus i_timeplus(new_timestep) == i_time(old_timestep) etc.
!---------- Did the length of the conservation table grow due to more nodes/tetrahedra in the new mesh? Then re-initialize the conservation table

	IF (.NOT. ALLOCATED(p_ttable)) THEN
	  CALL slm_interpolinit(p_mesh)
	END IF

	IF (size(p_ttable(i_timplus,:)) < i_enum_f) THEN
	  ALLOCATE(p_ttable_tmp(i_enum_f_old), stat=i_alct)
	  IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	  
	  DO i_cnt=1, i_enum_f_old
	    p_ttable_tmp(i_cnt)%cp => p_ttable(i_tim, i_cnt)%cp
	    DEALLOCATE(p_ttable(i_timplus, i_cnt)%cp)
	  END DO
	  
	  DO i_cnt= i_enum_f_old+1, size(p_ttable(i_tim,:))
	    DEALLOCATE(p_ttable(i_tim, i_cnt)%cp)
	    DEALLOCATE(p_ttable(i_timplus, i_cnt)%cp)
	  END DO
	  		
	  DEALLOCATE(p_ttable)
	  
	  ALLOCATE(p_ttable(GRID_timesteps, i_enum_f), stat=i_alct)    
	  IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
			 
	  DO i_cnt=1, i_enum_f_old
	    p_ttable(i_tim, i_cnt)%cp => p_ttable_tmp(i_cnt)%cp
	  END DO
	  
	  DEALLOCATE(p_ttable_tmp)
	  
	  DO i_cnt=i_enum_f_old+1, i_enum_f
	    ALLOCATE(p_ce, stat=i_alct)
	    IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	    p_ce%i_MPperTri     	= 3
	    p_ce%i_refinelvl		= 1
	    p_ce%r_mass		= 0.
	    p_ce%r_area     		= 0.
	    p_ce%l_massassigned  	= .FALSE.
	    p_ce%l_searchtri    	= .FALSE.
	    p_ce%l_refine     		= .FALSE.
	    p_ttable(i_tim, i_cnt)%cp 	=> p_ce
	  END DO
	  
	  DO i_cnt=1, i_enum_f
	    ALLOCATE(p_ce, stat=i_alct)
	    IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')   
	    p_ce%i_MPperTri     	= 3
	    p_ce%i_refinelvl		= 1
	    p_ce%r_mass			= 0.
	    p_ce%r_area     		= 0.
	    p_ce%l_massassigned 	= .FALSE.
	    p_ce%l_searchtri    	= .FALSE.
	    p_ce%l_refine     		= .FALSE.
	    p_ttable(i_timplus, i_cnt)%cp => p_ce
	  END DO
	  
	ELSE ! no array size adjustment necessary, reset i_timplus array to zero

	  DO i_cnt=1, i_enum_f
	    p_ttable(i_timplus, i_cnt)%cp%i_MPperTri    	= 3
	    p_ttable(i_timplus, i_cnt)%cp%i_refinelvl     	= 1
	    p_ttable(i_timplus, i_cnt)%cp%r_mass	   	= 0.
	    p_ttable(i_timplus, i_cnt)%cp%r_area	  	= 0.	    
	    p_ttable(i_timplus, i_cnt)%cp%l_massassigned 	= .FALSE.
	    p_ttable(i_timplus, i_cnt)%cp%l_searchtri   	= .FALSE.
	    p_ttable(i_timplus, i_cnt)%cp%l_refine	  	= .FALSE.   
	  END DO  
		     
	END IF
	
!----------	   


!---------- calculate upstream coordinates of the new mesh

	dim_loop: DO i_cnt=1, GRID_dimension
	  r_upstr(i_cnt,:) = r_coord(i_cnt,:)- r_fac(i_cnt)* r_alpha(i_cnt,:)
	END DO dim_loop
	
!----------   


!---------- collect necessary infos of the new mesh to assign the later generated mass points to the tetrahedra in the new mesh

	CALL grid_getinfo(p_mesh(i_timeplus), l_finelevel=.TRUE., l_relative=.TRUE., i_elementnodes= i_enodes)
	CALL grid_getinfo(p_mesh(i_timeplus), l_finelevel=.TRUE., l_relative=.TRUE., i_elementedges= i_elmtedges)
	CALL grid_getinfo(p_mesh(i_timeplus), l_finelevel=.TRUE., l_relative=.TRUE., i_edgenodes= i_gnodes)
	CALL grid_getinfo(p_mesh(i_timeplus), l_finelevel=.TRUE., l_relative=.TRUE., i_elementneighbors= i_eneighbours)

	
!----------


!----------- calculate area of upstreamed elements (trisngles) to control assignment of mass to the new mesh later	

	DO i_cnt= 1, i_enum_f
	  r_elmtarea_up(i_cnt) = calc_triangarea(r_upstr(:, i_enodes(1, i_cnt)), r_upstr(:, i_enodes(2, i_cnt)),&
					 r_upstr(:, i_enodes(3, i_cnt)))
	END DO 
	
!----------


!---------- collect necessary infos of the old mesh to generate mass packets

	CALL grid_getinfo(p_mesh(i_time), r_nodecoordinates = r_en_coor_old)
	CALL grid_getinfo(p_mesh(i_time), l_finelevel=.TRUE., l_relative=.TRUE., i_elementnodes= i_enodes_old)
	CALL grid_getinfo(p_mesh(i_time), l_finelevel=.TRUE., i_elementlevel= i_elmtlevel)
	
!----------


!---------- determine concentration max/min to compute refinement level of triangle later 
!---------- (only non zero min/max since there are only mass packets for triangles with nonzero mass)

	r_elmtconc_max = 0.
	
	!------ fill triangle concentration table
	DO i_triangle_cnt= 1, i_enum_f_old
	  r_elmtmass = SUM(p_ttable(i_tim, i_triangle_cnt)%cp%r_mass)
	  IF (r_elmtmass > 0.) THEN
	    r_elmtconc(i_triangle_cnt) = r_elmtmass / p_ttable(i_tim, i_triangle_cnt)%cp%r_area		! r_area > 0 for all triangles 
	    r_elmtconc_min = r_elmtconc(i_triangle_cnt)	!choose any non-zero start for minimum search
	  ELSE
	    r_elmtconc(i_triangle_cnt) = 0.
	  END IF
	END DO
        
	!------ get the max/min
	DO i_cnt=1,i_enum_f_old
	  IF (r_elmtconc(i_cnt) > 0.) THEN
	    IF (r_elmtconc(i_cnt) < r_elmtconc_min) THEN
	      r_elmtconc_min = r_elmtconc(i_cnt)
	    END IF
	    IF (r_elmtconc(i_cnt) > r_elmtconc_max) THEN
	      r_elmtconc_max = r_elmtconc(i_cnt)
	    END IF
	  END IF
	END DO 
	
!----------       

!---------- initialize all triangles with mass to be searched (a priori refinement level determination)

	i_enum_mass = 0  
	
	DO i_triangle_cnt= 1, i_enum_f_old
	  IF (p_ttable(i_tim, i_triangle_cnt)%cp%l_massassigned) THEN
	    i_enum_mass = i_enum_mass + 1
	    p_ttable(i_tim, i_triangle_cnt)%cp%l_searchtri = .TRUE.

	    r_relconc 	= (r_elmtconc(i_triangle_cnt) - r_elmtconc_min + r_eps) / &		! beware of zero-valued terms --> r_eps
			  (r_elmtconc_max - r_elmtconc_min + r_eps)
	    r_relreflvl	= (REAL((i_elmtlevel(i_triangle_cnt) - i_clvlbnd),GRID_SR) + r_eps) / &		
			  (REAL((i_rlvlbnd - i_clvlbnd),GRID_SR) + r_eps)
	       
            p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl = CEILING(r_relconc * r_relreflvl * REAL(MAX_REFLVL,GRID_SR)) ! - i_reflvl_incr))

	    IF (p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl > MAX_REFLVL) THEN
             p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl = MAX_REFLVL
	    ELSE
	      IF (p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl < 7) THEN
	        p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl = 7
              END IF
	    END IF  

	    p_ttable(i_tim, i_triangle_cnt)%cp%i_MPperTri = p_MPCtable(p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl)%i_cnum	    
	  ELSE 
	    p_ttable(i_tim, i_triangle_cnt)%cp%l_searchtri = .FALSE.
	    p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl = 1
	    p_ttable(i_tim, i_triangle_cnt)%cp%i_MPperTri  = 3
	  END IF
	  
	  ALLOCATE(i_MPidx_tmp(p_ttable(i_tim, i_triangle_cnt)%cp%i_MPperTri), stat=i_alct)
	  IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')	  
	  i_MPidx_tmp = 0
	  p_ttable(i_tim,i_triangle_cnt)%cp%i_MPindex => i_MPidx_tmp
	  NULLIFY(i_MPidx_tmp)
	  	  
	END DO
	
!-----------


!----------- initialize Mass Point table

	i_MPnum = 0
	
	!------ determine total number of Mass points
	DO i_triangle_cnt= 1, i_enum_f_old
	  IF (p_ttable(i_tim, i_triangle_cnt)%cp%l_massassigned) THEN
	    i_MPnum = i_MPnum + p_ttable(i_tim, i_triangle_cnt)%cp%i_MPperTri
	  END IF
	END DO
	!------
     	
	!------ Allocate and initialize new mass point table
	ALLOCATE(p_MPtable(i_MPnum), stat=i_alct)
	IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
    
	DO i_cnt = 1, i_MPnum
	  ALLOCATE(p_MP, stat=i_alct)
	  IF (i_alct/=0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
	  p_MP%i_MPinTri = 0
	  p_MP%i_MPbyTri = 0
	  p_MP%r_mass = 0.
	  p_MP%r_area = 0.
	  p_MP%r_coord = 0.
	  p_MP%l_searchMP = .FALSE.
	  p_MP%l_assignmass = .FALSE.
	  p_MPtable(i_cnt)%MP => p_MP
	  NULLIFY(p_MP)
	END DO
	!------
	
!----------
         

!---------- start iterating loop here--------------------

	i_loopnum = 0
	i_savedMP = 0
	i_totalMP = 0
	
	iterating_loop:DO 
	  i_loopnum = i_loopnum + 1
    
  	!------ create mass point table from triangle table      
      	  
	  !------ determine total number of Mass packets
	  i_MPnum = 0
	  
	  DO i_triangle_cnt= 1, i_enum_f_old
	    IF (p_ttable(i_tim, i_triangle_cnt)%cp%l_massassigned) THEN
	      i_MPnum = i_MPnum + p_ttable(i_tim, i_triangle_cnt)%cp%i_MPperTri
	    END IF
	  END DO
       	  !------		
		
	  !------ Create new mass points which still have to be searched (save old already found ones, if existent)
	  !------ temporary array is not nice, but necessary here, no dynamic memory allocation on p_MPtable in other module possible	  
	  ALLOCATE(p_MPtable_tmp(i_MPnum), stat=i_alct)
	  IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
          
	  CALL createMPs(p_MPtable,p_MPtable_tmp,i_MPnum, i_tim, r_en_coor_old, i_nnum, i_enodes_old, i_enum_f_old)
            
	  i_siz = size(p_MPtable)
	  DO i_cnt=1,i_siz
	    DEALLOCATE(p_MPtable(i_cnt)%MP)
	  END DO      
	  DEALLOCATE(p_MPtable)    
	  ALLOCATE(p_MPtable(i_MPnum), stat=i_alct)
	  IF (i_alct/=0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
        
	  DO i_cnt = 1, i_MPnum
	    p_MPtable(i_cnt)%MP => p_MPtable_tmp(i_cnt)%MP
	  END DO
      
	  i_siz = size(p_MPtable_tmp)
	  DO i_cnt = i_MPnum+1, i_siz
	    DEALLOCATE(p_MPtable_tmp(i_cnt)%MP)
	  END DO     
	  DEALLOCATE(p_MPtable_tmp)
	  !------
	  
	!------

	  
	!------ Now we have the coordinates of the barycenter and the mass of the mass packetsts. Lets look, in which 'new mesh' triangle they lie	  
	!------ find triangle of the upstreamed mesh in which each mass packet lies
 
	  masspacket_loop: DO i_cnt=1, i_MPnum
	  
	    IF (p_MPtable(i_cnt)%MP%l_searchMP) THEN				! if the position of MP is already known, why search again?
	      i_totalMP = i_totalMP + 1 
	      CALL searchMP(p_MPtable, i_MPnum, i_cnt, i_enum_f, i_gnum, &		! this subroutine is very time consuming!
			  i_nnum, i_gnodes, i_enodes, i_elmtedges, i_eneighbours, r_upstr)  	
	    END IF
	     
	  END DO masspacket_loop
	  
	!------

	
	!------------ What about mass packets lying outside the mesh? masses will NOT be assigned to any nodes


	!------ generate area stats: How much area does the triangles of the new get of the old mesh; for a posteriori adaption control mechanism
	
      	  r_totalarea(:) 	= 0.
	  i_mps_in_tri(:)  	= 0
	  
	  DO i_cnt=1, i_MPnum
	    IF (p_MPtable(i_cnt)%MP%l_assignmass) THEN
	      i_mpInTri = p_MPtable(i_cnt)%MP%i_MPinTri
	      r_totalarea(i_mpInTri) = r_totalarea(i_mpInTri) + p_MPtable(i_cnt)%MP%r_area
	      i_mps_in_tri(i_mpInTri) = i_mps_in_tri(i_mpInTri) + 1
	    END IF
	  END DO
	  
	!-------
	

	!------ generate stats how many tetras in the new mesh will get mass assigned
	
	  i_massassigned = 0
	  
	  DO i_cnt=1,i_enum_f
	    IF (i_mps_in_tri(i_cnt) > 0) THEN
	      i_massassigned = i_massassigned +1
	    END IF
	  END DO
	  
	  IF (i_massassigned == 0) THEN
	    CALL grid_error(c_error='[SLM_interpolation] No mass to assign.')
	  END IF
	  
	!------
	
	
	!------- calculate ratio of assigned area w.r.t. upstream area of triangle; get min, max and mean value
	  
	  r_arearatio_max 	= 1.0
	  r_arearatio_min 	= 1.0
	  r_arearatio_mean	= 1.0
	  j_cnt			= 0
	  
	  DO i_cnt=1, i_enum_f
	    IF (r_totalarea(i_cnt) > 0. .AND. r_elmtarea_up(i_cnt) > 0.) THEN
	      r_arearatio(i_cnt) = r_totalarea(i_cnt) / r_elmtarea_up(i_cnt)
	      IF (r_arearatio(i_cnt) > r_arearatio_max) THEN
		r_arearatio_max = r_arearatio(i_cnt)
	      END IF
	      IF (r_arearatio(i_cnt) < r_arearatio_min) THEN   
	        r_arearatio_min = r_arearatio(i_cnt)
	      END IF
	      j_cnt = j_cnt +1
	      r_arearatio_mean = r_arearatio_mean + r_arearatio(i_cnt)
	    END IF
	  END DO
	  
	  r_arearatio_mean = r_arearatio_mean / REAL(j_cnt,GRID_SR)		!without zero-valued tetras

	!-------
	
	
	!------- check the volume condition and refine in case of "area overload"
	
	  i_areaoverload = 0
	  
	  mp_loop: DO i_cnt=1, i_MPnum
	    i_mpInTri = p_MPtable(i_cnt)%MP%i_MPinTri
	    
	    !------ 
	    IF ((.NOT. p_MPtable(i_cnt)%MP%l_assignmass)  .OR. &
	  	(i_MPinTri == 0) 			  .OR. &
            	(p_MPtable(i_cnt)%MP%r_mass == 0.)) 	  THEN
		
              CYCLE mp_loop
            END IF
	    !------
        
            !------ if the total volume of mass packets landed in the upstreamed triangle is greater than r_gamma*area(upstreamed triangle), refine!       
            IF (r_gamma < r_arearatio(i_MPinTri)) THEN
               
              !------ dont assign mass of this tetra to the nodes of the new mesh; refine first         
              p_ttable(i_tim, p_MPtable(i_cnt)%MP%i_MPbyTri)%cp%l_refine = .TRUE.         

              !------ for the stats: note the number of tetra in new mesh with too much volume assigned
              IF (i_mps_in_tri(i_MPinTri) > 0) THEN
		i_areaoverload = i_areaoverload + 1
		i_mps_in_tri(i_MPinTri) = 0
	      END IF
         
	    END IF
	    !------
	         
	  END DO mp_loop
	  
	!----------
	
     	!---------- leave refinement loop if MAX_LOOPS is arrived   ! or no area overload is determined
	
	  IF ((i_loopnum == MAX_LOOPS) ) THEN	!.OR. (i_areaoverload == 0)
	    EXIT
	  ELSE    
            !------ rise refinement_lvl and mark triangles for re-search 
	    DO i_cnt= 1, i_enum_f_old
	    
	      IF ((p_ttable(i_tim, i_cnt)%cp%l_refine) .AND. (p_ttable(i_tim, i_cnt)%cp%i_refinelvl < MAX_REFLVL)) THEN
	      
	        !------ all MPs by one triangle must be recalculated, reset all MPs of the concerned triangle
	        DO j_cnt=1, p_ttable(i_tim, i_cnt)%cp%i_MPperTri
        	  i_MPidx = p_ttable(i_tim, i_cnt)%cp%i_MPindex(j_cnt)      
        	  p_MPtable(i_MPidx)%MP%l_assignmass = .FALSE.
		END DO
		!------
		
	      	!------ set new inner refinement level of triangle
		p_ttable(i_tim, i_cnt)%cp%i_refinelvl = p_ttable(i_tim, i_cnt)%cp%i_refinelvl + i_reflvl_incr
		IF (p_ttable(i_tim, i_cnt)%cp%i_refinelvl > MAX_REFLVL) p_ttable(i_tim, i_cnt)%cp%i_refinelvl = MAX_REFLVL
		p_ttable(i_tim, i_cnt)%cp%i_MPperTri = p_MPCtable(p_ttable(i_tim, i_cnt)%cp%i_refinelvl)%i_cnum
		!------
		
	      	!------ prolongue the dynamically administered Mass packet index array
		DEALLOCATE(p_ttable(i_tim, i_cnt)%cp%i_MPindex)
		ALLOCATE(i_MPidx_tmp(p_ttable(i_tim, i_cnt)%cp%i_MPperTri), stat=i_alct)
		IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolate] Allocation error.')
		i_MPidx_tmp = 0
		p_ttable(i_tim, i_cnt)%cp%i_MPindex => i_MPidx_tmp
		NULLIFY(i_MPidx_tmp)
		!------
		
	      	!------ set logical values for re-search	
		p_ttable(i_tim, i_cnt)%cp%l_refine = .FALSE.
		p_ttable(i_tim, i_cnt)%cp%l_searchtri = .TRUE.
		!------
		
	      ELSE ! triangle not to be refined or already on maximum refinement level	      
		p_ttable(i_tim, i_cnt)%cp%l_searchtri = .FALSE.
	      END IF
	      
	    END DO
	    !------
	    
	  END IF
	  
	!----------

	
	END DO iterating_loop  
	
!---------- end iterating loop here--------------------   


!---------- assign mass of the mass packets to the nodes of the triangle in which the mass packet landed; save mass and volume for next step

    mass_assignment_mp_loop: DO i_cnt=1, i_MPnum        
        
	  IF (.NOT. p_MPtable(i_cnt)%MP%l_assignmass 	.OR. &
	      p_MPtable(i_cnt)%MP%i_MPinTri == 0 	.OR. &
	      p_MPtable(i_cnt)%MP%r_mass == 0.) THEN
	    
	    CYCLE mass_assignment_mp_loop
	  
	  END IF
        
	  r_mass_tmp = 0
	  i_mpInTri = p_MPtable(i_cnt)%MP%i_MPinTri
        
	  DO i_node_cnt=1, GRID_elementnodes
	    r_en_coor(:,i_node_cnt) = r_upstr(:,i_enodes(i_node_cnt, i_mpInTri))
	  END DO  
        
	  r_mass_tmp = assign_mass(r_en_coor, p_MPtable(i_cnt)%MP%r_coord, p_MPtable(i_cnt)%MP%r_mass)
        
	  r_nmass(i_enodes(1,i_mpInTri)) = r_nmass(i_enodes(1,i_mpInTri)) + r_mass_tmp(1)
	  r_nmass(i_enodes(2,i_mpInTri)) = r_nmass(i_enodes(2,i_mpInTri)) + r_mass_tmp(2)
	  r_nmass(i_enodes(3,i_mpInTri)) = r_nmass(i_enodes(3,i_mpInTri)) + r_mass_tmp(3)
        
	  p_ttable(i_timplus,i_mpInTri)%cp%r_mass    =  p_ttable(i_timplus,i_mpInTri)%cp%r_mass + r_mass_tmp
	  p_ttable(i_timplus,i_mpInTri)%cp%l_massassigned = .TRUE.
	  p_ttable(i_timplus,i_mpInTri)%cp%l_searchtri   = .TRUE.
            
	END DO mass_assignment_mp_loop
      
!----------

     
!---------- calculate area of triangles of the new mesh to assign to the mass packets 
   
	DO i_cnt= 1, i_enum_f
	  IF (p_ttable(i_timplus,i_mpInTri)%cp%l_massassigned) THEN
	    p_ttable(i_timplus, i_cnt)%cp%r_area = &
	    	calc_triangarea(r_coord(:, i_enodes(1, i_cnt)), r_coord(:, i_enodes(2, i_cnt)), &
                            r_coord(:, i_enodes(3, i_cnt)))
          END IF
        END DO 
	
!----------  

  
!---------- write assignment stats

	write(GRID_parameters%ioout, 1000)
	write(GRID_parameters%ioout, 1400) i_enum_f
	write(GRID_parameters%ioout, 1401) i_massassigned
	write(GRID_parameters%ioout, 1402) i_areaoverload
	write(GRID_parameters%ioout, 1403) r_arearatio_max, r_arearatio_min, r_arearatio_mean
	write(GRID_parameters%ioout, 1404) i_totalMP - i_savedMP, i_loopnum
	
!----------


!---------- generate and write mass statistics

	r_mass_before  = 0.
	r_mass_outside = 0.
	r_mass_inside  = 0.
      
	DO i_cnt=1, i_enum_f_old
	  r_mass_before = r_mass_before + SUM(p_ttable(i_tim, i_cnt)%cp%r_mass)      
	END DO
      
	DO i_cnt= 1, i_MPnum
 	  IF (p_MPtable(i_cnt)%MP%i_MPinTri == 0) THEN
	    r_mass_outside = r_mass_outside + p_MPtable(i_cnt)%MP%r_mass
	  END IF
	END DO
            
	DO i_cnt=1,i_enum_f
	  r_mass_inside = r_mass_inside + SUM(p_ttable(i_timplus, i_cnt)%cp%r_mass)
	END DO
      
	write(GRID_parameters%ioout, 1000)
	write(GRID_parameters%ioout, 1001) r_mass_before 
	write(GRID_parameters%ioout, 1002) r_mass_inside
	write(GRID_parameters%ioout, 1003) r_mass_outside
	write(GRID_parameters%ioout, 1004) r_mass_inside+r_mass_outside
      
!----------


!---------- calculate the "influenced" area ( grid_nodearea() ) of each node and, by knowing the summarized mass, the concentration at the upstream node of the mesh

	CALL grid_nodearea(p_mesh(i_timeplus), i_nnum, r_narea)    
	
	r_rside =  r_nmass / r_narea

!----------	


!---------- generate and write concentration stats

	write(GRID_parameters%ioout, 1000)
	write(GRID_parameters%ioout, 1300) MAXVAL(r_rside), MINVAL(r_rside)
	write(GRID_parameters%ioout, 1000)
      
!----------
      
      
!---------- this is it! deallocate all temporal variables

	DEALLOCATE(i_elmtlevel)
	DEALLOCATE(i_maxlvl)
	DEALLOCATE(i_enodes_old)
	DEALLOCATE(r_en_coor_old)
	DEALLOCATE(r_elmtconc)
      
	DEALLOCATE(r_upstr)
	DEALLOCATE(r_elmtarea_up)
	DEALLOCATE(r_totalarea)
	DEALLOCATE(r_arearatio)
	DEALLOCATE(i_mps_in_tri)
	DEALLOCATE(i_gnodes)
	DEALLOCATE(i_enodes)
	DEALLOCATE(i_elmtedges)
	DEALLOCATE(i_eneighbours)
      
      	DO i_cnt=1, i_enum_f_old
	  DEALLOCATE(p_ttable(i_tim, i_cnt)%cp%i_MPindex)
	END DO
      
	DO i_cnt=1, i_MPnum
	  DEALLOCATE(p_MPtable(i_cnt)%MP)
	END DO
	DEALLOCATE(p_MPtable)
      
!---------     
      

      RETURN
      
      1000	FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****')
      1001	FORMAT(1x,'***** Total mass in last step:',11x,e12.4,' *****')
      1002	FORMAT(1x,'***** Total mass in new mesh :',11x,e12.4,' *****')
      1003	FORMAT(1x,'***** Total mass outside new mesh (lost):',e12.4,' *****')
      1004	FORMAT(1x,'***** Total mass in this step:',11x,e12.4,' *****')
      
      1100	FORMAT(1x,'----- Warning, windless field! more than one barycenter in: ',i12,' -----')
      1200	FORMAT(1x,'----- Warning, mass assignment critical in: ',i12,' -----')
      1300	FORMAT(1x,'***** Max conc.:',1x,e12.4,' Min conc.:',1x,e12.4,' *****')
      1400	FORMAT(1x,'***** Total triang in new mesh:',10x,i12,' *****')
      1401	FORMAT(1x,'***** --- with mass:',21x,i12,' *****')
      1402	FORMAT(1x,'***** --- with too much area assigned:',3x,i12,' *****')
      1403	FORMAT(1x,'***** Max vol.:',1x,e10.4,' Min vol.:',1x,e10.4,' Mean vol.:',1x,e10.4,' *****')
      1404	FORMAT(1x,'***** No. of mass points:',1x,i9,' in loops ',i8,' *****')
      
      
      
      END SUBROUTINE slm_ainterpolate
      
!*****************************************************************

      SUBROUTINE slm_interpolinit(p_mesh)

!---------- local declarations

	IMPLICIT NONE

	TYPE (grid_handle), DIMENSION(GRID_timesteps)       	:: p_mesh
	
	
	INTEGER, DIMENSION(:,:), ALLOCATABLE			:: i_enodes
	REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE             		:: r_nval
	REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE             		:: r_elmtarea
	REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)            		:: r_nmass_tmp
        
	TYPE (conservation_entry), POINTER              	:: p_ce1, p_ce2
      
	INTEGER                         			:: i_alct, i_cnt, i_enode_cnt, i_MP_cnt, i_ecnt
	INTEGER                         			:: i_enum_f_old, i_nnum_old, i_tim, i_timplus
      
!---------- preinitialize
      
	i_enum_f_old  	= p_mesh(i_time)%i_enumfine
	i_nnum_old	= p_mesh(i_time)%i_nnumber 
	i_tim         	= p_mesh(i_time)%i_timetag        
	i_timplus     	= p_mesh(i_timeplus)%i_timetag 
      

!---------- allocate work array
!-------------- 'old mesh' arrays

	ALLOCATE(i_enodes(GRID_elementnodes, i_enum_f_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolinit] Allocation error.')
	
	ALLOCATE(r_nval(1,i_nnum_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolinit] Allocation error.')
      
	ALLOCATE(r_elmtarea(i_enum_f_old), stat=i_alct)
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolinit] Allocation error.')
      
!--------------- allocate the table, which saves the mass to be conserved throughout all computation steps
	ALLOCATE(p_ttable(GRID_timesteps, i_enum_f_old), stat=i_alct) 
	IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolinit] Allocation error.')


!--------------- preinitialize
	i_enodes	= 0.
	r_nval		= 0.
	r_elmtarea	= 0.
          
!---------- initialize conservation table with the values to be conserved
	CALL grid_getinfo(p_mesh(i_time), l_finelevel=.TRUE., l_relative=.TRUE., i_elementnodes = i_enodes)
	CALL grid_getinfo(p_mesh(i_time), i_arraypoint= (/ GRID_tracer /), r_nodevalues = r_nval)
	CALL calc_alltriarea(p_mesh(i_time), i_enum_f_old, r_elmtarea)
      
!---------- initialize coefficient table
	CALL generate_MP_coeff(p_mesh)

!---------- initialize p_ttable with values where mass is present
	elmt_loop: DO i_ecnt= 1, i_enum_f_old       
	  ALLOCATE (p_ce1, p_ce2, stat=i_alct)        
	  IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:slm_interpolinit] Allocation error.')
        
	  IF(r_nval(1,i_enodes(1,i_ecnt))/=0. .OR. r_nval(1,i_enodes(2,i_ecnt))/=0. .OR. &
             r_nval(1,i_enodes(3,i_ecnt))/=0.) THEN
          !---- create new triangle entry with mass
	    p_ce1%i_MPperTri		= 3
	    p_ce1%l_massassigned	= .TRUE.
	    p_ce1%l_searchtri		= .TRUE.
	    p_ce1%l_refine		= .FALSE.
	    p_ce1%i_refinelvl		= 1
	    p_ce1%r_area		= r_elmtarea(i_ecnt)
	    DO i_enode_cnt= 1, GRID_elementnodes
	      p_ce1%r_mass(i_enode_cnt) = r_nval(1,i_enodes(i_enode_cnt, i_ecnt)) * (1./3.*p_ce1%r_area)
	    END DO
	    p_ttable(i_tim, i_ecnt)%cp => p_ce1
	    NULLIFY(p_ce1)
	  ELSE
	    p_ce1%i_MPperTri		= 3
	    p_ce1%l_massassigned	= .FALSE.
	    p_ce1%l_searchtri		= .FALSE.
	    p_ce1%l_refine		= .FALSE.
	    p_ce1%i_refinelvl		= 1
	    p_ce1%r_area		= 0.
	    p_ce1%r_mass		= 0. 
	    p_ttable(i_tim, i_ecnt)%cp => p_ce1
	    NULLIFY(p_ce1)
	  END IF
	  
	  p_ce2%i_MPperTri		= 3
	  p_ce2%l_massassigned		= .FALSE.
	  p_ce2%l_searchtri		= .FALSE.
	  p_ce2%l_refine		= .FALSE.
	  p_ce2%i_refinelvl		= 1
	  p_ce2%r_area			= 0.
	  p_ce2%r_mass			= 0.
	  p_ttable(i_timplus, i_ecnt)%cp => p_ce2
	  NULLIFY(p_ce2)                 
	END DO elmt_loop

!--------- deallocation      
      
	DEALLOCATE(r_nval)
	DEALLOCATE(i_enodes)
	DEALLOCATE(r_elmtarea)   
      
	RETURN
	
      END SUBROUTINE slm_interpolinit
      
!*****************************************************************

      SUBROUTINE slm_interpolquit
      
!---------- local declarations

	IMPLICIT NONE
      
	INTEGER				:: i_cnt, i_siz
      
      
	CALL remove_MP_coeff
      
	i_siz = size(p_ttable(1,:))
	DO i_cnt= 1, i_siz
	  DEALLOCATE(p_ttable(1,i_cnt)%cp)
	  DEALLOCATE(p_ttable(2,i_cnt)%cp)
	END DO
      

	DEALLOCATE(p_ttable)

	RETURN
	
      END SUBROUTINE slm_interpolquit
      
!*****************************************************************

      SUBROUTINE searchMP(p_MPtable, i_MPnum, i_MPcnt, i_enum_f, i_gnum, &
      			  i_nnum, i_gnodes, i_enodes, i_elmtedges, i_eneighbours, r_upstr)
      
!---------- local declarations

	IMPLICIT NONE
      
	TYPE (MP_entry_ptr), DIMENSION(i_MPnum)   		:: p_MPtable
	INTEGER							:: i_MPnum
	INTEGER							:: i_MPcnt
      
      
	INTEGER, DIMENSION(GRID_edgenodes, i_gnum)		:: i_gnodes
	INTEGER, DIMENSION(GRID_elementnodes, i_enum_f)		:: i_enodes
	INTEGER, DIMENSION(GRID_elementedges, i_enum_f)		:: i_elmtedges
	INTEGER, DIMENSION(3, i_enum_f)				:: i_eneighbours
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension, i_nnum)			:: r_upstr
      
      
      
	INTEGER							:: i_enum_f, i_gnum, i_nnum, i_enum_f_old, i_enum
	
	INTEGER							:: i_cnt, j_cnt, i_node_cnt, i_gcnt, i_elmt_cnt, i_loopcnt
	INTEGER							:: i_elmtindex
	INTEGER							:: i_gnode_loc, i_elmtnode_loc, i_elmtedge_loc
      
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)				:: r_ref
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension, GRID_edgenodes)		:: r_gn_coor
      
	LOGICAL							:: l_elmtfound, l_sameside, l_circled
      


   
!	i_elmtindex = 1
	i_loopcnt=0
        
	!------ set starting triangle
	IF (p_MPtable(i_MPcnt)%MP%i_MPbyTri < i_enum_f) THEN
          i_elmtindex = p_MPtable(i_MPcnt)%MP%i_MPbyTri            
        ELSE
          i_elmtindex = i_enum_f
        END IF
        !------
	 
	 
                
        !------ in which triangle of the new upstreamed mesh does the mass packet of the old mesh lie?
        search_loop: DO 
          l_elmtfound = .TRUE.
	  i_loopcnt = i_loopcnt +1
          !------ check whether mass packet is in triangle
          edge_loop: DO i_gcnt=1, GRID_elementedges
	    i_elmtedge_loc = i_elmtedges(i_gcnt,i_elmtindex)
        
	    !------ get the upstream coords for the two edge nodes
	    node_loop: DO i_node_cnt=1, GRID_edgenodes
	      i_gnode_loc = i_gnodes(i_node_cnt,i_elmtedge_loc)
              r_gn_coor(:,i_node_cnt) = r_upstr(:,i_gnode_loc)
            END DO node_loop
	    !------
	    
            !------ each edge has two nodes, get the upstrean coordinates of the third trianglenode (called r_ref)
            trianglenode_loop: DO i_node_cnt=1,GRID_elementnodes
	      i_elmtnode_loc = i_enodes(i_node_cnt,i_elmtindex)
              IF (i_gnodes(1,i_elmtedge_loc) /= i_elmtnode_loc) THEN
	        IF (i_gnodes(2,i_elmtedge_loc) /= i_elmtnode_loc) THEN
        	  r_ref = r_upstr(:,i_elmtnode_loc)
        	  EXIT
		END IF
              END IF 
            END DO trianglenode_loop
	    !------ 
	    	    
	    !------ is the barycenter of the mass packet on the same side of the triangle edge as the reference point?
            l_sameside= sameside_2d(r_gn_coor(:,1), r_gn_coor(:,2), r_ref, p_MPtable(i_MPcnt)%MP%r_coord)

            !------ if not, mass packet is not in the currently observed triangle, get triangle next to the current triangle via edge which resulted "packet outside"
            IF (.NOT. l_sameside) THEN
              i_elmtindex = i_eneighbours(i_gcnt, i_elmtindex)
              l_elmtfound = .FALSE.
            END IF
	    !------
	       
          END DO edge_loop
          !------
          
          !------ neighbour triangle does not exist, mass packet outside the mesh, mass lost; check next mass packet
          IF (i_elmtindex==0) THEN
            p_MPtable(i_MPcnt)%MP%i_MPinTri = 0
            p_MPtable(i_MPcnt)%MP%l_searchMP = .FALSE.
            p_MPtable(i_MPcnt)%MP%l_assignmass = .FALSE.
            EXIT
          END IF
          !------
          
          !------ triangle found! remember triangle for assigning the mass packet's mass to the triangle nodes later, check next mass packet
          IF (l_elmtfound) THEN 
            p_MPtable(i_MPcnt)%MP%i_MPinTri = i_elmtindex
            p_MPtable(i_MPcnt)%MP%l_searchMP = .FALSE.
            p_MPtable(i_MPcnt)%MP%l_assignmass = .TRUE. 
            EXIT
          END IF
          !------
                  
          !------ triangle not found: stay in search_loop and search on with next triangle (no EXIT command)
        END DO search_loop

	RETURN
	
      END SUBROUTINE searchMP
      
!*****************************************************************

      FUNCTION assign_mass(r_en_coor, r_mp_coor, r_mp_mass) RESULT(r_nmass_tmp)


!---------- compute partial mass of mass packet which will be assigned to the nodes of the triangle 
!---------- the mass packet landed in.

    
!---------- local declarations

        IMPLICIT NONE   
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension, GRID_elementnodes), INTENT(in)	:: r_en_coor
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)         		:: r_mp_coor
	REAL (KIND = GRID_SR), INTENT(in)                        			:: r_mp_mass
    
	REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)              		:: r_nmass_tmp

	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension, GRID_elementnodes-1)     	:: r_A
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                 		:: r_b
	REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)                		:: r_x, r_y
	REAL (KIND = GRID_SR)                                				:: r_det, r_det1, r_det2
    
	INTEGER                             				:: i_node_cnt
    

!--------- assign coordinates to local variables  
    
	DO i_node_cnt=1, GRID_elementnodes
	  r_x(i_node_cnt) = r_en_coor(1,i_node_cnt)
	  r_y(i_node_cnt) = r_en_coor(2,i_node_cnt)
	END DO

!--------- normalize triangle and mass packet coordinates   
	r_A(1,1) = r_x(2) - r_x(1)
	r_A(1,2) = r_x(3) - r_x(1)
	r_A(2,1) = r_y(2) - r_y(1)
	r_A(2,2) = r_y(3) - r_y(1)
        
	r_b(1) = r_mp_coor(1) - r_x(1)
	r_b(2) = r_mp_coor(2) - r_y(1)

!--------- solve linear equation system (dim2x2)(dim2)=(dim2)        
	r_det  =   r_A(1,1)*r_A(2,2) - r_A(2,1)*r_A(1,2)
	IF (r_det == 0.) CALL grid_error(c_error='SLM: assign_mass(): Determinant zero. Triangle is degenerated.')
		
	r_det1 =   r_A(1,1)*r_b(2) - r_A(2,1)*r_b(1)
	r_det2 =   r_b(1)*r_A(2,2) - r_b(2)*r_A(1,2)
        
	r_nmass_tmp(3) = r_det1 / r_det *r_mp_mass
	r_nmass_tmp(2) = r_det2 / r_det *r_mp_mass
	r_nmass_tmp(1) = r_mp_mass - r_nmass_tmp(3) - r_nmass_tmp(2)
        
	RETURN
	
      END FUNCTION assign_mass

!*****************************************************************************

      FUNCTION sameside_2d(r_vtx_a, r_vtx_b, r_vtx_ref, r_vtx_check) RESULT (l_same)


!---------- Check if a given 2d-point r_vtx_check is on the same side of a line (defined by two points r_vtx_a, r_vtx_b) 
!---------- as a known reference point r_vtx_ref (the third point of the triangle


!---------- local declarations

	IMPLICIT NONE

	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)	:: r_vtx_a, r_vtx_b, r_vtx_ref, r_vtx_check
	LOGICAL 					:: l_same

	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)			:: r_a, r_b, r_tmp1, r_tmp2, r_tmp3
	REAL (KIND = GRID_SR)						:: r_sign, r_dot1, r_dot2
    

!---------- initialize
    
	l_same = .FALSE.

	  
!---------- r_tmp1 is the direction vector which one edge of the triangle has. It cuts the plane in two halfs

	r_tmp1 = r_vtx_b - r_vtx_a
    
!---------- r_tmp2 and r_tmp3 are the vectors to the reference point and to the point to be checked respectively
	r_tmp2 	= 1.e+6*(r_vtx_check - r_vtx_a)
	r_tmp3 	= 1.e+6*(r_vtx_ref   - r_vtx_a)

!---------- if the dot product (projection onto the normal of r_tmp1) has the same sign for both r_tmp2 and r_tmp3
!---------- then the points are on the same side
	r_dot1 	= (r_tmp1(1)*r_tmp2(2) - r_tmp1(2)*r_tmp2(1))
	r_dot2	= (r_tmp1(1)*r_tmp3(2) - r_tmp1(2)*r_tmp3(1))
	
	r_sign	= (r_dot1*r_dot2)

	IF (r_sign >= 0.) l_same = .TRUE.

	RETURN
	
      END FUNCTION sameside_2d
      
!*****************************************************************

      FUNCTION calc_triangarea(r_coord1, r_coord2, r_coord3) RESULT (r_area)
 
!---------- local declarations
 
	IMPLICIT NONE
 
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) 	:: r_coord1, r_coord2, r_coord3

	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)			:: r_a, r_b, r_tmp
	REAL (KIND = GRID_SR)                                        	:: r_area
 

 
!---------- calculate triangle vectors
 
	r_a= r_coord2- r_coord1
	r_b= r_coord3- r_coord1
 
 
!---------- calculate area of a triangle (point c in the origin) 
 
	r_area = 0.5 * ABS( r_b(2)*r_a(1) - r_a(2)*r_b(1) )
 
	RETURN
	
      END FUNCTION calc_triangarea
      
!*****************************************************************

      SUBROUTINE generate_MP_coeff(p_ghand)

!---------- local declarations

	IMPLICIT NONE
    
	TYPE (grid_handle), DIMENSION(GRID_timesteps)	:: p_ghand
    
	INTEGER                     			:: i_reflvl_max     		! maximum refinement level
	INTEGER                     			:: i_celltot        		! total number of mass packets in triangle; 
											! reflvl 1: 3MPs, 2: 6MPs, 3: 10 MPs, 4: 15MPs, etc.
    
	INTEGER                     			:: i_cnt, j_cnt, k_cnt, i_loop	! loop counter
	INTEGER                     			:: i_mpcnt    			! mass packet counter
	INTEGER                     			:: i_cindex     		! coefficient index
	INTEGER                     			:: i_alct       		! allocated flag, error if nonzero
    
	INTEGER                     			:: i_origin     		! local 
	INTEGER, DIMENSION(GRID_dimension)		:: i_prvdir     		! local
    
	REAL (KIND = GRID_SR)                        			:: r_projfac       		! projection factor
	REAL (KIND = GRID_SR)                        			:: r_fac        		! factor
    
	REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)		:: r_mpc_sum        		! checksum; check if the mass coefficients sum up to 1
    
	REAL (KIND = GRID_SR)                        			:: r_vol_sum        		! checksum; check if the volume coefficient sums up to 1
    
	REAL (KIND = GRID_SR)                        			:: r_eps        		! epsilon; very small number greater than 0
    
	TYPE (MP_coeff), POINTER           		:: p_MPC_tmp(:)
	TYPE (MP_coeff_arr), POINTER      		:: p_MPCarray_tmp

    
!---------- initializations
!	i_reflvl_max = p_ghand(i_timeplus)%i_reflvlbnd - p_ghand(i_timeplus)%i_crslvlbnd + 5
!	IF (i_reflvl_max > 27) THEN
	  i_reflvl_max = 7
!	END IF 
    
	MAX_REFLVL = i_reflvl_max
    
	r_eps   = 1.E-5
    
    
!---------- allocate arrays
	ALLOCATE (p_MPCtable(i_reflvl_max), stat = i_alct)
	IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:generate_MP_coeff] Allocation error.')
 

	i_celltot = 1 
	DO i_cnt = 1, i_reflvl_max  
	  i_celltot = i_celltot + (i_cnt+1)
      
	  ALLOCATE (p_MPC_tmp(i_celltot), stat = i_alct)
	  IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:generate_MP_coeff] Allocation error.')
      
	  ALLOCATE (p_MPCarray_tmp, stat = i_alct)
 	  IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:generate_MP_coeff] Allocation error.')

	  p_MPCarray_tmp%mpc => p_MPC_tmp
            
	  p_MPCtable(i_cnt)%i_cnum = i_celltot      
	  p_MPCtable(i_cnt)%p_mpc => p_MPCarray_tmp
      
	END DO
	NULLIFY(p_MPC_tmp)
	NULLIFY(p_MPCarray_tmp)
!----------

    
!---------- set refinement level 1; 3 MPs
	DO i_mpcnt= 1, 3
	  
	  p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%i_origin 	= i_mpcnt
	  p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%i_prvdir   	= i_mpcnt         
	  p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%i_celltype   = 1
	  p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%r_area	= 1./3.
      
	  DO j_cnt=1,3
	    IF (j_cnt == i_mpcnt) THEN
	      p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%r_coeff(j_cnt)   = 1.
	      p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%r_mass(j_cnt)    = 1.
	    ELSE
	      p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%r_coeff(j_cnt)   = 0.
	      p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%r_mass(j_cnt)    = 0.
	    END IF
	  END DO
	       
	  p_MPCtable(1)%p_mpc%mpc(i_mpcnt)%l_end        = .FALSE.
	  
	END DO
!-----------


!----------- generate mass packets of higher refinement levels   
	DO i_cnt = 2, i_reflvl_max
	  i_mpcnt = 0
	  r_projfac = REAL((i_cnt-1),GRID_SR) / REAL(i_cnt,GRID_SR)
	  
	  DO j_cnt=1 , p_MPCtable(i_cnt-1)%i_cnum
            ! project old mass packet in direction of his origin (only if MP is not generator of new packets (l_end==.TRUE.)); set l_end = .FALSE.
	    i_mpcnt = i_mpcnt + 1
	    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt) = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)
            
     
            !projection here (area, coeff, mass changes)

            !area proj.
	    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_area = r_projfac*r_projfac *&
		p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_area

            !projection in direction of the origin
	    i_origin = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_origin
	    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_origin) = &
		(1.-r_projfac) + r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_origin)
	    DO i_loop = 1, 2
	      i_cindex = MODULO((i_origin+i_loop-1),3) + 1
	      p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_cindex) = &
		r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_cindex)
	    END DO

	    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end = .FALSE.
                
	    IF (p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%l_end) THEN
            ! create new mass packets via projecting the old packets which are at the end of the tree
        
	      SELECT CASE(p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype)
		CASE(1) ! corner packets are not to be reproduced
		  CALL grid_error(c_error='[SLM_advanced.mpslm:generate_MP_coeff] This should never happen. L. Mentrup')
		  
		CASE(2) ! edge packets are to be projected versus the origin and additionally one packet to the other edgecorner
		  i_mpcnt = i_mpcnt +1
		  !area
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_area = r_projfac*r_projfac *&
			p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_area

		  !projection in direction of the other edge corner
		  i_prvdir = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_prvdir(1)) = &
			(1.-r_projfac) + r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_prvdir(1))
		  DO i_loop = 1, 2
		    i_cindex = MODULO((i_prvdir(1)+i_loop-1),3)+1
		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_cindex) = &
			r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_cindex)
		  END DO    
          
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_origin     = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_origin
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(1)  = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(1)
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(2)  = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(1)
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype   = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_celltype
	 	  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end        = .TRUE.
          
		CASE(3) ! area packets are to be projected versus the origin and additionally one or two packet to the other areacorners
		  i_mpcnt = i_mpcnt +1

		  !projection in direction of the other area corner
		  i_prvdir = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_prvdir(1)) = &
			(1.-r_projfac) + r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_prvdir(1))
		  DO i_loop = 1, 2
		    i_cindex = MODULO((i_prvdir(1)+i_loop-1),3)+1
		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_cindex) = &
		  	r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_cindex)
		  END DO
          
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_origin     = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_origin
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(1)  = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(1)
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(2)  = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(1)
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype   = p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_celltype
		  p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end        = .TRUE.
          
		  IF (p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(1) /= p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(2)) THEN
		    i_mpcnt = i_mpcnt +1
            
		    !projection in direction of the other area corner
		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_prvdir(2)) = &
			(1.-r_projfac) + r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_prvdir(2))
		    DO i_loop = 1, 2
		      i_cindex = MODULO((i_prvdir(2)+i_loop-1),3)+1
		      p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(i_cindex) = &
			r_projfac* p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%r_coeff(i_cindex)
		    END DO

		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_origin	= p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_origin
		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(1)	= p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(2)
		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(2)	= p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_prvdir(1)
		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype	= p_MPCtable(i_cnt-1)%p_mpc%mpc(j_cnt)%i_celltype
		    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end		= .TRUE.
		  END IF
		  
	      END SELECT
            END IF
	  END DO  
    
        
	  !---------- generate new MPs of refinement level 2; 3 additional MPs
	  IF (i_cnt == 2) THEN
            !new packet nr. 1
            i_mpcnt = i_mpcnt +1
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(1)   = 0.5 
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(2)   = 0.5
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(3)   = 0.
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_origin = 1
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(1)  = 2
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(2)  = 2
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype   = 2
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end    = .TRUE.

           !new packet nr. 2
            i_mpcnt = i_mpcnt +1
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(1)   = 0.5 
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(2)   = 0.
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(3)   = 0.5
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_origin = 1
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(1)  = 3
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(2)  = 3 
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype   = 2
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end    = .TRUE.

           !new packet nr. 3
            i_mpcnt = i_mpcnt +1
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(1)   = 0. 
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(2)   = 0.5
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(3)   = 0.5
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_origin = 2
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(1)  = 3
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(2)  = 3
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype   = 2
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end    = .TRUE.
	    
          END IF
          !-----------

          !---------- generate new MPs of refinement level 3; 1 additional MPs
          IF (i_cnt == 3) THEN
            !new packet nr. 1
            i_mpcnt = i_mpcnt +1
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(1)   = 1./3.
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(2)   = 1./3.
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff(3)   = 1./3.
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_origin = 3
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(1)  = 2
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_prvdir(2)  = 1
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype   = 3
            p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%l_end    = .TRUE.

          END IF
          !-----------


          !assign area coefficients
          r_fac = REAL(i_cnt,GRID_SR)*REAL(i_cnt,GRID_SR)
          DO i_mpcnt=1, p_MPCtable(i_cnt)%i_cnum
            SELECT CASE(p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%i_celltype)
            CASE(1)
              p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_area = 1./3. / r_fac
            CASE(2)
              p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_area = 1. / r_fac
            CASE(3)
              p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_area = 2. / r_fac
            END SELECT    
          END DO
        
	  ! assign mass coefficients; 
	  r_mpc_sum = 0
	  DO i_mpcnt=1, p_MPCtable(i_cnt)%i_cnum
	    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_mass = &
		p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_coeff * &
		p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_area
	    r_mpc_sum = r_mpc_sum + p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_mass
	  END DO
        
	  DO i_mpcnt=1, p_MPCtable(i_cnt)%i_cnum
	    p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_mass = p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_mass / r_mpc_sum     
	  END DO
        
!----------- check if mass/volume coefficients sum up to 1 
	  r_mpc_sum = 0
	  r_vol_sum = 0
	  DO i_mpcnt=1, p_MPCtable(i_cnt)%i_cnum
	    r_mpc_sum = r_mpc_sum + p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_mass
	    r_vol_sum = r_vol_sum + p_MPCtable(i_cnt)%p_mpc%mpc(i_mpcnt)%r_area
	  END DO
             
	  IF ((r_mpc_sum(1) < (1.-r_eps)) .OR. (r_mpc_sum(1) > (1.+r_eps))) THEN
	    CALL grid_error(c_error='[SLM_advanced.mpslm:generate_MP_coeff] Mass packet coefficient r_mass(1) doesnt sum up to 1.')
	  END IF
	  IF ((r_mpc_sum(2) < (1.-r_eps)) .OR. (r_mpc_sum(2) > (1.+r_eps))) THEN
 	    CALL grid_error(c_error='[SLM_advanced.mpslm:generate_MP_coeff] Mass packet coefficient r_mass(2) doesnt sum up to 1.')
	  END IF
	  IF ((r_mpc_sum(3) < (1.-r_eps)) .OR. (r_mpc_sum(3) > (1.+r_eps))) THEN
	    CALL grid_error(c_error='[SLM_advanced.mpslm:generate_MP_coeff] Mass packet coefficient r_mass(3) doesnt sum up to 1.')
	  END IF
	  IF ((r_vol_sum < (1.-r_eps)) .OR. (r_vol_sum > (1.+r_eps))) THEN
	    CALL grid_error(c_error='[SLM_advanced.mpslm:generate_MP_coeff] Mass packet coefficient r_area doesnt sum up to 1.')
	  END IF
!-----------   
      
	END DO
      
	CALL io_MP_coeff(p_MPCtable)
   
	RETURN
    
      END SUBROUTINE generate_MP_coeff

!*****************************************************************

      SUBROUTINE createMPs(p_MPtable,p_MPtable_tmp,i_MPnum, i_tim, r_coor, i_nnum_old, i_enodes_old, i_enum_f_old)
      
!---------- local declarations

	IMPLICIT NONE
      
	TYPE (MP_entry_ptr), DIMENSION(i_MPnum), INTENT(in)			:: p_MPtable
	TYPE (MP_entry_ptr), DIMENSION(i_MPnum), INTENT(inout)			:: p_MPtable_tmp
	INTEGER, INTENT(inout)                    				:: i_MPnum      	! Number of mass packets
	INTEGER, INTENT(in)                       				:: i_tim
      
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension, i_nnum_old), INTENT(in)			:: r_coor       	! coordinates array of triangle nodes (old mesh)
	INTEGER, INTENT(in)                       				:: i_nnum_old   	! number of nodes in mesh (old mesh)
      
	INTEGER, DIMENSION(GRID_elementnodes, i_enum_f_old), INTENT(in) 	:: i_enodes_old     	! index array of triangles nodes (old mesh)
	INTEGER, INTENT(in)                       				:: i_enum_f_old     	! Number of triangles (finite elements e) in this step
	INTEGER                           					:: i_treflvl        	! refinement level of a triangle

	REAL (KIND = GRID_SR), DIMENSION(GRID_elementnodes)              			:: r_nmass_tmp      	! mass at the nodes of the triangle
	REAL (KIND = GRID_SR)                              					:: r_mass_tot, r_mass_in_tri
      
	INTEGER                           					:: i_MPnum_new
	INTEGER                           					:: i_MPidx        
	INTEGER                           					:: i_cnt, i_triangle_cnt, i_MP_cnt, i_node_cnt, i_dim  ! loop counter
	INTEGER                               					:: i_alct
      
      
	TYPE (MP_entry), POINTER                  				:: p_MP



!----------- preinitialize

	i_MPnum_new = 0


!----------- allocate memory for new mass packets       
	DO i_MP_cnt=1, i_MPnum
          ALLOCATE(p_MP, stat=i_alct)
          IF (i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:create_MPs] Allocation error.')
          p_MP%i_MPbyTri 	= 0
          p_MP%i_MPinTri 	= 0
          p_MP%r_mass 		= 0.
          p_MP%r_area 		= 0.
          p_MP%r_coord 		= 0.
          p_MP%l_searchMP 	= .FALSE.
          p_MP%l_assignmass 	= .FALSE.
          p_MPtable_tmp(i_MP_cnt)%MP => p_MP
	  NULLIFY(p_MP)
	END DO

        
!----------- if already searched for mass packets, save mass packets which are already found and that can be assigned to the nodes later
	DO i_triangle_cnt=1, i_enum_f_old
          IF ((.NOT. p_ttable(i_tim, i_triangle_cnt)%cp%l_searchtri) &
	  	.AND. (p_ttable(i_tim, i_triangle_cnt)%cp%i_MPindex(1) /= 0)) THEN
            DO i_MP_cnt = 1, p_ttable(i_tim, i_triangle_cnt)%cp%i_MPperTri
              i_MPidx = p_ttable(i_tim, i_triangle_cnt)%cp%i_MPindex(i_MP_cnt)
              i_MPnum_new = i_MPnum_new + 1

              p_MPtable_tmp(i_MPnum_new)%MP%i_MPinTri 		= p_MPtable(i_MPidx)%MP%i_MPinTri
              p_MPtable_tmp(i_MPnum_new)%MP%i_MPbyTri 		= p_MPtable(i_MPidx)%MP%i_MPbyTri
              p_MPtable_tmp(i_MPnum_new)%MP%r_mass  		= p_MPtable(i_MPidx)%MP%r_mass
              p_MPtable_tmp(i_MPnum_new)%MP%r_coord 		= p_MPtable(i_MPidx)%MP%r_coord
              p_MPtable_tmp(i_MPnum_new)%MP%r_area		= p_MPtable(i_MPidx)%MP%r_area
              p_MPtable_tmp(i_MPnum_new)%MP%l_searchMP 		= p_MPtable(i_MPidx)%MP%l_searchMP
              p_MPtable_tmp(i_MPnum_new)%MP%l_assignmass	= p_MPtable(i_MPidx)%MP%l_assignmass

              p_ttable(i_tim, i_triangle_cnt)%cp%i_MPindex(i_MP_cnt) = i_MPnum_new
            END DO
          END IF
	END DO
      

!----------- set values for mass packets;               
	DO i_triangle_cnt= 1, i_enum_f_old
          IF (p_ttable(i_tim, i_triangle_cnt)%cp%l_searchtri) THEN
            r_nmass_tmp = p_ttable(i_tim, i_triangle_cnt)%cp%r_mass
            r_mass_in_tri = SUM(r_nmass_tmp)
            i_treflvl = p_ttable(i_tim, i_triangle_cnt)%cp%i_refinelvl

            IF (r_mass_in_tri > 0.) THEN   
              DO i_MP_cnt = 1, p_MPCtable(i_treflvl)%i_cnum 
        	i_MPnum_new = i_MPnum_new + 1
        	p_MPtable_tmp(i_MPnum_new)%MP%i_MPbyTri = i_triangle_cnt 
!------------------ stepfct assignment
        	DO i_node_cnt= 1, GRID_elementnodes
                  p_MPtable_tmp(i_MPnum_new)%MP%r_coord = p_MPtable_tmp(i_MPnum_new)%MP%r_coord + &
                           (p_MPCtable(i_treflvl)%p_mpc%mpc(i_MP_cnt)%r_coeff(i_node_cnt)&
                      	   *r_coor(:,i_enodes_old(i_node_cnt, i_triangle_cnt)))
        	END DO
        	p_MPtable_tmp(i_MPnum_new)%MP%r_mass = &
                  SUM(p_MPCtable(i_treflvl)%p_mpc%mpc(i_MP_cnt)%r_mass * r_nmass_tmp)
!------------------
        	p_MPtable_tmp(i_MPnum_new)%MP%r_area = &
                      p_MPCtable(i_treflvl)%p_mpc%mpc(i_MP_cnt)%r_area * &
                      p_ttable(i_tim, i_triangle_cnt)%cp%r_area
        	p_MPtable_tmp(i_MPnum_new)%MP%l_searchMP = .TRUE.
        	p_MPtable_tmp(i_MPnum_new)%MP%l_assignmass = .FALSE.

        	p_ttable(i_tim, i_triangle_cnt)%cp%i_MPindex(i_MP_cnt) = i_MPnum_new
              END DO
            END IF            
          END IF
	END DO 
      
	i_MPnum = i_MPnum_new    

	RETURN
      
      END SUBROUTINE createMPs
    
!*****************************************************************

      SUBROUTINE remove_MP_coeff

!---------- local declarations
	IMPLICIT NONE
    
	TYPE(grid_handle), DIMENSION(GRID_timesteps)    :: p_ghand   
	INTEGER                     			:: i_reflvl_max     ! maximum refinement level
	INTEGER                     			:: i_cnt
    
!---------- initializations
	i_reflvl_max = size(p_MPCtable)
    
!----------
	DO i_cnt = 1, i_reflvl_max
      	  DEALLOCATE(p_MPCtable(i_cnt)%p_mpc%mpc)
      	  DEALLOCATE(p_MPCtable(i_cnt)%p_mpc)
	END DO 
    
	DEALLOCATE(p_MPCtable)
    
	RETURN
    
      END SUBROUTINE remove_MP_coeff
      
!*****************************************************************

      SUBROUTINE io_MP_coeff(p_MPCtable)

!---------- local declarations
	IMPLICIT NONE
    
	TYPE (MP_coeff_arr_ptr), DIMENSION(MAX_REFLVL)  :: p_MPCtable
    
	INTEGER                     		:: i_reflvl_max     ! maximum refinement level
    
	INTEGER                              	:: i_io1, i_io2, i_fst
	INTEGER                              	:: i_cnt, j_cnt, i_tcnt, i_mpcnt, i_filecnt
	INTEGER					:: i_alct, i_nnum, i_tnum, i_dim
      	INTEGER                  		:: i_timecounter
      	REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    	:: r_cox
      	CHARACTER (len=11)                   	:: c_mfile
	CHARACTER (len=32)			:: c_matfile
      	CHARACTER (len=8)                    	:: c_tmp
      
      	REAL (KIND = GRID_SR), DIMENSION(3)            		:: r_a = (/0.,0.,0./)
      	REAL (KIND = GRID_SR), DIMENSION(3)            		:: r_b = (/1.,0.,0./)
      	REAL (KIND = GRID_SR), DIMENSION(3)            		:: r_c = (/0.,1.,0./)

    
!---------- initializations
    	i_reflvl_max = MAX_REFLVL
    	i_timecounter = 0


    	DO i_filecnt=1, i_reflvl_max
!---------- file handling (open)
     
      	  i_tcnt= i_timecounter
      	  i_timecounter= i_timecounter+1
      	  write(c_mfile,10101) 'MP_gmv.', i_tcnt
	  write(c_matfile,10102) 'MP_matlab.', i_tcnt
      	  c_mfile= adjustl(c_mfile)
	  c_matfile= adjustl(c_matfile)
      	  i_io1= 15
      	  OPEN(i_io1, file= c_mfile, form= 'formatted', iostat= i_fst)
      	  IF(i_fst /= 0) THEN
            RETURN
      	  END IF
	  
	  i_io2 = 16
	  OPEN(i_io2, file= c_matfile, form= 'formatted', iostat= i_fst)
	  IF (i_fst/=0) THEN
	    RETURN
	  END IF

!---------- write header

      	  WRITE(i_io1,1000)
      	  WRITE(i_io1,1001)
      	  WRITE(i_io1,1002) c_mfile
      	  WRITE(i_io1,1003) GRID_parameters%program_name, GRID_parameters%version, GRID_parameters%subversion, &
            GRID_parameters%patchversion
      	  WRITE(i_io1,1004) GRID_parameters%author_name, GRID_parameters%author_email
      	  WRITE(i_io1,1005)

!---------- the nodes

      	  i_nnum = p_MPCtable(i_filecnt)%i_cnum
      	  WRITE(i_io1,1010) i_nnum+3

!---------- extract nodal grid data

      	  ALLOCATE(r_cox(3, i_nnum+3), stat=i_alct)
      	  IF(i_alct /= 0) CALL grid_error(c_error='[SLM_simple.mpslm:io_MP_coeff] Allocation error.')
      
      	  r_cox(:,1) = r_a
      	  r_cox(:,2) = r_b
      	  r_cox(:,3) = r_c

      	  DO i_mpcnt = 1, i_nnum
            r_cox(:,i_mpcnt+3) = p_MPCtable(i_filecnt)%p_mpc%mpc(i_mpcnt)%r_coeff(1)*r_a + &
                      		 p_MPCtable(i_filecnt)%p_mpc%mpc(i_mpcnt)%r_coeff(2)*r_b + &
                     		 p_MPCtable(i_filecnt)%p_mpc%mpc(i_mpcnt)%r_coeff(3)*r_c 
      	  END DO
        
      	  DO i_dim=1,GRID_dimension
            WRITE(i_io1,1011) (r_cox(i_dim,i_cnt), i_cnt=1,i_nnum+3)
      	  END DO
	  
	  DO i_cnt = 1, i_nnum+3
	    WRITE(i_io2,1012) r_cox(1, i_cnt), r_cox(2, i_cnt)
	  END DO
	  
      	  DEALLOCATE(r_cox)

!---------- the elements

      	  i_tnum = 1
      	  WRITE(i_io1,1020) i_tnum
      	  WRITE(i_io1,1021)
      	  WRITE(i_io1, *) '1 2 3'
      

!---------- close file

      	  WRITE(i_io1,1100)
      	  CLOSE(i_io1)
	  
	  CLOSE(i_io2)
      
	END DO  

	RETURN
	
	1000	FORMAT('gmvinput ascii')
	1001	FORMAT('comments')
	1002    FORMAT(' File: ',a48)
	1003    FORMAT(' Written by: ',a15,' Version ',i1,'.',i1,'.',i1)
	1004    FORMAT(' Author: ',a48,' <',a48,'>')
	1005    FORMAT('endcomm')
	1010    FORMAT('nodes ',i8)
	1011    FORMAT(10f15.5)
	1012	FORMAT(3f15.5)
	1020    FORMAT('cells ',i8)
	1021    FORMAT('tri 3')
	1022    FORMAT(10i8)
	1031    FORMAT('cellids')
	1040    FORMAT('variable')
	1041    FORMAT('level 0')
	1043    FORMAT('status 0')
	1042    FORMAT('tracr 1')
	1049    FORMAT('endvars')
	1100    FORMAT('endgmv')
	10101	FORMAT(a7,i4.4)
    	10102	FORMAT(a10,i4.4)
	
      END SUBROUTINE io_MP_coeff
      
!*****************************************************************

      SUBROUTINE calc_alltriarea(p_handle, i_len, r_elmtarea)

!---------- local declarations

	IMPLICIT NONE

	TYPE (grid_handle)                                      :: p_handle
	INTEGER, INTENT(in)                                     :: i_len
	REAL (KIND = GRID_SR), DIMENSION(:)                                      :: r_elmtarea
	
	REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE                       :: r_xy
	INTEGER, DIMENSION(:,:), ALLOCATABLE                    :: i_enods
	INTEGER :: i_enum, i_alct, i_nnum, i_cnt
	REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                         :: r_1, r_2, r_3
	
!---------- check array sizes

	i_enum= p_handle%i_enumfine
	i_nnum= p_handle%i_nnumber
	IF(i_len /= i_enum) CALL grid_error(c_error='[calc_alltriarea]: array sizes do not match!')
	
!---------- get coordinates for each element

	allocate(r_xy(GRID_dimension,i_nnum), i_enods(GRID_elementnodes,i_enum), stat=i_alct)
	IF(i_alct /= 0) THEN
	  CALL grid_error(c_error='[calc_alltriarea]: could not allocate aux. arrays r_xy...')
	END IF
	CALL grid_getinfo(p_handle, r_nodecoordinates= r_xy, i_elementnodes=i_enods)
	
!---------- now calculate element's areas

	DO i_cnt=1,i_enum
	  r_1(:)= r_xy(:,i_enods(1,i_cnt))
	  r_2(:)= r_xy(:,i_enods(2,i_cnt))
	  r_3(:)= r_xy(:,i_enods(3,i_cnt))
	  r_elmtarea(i_cnt)= calc_triangarea(r_1, r_2, r_3)
	END DO

	DEALLOCATE(r_xy, i_enods)
	RETURN
	
      END SUBROUTINE calc_alltriarea

!*****************************************************************

	END MODULE SLM_advanced
