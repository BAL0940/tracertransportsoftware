!*****************************************************************
!
! MODULE NAME:
!	SLM_advanced
! FUNCTION:
!	provide advanced semi-Lagrangian routines
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_step
! FUNCTION:
!	one step of the basic SLM algorithm
! SYNTAX:
!	CALL slm_step(int, real.arr, real.arr)
! ON INPUT:
!	...
! ON OUTPUT:
!	r_tracer: array with tracer values	real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_displace
! FUNCTION:
!	extrapolate the alpha, values for the displacements of the upstream
!	points from the gridpoints
! SYNTAX:
!	CALL slm_displace(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_coord: real array of xy-coordinates		real
! ON OUTPUT:
!	r_alpha: displacement vectors to each point	real
! CALLS:
!	wind_field
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_update
! FUNCTION:
!	calculate the update to the velocity
! SYNTAX:
!	CALL slm_update(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_rside: array with right hand side values	real
! ON OUTPUT:
!	r_udate: array with new (updated) gid values	real
! CALLS:
!
! COMMENTS:
!	this routine is trivial for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_upstream
! FUNCTION:
!	calculate right hand side of the equation (upstream values)
! SYNTAX:
!	CALL slm_upstream(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_alpha: displacement vectors to each point	real
! ON OUTPUT:
!	r_rside: array with right hand side values	real
! CALLS:
!
! COMMENTS:
!	this routine is just interpolation for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_interpolate
! FUNCTION:
!	do the interpolation
! SYNTAX:
!	CALL slm_interpolate(grid, int, real, real.arr, real.arr, real.arr)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
!	r_fac:   factor at which point to interpolate	REAL
!	i_arlen: array length for the following arrays	INTEGER
!	r_coord: coordinate array (new grid)		REAL
!	r_alpha: displacement array (corr. to r_coord)	REAL
!	r_value: values on the old grid (array)		REAL
! ON OUTPUT:
!	r_rside: right hand side (interpolated)		REAL
! CALLS:
!
! COMMENTS:
!	this one is plain bi-cubic spline interpolation
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_interpolinit
! FUNCTION:
!	initialize the interpolation (conservative interpolation)
! SYNTAX:
!	CALL slm_interpolate(grid)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!	we set a global value present in this module:
!	r_conservation: conservation value		REAL
!
!-----------------------------------------------------------------
!
! NAME:
!	triang_ar
! FUNCTION:
!	calculate triangle area given by three coordinates
! SYNTAX:
!
! ON INPUT:
!
! ON OUTPUT:
!
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_displace, slm_update, slm_upstream
! COMMENTS:
!
! USES:
!	FLASH_parameters, GRID_api, SLM_interpolation, ADV_wind, ADV_rhs
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	4/2002
!	2. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_advanced
	  USE FLASH_parameters
	  USE MISC_timing
	  USE GRID_api
	  USE ADV_wind
	  USE ADV_rhs
	  PRIVATE
	  PUBLIC  :: slm_astep
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_astep(p_ghand, p_param, p_time, r_modtime, i_size, &
	                       r_coord, r_tracer, i_newsdepth)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
	  TYPE (control_struct), INTENT(in)                         :: p_param
	  TYPE (sw_info), INTENT(inout)                             :: p_time
	  REAL (KIND = GRID_SR), INTENT(in)                                          :: r_modtime
	  INTEGER, INTENT(in)                                       :: i_size
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size), INTENT(in)        :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_size), INTENT(out)                      :: r_tracer
	  INTEGER, OPTIONAL, INTENT(in)                             :: i_newsdepth
	  
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE      :: r_newvl
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    :: r_alpha
	  INTEGER                              :: i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), POINTER        :: r_dualcoordinates
	  INTEGER, DIMENSION(:,:,:), POINTER   :: i_dualedges
	  INTEGER                              :: i_dual
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE    :: r_dualalpha
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE  :: r_newdualalpha
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), POINTER      :: r_newdualcoor

!---------- check size!

	  IF(i_size <= 0) THEN
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'INFO [slm_astep]: Zero step size, returning to calling routine'
	    RETURN
	  END IF

!---------- allocate auxiliary arrays

	  nullify(r_newdualcoor)
	  allocate(r_newvl(i_size), r_alpha(GRID_dimension,i_size), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(38)
	  END IF not_alloc

!---------- create dual mesh

	  IF(present(i_newsdepth)) THEN
	    CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
	                         r_dualcoordinates, i_newsdepth= i_newsdepth, i_newlength=i_size)
	    IF(i_alct /= i_size) CALL grid_error(c_error='[slm_step]: incompatible new lengths...')
	  ELSE
	    CALL grid_createdual(p_ghand(i_timeplus), i_dual, i_dualedges, &
	                         r_dualcoordinates)
	  END IF

!---------- calculate dual triangle positions

	  CALL create_newdualpoints(i_size, r_coord, &
	                            i_dual, r_dualcoordinates, i_dualedges, &
	                            r_newdualcoor)

!---------- allocate arrays for dual displacements

	  allocate(r_newdualalpha(GRID_dimension,GRID_patchelements,i_size), &
	           r_dualalpha(GRID_dimension,i_dual), stat=i_alct)
	  not_allocdual: IF(i_alct /= 0) THEN
	    CALL grid_error(40)
	  END IF not_allocdual

!-SLM--------- calculate trajectory pieces (displacements)

	  CALL stop_watch('start',3,p_time)
	  CALL slm_adisplace(p_param, i_size, r_coord, r_alpha, &
	                     i_dual, r_dualcoordinates, r_dualalpha, &
	                     r_newdualcoor, r_newdualalpha, r_time=r_modtime)
	  CALL stop_watch('stop ',3,p_time)

!-SLM--------- calculate right hand side

	  CALL stop_watch('start',4,p_time)
	  CALL slm_aupstream(p_ghand, i_size, r_coord, r_alpha, &
	  		    i_dual, r_dualcoordinates, r_dualalpha, &
	  		    i_dualedges, r_newdualcoor, r_newdualalpha, &
	  		    r_newvl)
	  CALL stop_watch('stop ',4,p_time)

!-SLM--------- calculate new grid values

	  CALL stop_watch('start',5,p_time)
	  CALL slm_aupdate(p_param, i_size, r_coord, r_newvl, r_tracer, r_time=r_modtime)
	  CALL stop_watch('stop ',5,p_time)

!---------- destroy dual mesh

	  CALL grid_destroydual(i_dual, i_dualedges, r_dualcoordinates)

!-SLM--------- put alpha values to u and v field entries

	  r_alpha= -r_alpha
	  IF(present(i_newsdepth)) THEN
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_newsdepth=i_newsdepth, i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  ELSE
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  END IF

!-SLM--------- deallocate work arrays

	  deallocate(r_dualalpha, r_alpha, r_newvl, r_newdualcoor, r_newdualalpha)
	  nullify(r_newdualcoor)

	  RETURN
	  END SUBROUTINE slm_astep

!*****************************************************************
	  SUBROUTINE slm_adisplace(p_param, i_arlen, r_coord, r_alpha, &
	                          i_darlen, r_dcoord, r_dalpha, &
				  r_ndcoord, r_ndalpha, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                      :: p_param
	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_alpha
	  INTEGER, INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in)  :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(out) :: r_dalpha
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), POINTER                      :: r_ndcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,GRID_patchelements,i_arlen), INTENT(out) :: r_ndalpha
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                           :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_fac, r_caf, &
	    r_axy, r_xyc
	  REAL (KIND = GRID_SR)                                                 :: r_dt0, r_dt1, &
	    r_dt2, r_tim
	  INTEGER                                              :: i_cnt1, i_cnt2, i_cnt3

!---------- set constants

	  r_dt0= p_param%num%r_deltatime
	  r_dt1= 0.5_GRID_SR* p_param%num%r_deltatime
	  r_dt2= 1.5_GRID_SR* p_param%num%r_deltatime
	  r_fac= 0.5_GRID_SR
	  r_caf= 2.0_GRID_SR
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- calculate in an iteration process the displacements

	  unknown_loop: DO i_cnt1=1,i_arlen
	    r_axy= 0.0_GRID_SR

	    iter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_coord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO iter_loop

	    r_alpha(:,i_cnt1)= r_axy

!---------- the same for the dual triangle positions

	    ndual_loop: DO i_cnt3=1,GRID_patchelements
	      r_axy= 0.0_GRID_SR

	      nditer_loop: DO i_cnt2=1, p_param%num%i_adviterations
	        r_xyc= r_ndcoord(:,i_cnt3,i_cnt1)- r_fac* r_axy
	        r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	      END DO nditer_loop

	      r_ndalpha(:,i_cnt3,i_cnt1)= r_axy
	    END DO ndual_loop
	  END DO unknown_loop

!---------- the same for the dual nodes

	  dual_loop: DO i_cnt1=1,i_darlen
	    r_axy= 0.0_GRID_SR

	    diter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_dcoord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO diter_loop

	    r_dalpha(:,i_cnt1)= r_axy
	  END DO dual_loop

	  RETURN
	  END SUBROUTINE slm_adisplace

!*****************************************************************
	  SUBROUTINE slm_aupdate(p_param, i_arlen, r_coord, r_rside, r_udate, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                   :: p_param
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_udate
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                          :: r_time
	  INTEGER                                             :: i_cnt
	  REAL (KIND = GRID_SR)                                                :: r_dt, r_tim

!---------- in the linear advection case and with f90 this is just

!	  r_udate= r_rside

!---------- including a non-zero right hand side, we have

	  r_dt= p_param%num%r_deltatime
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

	  main_loop: DO i_cnt=1, i_arlen
	    r_udate(i_cnt)= r_rside(i_cnt)+ r_dt* slm_righthand(r_coord(:,i_cnt))
	  END DO main_loop

	  RETURN
	  END SUBROUTINE slm_aupdate

!*****************************************************************
	  SUBROUTINE slm_aupstream(p_mesh, i_arlen, r_coord, r_alpha, &
	                             i_darlen, r_dcoord, r_dalpha, &
	                             i_dedge, r_ndcoor, r_ndalpha, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)        :: p_mesh
	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_alpha
	  INTEGER, INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dcoord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dalpha
	  INTEGER, DIMENSION(2,GRID_patchelements,i_arlen), INTENT(in) :: i_dedge
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,GRID_patchelements,i_arlen), INTENT(in) :: r_ndcoor
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,GRID_patchelements,i_arlen), INTENT(in) :: r_ndalpha
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)                :: r_rside
	  INTEGER                                              :: i_cnt, i_tnc, i_out, i_stat
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_1
	  REAL (KIND = GRID_SR)                                                 :: r_oarea, r_narea
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,GRID_elementnodes)    :: r_vertx
	  REAL (KIND = GRID_SR)                                                 :: r_val, r_tmp

	  i_stat= 0

!---------- main loop over the nodes of the new grid

	  node_loop: DO i_cnt=1,i_arlen

	    r_val= 0.0_GRID_SR
	    r_narea= 0.0_GRID_SR
	    r_vertx= 0.0_GRID_SR

!---------- the upstream dual element's center node

	    r_vertx(1,1)= r_coord(1,i_cnt)- r_alpha(1,i_cnt)
	    r_vertx(2,1)= r_coord(2,i_cnt)- r_alpha(2,i_cnt)

!---------- now calculate the interference of the voronoi thing with elements from old mesh

	    ptch_loop: DO i_tnc= 1, GRID_patchelements
	    IF(i_dedge(1,i_tnc,i_cnt) == 0) THEN
	      EXIT ptch_loop
	    ELSE

!---------- calculate area

	      r_narea= r_narea+ triang_ar(r_coord(:,i_cnt), &
	                                    r_dcoord(:,i_dedge(1,i_tnc,i_cnt)), &
	                                    r_dcoord(:,i_dedge(2,i_tnc,i_cnt)))

!---------- the upstream dual element's outer nodes

	      r_vertx(1,2)= r_dcoord(1,i_dedge(1,i_tnc,i_cnt))- r_dalpha(1,i_dedge(1,i_tnc,i_cnt))
	      r_vertx(2,2)= r_dcoord(2,i_dedge(1,i_tnc,i_cnt))- r_dalpha(2,i_dedge(1,i_tnc,i_cnt))
	      r_vertx(1,3)= r_dcoord(1,i_dedge(2,i_tnc,i_cnt))- r_dalpha(1,i_dedge(2,i_tnc,i_cnt))
	      r_vertx(2,3)= r_dcoord(2,i_dedge(2,i_tnc,i_cnt))- r_dalpha(2,i_dedge(2,i_tnc,i_cnt))

!---------- calculate area of upstream dual triangle

	      r_oarea= triang_ar(r_vertx(:,1),r_vertx(:,2),r_vertx(:,3))

!---------- check if upstream value is outside of the domain

	      r_1= r_ndcoor(:,i_tnc,i_cnt)- r_ndalpha(:,i_tnc,i_cnt)
	      i_out= grid_domaincheck(p_mesh(i_time), r_1)

!---------- take the intersection of the trajectory with the boundary as new upstream point

	      out_domain: IF(i_out /= 0) then
	        r_1= grid_boundintersect(p_mesh(i_time), &
	             r_ndcoor(:,i_tnc,i_cnt), r_1, i_info=i_stat)
	      END IF out_domain

!---------- interpolate

              no_intersect: IF(i_stat /= 0) THEN
                r_tmp= 0.0_GRID_SR
	      ELSE no_intersect
	        r_tmp= grid_coordvalue(p_mesh(i_time), r_1, i_interpolorder=GRID_highorder)
	        IF(abs(r_tmp) < GRID_EPS) THEN
		  r_tmp= 0.0_GRID_SR
		END IF
              END IF no_intersect

!---------- interpolate upstream concentration and calculate integral with quadrature rule

	      r_val= r_val+ r_oarea* r_tmp

	    END IF
	    END DO ptch_loop

!---------- now calculate concentration

	    r_rside(i_cnt)= r_val/r_narea

	  END DO node_loop

	  RETURN
	  END SUBROUTINE slm_aupstream
!*****************************************************************
	  FUNCTION triang_ar(r_coord1, r_coord2, r_coord3) RESULT (r_area)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord1, r_coord2, r_coord3
	  REAL (KIND = GRID_SR)                                        :: r_area
	  REAL (KIND = GRID_SR)                                        :: r_c
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_a, r_b

!---------- calculate vector components

	  r_a= r_coord2- r_coord1
	  r_b= r_coord3- r_coord1

!---------- calculate components (a,b,c) of cross product vector

	  r_c= (r_a(1)* r_b(2)- r_a(2)* r_b(1))

!---------- calculate area

	  r_area= abs(r_c)* 0.5_GRID_SR

	  RETURN
	  END FUNCTION triang_ar

!*****************************************************************
	  SUBROUTINE create_newdualpoints(i_arlen, r_coord, &
	                                  i_darlen, r_dcoord, i_dedge, r_ndcoor)

!---------- local declarations

	  IMPLICIT NONE

	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  INTEGER, INTENT(in)                                  :: i_darlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_darlen), INTENT(in) :: r_dcoord
	  INTEGER, DIMENSION(2,GRID_patchelements,i_arlen), INTENT(in) :: i_dedge
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), POINTER                      :: r_ndcoor

	  REAL (KIND = GRID_SR)                                                 :: r_onethird=1./3.
	  INTEGER                                              :: i_alct, i_cnt, i_tnc
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,GRID_elementnodes)    :: r_vertx

!---------- allocate array for dual coordinates

	  allocate(r_ndcoor(GRID_dimension,GRID_patchelements,i_arlen), stat=i_alct)
	  not_allocdual: IF(i_alct /= 0) THEN
	    CALL grid_error(c_error='[create_newdualpoints]: dual points array not allocated!')
	  END IF not_allocdual

!---------- main loop over the nodes of the new grid

	  node_loop: DO i_cnt=1,i_arlen

	    r_vertx= 0.0_GRID_SR

!---------- the upstream dual element's center node

	    r_vertx(:,1)= r_coord(:,i_cnt)

!---------- now calculate the interference of the voronoi thing with elements from old mesh

	    ptch_loop: DO i_tnc= 1, GRID_patchelements
	    IF(i_dedge(1,i_tnc,i_cnt) == 0) THEN
	      EXIT ptch_loop
	    ELSE

!---------- the upstream dual element's outer nodes

	      r_vertx(:,2)= r_dcoord(:,i_dedge(1,i_tnc,i_cnt))
	      r_vertx(:,3)= r_dcoord(:,i_dedge(2,i_tnc,i_cnt))

!---------- calculate dual triangles center point

	      r_ndcoor(:, i_tnc, i_cnt)= r_onethird* &
	                                (r_vertx(:,1)+ r_vertx(:,2)+ r_vertx(:,3))
	    END IF
	    END DO ptch_loop
	  END DO node_loop

	  RETURN
	  END SUBROUTINE create_newdualpoints

	END MODULE SLM_advanced
