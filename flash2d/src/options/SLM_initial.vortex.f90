!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize values for semi-Lagrangian advection
!	provides slm_analyticsolution to evaluate error and mass 
!	conservation (when vortex test case is applied)
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initvortex
! FUNCTION:
!	initialize a grid with values of initial vortex conditions
! see: Doswell 1984, Nair 2006
! SYNTAX:
!	CALL slm_initvortex(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the analytic solution to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		l.mentrup    12/06
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_center = (/ 0._GRID_SR, 0._GRID_SR /)
		REAL (KIND = GRID_SR) 													 :: r_delta  = 0.05_GRID_SR 					! controls the width of the decay from value 2 to value 0
		REAL (KIND = GRID_SR)                            :: r_vnot   = 2.598_GRID_SR					! tangential velocity such that omega < 1. 
		REAL (KIND = GRID_SR)                            :: r_scale  = 86400._GRID_SR 				! scale on one day
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand
	  INTEGER                                    :: i_lev= 6

!---------- initialize vortex
	  CALL slm_initvortex(p_ghand)

	  RETURN
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                               :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_time
	  INTEGER, INTENT(in)                                          :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)       :: r_array

!---------- this is a dummy
!	  r_array= 0.0_GRID_SR
	  
!---------- analyticsolution: vortex
	  CALL slm_analyticsol_vortex(p_ghand, r_time, i_arlen, r_array)

	  RETURN
	  END SUBROUTINE slm_analyticsolution
	  
!*****************************************************************
	  SUBROUTINE slm_analyticsol_vortex(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                               :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_time
	  INTEGER, INTENT(in)                                          :: i_arlen
		
		REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)       :: r_array

	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE           :: r_coo		
	  REAL (KIND = GRID_SR)                                        :: r_x, r_y, r_xc, r_yc
	  REAL (KIND = GRID_SR)                                        :: r_r
		REAL (KIND = GRID_SR)                                        :: r_dlt 								! controls the width of the decay from value 2 to value 0
	  REAL (KIND = GRID_SR)                                        :: r_t
		REAL (KIND = GRID_SR)                                        :: r_tanh
	  REAL (KIND = GRID_SR)                                        :: r_vt, r_omg
		REAL (KIND = GRID_SR)                                        :: r_eps = 1.E-6_GRID_SR
		REAL (KIND = GRID_SR)                                        :: r_twopi 							! scale the domain from [-0.5,0,5]to [-Pi, Pi]

	  INTEGER                                                      :: i_count, i_num, i_alct
	 
!---------- initialize some values
    r_twopi = GRID_PI * 2._GRID_SR
		
		
	  r_xc     = r_center(1) * r_twopi
		r_yc     = r_center(2) * r_twopi
		r_dlt    = r_delta

!---------- here is the crux: what is the distribution at time r_time
  
	  r_t = r_time
		write(*,*) r_t

!---------- allocate workspace
	  
	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_coo(GRID_dimension,i_num), stat=i_alct)
    IF(i_alct /= 0) THEN
		  CALL grid_error(55)
	  END IF	

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!----------	compute analytic solution (ref. to Nair 2006, Appl. Num. Math.)  		
		
    node_loop: DO i_count= 1, i_num
		  r_x = r_coo(1,i_count) * r_twopi
			r_y = r_coo(2,i_count) * r_twopi
			
			r_r = twonorm((/r_x - r_xc, r_y - r_yc/))	
			
			IF (r_r <= r_eps) THEN
			  r_array(i_count) = 1._GRID_SR
			ELSE
			  r_tanh = tanh(r_r)
		    r_vt   = (1._GRID_SR - r_tanh*r_tanh) * r_tanh * r_vnot * GRID_PI / r_scale
		    r_omg  = r_vt / r_r
				r_array(i_count) = tanh( ((r_y - r_yc)/r_dlt * cos(r_omg * r_t)) - &
			                           ((r_x - r_xc)/r_dlt * sin(r_omg * r_t)) ) + 1._GRID_SR
			END IF
    END DO node_loop

!---------- deallocate workspace

	  DEALLOCATE(r_coo)


	  RETURN
	  END SUBROUTINE slm_analyticsol_vortex
!*****************************************************************
	  SUBROUTINE slm_initvortex(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                              :: p_ghand
		
	  REAL (KIND = GRID_SR)                                       :: r_y
	  REAL (KIND = GRID_SR)                                       :: r_dlt				! controls the width of the decay from value 2 to value 0
		REAL (KIND = GRID_SR)                                       :: r_twopi			! scale the domain from [-0.5,0,5]to [-Pi, Pi]
		
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)            :: r_tmp
	  INTEGER                                                     :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE          :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE          :: r_coo
	  INTEGER, DIMENSION(1)                                       :: i_valind

!---------- initialize some constant for the slotted cylinder

	  
		r_dlt = r_delta
				
!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(55)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- loop over the nodes
    
		r_twopi = GRID_PI * 2._GRID_SR				
		
	  node_loop: DO i_count= 1, i_num
		  r_y = r_coo(2, i_count) * r_twopi
	    r_aux(1,i_count)= tanh((r_y - r_center(2)) / r_dlt) + 1._GRID_SR
	  END DO node_loop

!---------- update grid information

	  i_valind = (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint= i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace
	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_initvortex
!*****************************************************************
    FUNCTION twonorm(r_vector) RESULT(r_nrm)
		
		IMPLICIT NONE

		REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)           :: r_vector
		REAL (KIND = GRID_SR)                                      :: r_nrm
		
		INTEGER                                                    :: i_cnt
		
		r_nrm = 0._GRID_SR
		r_nrm = dot_product(r_vector,r_vector)		
		r_nrm = sqrt(r_nrm)
		
		END FUNCTION twonorm
		

	END MODULE SLM_initial
