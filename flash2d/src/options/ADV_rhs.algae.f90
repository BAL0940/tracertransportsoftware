!*****************************************************************
!
! MODULE NAME:
!	ADV_rhs
! FUNCTION:
!	calculate the (nonhomogeneous) right hand side
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_righthand
! FUNCTION:
!	calculate the rhs of the advection equation
! SYNTAX:
!	real= slm_righthand(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_rhs:   right hand side value		real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!	this is the homogeneous case!
! USES:
!	MISC_globalparam, MISC_error
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	2/98
!	2. compliant to amatos 1.0	j. behrens	12/2000
!	3. compliant to amatos 1.2	j. behrens	3/2002
!
!*****************************************************************
	MODULE ADV_rhs
	  USE GRID_api
	  PRIVATE
	  PUBLIC :: slm_righthand
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_righthand(r_coord, r_values, r_time) RESULT (r_rhs)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(2), INTENT(in), OPTIONAL    :: r_values
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                  :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(2)                          :: r_rhs
	  REAL (KIND = GRID_SR)                                        :: r_tim
	  REAL (KIND = GRID_SR)                                        :: r_secondsperday

    REAL (KIND = GRID_SR) :: r_mumax, r_Topt, r_Tslope, r_Cmort, r_mu, r_tmp

!---------- set constants

    r_secondsperday = 86400._GRID_SR
    r_mumax  = 0.5_GRID_SR/r_secondsperday  ! [per day]
    r_Topt   = 22._GRID_SR   ! [°C]
    r_Tslope = 5._GRID_SR    ! [°C]
    r_Cmort  = 0.05_GRID_SR/r_secondsperday ! [per day]
    
!---------- set time

	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0
	  END IF

!---------- compute growth

    r_tmp= r_values(2)-r_Topt
    r_mu = r_mumax* exp(-((r_tmp * r_tmp)/(r_Tslope * r_Tslope)))
    
!---------- calculate the advection at (x,y) (velocity increasing)

	  r_rhs= 0.0
	  r_rhs(1)= r_mu* r_values(1)- r_Cmort* r_values(1)
	
	  RETURN
	  END FUNCTION slm_righthand

!*****************************************************************
	END MODULE ADV_rhs
