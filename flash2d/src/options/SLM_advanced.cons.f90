!*****************************************************************
!
! MODULE NAME:
!	SLM_advanced
! FUNCTION:
!	provide advanced semi-Lagrangian routines
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_step
! FUNCTION:
!	one step of the basic SLM algorithm
! SYNTAX:
!	CALL slm_step(int, real.arr, real.arr)
! ON INPUT:
!	...
! ON OUTPUT:
!	r_tracer: array with tracer values	real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_displace
! FUNCTION:
!	extrapolate the alpha, values for the displacements of the upstream
!	points from the gridpoints
! SYNTAX:
!	CALL slm_displace(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_coord: real array of xy-coordinates		real
! ON OUTPUT:
!	r_alpha: displacement vectors to each point	real
! CALLS:
!	wind_field
! COMMENTS:
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_update
! FUNCTION:
!	calculate the update to the velocity
! SYNTAX:
!	CALL slm_update(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_rside: array with right hand side values	real
! ON OUTPUT:
!	r_udate: array with new (updated) gid values	real
! CALLS:
!
! COMMENTS:
!	this routine is trivial for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_upstream
! FUNCTION:
!	calculate right hand side of the equation (upstream values)
! SYNTAX:
!	CALL slm_upstream(int, real.arr, real.arr)
! ON INPUT:
!	i_arlen: array length for the real arrays	integer
!	r_alpha: displacement vectors to each point	real
! ON OUTPUT:
!	r_rside: array with right hand side values	real
! CALLS:
!
! COMMENTS:
!	this routine is just interpolation for linear advection
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_interpolate
! FUNCTION:
!	do the interpolation
! SYNTAX:
!	CALL slm_interpolate(grid, int, real, real.arr, real.arr, real.arr)
! ON INPUT:
!	p_ogrid: grid handle to old grid (with data)	TYPE (grid_handle)
!	r_fac:   factor at which point to interpolate	REAL
!	i_arlen: array length for the following arrays	INTEGER
!	r_coord: coordinate array (new grid)		REAL
!	r_alpha: displacement array (corr. to r_coord)	REAL
!	r_value: values on the old grid (array)		REAL
! ON OUTPUT:
!	r_rside: right hand side (interpolated)		REAL
! CALLS:
!
! COMMENTS:
!	this one is plain bi-cubic spline interpolation
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_astep
! COMMENTS:
!
! USES:
!	FLASH_parameters, GRID_api, ADV_wind, ADV_rhs
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	4/2002
!	2. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_advanced
	  USE FLASH_parameters
	  USE MISC_timing
	  USE GRID_api
	  USE ADV_wind
	  USE ADV_rhs
	  PRIVATE
	  REAL    :: r_conservation
	  LOGICAL :: l_interpolinitialized=.FALSE.
	  PUBLIC  :: slm_astep
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_astep(p_ghand, p_param, p_time, r_modtime, i_size, &
	                       r_coord, r_tracer, i_newsdepth)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps), INTENT(in) :: p_ghand
	  TYPE (control_struct), INTENT(in)                         :: p_param
	  TYPE (sw_info), INTENT(inout)                             :: p_time
	  REAL (KIND = GRID_SR), INTENT(in)                         :: r_modtime
	  INTEGER, INTENT(in)                                       :: i_size
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_size), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_size), INTENT(out)     :: r_tracer
	  INTEGER, OPTIONAL, INTENT(in)                             :: i_newsdepth
	  
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE          :: r_newvl
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE        :: r_alpha
	  INTEGER                                                   :: i_alct

!---------- check size!

	  IF(i_size <= 0) THEN
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'INFO [slm_astep]: Zero step size, returning to calling routine'
	    RETURN
	  END IF

!---------- Initialize, if not yet done

	  IF(.NOT. l_interpolinitialized) THEN
	    CALL slm_interpolinit(p_ghand(i_time))
	    l_interpolinitialized= .TRUE.
	  END IF

!---------- allocate auxiliary arrays

	  allocate(r_newvl(i_size), r_alpha(GRID_dimension,i_size), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(38)
	  END IF not_alloc

!-SLM--------- calculate trajectory pieces (displacements)

	  CALL stop_watch('start',3,p_time)
	  CALL slm_adisplace(p_param, i_size, r_coord, r_alpha, r_time=r_modtime)
	  CALL stop_watch('stop ',3,p_time)

!-SLM--------- calculate right hand side

	  CALL stop_watch('start',4,p_time)
	  CALL slm_aupstream(p_ghand, i_size, r_coord, r_alpha, r_newvl)
	  CALL stop_watch('stop ',4,p_time)

!-SLM--------- calculate new grid values

	  CALL stop_watch('start',5,p_time)
	  CALL slm_aupdate(p_param, i_size, r_coord, r_newvl, r_tracer, r_time=r_modtime)
	  CALL stop_watch('stop ',5,p_time)

!-SLM--------- put alpha values to u and v field entries

	  r_alpha= -r_alpha
	  IF(present(i_newsdepth)) THEN
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_newsdepth=i_newsdepth, i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  ELSE
	    CALL grid_putinfo(p_ghand(i_timeplus), r_nodevalues=r_alpha, &
	  		      i_arraypoint=(/ GRID_ucomp, GRID_vcomp /))
	  END IF

!-SLM--------- deallocate work arrays

	  deallocate(r_alpha, r_newvl)

	  RETURN
	  END SUBROUTINE slm_astep

!*****************************************************************
	  SUBROUTINE slm_adisplace(p_param, i_arlen, r_coord, r_alpha, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                      :: p_param
	  INTEGER, INTENT(in)                                  :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in)  :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(out) :: r_alpha
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                           :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                      :: r_fac, r_caf, &
	    r_axy, r_xyc
	  REAL (KIND = GRID_SR)                                                 :: r_dt0, r_dt1, &
	    r_dt2, r_tim
	  INTEGER                                              :: i_cnt1, i_cnt2
          
!---------- set constants

	  r_dt0= p_param%num%r_deltatime
	  r_dt1= 0.5_GRID_SR* p_param%num%r_deltatime
	  r_dt2= 1.5_GRID_SR* p_param%num%r_deltatime
	  r_fac= 0.5_GRID_SR
	  r_caf= 2.0_GRID_SR
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- calculate in an iteration process the displacements

	  unknown_loop: DO i_cnt1=1,i_arlen
	    r_axy= 0.0_GRID_SR

	    iter_loop: DO i_cnt2=1, p_param%num%i_adviterations
	      r_xyc= r_coord(:,i_cnt1)- r_fac* r_axy
	      r_axy= r_dt0* slm_windfield(r_xyc, r_time=r_tim)
	    END DO iter_loop

	    r_alpha(:,i_cnt1)= r_axy
	  END DO unknown_loop

	  RETURN
	  END SUBROUTINE slm_adisplace

!*****************************************************************
	  SUBROUTINE slm_aupdate(p_param, i_arlen, r_coord, r_rside, r_udate, r_time)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct), INTENT(in)                   :: p_param
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(in)                :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_udate
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                          :: r_time
	  INTEGER                                             :: i_cnt
	  REAL (KIND = GRID_SR)                                                :: r_dt, r_tim

!---------- in the linear advection case and with f90 this is just

!	  r_udate= r_rside

!---------- including a non-zero right hand side, we have

	  r_dt= p_param%num%r_deltatime
	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

	  main_loop: DO i_cnt=1, i_arlen
	    r_udate(i_cnt)= r_rside(i_cnt)+ r_dt* slm_righthand(r_coord(:,i_cnt))
	  END DO main_loop

	  RETURN
	  END SUBROUTINE slm_aupdate

!*****************************************************************
	  SUBROUTINE slm_aupstream(p_mesh, i_arlen, r_coord, &
	                          r_alpha, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)       :: p_mesh
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_alpha
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)                     :: r_fac

!---------- set factor (at which point of trajectory shall i interpolate)

	  r_fac= 1.0_GRID_SR

!---------- in the linear advection case this is just interpolation

	  CALL slm_ainterpolate(p_mesh, r_fac, i_arlen, r_coord, &
	                       r_alpha, r_rside)

	  RETURN
	  END SUBROUTINE slm_aupstream
!*****************************************************************
	  SUBROUTINE slm_ainterpolate(p_mesh, r_fac, i_arlen, &
	                             r_coord, r_alpha, r_rside)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), DIMENSION(GRID_timesteps)       :: p_mesh
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)         :: r_fac
	  INTEGER, INTENT(in)                                 :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_arlen), INTENT(in) :: r_alpha
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)               :: r_rside
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE                   :: r_upstr
	  REAL (KIND = GRID_SR), DIMENSION(GRID_nodevalues)                    :: r_tval
	  INTEGER, DIMENSION(GRID_elementnodes)               :: i_ttmp
	  REAL (KIND = GRID_SR)                                                :: r_eps, r_max, r_min, &
	    r_tmp, r_avg, r_sp, r_sp1, r_sb, r_plus, r_minus, r_cs, r_sm, r_ccs
	  INTEGER                                             :: i_cnt, i_alct, i_val, &
	    i_ind, i_tim, j_cnt, i_points, i_out, i_stat, i_tmp
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE                     :: r_low, r_hig, &
	    r_nar, r_bfc, r_mfc, r_cfc
	  INTEGER, DIMENSION(:), ALLOCATABLE                  :: i_fil
	  LOGICAL                                             :: l_changed
	  INTEGER                                             :: i_newlen
!	  INTEGER, DIMENSION(:), POINTER                      :: i_newind

!---------- initialize pointer

!	  NULLIFY(i_newind)

!---------- initialize constant

	  i_val= GRID_tracer
	  r_eps= GRID_EPS
	  i_tim= p_mesh(i_timeplus)%i_timetag
!	  i_points= p_mesh(i_timeplus)%i_nnumber
	  i_points= i_arlen

!---------- allocate work array

	  ALLOCATE(r_upstr(GRID_dimension,i_points), stat=i_alct)
	  not_alloc1: IF(i_alct /= 0) THEN
	    CALL grid_error(60)
	  END IF not_alloc1

	  ALLOCATE(r_low(i_points), r_hig(i_points), &
	           r_bfc(i_points), r_mfc(i_points), &
	           r_nar(i_points), r_cfc(i_points), &
	           i_fil(i_points), stat= i_alct)
	  not_alloc2: IF(i_alct /= 0) THEN
	    CALL grid_error(60)
	  END IF not_alloc2
	  r_low=0.0_GRID_SR
	  r_hig=0.0_GRID_SR

!---------- calculate upstream coordinates

	  dim_loop: DO i_cnt=1, GRID_dimension
	    r_upstr(i_cnt,:) = r_coord(i_cnt,:)- r_fac(i_cnt)* r_alpha(i_cnt,:)
	  END DO dim_loop

!---------- loop over nodes: find element containing upstream point

	  node_loop: DO i_cnt=1, i_points

!---------- check if upstream value is outside of the domain

	    i_out= grid_domaincheck(p_mesh(i_time), r_upstr(:,i_cnt))

!---------- take the intersection of the trajectory with the boundary as new upstream point

	    out_domain: IF(i_out /= 0) then
	      r_upstr(:,i_cnt)= grid_boundintersect(p_mesh(i_time), &
	                        r_coord(:,i_cnt), r_upstr(:,i_cnt), i_info=i_stat)
	      no_intersect: IF(i_stat /= 0) THEN
	        r_bfc(i_cnt)= 0.0_GRID_SR
	        CYCLE node_loop
	      END IF no_intersect
	    END IF out_domain

!---------- interpolate

	    r_low(i_cnt)= grid_coordvalue(p_mesh(i_time), r_upstr(:,i_cnt), &
	           i_interpolorder=GRID_loworder, i_valpoint=i_val, i_index=i_ind, l_relative=.FALSE., l_sfcorder=.FALSE.)
	    r_hig(i_cnt)= grid_coordvalue(p_mesh(i_time), r_upstr(:,i_cnt), &
	           i_interpolorder=GRID_highorder, i_valpoint=i_val, l_relative=.FALSE., l_sfcorder=.FALSE.)

	    index_valid: IF(i_ind > 0) THEN

!---------- get nodes of element surrounding r_upstr

	      CALL grid_getiteminfo(i_ind, 'elmt', i_arrlen=GRID_elementnodes, i_nodes=i_ttmp)

!---------- get values at nodes of element surrounding r_upstr, and obtain min/max

	      r_min= 0.0_GRID_SR; r_max= 0.0_GRID_SR
	      elmt_loop: DO j_cnt=1,GRID_elementnodes
	        CALL grid_getiteminfo(i_ttmp(j_cnt), 'node', i_arrlen=GRID_nodevalues, &
	                              r_values= r_tval, i_time=p_mesh(i_time)%i_timetag)
	        IF(j_cnt == 1) THEN
	          r_min= r_tval(i_val)
	          r_max= r_tval(i_val)
	        ELSE
	          r_min= MIN(r_min, r_tval(i_val))
	          r_max= MAX(r_max, r_tval(i_val))	      
	        END IF
	      END DO elmt_loop

!---------- factor for the low and high order blending

	      r_tmp  = r_hig(i_cnt)- r_low(i_cnt)
	      r_plus = r_max- r_low(i_cnt)
	      r_minus= r_min- r_low(i_cnt)

	      IF(r_tmp > 0.0_GRID_SR) THEN
	        r_bfc(i_cnt)= MIN(1.0_GRID_SR, r_plus/r_tmp)
	      ELSE IF(r_tmp < 0.0_GRID_SR) THEN
	        r_bfc(i_cnt)= MIN(1.0_GRID_SR, r_minus/r_tmp)
	      ELSE
	        r_bfc(i_cnt)= 0.0_GRID_SR
	      END IF
	    ELSE index_valid
	      r_bfc(i_cnt)= 0.0_GRID_SR
	    END IF index_valid
	  END DO node_loop

!---------- get nodal area for integration

! 	  CALL grid_newitems(p_mesh(i_timeplus), i_newnodes=i_newlen, &
! 	                     i_newnodeindex=i_newind)
! 	  IF(i_newlen > i_points) THEN
! 	    CALL grid_error(i_error=2,c_error='[slm_interpolate]: index array length mismatch encountered')
! 	  END IF
! 	  CALL grid_nodearea(p_mesh(i_timeplus), i_tmp, r_nar, &
! 	                     i_selectlength=i_newlen, i_selectindex=i_newind)
	  CALL grid_nodearea(p_mesh(i_timeplus), i_tmp, r_nar)

!---------- calculate an auxiliary weighted factor for convenience

	  r_cfc= (r_hig- r_low)* r_nar

!---------- check for conservation

	  r_cs= DOT_PRODUCT(r_low, r_nar)
	  r_sm= DOT_PRODUCT(r_bfc, r_cfc)
	  r_ccs= r_conservation- r_cs
	  IF(r_sm < r_ccs) THEN
	    r_ccs= -r_ccs
	    r_cfc= -r_cfc
	  END IF

!---------- check local underestimation

	  DO i_cnt=1,i_points
	    IF(r_cfc(i_cnt) <= 0.0_GRID_SR) THEN
	      r_mfc(i_cnt)= r_bfc(i_cnt)
	      i_fil(i_cnt)= 1
	    ELSE
	      r_mfc(i_cnt)= 0.0_GRID_SR
	      i_fil(i_cnt)= 0
	    END IF
	  END DO

!---------- the modification loop

	  l_changed= .TRUE.
	  mod_loop: DO WHILE(l_changed)
	    l_changed= .FALSE.

!---------- calculate surplus and average alpha

	    r_sp1= 0.0_GRID_SR
	    r_sb = 0.0_GRID_SR
	    DO i_cnt= 1, i_points
	      IF(i_fil(i_cnt) == 1) THEN
	        r_sp1= r_sp1+ r_mfc(i_cnt)* r_cfc(i_cnt)
	      ELSE
	        r_sb= r_sb+ r_cfc(i_cnt)
	      END IF
	    END DO
	    r_sp= r_ccs- r_sp1

!---------- if surplus is negative, use the quasi-monotone factors

	    IF(r_sp <= 0.0_GRID_SR) THEN
	      r_mfc= r_bfc
	      EXIT mod_loop
	    END IF

!---------- if surplus is zero, we're done

	    IF(r_sb == 0.0_GRID_SR) EXIT mod_loop

!---------- calculate an average weight

	    r_avg= r_sp/ r_sb

!---------- else put average weights to the factors

	    DO i_cnt=1, i_points
	      IF(i_fil(i_cnt) == 0) THEN
	        IF(r_avg <= r_bfc(i_cnt)) THEN
	          r_mfc(i_cnt)= r_avg
	        ELSE 
	          r_mfc(i_cnt)= r_bfc(i_cnt)
	          i_fil(i_cnt)= 1
	          l_changed= .TRUE.
	        END IF
	      END IF
	    END DO

	  END DO mod_loop

!---------- blend the final values of the interpolation

	  r_rside= r_mfc* r_hig+ (1.-r_mfc)* r_low

!---------- remove small oscillations

	  r_rside= MERGE(r_rside, 0.0_GRID_SR, ABS(r_rside)>=r_eps)

!---------- deallocate work arrays

	  DEALLOCATE(r_upstr, r_low, r_hig, r_bfc, r_mfc, &
	             r_nar, r_cfc, i_fil) !, i_newind)
!	  NULLIFY(i_newind)

	  RETURN
	  END SUBROUTINE slm_ainterpolate
!*****************************************************************
	  SUBROUTINE slm_interpolinit(p_ogrid)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle)              :: p_ogrid
	  INTEGER                         :: i_points, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE   :: r_area
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_values
	  INTEGER, DIMENSION(1)           :: i_valind

!---------- initialize constants

	  i_valind= (/ GRID_tracer /)
	  i_points= p_ogrid%i_nnumber

!---------- allocate work arrays

	  ALLOCATE(r_area(i_points), r_values(1,i_points), stat=i_alct)
	  not_alloc: IF(i_alct /= 0) THEN
	    CALL grid_error(61)
	  END IF not_alloc

!---------- get values

	  CALL grid_getinfo(p_ogrid, i_arraypoint=i_valind, &
	                    r_nodevalues= r_values)

!---------- calculate area pieces for each node

	  CALL grid_nodearea(p_ogrid, i_points, r_area)

!---------- calculate integral

	  r_conservation= dot_product(r_area, r_values(1,:))

!---------- deallocate work arrays

	  DEALLOCATE(r_area, r_values)

	  RETURN
	  END SUBROUTINE slm_interpolinit

	END MODULE SLM_advanced
