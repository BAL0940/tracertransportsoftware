!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize slotted cylinder for semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initslot
! FUNCTION:
!	initialize a grid with values of slotted cylinder test case
! SYNTAX:
!	CALL slm_initslot(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initcylndr
! FUNCTION:
!	initialize a grid with values of a cylinder test case
! SYNTAX:
!	CALL slm_initcylndr(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_centr: coordinates of cyl. centre	REAL
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	in_side
! FUNCTION:
!	checks, if a given point (x,y) lies within a given triangle
! SYNTAX:
!	logical= in_side(real.arr, real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord: coordinate array		REAL
!	r_node1: node1 of triangle		REAL
!	r_node2: node2 of triangle		REAL
!	r_node3: node3 of triangle		REAL
! ON OUTPUT:
!	l_in:    .true. if r_coord \in p_elem	logical
! CALLS:
!
! COMMENTS:
!	this routine decides whether a point lies in or out of a
!	triangle. this is done with the following approach:
!	calculate the area for the given triangle and for the 
!	three triangles resulting from drawing lines from the given
!	point to all three triangle nodes.
!	if the sum of the areas of those three trianlges exceeds
!	the area of the given trianlge, the the point lies outside
!	of it.
!	for calculation of the areas following formula is used:
!	(Herons formula(?))
!
!	A = sqrt(s* (s- a)* (s- b)* (s- c)),
!
!	where s= 1/2* (a+ b+ c)
!	      a, b, c sides.
!
!	in order to calculate the sidelengths |x-y|= sqrt(x**2-y**2)
!	the complex absolute value intrinsic function is used.
!	hopefully this fuction is faster than using sqrt and
!	power-of-two instead.
!
!	internally double precision is used in this function, because
!	machine precision is crucial when a coordinate pair appears
!	near the edge or corner of an element.
!
!-----------------------------------------------------------------
!
! NAME:
!	dc_area
! FUNCTION:
!	calculate area of a triangle (in a plain) in double precision
! SYNTAX:
!	real= dc_area(real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord1: node1 of triangle			REAL
!	r_coord2: node2 of triangle			REAL
!	r_coord3: node3 of triangle			REAL
! ON OUTPUT:
!	r_area:   area of triangle			REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	7/97
!	2. names changed		j. behrens	7/97
!	3. changed to use GRID_api	j. behrens	11/97
!	4. changed interfaces		j. behrens	12/97
!	5. compliant to amatos 1.0	j. behrens	12/2000
!	6. compliant to amatos 1.2	j. behrens	3/2002
!	7. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
	  REAL (KIND = GRID_SR)                            :: r_hgt=4.0
	  REAL (KIND = GRID_SR)                            :: r_srd=0.15
	  REAL (KIND = GRID_SR)                            :: r_sln=0.22
	  REAL (KIND = GRID_SR)                            :: r_swd=0.06
	  LOGICAL                         :: l_circlinput
	  INTEGER, PARAMETER              :: i_ioerr=0
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in) :: p_ghand
	  INTEGER                        :: i_iofil= 11
	  CHARACTER (len=80)             :: a_filrow
	  INTEGER                        :: i_iost, i_ioend, i_alct, i_cnt
	  INTEGER                        :: i_numcircl, i_tmp
	  INTEGER, DIMENSION(3)          :: i_cntcircl
	  INTEGER, PARAMETER             :: i_coor=1
	  INTEGER, PARAMETER             :: i_diam=2
	  INTEGER, PARAMETER             :: i_conc=3
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coocircl
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE   :: r_concircl, r_diacircl

!---------- open file

	  open(unit= i_iofil, file= 'Initial.dat', status= 'OLD', action= 'READ', iostat= i_iost)
	  file_notopen: IF(i_iost /= 0) THEN
	    write(i_ioerr,*) 'ERROR: Filename: Initial.dat'
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'ERROR: Filename: Initial.dat'
	    CALL grid_error(56)
	  END IF file_notopen
	  IF(GRID_parameters%iolog > 0) &
	    write(GRID_parameters%iolog,*) 'INFO: Opened file on unit: ', i_iofil

!---------- read file entries in a loop

	  read_loop: DO
	    read(i_iofil,2000,iostat=i_ioend) a_filrow

!---------- if file ended

	    file_end: IF(i_ioend /= 0) THEN
	      close(i_iofil)
	      IF(GRID_parameters%iolog > 0) &
	        write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_iofil
	      EXIT read_loop
	    ELSE file_end

!---------- decide what to DO with line according to first character

	    comment_line: IF(a_filrow(1:1) == '#' .or. a_filrow(1:1) == '!') THEN
	      CYCLE read_loop
	    ELSE IF(a_filrow(1:11) == 'CIRCLES_INP') THEN comment_line
	      l_circlinput= .TRUE.
	      i_cntcircl= 0
	    ELSE IF(a_filrow(1:11) == 'FILES_INPUT') THEN comment_line
	      IF(GRID_parameters%iolog  > 0) &
	        write(GRID_parameters%iolog,*) 'INFO [slm_initialvalue]: File input not yet supported'
	      l_circlinput= .FALSE.
	    ELSE IF(a_filrow(1:11) == 'NUMBER_OF_C') THEN comment_line
	      IF(.NOT. l_circlinput) CALL grid_error(57)
	      read(i_iofil,*) i_numcircl
	      IF(ALLOCATED(r_coocircl)) DEALLOCATE(r_coocircl)
	      IF(ALLOCATED(r_concircl)) DEALLOCATE(r_concircl)
	      IF(ALLOCATED(r_diacircl)) DEALLOCATE(r_diacircl)
	      ALLOCATE(r_coocircl(GRID_dimension,i_numcircl), stat= i_alct)
	      IF(i_alct /= 0) CALL grid_error(58)
	      ALLOCATE(r_concircl(i_numcircl), r_diacircl(i_numcircl), stat= i_alct)
	      IF(i_alct /= 0) CALL grid_error(58)
	    ELSE IF(a_filrow(1:11) == 'NUMBER_OF_W') THEN comment_line
	      IF(GRID_parameters%iolog  > 0) &
	        write(GRID_parameters%iolog,*) 'INFO [slm_initialvalue]: File input not yet supported'
	      IF(l_circlinput) CALL grid_error(57)
	    ELSE IF(a_filrow(1:11) == 'CIRCLE_COOR') THEN comment_line
	      IF(.NOT. l_circlinput) CALL grid_error(57)
	      i_cntcircl(i_coor)= i_cntcircl(i_coor)+ 1
	      i_tmp= i_cntcircl(i_coor)
	      IF(i_tmp > i_numcircl) CALL grid_error(57)
	      dim_loop: DO i_cnt=1,GRID_dimension
	        read(i_iofil,*) r_coocircl(i_cnt,i_cntcircl(i_coor))
	      END DO dim_loop
	    ELSE IF(a_filrow(1:11) == 'CIRCLE_DIAM') THEN comment_line
	      IF(.NOT. l_circlinput) CALL grid_error(57)
	      i_cntcircl(i_diam)= i_cntcircl(i_diam)+ 1
	      i_tmp= i_cntcircl(i_diam)
	      IF(i_tmp > i_numcircl) CALL grid_error(57)
	      read(i_iofil,*) r_diacircl(i_cntcircl(i_diam))
	    ELSE IF(a_filrow(1:11) == 'CIRCLE_CONC') THEN comment_line
	      IF(.NOT. l_circlinput) CALL grid_error(57)
	      i_cntcircl(i_conc)= i_cntcircl(i_conc)+ 1
	      i_tmp= i_cntcircl(i_conc)
	      IF(i_tmp > i_numcircl) CALL grid_error(57)
	      read(i_iofil,*) r_concircl(i_cntcircl(i_conc))
	    ELSE IF(a_filrow(1:11) == 'INPUT_FILE_') THEN comment_line
	      IF(GRID_parameters%iolog  > 0) &
	        write(GRID_parameters%iolog,*) 'INFO [slm_initialvalue]: File input not yet supported'
	      IF(l_circlinput) CALL grid_error(57)
	    ELSE IF(a_filrow(1:11) == 'WATERMARK_L') THEN comment_line
	      IF(GRID_parameters%iolog  > 0) &
	        write(GRID_parameters%iolog,*) 'INFO [slm_initialvalue]: File input not yet supported'
	      IF(l_circlinput) CALL grid_error(57)
	    ELSE IF(a_filrow(1:11) == 'WATERMARK_C') THEN comment_line
	      IF(GRID_parameters%iolog  > 0) &
	        write(GRID_parameters%iolog,*) 'INFO [slm_initialvalue]: File input not yet supported'
	      IF(l_circlinput) CALL grid_error(57)
	    END IF comment_line

	    END IF file_end
	  END DO read_loop

!---------- initialize cylinders

	  CALL initcylndr(p_ghand, i_numcircl, r_coocircl, r_diacircl, r_concircl)

	  RETURN
 2000	  FORMAT(a80)
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)              :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_time
	  INTEGER, INTENT(in)                         :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)       :: r_array
	  REAL (KIND = GRID_SR)                                        :: r_fac=.363610260832151995e-4
	  REAL (KIND = GRID_SR)                                        :: r_rds, r_ras, r_rad, &
	    r_ads, r_bds, r_cds, r_f1, r_f2, r_xya, r_dpt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_fc, r_p1, r_p2, &
	    r_pa, r_pb, r_pc, r_pd, r_tmp, r_centr
	  INTEGER                                     :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE           :: r_coo
	  LOGICAL                                     :: l_ino

!---------- initialize some constants for the slotted cylinder

	  r_centr(1)= -cos(r_fac* r_time)* 0.25_GRID_SR ! <--- based on 2D
	  r_centr(2)= -sin(r_fac* r_time)* 0.25_GRID_SR ! <--- based on 2D

	  r_rds= r_srd* r_srd
	  r_ras= dot_product(r_centr, r_centr)
	  r_rad= sqrt(r_ras)
	  r_ads= r_swd* 0.5_GRID_SR
	  r_bds= r_srd- r_sln
	  r_cds= r_srd
	  r_f1 = (r_rad- r_ads)/ r_rad
	  r_f2 = (r_rad+ r_ads)/ r_rad
	  r_p1 = abs(r_centr)
	  r_xya= sum(r_p1)
	  IF(r_xya == 0.0_GRID_SR) THEN
	    r_fc= 0.0_GRID_SR
	  ELSE
	    r_fc(1)= -r_centr(2)/ r_xya
	    r_fc(2)=  r_centr(1)/ r_xya
	  END IF

!---------- calculate endpoints of the slot

	  r_p1= r_f1* r_centr
	  r_p2= r_f2* r_centr

	  r_pa= r_p1+ r_fc* r_bds
	  r_pb= r_p1+ r_fc* r_cds
	  r_pc= r_p2+ r_fc* r_cds
	  r_pd= r_p2+ r_fc* r_bds

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  IF(i_arlen /= i_num) THEN
	    CALL grid_error(52)
	  END IF

	  ALLOCATE(r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(53)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_array(i_count)= 0.0_GRID_SR
	    r_tmp(:)        = r_coo(:,i_count)- r_centr(:)
	    r_dpt= dot_product(r_tmp, r_tmp)
	    inside: IF(r_dpt < r_rds) THEN
	      r_array(i_count)= r_hgt
	      l_ino= in_side(r_coo(:,i_count), r_pa, r_pb, r_pc)
	      IF(l_ino) r_array(i_count)= 0.0_GRID_SR
	      l_ino= in_side(r_coo(:,i_count), r_pa, r_pc, r_pd)
	      IF(l_ino) r_array(i_count)= 0.0_GRID_SR
	    END IF inside
	  END DO node_loop

!---------- deallocate workspace

	  DEALLOCATE(r_coo)

	  RETURN
	  END SUBROUTINE slm_analyticsolution

!*****************************************************************
	  FUNCTION dc_area(r_coord1, r_coord2, r_coord3) RESULT (r_area)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord1, r_coord2, r_coord3
	  REAL (KIND = GRID_DR)                            :: r_area
	  REAL (KIND = GRID_DR)                            :: r_c
	  REAL (KIND = GRID_DR), DIMENSION(GRID_dimension) :: r_a, r_b
	  INTEGER                                     :: i_cnt

!---------- calculate vector components

	  dim_loop: DO i_cnt=1, GRID_dimension
	    r_a(i_cnt)= REAL((r_coord2(i_cnt)- r_coord1(i_cnt)),GRID_DR)
	    r_b(i_cnt)= REAL((r_coord3(i_cnt)- r_coord1(i_cnt)),GRID_DR)
	  END DO dim_loop

!---------- calculate components (a,b,c) of cross product vector

	  r_c= (r_a(1)* r_b(2)- r_a(2)* r_b(1))

!---------- calculate area

	  r_area= abs(r_c)* 0.5_GRID_DR

	  RETURN
	  END FUNCTION dc_area

!*****************************************************************
	  FUNCTION in_side(r_coord, r_node1, r_node2, r_node3) RESULT (l_in)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_node1, r_node2, r_node3
	  LOGICAL                                     :: l_in
	  REAL (KIND = GRID_DR)                            :: r_sum, r_f, r_f1, r_f2, r_f3
	  REAL (KIND = GRID_DR)                            :: r_eps

!---------- set in_side value to out

 	  l_in= .FALSE.

!---------- set epsilon (machine precision)

	  r_eps= GRID_eps

!---------- calculate areas (f, f1, f2, f3)

	  r_f = dc_area(r_node1, r_node2, r_node3)
	  r_f1= dc_area(r_coord, r_node2, r_node3)
	  r_f2= dc_area(r_coord, r_node1, r_node2)
	  r_f3= dc_area(r_coord, r_node1, r_node3)

!---------- check if summed area is bigger than triangle area

	  r_sum= r_f1+ r_f2+ r_f3
	  if((r_sum- r_f) < r_eps) l_in= .TRUE.

	  RETURN
	  END FUNCTION in_side

!*****************************************************************
	  SUBROUTINE initcylndr(p_ghand, i_numcircl, r_coocircl, r_diacircl, r_concircl)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand
	  INTEGER                                    :: i_numcircl
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_numcircl) :: r_coocircl
	  REAL (KIND = GRID_SR), DIMENSION(i_numcircl)                :: r_diacircl, r_concircl
	  REAL (KIND = GRID_SR)                                       :: r_radsq, r_heigt, r_dpt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)            :: r_tmp, r_centr
	  INTEGER                                    :: i_count, i_num, i_alct, i_circ
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE          :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE          :: r_coo
	  INTEGER, DIMENSION(1)                       :: i_valind

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(55)
	  END IF
	  r_aux= 0.0_GRID_SR

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- loop over the circles

	  circl_loop: DO i_circ= 1, i_numcircl

	    r_radsq= r_diacircl(i_circ)* r_diacircl(i_circ)
	    r_centr= r_coocircl(:,i_circ)
	    r_heigt= r_concircl(i_circ)

!---------- loop over the nodes

	    node_loop: DO i_count= 1, i_num
	      r_tmp(:)      = r_coo(:,i_count)- r_centr(:)
	      r_dpt= dot_product(r_tmp, r_tmp)
	      inside: IF(r_dpt < r_radsq) THEN
	        r_aux(1,i_count)= r_aux(1,i_count)+ r_heigt
	      END IF inside
	    END DO node_loop
	  END DO circl_loop

!---------- update grid information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE initcylndr

	END MODULE SLM_initial
