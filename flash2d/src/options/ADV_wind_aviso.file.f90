!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	12/97
!	2. bug fix concerning interp.	j. behrens	2/98
!	3. compliant to amatos 1.0	j. behrens	12/2000
!	4. compliant to amatos 1.2	j. behrens	3/2002
!	5. new version for course tracer transport	j. behrens 01/2012
!
!*****************************************************************
	MODULE ADV_wind
	  USE GRID_api
	  USE FLASH_parameters
	  PRIVATE
	  INTEGER, PARAMETER    :: i_ioerr=0
	  REAL (KIND = GRID_SR) :: r_intervallen, r_readlast
	  REAL (KIND = GRID_SR) :: r_ncupdatelast, r_secperstep
	  CHARACTER (LEN=128)   :: c_windpath
	  CHARACTER (LEN=64), DIMENSION(:), ALLOCATABLE        :: c_windfile
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE :: r_flowx
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE :: r_flowy
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE     :: r_lat
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE     :: r_lon
      INTEGER (KIND = GRID_SI)  :: i_lon, i_lat, i_timesteps, i_numfiles
	  INTEGER (KIND = GRID_SI)  :: i_pointsx, i_pointsy
	  INTEGER (KIND = GRID_SI)  :: i_timeinterval, i_intervalnum, i_stepcounter
	  
	  PUBLIC                    :: slm_windfield, slm_windinit, slm_windquit
	  
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coord, r_time) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_field
	  REAL (KIND = GRID_SR)                       :: r_tim
	  CHARACTER (LEN=192)                         :: c_ncfile
	  INTEGER (KIND = GRID_SI)                    :: i_iost, i_cnt, j_cnt

!---------- set time

	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- decide, if data has to be read

	  data_read: IF(r_readlast <= r_tim) THEN

!---------- update values for next open

	    r_readlast    = r_readlast+ r_intervallen
	    i_timeinterval= i_timeinterval+ 1
	    IF(i_timeinterval > i_numfiles) THEN
	      CALL grid_error(i_error=1,c_error='[slm_windfield]: no more current files in list, fixing last one')
          i_timeinterval = i_numfiles
        END IF
        i_stepcounter = 0_GRID_SI

!---------- create filenames

	    write(c_ncfile,*) trim(c_windpath),c_windfile(i_timeinterval)
	    c_ncfile= adjustl(c_ncfile)

!---------- read current data from NetCDF file

	    CALL read_netcdf_currents(c_ncfile)

	  END IF data_read

!---------- decide, if counter needs to be updated

      update_nccount: IF(r_ncupdatelast <= r_tim) THEN
        i_stepcounter= i_stepcounter+1
        IF(i_stepcounter > i_timesteps) &
          CALL grid_error(c_error='[slm_windfield]: step counter overflow!')
        r_ncupdatelast= r_ncupdatelast+ r_secperstep
      END IF update_nccount

!---------- interpolate to coordinate

	  r_field= data_interpol(r_coord,i_stepcounter)

	  RETURN
 1000	  FORMAT(i3.3)
	  END FUNCTION slm_windfield

!*****************************************************************
	  SUBROUTINE read_netcdf_currents(c_filename)

!---------- local declarations

	  IMPLICIT NONE
      INCLUDE "netcdf.inc"
      
!---------- input parameters      
	  CHARACTER (LEN=192), INTENT(in)       :: c_filename
	  
!---------- local variables
	  INTEGER (KIND = GRID_SI)              :: i_alct, i_ncstat
      INTEGER (KIND = GRID_SI)              :: i_fileid
      INTEGER (KIND = GRID_SI)              :: i_dimid, i_varid
      INTEGER (KIND = GRID_SI)              :: i_tm, i_ln, i_lt, i_tmaux
      INTEGER*4, DIMENSION(:,:,:), ALLOCATABLE :: i_u, i_v
	  REAL, DIMENSION(:), ALLOCATABLE       :: r_auxlat
	  REAL, DIMENSION(:), ALLOCATABLE       :: r_auxlon

!---------- open current file

      i_ncstat= nf_open(trim(c_filename),NF_NOWRITE,i_fileid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not open currents data file')
    
!---------- determine lon/lat/time dimension sizes (grid size of currents field)

      i_ncstat= nf_inq_dimid(i_fileid, 'lon', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify lon dimension')
      i_ncstat= nf_inq_dimlen(i_fileid, i_dimid, i_lon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lon dimension')
      
      i_ncstat= nf_inq_dimid(i_fileid, 'lat', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify lat dimension')
      i_ncstat= nf_inq_dimlen(i_fileid, i_dimid, i_lat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lat dimension')

      i_ncstat= nf_inq_dimid(i_fileid, 'time', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify time dimension')
      i_ncstat= nf_inq_dimlen(i_fileid, i_dimid, i_tmaux)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read time dimension')
      IF(i_tmaux .NE. i_timesteps) &
        CALL grid_error(c_error='[read_netcdf_currents]: incompatible time step count')

!---------- allocate latitude and longitude coordinate arrays
!           NOTE: if lat or lon are allocated, we do not need to read
!                 them again, since we expect to have same sized data files!

	  latlon: IF((.NOT. ALLOCATED(r_lat)) .OR. (.NOT. ALLOCATED(r_lon))) THEN
	  ALLOCATE(r_lat(i_lat), r_lon(i_lon), r_auxlat(i_lat), r_auxlon(i_lon), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not allocate lat/lon field')

!---------- read latitude and longitude coordinate values

      i_ncstat= nf_inq_varid(i_fileid, 'lon', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine lon varid')
      i_ncstat= nf_get_var_real(i_fileid, i_varid, r_auxlon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lon data')
      r_lon= REAL(r_auxlon, GRID_SR)

      i_ncstat= nf_inq_varid(i_fileid, 'lat', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine lat varid')
      i_ncstat= nf_get_var_real(i_fileid, i_varid, r_auxlat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lat data')
      r_lat= REAL(r_auxlat, GRID_SR)
      DEALLOCATE(r_auxlat, r_auxlon)
      END IF latlon

!---------- allocate current data arrays
	  IF((.NOT. ALLOCATED(r_flowx)) .OR. (.NOT. ALLOCATED(r_flowy))) THEN
        ALLOCATE(r_flowx(i_lon, i_lat, i_timesteps), r_flowy(i_lon, i_lat, i_timesteps), stat= i_alct)
        IF(i_alct /= 0) &
	      CALL grid_error(c_error='[read_netcdf_currents]: could not allocate currents fields')
      END IF
	  ALLOCATE(i_u(i_lon, i_lat, i_timesteps), i_v(i_lon, i_lat, i_timesteps), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not allocate local currents fields')

!---------- read x-/y-direction data of currents

      i_ncstat= nf_inq_varid(i_fileid, 'u', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine varid of var49')
      i_ncstat= nf_get_var_int(i_fileid, i_varid, i_u)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read var49 data')

      i_ncstat= nf_inq_varid(i_fileid, 'v', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine varid of var50')
      i_ncstat= nf_get_var_int(i_fileid, i_varid, i_v)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read var50 data')

!---------- Fix mask values

	  DO i_tm=1,i_timesteps
	  	DO i_lt= 1,i_lat
	  	  DO i_ln= 1,i_lon
	  	    IF(i_u(i_ln,i_lt,i_tm) == -2147483647) THEN
	  	      r_flowx(i_ln,i_lt,i_tm)= 0._GRID_SR
	  	    ELSE
	  	      r_flowx(i_ln,i_lt,i_tm)= REAL(i_u(i_ln,i_lt,i_tm),GRID_SR)* 0.0001_GRID_SR
	  	    END IF
	  	    IF(i_v(i_ln,i_lt,i_tm) == -2147483647) THEN
	  	      r_flowy(i_ln,i_lt,i_tm)= 0._GRID_SR
	  	    ELSE
	  	      r_flowy(i_ln,i_lt,i_tm)= REAL(i_v(i_ln,i_lt,i_tm),GRID_SR)* 0.0001_GRID_SR
	  	    END IF
	  	  END DO
	  	END DO
	  END DO
	  
!---------- deallocate local arrays

      DEALLOCATE(i_u,i_v)

!---------- close currents file

	  i_ncstat= nf_close(i_fileid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not close currents data file')

	  END SUBROUTINE read_netcdf_currents

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct)     :: p_control
	  INTEGER                   :: i_iofil= 19
	  CHARACTER (len=io_fillen+i_charlength) :: c_name
	  CHARACTER (len=128)       :: a_filrow
	  INTEGER                   :: i_iost, i_ioend, i_alct, i_cnt

!---------- initialize

	  WRITE(c_name,*) trim(p_param%cmd%c_directory),p_control%tst%tst_char(1)
	  c_name= adjustl(c_name)
	  c_windpath= p_param%cmd%c_directory
      i_numfiles= -1

!---------- open wind parameter file

	  open(unit= i_iofil, file= c_name, status= 'OLD', action= 'READ', iostat= i_iost)
	  file_notopen: IF(i_iost /= 0) THEN
	    write(i_ioerr,*) 'ERROR: Filename: ', c_name
	    IF(GRID_parameters%iolog > 0) &
	      write(GRID_parameters%iolog,*) 'ERROR: Filename: ', c_name
	    CALL grid_error(c_error='[slm_windinit]: could not open wind parameters file')
	  END IF file_notopen
	  IF(GRID_parameters%iolog > 0) &
	    write(GRID_parameters%iolog,*) 'INFO: Opened file on unit: ', i_iofil

!---------- read wind parameter file

	  read_loop: DO
	    read(i_iofil,2000,iostat=i_ioend) a_filrow

!---------- if file ended

	    file_end: IF(i_ioend /= 0) THEN
	      close(i_iofil)
	      IF(GRID_parameters%iolog > 0) &
	        write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_iofil
	      EXIT read_loop
	    ELSE file_end

!---------- decide what to DO with line according to first character

	    comment_line: IF(a_filrow(1:1) == '#' .or. a_filrow(1:1) == '!') THEN
	      CONTINUE
	    ELSE IF(a_filrow(1:8) == 'ABS_PATH') THEN comment_line
	      read(i_iofil,2000) c_windpath
	    ELSE IF(a_filrow(1:8) == 'SEC_PER_') THEN comment_line
	      read(i_iofil,*) r_secperstep
	    ELSE IF(a_filrow(1:8) == 'STEPS_PE') THEN comment_line
	      read(i_iofil,*) i_timesteps
	    ELSE IF(a_filrow(1:8) == 'INFILE_N') THEN comment_line
	      read(i_iofil,*) i_numfiles
          ALLOCATE(c_windfile(i_numfiles), stat=i_alct)
          IF(i_alct /= 0) &
            CALL grid_error(c_error='[slm_windinit]: could not allocate windname array')
	    ELSE IF(a_filrow(1:8) == 'FILE_LIS') THEN comment_line
	      IF(i_numfiles < 1) &
	        CALL grid_error(c_error='[slm_windinit]: number of wind files unknown')
          DO i_cnt=1,i_numfiles
            c_windfile(i_cnt)= '                                                                '
	        read(i_iofil,2010) c_windfile(i_cnt)
	      END DO
	    END IF comment_line

	    END IF file_end
	  END DO read_loop

!---------- initialize some values

	  r_readlast     = 0.0_GRID_SR
	  r_ncupdatelast = 0.0_GRID_SR
	  i_timeinterval = 0_GRID_SI
	  i_stepcounter  = 0_GRID_SI
	  r_intervallen  = r_secperstep* i_timesteps

	  RETURN
 2000 FORMAT(a128)
 2010 FORMAT(a64)
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

!---------- deallocate wind data arrays

	  DEALLOCATE(r_flowx, r_flowy)
	  DEALLOCATE(r_lon, r_lat)

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
	  FUNCTION data_interpol(r_coord, i_tim) RESULT (r_inter)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_coord
	  INTEGER (KIND = GRID_SI)                         :: i_tim
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_inter
	  INTEGER (KIND = GRID_SI)                         :: i_lox, i_hix, i_loy, i_hiy, i_cnt
	  REAL (KIND = GRID_SR)                            :: r_dx, r_dy, r_lx, r_hx, &
	    r_ly, r_hy, r_l1, r_h1, r_l2, r_h2, r_dxi, r_dyi
	  REAL (KIND = GRID_SR)                            :: r_scalx, r_scaly
	  REAL (KIND = GRID_SR)                            :: r_deg, r_radians
	  REAL (KIND = GRID_SR), PARAMETER                 :: r_earth=6.371e6_GRID_SR ! earth's radius

!---------- initialize radians

	  r_radians= GRID_PI/180._GRID_SR
	  r_deg= r_radians* r_earth

!---------- find wind box corresponding to coordinate

	  i_lox=1_GRID_SI
	  i_hix=2_GRID_SI
	  i_loy=1_GRID_SI
	  i_hiy=2_GRID_SI

	  determine_lon: DO i_cnt=1, i_lon-1
	    IF(r_lon(i_cnt) .LT. r_coord(1)) THEN
	      IF(r_lon(i_cnt +1) .GE. r_coord(1)) THEN
	        i_lox= i_cnt
	        i_hix= i_cnt+1
	        exit determine_lon
	      END IF
	    END IF
	  END DO determine_lon
	  IF(r_lon(i_lon) .LT. r_coord(1)) THEN
	    i_lox = i_lon-1
	    i_hix = i_lon
	  END IF

	  determine_lat: DO i_cnt=1, i_lat-1
	    IF(r_lat(i_cnt) .LT. r_coord(2)) THEN
	      IF(r_lat(i_cnt +1) .GE. r_coord(2)) THEN
	        i_loy= i_cnt
	        i_hiy= i_cnt+1
	        exit determine_lat
	      END IF
	    END IF
	  END DO determine_lat
	  IF(r_lat(i_lat) .LT. r_coord(2)) THEN
	    i_loy = i_lat-1
	    i_hiy = i_lat
	  END IF
	  

!---------- calculate weights for bilinear interpolation

	  r_dx= r_lon(i_hix)- r_lon(i_lox)
	  r_dy= r_lat(i_hiy)- r_lat(i_loy)
	  r_lx= r_coord(1) - r_lon(i_lox)
	  r_ly= r_coord(2) - r_lat(i_loy)
	  r_hx= r_lon(i_hix) - r_coord(1)
	  r_hy= r_lat(i_hiy) - r_coord(2)
	  r_scalx=1._GRID_SR/(r_deg*cos(r_radians*r_lat(i_hiy))) !r_coord(2)))
	  r_scaly=1._GRID_SR/(r_deg)

!---------- linear interpolation in x-direction

	  IF(r_dx /= 0.0_GRID_SR) THEN
	    r_dxi= 1._GRID_SR/r_dx
	    r_l1= (r_hx* r_flowx(i_lox, i_loy, i_tim)+ &
	           r_lx* r_flowx(i_hix, i_loy, i_tim))* r_dxi
	    r_h1= (r_hx* r_flowx(i_lox, i_hiy, i_tim)+ &
	           r_lx* r_flowx(i_hix, i_hiy, i_tim))* r_dxi
	    r_l2= (r_hx* r_flowy(i_lox, i_loy, i_tim)+ &
	           r_lx* r_flowy(i_hix, i_loy, i_tim))* r_dxi
	    r_h2= (r_hx* r_flowy(i_lox, i_hiy, i_tim)+ &
	           r_lx* r_flowy(i_hix, i_hiy, i_tim))* r_dxi
	  ELSE
	    r_l1= r_flowx(i_lox, i_loy, i_tim)
	    r_h1= r_flowx(i_lox, i_hiy, i_tim)
	    r_l2= r_flowy(i_lox, i_loy, i_tim)
	    r_h2= r_flowy(i_lox, i_hiy, i_tim)
	  END IF

!---------- linear interpolation in y-direction

	  IF(r_dy /= 0.0_GRID_SR) THEN
	    r_dyi= 1._GRID_SR/r_dy
	    r_inter(1)= (r_hy* r_l1+ r_ly* r_h1)* r_dyi* r_scalx
	    r_inter(2)= (r_hy* r_l2+ r_ly* r_h2)* r_dyi* r_scaly
	  ELSE
	    r_inter(1)= r_l1* r_scalx
	    r_inter(2)= r_l2* r_scaly
	  END IF

	  RETURN
	  END FUNCTION data_interpol

!*****************************************************************
	END MODULE ADV_wind
