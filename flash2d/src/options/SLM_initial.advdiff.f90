!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize slotted cylinder for semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initslot
! FUNCTION:
!	initialize a grid with values of slotted cylinder test case
! SYNTAX:
!	CALL slm_initslot(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initcylndr
! FUNCTION:
!	initialize a grid with values of a cylinder test case
! SYNTAX:
!	CALL slm_initcylndr(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_centr: coordinates of cyl. centre	REAL
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	in_side
! FUNCTION:
!	checks, if a given point (x,y) lies within a given triangle
! SYNTAX:
!	logical= in_side(real.arr, real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord: coordinate array		REAL
!	r_node1: node1 of triangle		REAL
!	r_node2: node2 of triangle		REAL
!	r_node3: node3 of triangle		REAL
! ON OUTPUT:
!	l_in:    .true. if r_coord \in p_elem	logical
! CALLS:
!
! COMMENTS:
!	this routine decides whether a point lies in or out of a
!	triangle. this is done with the following approach:
!	calculate the area for the given triangle and for the 
!	three triangles resulting from drawing lines from the given
!	point to all three triangle nodes.
!	if the sum of the areas of those three trianlges exceeds
!	the area of the given trianlge, the the point lies outside
!	of it.
!	for calculation of the areas following formula is used:
!	(Herons formula(?))
!
!	A = sqrt(s* (s- a)* (s- b)* (s- c)),
!
!	where s= 1/2* (a+ b+ c)
!	      a, b, c sides.
!
!	in order to calculate the sidelengths |x-y|= sqrt(x**2-y**2)
!	the complex absolute value intrinsic function is used.
!	hopefully this fuction is faster than using sqrt and
!	power-of-two instead.
!
!	internally double precision is used in this function, because
!	machine precision is crucial when a coordinate pair appears
!	near the edge or corner of an element.
!
!-----------------------------------------------------------------
!
! NAME:
!	dc_area
! FUNCTION:
!	calculate area of a triangle (in a plain) in double precision
! SYNTAX:
!	real= dc_area(real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord1: node1 of triangle			REAL
!	r_coord2: node2 of triangle			REAL
!	r_coord3: node3 of triangle			REAL
! ON OUTPUT:
!	r_area:   area of triangle			REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	7/97
!	2. names changed		j. behrens	7/97
!	3. changed to use GRID_api	j. behrens	11/97
!	4. changed interfaces		j. behrens	12/97
!	5. compliant to amatos 1.0	j. behrens	12/2000
!	6. compliant to amatos 1.2	j. behrens	3/2002
!	7. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
!	  USE mkl_vml
      include "mkl_vml.f90"

!#ifdef USE_MKL
!      INTERFACE erf ! define universal interface for error function
!        MODULE PROCEDURE vserf, vderf
!      END INTERFACE
!#endif
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_cntr=(/ 4., 1. /)
	  REAL (KIND = GRID_SR)                            :: r_onethird=1./3.
	  REAL (KIND = GRID_SR)                            :: r_twothird=2./3.
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand
	  INTEGER                                    :: i_lev= 6

!---------- initialize diffusion coefficient

      p_contr%tst%tst_real(1,1)= 0.01_GRID_SR

      CALL initerf(p_ghand)

	  RETURN
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array, l_advective)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)              :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_time
	  INTEGER, INTENT(in)                         :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(inout)     :: r_array
	  LOGICAL, OPTIONAL                                            :: l_advective
	  REAL (KIND = GRID_SR)                                        :: r_fac=.363610260832151995e-4
	  REAL (KIND = GRID_SR)                                        :: r_rds, r_ras, r_rad, &
	    r_ads, r_bds, r_cds, r_f1, r_f2, r_xya, r_dpt, r_diffuse
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_fc, r_p1, r_p2, &
	    r_pa, r_pb, r_pc, r_pd, r_tmp, r_centr
	  INTEGER                                     :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE           :: r_coo
	  LOGICAL                                     :: l_aux

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  IF(i_arlen /= i_num) THEN
	    CALL grid_error(52)
	  END IF

	  ALLOCATE(r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(53)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- if advective part is included then we need to correct the position

      advect_true: IF(present(l_advective)) THEN
        l_aux= l_advective
      ELSE advect_true
        l_aux= .TRUE.
      END IF advect_true
      IF (l_aux) &
        r_coo(1,:)= r_coo(1,:)-p_contr%tst%tst_real(1,2)*(r_time) !-3._GRID_SR)

!---------- compute values for for error function

      r_diffuse= p_contr%tst%tst_real(1,1)
      CALL compute_erf(i_num, r_time, r_diffuse, r_coo, r_array)

!---------- deallocate workspace

	  DEALLOCATE(r_coo)

	  RETURN
	  END SUBROUTINE slm_analyticsolution

!*****************************************************************
	  SUBROUTINE initerf(p_ghand, l_initialize)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                     :: p_ghand
	  LOGICAL, OPTIONAL                                  :: l_initialize

	  INTEGER                                            :: i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  REAL (KIND = GRID_SR)                              :: r_time, r_diffuse
	  INTEGER, DIMENSION(1)                              :: i_valind

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(54)
	  END IF

!---------- initialize values to zero if requested
	  
	  IF(present(l_initialize) .AND. l_initialize) r_aux= 0._GRID_SR

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- compute values for slotted cylinder

      r_time    = 0._GRID_SR
      r_diffuse = p_contr%tst%tst_real(1,1)
      CALL compute_erf(i_num, r_time, r_diffuse, r_coo, r_aux(1,:))

!---------- update grid information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE initerf

!*****************************************************************
      SUBROUTINE compute_erf(i_num, r_time, r_diff, r_coo, r_aux)

!---------- local declarations

      IMPLICIT NONE

      INTEGER                                          :: i_num
      REAL (KIND = GRID_SR)                            :: r_time
      REAL (KIND = GRID_SR)                            :: r_diff
      REAL (KIND = GRID_SR), DIMENSION(:)              :: r_aux
      REAL (KIND = GRID_SR), DIMENSION(:,:)            :: r_coo

      DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE      :: r_aux1, r_aux2
	  INTEGER                                          :: i_alct
	  REAL (KIND = GRID_SR)                            :: r_denom

!---------- allocate additional memory

      ALLOCATE(r_aux1(i_num), r_aux2(i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(c_error='[compute_erf] could not allocate aux mem.')
	  END IF

!---------- compute auxiliary values...

      r_denom= 1._GRID_SR/SQRT(4._GRID_SR*r_diff* (1._GRID_SR+ r_time))

!---------- compute a hump from error function

      r_aux1(1:i_num)= DBLE((.75_GRID_SR- ABS(r_cntr(1)-r_coo(1,1:i_num)))* r_denom)
      CALL vderf(i_num, r_aux1, r_aux2)
      r_aux= REAL(r_aux2,GRID_SR)
      r_aux1(1:i_num)= DBLE((.75_GRID_SR+ ABS(r_cntr(1)-r_coo(1,1:i_num)))* r_denom)
      CALL vderf(i_num, r_aux1, r_aux2)
      r_aux= (r_aux+ REAL(r_aux2,GRID_SR))* 0.5_GRID_SR
	
!---------- deallocate auxiliary arrays

      DEALLOCATE(r_aux1, r_aux2)

	  RETURN
	  END SUBROUTINE compute_erf

	END MODULE SLM_initial
