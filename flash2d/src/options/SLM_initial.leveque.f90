!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize slotted cylinder for semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initslot
! FUNCTION:
!	initialize a grid with values of slotted cylinder test case
! SYNTAX:
!	CALL slm_initslot(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initcylndr
! FUNCTION:
!	initialize a grid with values of a cylinder test case
! SYNTAX:
!	CALL slm_initcylndr(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_centr: coordinates of cyl. centre	REAL
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	in_side
! FUNCTION:
!	checks, if a given point (x,y) lies within a given triangle
! SYNTAX:
!	logical= in_side(real.arr, real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord: coordinate array		REAL
!	r_node1: node1 of triangle		REAL
!	r_node2: node2 of triangle		REAL
!	r_node3: node3 of triangle		REAL
! ON OUTPUT:
!	l_in:    .true. if r_coord \in p_elem	logical
! CALLS:
!
! COMMENTS:
!	this routine decides whether a point lies in or out of a
!	triangle. this is done with the following approach:
!	calculate the area for the given triangle and for the 
!	three triangles resulting from drawing lines from the given
!	point to all three triangle nodes.
!	if the sum of the areas of those three trianlges exceeds
!	the area of the given trianlge, the the point lies outside
!	of it.
!	for calculation of the areas following formula is used:
!	(Herons formula(?))
!
!	A = sqrt(s* (s- a)* (s- b)* (s- c)),
!
!	where s= 1/2* (a+ b+ c)
!	      a, b, c sides.
!
!	in order to calculate the sidelengths |x-y|= sqrt(x**2-y**2)
!	the complex absolute value intrinsic function is used.
!	hopefully this fuction is faster than using sqrt and
!	power-of-two instead.
!
!	internally double precision is used in this function, because
!	machine precision is crucial when a coordinate pair appears
!	near the edge or corner of an element.
!
!-----------------------------------------------------------------
!
! NAME:
!	dc_area
! FUNCTION:
!	calculate area of a triangle (in a plain) in double precision
! SYNTAX:
!	real= dc_area(real.arr, real.arr, real.arr)
! ON INPUT:
!	r_coord1: node1 of triangle			REAL
!	r_coord2: node2 of triangle			REAL
!	r_coord3: node3 of triangle			REAL
! ON OUTPUT:
!	r_area:   area of triangle			REAL
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	7/97
!	2. names changed		j. behrens	7/97
!	3. changed to use GRID_api	j. behrens	11/97
!	4. changed interfaces		j. behrens	12/97
!	5. compliant to amatos 1.0	j. behrens	12/2000
!	6. compliant to amatos 1.2	j. behrens	3/2002
!	7. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_cntr=(/ 0.5, 0.75 /)
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_hcntr=(/ 0.25, 0.5 /)
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_ccntr=(/ 0.5, 0.25 /)
	  REAL (KIND = GRID_SR)                            :: r_hgt=1.0
	  REAL (KIND = GRID_SR)                            :: r_srd=0.15
	  REAL (KIND = GRID_SR)                            :: r_sln=0.22
	  REAL (KIND = GRID_SR)                            :: r_swd=0.06
	  REAL (KIND = GRID_SR)                            :: r_onethird=1./3.
	  REAL (KIND = GRID_SR)                            :: r_twothird=2./3.
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand
	  INTEGER                                    :: i_lev= 6

!---------- initialize some constant for the slotted cylinder

	  cyl_slot: IF(p_ghand%i_maxlvl < i_lev) THEN
	    CALL slm_initcylndr(p_ghand)
	    CALL slm_inithump(p_ghand)
	    CALL slm_initcone(p_ghand)
	  ELSE cyl_slot
	    CALL slm_initslot(p_ghand)
	    CALL slm_inithump(p_ghand)
	    CALL slm_initcone(p_ghand)
	  END IF cyl_slot

	  RETURN
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)              :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_time
	  INTEGER, INTENT(in)                         :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(inout)     :: r_array
	  REAL (KIND = GRID_SR)                                        :: r_fac=.363610260832151995e-4
	  REAL (KIND = GRID_SR)                                        :: r_rds, r_ras, r_rad, &
	    r_ads, r_bds, r_cds, r_f1, r_f2, r_xya, r_dpt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_fc, r_p1, r_p2, &
	    r_pa, r_pb, r_pc, r_pd, r_tmp, r_centr
	  INTEGER                                     :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE           :: r_coo
	  LOGICAL                                     :: l_aux

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  IF(i_arlen /= i_num) THEN
	    CALL grid_error(52)
	  END IF

	  ALLOCATE(r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(53)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)


!---------- compute values for slotted cylinder

      CALL compute_slot(i_num, r_coo, r_array)

!---------- compute values for Gaussian hump

      CALL compute_hump(i_num, r_coo, r_array)

!---------- compute values for cone

      CALL compute_cone(i_num, r_coo, r_array)

!---------- deallocate workspace

	  DEALLOCATE(r_coo)

	  RETURN
	  END SUBROUTINE slm_analyticsolution

!*****************************************************************
	  FUNCTION dc_area(r_coord1, r_coord2, r_coord3) RESULT (r_area)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord1, r_coord2, r_coord3
	  REAL (KIND = GRID_DR)                            :: r_area
	  REAL (KIND = GRID_DR)                            :: r_c
	  REAL (KIND = GRID_DR), DIMENSION(GRID_dimension) :: r_a, r_b
	  INTEGER                                     :: i_cnt

!---------- calculate vector components

	  dim_loop: DO i_cnt=1, GRID_dimension
	    r_a(i_cnt)= REAL((r_coord2(i_cnt)- r_coord1(i_cnt)),GRID_DR)
	    r_b(i_cnt)= REAL((r_coord3(i_cnt)- r_coord1(i_cnt)),GRID_DR)
	  END DO dim_loop

!---------- calculate components (a,b,c) of cross product vector

	  r_c= (r_a(1)* r_b(2)- r_a(2)* r_b(1))

!---------- calculate area

	  r_area= abs(r_c)* 0.5_GRID_DR

	  RETURN
	  END FUNCTION dc_area

!*****************************************************************
	  FUNCTION in_side(r_coord, r_node1, r_node2, r_node3) RESULT (l_in)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_node1, r_node2, r_node3
	  LOGICAL                                     :: l_in
	  REAL (KIND = GRID_DR)                            :: r_sum, r_f, r_f1, r_f2, r_f3
	  REAL (KIND = GRID_DR)                            :: r_eps

!---------- set in_side value to out

 	  l_in= .FALSE.

!---------- set epsilon (machine precision)

	  r_eps= GRID_eps

!---------- calculate areas (f, f1, f2, f3)

	  r_f = dc_area(r_node1, r_node2, r_node3)
	  r_f1= dc_area(r_coord, r_node2, r_node3)
	  r_f2= dc_area(r_coord, r_node1, r_node2)
	  r_f3= dc_area(r_coord, r_node1, r_node3)

!---------- check if summed area is bigger than triangle area

	  r_sum= r_f1+ r_f2+ r_f3
	  if((r_sum- r_f) < r_eps) l_in= .TRUE.

	  RETURN
	  END FUNCTION in_side

!*****************************************************************
	  SUBROUTINE slm_initslot(p_ghand, l_initialize)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                     :: p_ghand
	  LOGICAL, OPTIONAL                                  :: l_initialize

	  INTEGER                                            :: i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  INTEGER, DIMENSION(1)                              :: i_valind

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(54)
	  END IF

!---------- initialize values to zero if requested
	  
	  IF(present(l_initialize) .AND. l_initialize) r_aux= 0._GRID_SR

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- compute values for slotted cylinder

      CALL compute_slot(i_num, r_coo, r_aux(1,:))

!---------- update grid information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_initslot

!*****************************************************************
	  SUBROUTINE compute_slot(i_num, r_coo, r_aux)

!---------- local declarations

	  IMPLICIT NONE

	  INTEGER                                          :: i_num
	  REAL (KIND = GRID_SR), DIMENSION(:)              :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:)            :: r_coo

	  INTEGER                                          :: i_count
	  REAL (KIND = GRID_SR)                            :: r_rds, r_ras, r_rad, &
	    r_dpt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_tmp, &
	    r_nw, r_sw, r_ne, r_se, r_centr
	  LOGICAL                                          :: l_aux

!---------- initialize some constants for the slotted cylinder

	  r_centr= r_cntr
	  r_rds= r_srd* r_srd
	  r_ras= dot_product(r_centr, r_centr)
	  r_rad= sqrt(r_ras)
	  
	  r_nw= r_centr + (/ -r_srd*r_onethird , r_srd*r_twothird /)
	  r_se= r_centr + (/ r_srd*r_onethird , -r_srd*2 /)
	  r_ne= r_centr + (/ r_srd*r_onethird , r_srd*r_twothird /)
	  r_sw= r_centr + (/ -r_srd*r_onethird , -r_srd*2 /)
	
!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_aux(i_count)= 0.0_GRID_SR
	    r_tmp(:)      = r_coo(:,i_count)- r_centr(:)
	    r_dpt= dot_product(r_tmp, r_tmp)
	    inside: IF(r_dpt < r_rds) THEN
	      r_aux(i_count)= r_hgt
	      l_aux= in_side(r_coo(:,i_count), r_nw, r_sw, r_se)
	      IF(l_aux) r_aux(i_count)= 0.0
	      l_aux= in_side(r_coo(:,i_count), r_nw, r_se, r_ne)
	      IF(l_aux) r_aux(i_count)= 0.0
	    END IF inside
	  END DO node_loop

	  RETURN
	  END SUBROUTINE compute_slot

!*****************************************************************
	  SUBROUTINE slm_initcylndr(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)            :: r_centr
	  REAL (KIND = GRID_SR)                                       :: r_rds, r_dpt
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)            :: r_tmp
	  INTEGER                                    :: i_count, i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE          :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE          :: r_coo
	  INTEGER, DIMENSION(1)                       :: i_valind

!---------- initialize some constant for the slotted cylinder

	  r_rds= r_srd* r_srd
	  r_centr= r_cntr

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(55)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_aux(1,i_count)= 0.0_GRID_SR
	    r_tmp(:)      = r_coo(:,i_count)- r_centr(:)
	    r_dpt= dot_product(r_tmp, r_tmp)
	    inside: IF(r_dpt < r_rds) THEN
	      r_aux(1,i_count)= r_hgt
	    END IF inside
	  END DO node_loop

!---------- update grid information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_initcylndr
!*****************************************************************
	  SUBROUTINE slm_inithump(p_ghand, l_initialize)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                     :: p_ghand
	  LOGICAL, OPTIONAL                                  :: l_initialize

	  INTEGER                                            :: i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  INTEGER, DIMENSION(1)                              :: i_valind


!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(55)
	  END IF

!---------- get information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_getinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux, &
	    r_nodecoordinates= r_coo)

!---------- initialize values to zero if requested

	  IF(present(l_initialize) .AND. l_initialize) r_aux= 0._GRID_SR

!---------- compute values for Gaussian hump

      CALL compute_hump(i_num, r_coo, r_aux(1,:))

!---------- update grid information

	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_inithump
!*****************************************************************
	  SUBROUTINE compute_hump(i_num, r_coo, r_aux)

!---------- local declarations

	  IMPLICIT NONE

	  INTEGER                                          :: i_num
	  REAL (KIND = GRID_SR), DIMENSION(:)              :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:)            :: r_coo

	  INTEGER                                          :: i_count
	  REAL (KIND = GRID_SR)                            :: r_rds, r_dpt, r_rad, r_pr
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_centr, r_tmp

!---------- initialize some constant for the slotted cylinder

	  r_rds= r_srd* r_srd
	  r_centr= r_hcntr
	
!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_tmp(:)      = r_coo(:,i_count)- r_centr(:)
	    r_dpt= dot_product(r_tmp, r_tmp)
	    inside: IF(r_dpt < r_rds) THEN
	      r_rad= min(sqrt(r_dpt),r_srd)/r_srd
	      r_pr= GRID_PI* r_rad
	      r_aux(i_count)= 0.25_GRID_SR* (1+ cos(r_pr))
	    END IF inside
	  END DO node_loop

	  RETURN
	  END SUBROUTINE compute_hump

!*****************************************************************
	  SUBROUTINE slm_initcone(p_ghand, l_initialize)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)                     :: p_ghand
	  LOGICAL, OPTIONAL                                  :: l_initialize

	  INTEGER                                            :: i_num, i_alct
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  INTEGER, DIMENSION(1)                              :: i_valind

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber
	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(55)
	  END IF

!---------- get information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_getinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux, &
	    r_nodecoordinates= r_coo)

!---------- initialize values to zero if requested
	  
	  IF(present(l_initialize) .AND. l_initialize) r_aux= 0._GRID_SR

!---------- compute values for cone

      CALL compute_cone(i_num, r_coo, r_aux(1,:))

!---------- update grid information

	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_initcone
!*****************************************************************
	  SUBROUTINE compute_cone(i_num, r_coo, r_aux)

!---------- local declarations

	  IMPLICIT NONE

	  INTEGER                                          :: i_num
	  REAL (KIND = GRID_SR), DIMENSION(:)              :: r_aux
	  REAL (KIND = GRID_SR), DIMENSION(:,:)            :: r_coo

	  INTEGER                                          :: i_count
	  REAL (KIND = GRID_SR)                            :: r_rds, r_dpt, r_rad, r_pr
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_centr, r_tmp

!---------- initialize some constant for the slotted cylinder

	  r_rds= r_srd* r_srd
	  r_centr= r_ccntr
	
!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_tmp(:)      = r_coo(:,i_count)- r_centr(:)
	    r_dpt= dot_product(r_tmp, r_tmp)
	    inside: IF(r_dpt < r_rds) THEN
	      r_rad= min(sqrt(r_dpt),r_srd)
	      r_aux(i_count)= -(r_hgt/r_srd)* r_rad + r_hgt
	    END IF inside
	  END DO node_loop

	  RETURN
	  END SUBROUTINE compute_cone

	END MODULE SLM_initial
