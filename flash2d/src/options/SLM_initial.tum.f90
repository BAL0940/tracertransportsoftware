!*****************************************************************
!
! MODULE NAME:
!	SLM_initial
! FUNCTION:
!	initialize slotted cylinder for semi-Lagrangian advection
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_initialvalues
! FUNCTION:
!	initialize a grid with values
! SYNTAX:
!	CALL slm_initialvalues(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_initbar
! FUNCTION:
!	initialize a grid with values of a bar
! SYNTAX:
!	CALL slm_initbar(grid)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! ON OUTPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! NAME:
!	slm_analyticsolution
! FUNCTION:
!	calculates the 'analytic solution' to compare with in diagnostics
! SYNTAX:
!	CALL slm_analyticsolution(grid, real, int, real.arr)
! ON INPUT:
!	p_ghand: grid handle			TYPE (grid_handle)
!	r_time:  model time			REAL
!	i_arlen: array length for values array	INTEGER
! ON OUTPUT:
!	r_array: values at gridpoints		REAL
! CALLS:
!
! COMMENTS:
!	the routine is made for two dimensions only
!
!-----------------------------------------------------------------
!
! PUBLIC:
!	slm_initialvalues, slm_analyticsolution
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error, GRID_api
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	7/97
!	2. names changed		j. behrens	7/97
!	3. changed to use GRID_api	j. behrens	11/97
!	4. changed interfaces		j. behrens	12/97
!	5. compliant to amatos 1.0	j. behrens	12/2000
!	6. compliant to amatos 1.2	j. behrens	3/2002
!	7. compliant to amatos 2.0	j. behrens	7/2003
!
!*****************************************************************
	MODULE SLM_initial
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC slm_initialvalues, slm_analyticsolution
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_cntr=(/ -0.25, 0.0 /)
	  REAL (KIND = GRID_SR)                            :: r_hgt=4.0
	  REAL (KIND = GRID_SR)                            :: r_srd=0.15
	  REAL (KIND = GRID_SR)                            :: r_sln=0.22
	  REAL (KIND = GRID_SR)                            :: r_swd=0.06
	  CONTAINS
!*****************************************************************
	  SUBROUTINE slm_initialvalues(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand

!---------- initialize some constant for the slotted cylinder

	  CALL slm_initbar(p_ghand)

	  RETURN
	  END SUBROUTINE slm_initialvalues
!*****************************************************************
	  SUBROUTINE slm_analyticsolution(p_ghand, r_time, i_arlen, r_array)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)              :: p_ghand
	  REAL (KIND = GRID_SR), INTENT(in)                            :: r_time
	  INTEGER, INTENT(in)                         :: i_arlen
	  REAL (KIND = GRID_SR), DIMENSION(i_arlen), INTENT(out)       :: r_array

!---------- dummy routine

	  r_array= 0.0_GRID_SR

	  RETURN
	  END SUBROUTINE slm_analyticsolution

!*****************************************************************
	  SUBROUTINE slm_initbar(p_ghand)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (grid_handle), INTENT(in)             :: p_ghand
	  INTEGER                                    :: i_count, i_num, i_alct
	  REAL (KIND = GRID_DR), DIMENSION(:,:), ALLOCATABLE :: r_aux
	  REAL (KIND = GRID_DR), DIMENSION(:,:), ALLOCATABLE :: r_coo
	  REAL (KIND = GRID_DR), DIMENSION(GRID_dimension)   :: p_1, p_2, p_3, p_4, &
	    p_5, p_6, p_7, p_8, p_9, p_10, p_11, p_12, p_13, p_14, p_15, p_16
	  INTEGER, DIMENSION(1)                      :: i_valind

!---------- initialize some constants for the Logo

	  p_1=  (/ -0.375_GRID_SR,  0.0375_GRID_SR /)
	  p_2=  (/  -0.35_GRID_SR, -0.0625_GRID_SR /)
	  p_3=  (/ -0.325_GRID_SR,  0.0375_GRID_SR /)
	  p_4=  (/   -0.3_GRID_SR, -0.0375_GRID_SR /)
	  p_5=  (/   -0.3_GRID_SR, -0.0625_GRID_SR /)
	  p_6=  (/ -0.275_GRID_SR,  0.0375_GRID_SR /)
	  p_7=  (/ -0.275_GRID_SR,  0.0625_GRID_SR /)
	  p_8=  (/  -0.25_GRID_SR,  0.0375_GRID_SR /)
	  p_9=  (/  -0.25_GRID_SR, -0.0375_GRID_SR /)
	  p_10= (/ -0.225_GRID_SR, -0.0375_GRID_SR /)
	  p_11= (/ -0.225_GRID_SR,  0.0375_GRID_SR /)
	  p_12= (/   -0.2_GRID_SR, -0.0625_GRID_SR /)
	  p_13= (/ -0.175_GRID_SR,  0.0375_GRID_SR /)
	  p_14= (/  -0.15_GRID_SR, -0.0625_GRID_SR /)
	  p_15= (/ -0.125_GRID_SR,  0.0375_GRID_SR /)
	  p_16= (/ -0.125_GRID_SR,  0.0625_GRID_SR /)

!---------- allocate workspace

	  i_num= p_ghand%i_nnumber

	  ALLOCATE(r_aux(1,i_num), r_coo(GRID_dimension,i_num), stat= i_alct)
	  IF(i_alct /= 0) THEN
	    CALL grid_error(51)
	  END IF

!---------- get information

	  CALL grid_getinfo(p_ghand, r_nodecoordinates= r_coo)

!---------- loop over the nodes

	  node_loop: DO i_count= 1, i_num
	    r_aux(1,i_count)= 0.0_GRID_SR
	    inside: IF(inoutrect(r_coo(:,i_count),p_1,p_7) .OR. &
	               inoutrect(r_coo(:,i_count),p_2,p_3) .OR. &
	               inoutrect(r_coo(:,i_count),p_4,p_6) .OR. &
	               inoutrect(r_coo(:,i_count),p_5,p_10) .OR. &
	               inoutrect(r_coo(:,i_count),p_9,p_11) .OR. &
	               inoutrect(r_coo(:,i_count),p_8,p_16) .OR. &
	               inoutrect(r_coo(:,i_count),p_12,p_13) .OR. &
	               inoutrect(r_coo(:,i_count),p_14,p_15)) THEN
	      r_aux(1,i_count)= r_hgt
	    END IF inside
	  END DO node_loop

!---------- update grid information

	  i_valind= (/ GRID_tracer /)
	  CALL grid_putinfo(p_ghand, i_arraypoint=i_valind, r_nodevalues= r_aux)

!---------- deallocate workspace

	  DEALLOCATE(r_aux, r_coo)

	  RETURN
	  END SUBROUTINE slm_initbar

!*****************************************************************
	  FUNCTION inoutrect(r_coo, r_sw, r_ne) RESULT (l_inout)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_DR), DIMENSION(GRID_dimension), INTENT(in) :: r_coo, r_sw, r_ne
	  LOGICAL                                     :: l_inout

!---------- initialize output

	  l_inout= .FALSE.

	  inside: IF((r_coo(1)>r_sw(1) .AND. r_coo(1)<r_ne(1)) .AND. &
	             (r_coo(2)>r_sw(2) .AND. r_coo(2)<r_ne(2))) THEN !! CAUTION 2D ONLY !!
	    l_inout= .TRUE.
	  END IF inside

	  END FUNCTION inoutrect

	END MODULE SLM_initial
