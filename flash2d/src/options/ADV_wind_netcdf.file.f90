!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam, MISC_error
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	12/97
!	2. bug fix concerning interp.	j. behrens	2/98
!	3. compliant to amatos 1.0	j. behrens	12/2000
!	4. compliant to amatos 1.2	j. behrens	3/2002
!	5. new version for course tracer transport	j. behrens 01/2012
!
!*****************************************************************
	MODULE ADV_wind
	  USE GRID_api
	  USE FLASH_parameters
	  PRIVATE
	  INTEGER, PARAMETER    :: i_ioerr=0
	  REAL (KIND = GRID_SR) :: r_intervallen, r_scalfacx, r_scalfacy, &
	                           r_readlast, r_scalinvx, r_scalinvy
	  CHARACTER (LEN=32)    :: c_prfixx, c_prfixy, c_pofixx, c_pofixy
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE :: r_flowx
	  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE :: r_flowy
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE     :: r_lat
	  REAL (KIND = GRID_SR), DIMENSION(:), ALLOCATABLE     :: r_lon
      INTEGER (KIND = GRID_SI)  :: i_lon, i_lat, i_timesteps
	  INTEGER (KIND = GRID_SI)  :: i_pointsx, i_pointsy, i_timeinterval, i_intervalnum
	  
	  PUBLIC                    :: slm_windfield, slm_windinit, slm_windquit
	  
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coord, r_time) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_field
	  REAL (KIND = GRID_SR)                       :: r_tim
	  CHARACTER (LEN=67)                          :: c_xfile, c_yfile
	  CHARACTER (LEN=35)                          :: c_tmp
	  CHARACTER (LEN=3)                           :: c_num
	  INTEGER (KIND = GRID_SI)                    :: i_iost, i_cnt, j_cnt

!---------- set time

	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- decide, if data has to be read

	  data_read: IF(r_readlast <= r_tim) THEN

!---------- update values for next open

	    r_readlast    = r_readlast+ r_intervallen
	    i_timeinterval= i_timeinterval+ 1
	  END IF data_read

!---------- interpolate to coordinate

	  r_field= data_interpol(r_coord,i_timeinterval)

	  RETURN
 1000	  FORMAT(i3.3)
	  END FUNCTION slm_windfield

!*****************************************************************
	  SUBROUTINE read_netcdf_currents(c_filename)

!---------- local declarations

	  IMPLICIT NONE
      INCLUDE "netcdf.inc"
      
!---------- input parameters      
	  CHARACTER (LEN=i_charlength+io_fillen), INTENT(in)  :: c_filename
	  
!---------- local variables
	  INTEGER (KIND = GRID_SI)        :: i_alct, i_ncstat
      INTEGER (KIND = GRID_SI)        :: i_fileid
      INTEGER (KIND = GRID_SI)        :: i_dimid, i_varid
      INTEGER (KIND = GRID_SI)        :: i_tm, i_ln, i_lt

!---------- open current file

      i_ncstat= nf_open(c_filename,NF_NOWRITE,i_fileid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not open currents data file')
    
!---------- determine lon/lat/time dimension sizes (grid size of currents field)

      i_ncstat= nf_inq_dimid(i_fileid, 'lon', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify lon dimension')
      i_ncstat= nf_inq_dimlen(i_fileid, i_dimid, i_lon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lon dimension')
      
      i_ncstat= nf_inq_dimid(i_fileid, 'lat', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify lat dimension')
      i_ncstat= nf_inq_dimlen(i_fileid, i_dimid, i_lat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lat dimension')

      i_ncstat= nf_inq_dimid(i_fileid, 'time', i_dimid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not identify time dimension')
      i_ncstat= nf_inq_dimlen(i_fileid, i_dimid, i_timesteps)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read time dimension')
    
!---------- allocate latitude and longitude coordinate arrays

	  ALLOCATE(r_lat(i_lat), r_lon(i_lon), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not allocate lat/lon field')

!---------- read latitude and longitude coordinate values

      i_ncstat= nf_inq_varid(i_fileid, 'lon', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine lon varid')
      i_ncstat= nf_get_var_real(i_fileid, i_varid, r_lon)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lon data')

      i_ncstat= nf_inq_varid(i_fileid, 'lat', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine lat varid')
      i_ncstat= nf_get_var_real(i_fileid, i_varid, r_lat)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read lat data')
    
!---------- allocate current data arrays

	  ALLOCATE(r_flowx(i_lon, i_lat, i_timesteps), r_flowy(i_lon, i_lat, i_timesteps), stat= i_alct)
      IF(i_alct /= 0) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not allocate currents fields')

!---------- read x-/y-direction data of currents

      i_ncstat= nf_inq_varid(i_fileid, 'var49', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine varid of var49')
      i_ncstat= nf_get_var_real(i_fileid, i_varid, r_flowx)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read var49 data')

      i_ncstat= nf_inq_varid(i_fileid, 'var50', i_varid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not determine varid of var50')
      i_ncstat= nf_get_var_real(i_fileid, i_varid, r_flowy)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not read var50 data')

!---------- Fix mask values

	  DO i_tm=1,i_timesteps
	  	DO i_lt= 1,i_lat
	  	  DO i_ln= 1,i_lon
	  	    IF(r_flowx(i_ln,i_lt,i_tm) <= -8.99E+33) r_flowx(i_ln,i_lt,i_tm)= 0._GRID_SR
	  	    IF(r_flowy(i_ln,i_lt,i_tm) <= -8.99E+33) r_flowy(i_ln,i_lt,i_tm)= 0._GRID_SR
	  	  END DO
	  	END DO
	  END DO

!---------- close currents file

	  i_ncstat= nf_close(i_fileid)
	  IF(i_ncstat /= NF_NOERR) &
	    CALL grid_error(c_error='[read_netcdf_currents]: could not close currents data file')

	  END SUBROUTINE read_netcdf_currents

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE

	  TYPE (control_struct)     :: p_control
	  INTEGER                   :: i_iofil= 19
	  CHARACTER (len=i_charlength+io_fillen) :: c_name
	  CHARACTER (len=80)        :: a_filrow
	  INTEGER                   :: i_iost, i_ioend, i_alct

!---------- initialize

	  WRITE(c_name,*) trim(p_param%cmd%c_directory),p_control%tst%tst_char(1)
	  c_name= adjustl(c_name)

!---------- read current data from NetCDF file

	  CALL read_netcdf_currents(c_name)

!---------- initialize some values

	  r_readlast= 0.0_GRID_SR
	  i_timeinterval= 0_GRID_SI
	  r_intervallen= 3600._GRID_SR

	  RETURN
 2000	  FORMAT(a80)
 2010	  FORMAT(a32)
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

!---------- deallocate wind data arrays

	  DEALLOCATE(r_flowx, r_flowy)
	  DEALLOCATE(r_lon, r_lat)

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
	  FUNCTION data_interpol(r_coord, i_tim) RESULT (r_inter)

!---------- local declarations

	  IMPLICIT NONE
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_coord
	  INTEGER (KIND = GRID_SI)                         :: i_tim
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension) :: r_inter
	  INTEGER (KIND = GRID_SI)                         :: i_lox, i_hix, i_loy, i_hiy, i_cnt
	  REAL (KIND = GRID_SR)                            :: r_dx, r_dy, r_lx, r_hx, &
	    r_ly, r_hy, r_l1, r_h1, r_l2, r_h2, r_dxi, r_dyi
	  REAL (KIND = GRID_SR)                            :: r_scalx, r_scaly
	  REAL (KIND = GRID_SR)                            :: r_deg
	  REAL (KIND = GRID_SR), PARAMETER                 :: r_earth=4011436.8_GRID_SR ! earth circumference
 	
!---------- initialize radians

	  r_deg= 360._GRID_SR/r_earth

!---------- find wind box corresponding to coordinate

	  i_lox=1_GRID_SI
	  i_hix=2_GRID_SI
	  i_loy=1_GRID_SI
	  i_hiy=2_GRID_SI

	  determine_lon: DO i_cnt=1, i_lon-1
	    IF(r_lon(i_cnt) .LT. r_coord(1)) THEN
	      IF(r_lon(i_cnt +1) .GE. r_coord(1)) THEN
	        i_lox= i_cnt
	        i_hix= i_cnt+1
	        exit determine_lon
	      END IF
	    END IF
	  END DO determine_lon
	  IF(r_lon(i_lon) .LT. r_coord(1)) THEN
	    i_lox = i_lon-1
	    i_hix = i_lon
	  END IF

	  determine_lat: DO i_cnt=1, i_lat-1
	    IF(r_lat(i_cnt) .LT. r_coord(2)) THEN
	      IF(r_lat(i_cnt +1) .GE. r_coord(2)) THEN
	        i_loy= i_cnt
	        i_hiy= i_cnt+1
	        exit determine_lat
	      END IF
	    END IF
	  END DO determine_lat
	  IF(r_lat(i_lat) .LT. r_coord(2)) THEN
	    i_loy = i_lat-1
	    i_hiy = i_lat
	  END IF
	  

!---------- calculate weights for bilinear interpolation

	  r_dx= r_lon(i_hix)- r_lon(i_lox)
	  r_dy= r_lat(i_hiy)- r_lat(i_loy)
	  r_lx= r_coord(1) - r_lon(i_lox)
	  r_ly= r_coord(2) - r_lat(i_loy)
	  r_hx= r_lon(i_hix) - r_coord(1)
	  r_hy= r_lat(i_hiy) - r_coord(2)
	  r_scalx=r_deg
	  r_scaly=r_deg*cos(r_coord(2))

!---------- linear interpolation in x-direction

	  IF(r_dx /= 0.0_GRID_SR) THEN
	    r_dxi= 1._GRID_SR/r_dx
	    r_l1= (r_hx* r_flowx(i_lox, i_loy, i_tim)+ &
	           r_lx* r_flowx(i_hix, i_loy, i_tim))* r_dxi
	    r_h1= (r_hx* r_flowx(i_lox, i_hiy, i_tim)+ &
	           r_lx* r_flowx(i_hix, i_hiy, i_tim))* r_dxi
	    r_l2= (r_hx* r_flowy(i_lox, i_loy, i_tim)+ &
	           r_lx* r_flowy(i_hix, i_loy, i_tim))* r_dxi
	    r_h2= (r_hx* r_flowy(i_lox, i_hiy, i_tim)+ &
	           r_lx* r_flowy(i_hix, i_hiy, i_tim))* r_dxi
	  ELSE
	    r_l1= r_flowx(i_lox, i_loy, i_tim)
	    r_h1= r_flowx(i_lox, i_hiy, i_tim)
	    r_l2= r_flowy(i_lox, i_loy, i_tim)
	    r_h2= r_flowy(i_lox, i_hiy, i_tim)
	  END IF

!---------- linear interpolation in y-direction

	  IF(r_dy /= 0.0_GRID_SR) THEN
	    r_dyi= 1._GRID_SR/r_dy
	    r_inter(1)= (r_hy* r_l1+ r_ly* r_h1)* r_dyi* r_scalx
	    r_inter(2)= (r_hy* r_l2+ r_ly* r_h2)* r_dyi* r_scaly
	  ELSE
	    r_inter(1)= r_l1* r_scalx
	    r_inter(2)= r_l2* r_scaly
	  END IF

	  RETURN
	  END FUNCTION data_interpol

!*****************************************************************
	END MODULE ADV_wind
