!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	12/97
!	2. compliant to amatos 1.0	j. behrens	12/2000
!	3. done some work for 2d	l. mentrup	2003
!
!*****************************************************************
	MODULE ADV_wind
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC :: slm_windfield, slm_windinit, slm_windquit
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coord, r_time) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                  :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_field

!---------- no wind!


	  r_field(:)=  0.

	
	  RETURN
	  END FUNCTION slm_windfield

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE
	  TYPE (control_struct)     :: p_control

	  RETURN
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
	END MODULE ADV_wind
