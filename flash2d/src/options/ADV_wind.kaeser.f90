!*****************************************************************
!
! MODULE NAME:
!	ADV_wind
! FUNCTION:
!	calculate the windfield for the advection problem
!	according to kaeser test case (cf. phd thesis of M. Kaeser)
!	but with modified coefficients (l. mentrup)
! CONTAINS:
!-----------------------------------------------------------------
!
! NAME:
!	slm_windfield
! FUNCTION:
!	calculate the advecting force for simple advection
! SYNTAX:
!	real.arr= slm_windfield(real.arr, real)
! ON INPUT:
!	r_coord: coordinates of point		real
!	r_time:  time coordinate (optional)	real
! ON OUTPUT:
!	r_field: windfield			real
! CALLS:
!
! COMMENTS:
!
!-----------------------------------------------------------------
!
! PUBLIC:
!
! COMMENTS:
!
! USES:
!	MISC_globalparam
! LIBRARIES:
!
! REFERENCES:
!
! VERSION(S):
!	1. original version		j. behrens	12/97
!	2. compliant to amatos 1.0	j. behrens	12/2000
!	3. kaeser test case implemented	l. mentrup	12/2003
!
!*****************************************************************
	MODULE ADV_wind
	  USE FLASH_parameters
	  USE GRID_api
	  PRIVATE
	  PUBLIC :: slm_windfield, slm_windinit, slm_windquit
	  CONTAINS
!*****************************************************************
	  FUNCTION slm_windfield(r_coord, r_time) RESULT (r_field)

!---------- local declarations

	  IMPLICIT NONE

	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in) :: r_coord
	  REAL (KIND = GRID_SR), INTENT(in), OPTIONAL                  :: r_time
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_field
	  REAL (KIND = GRID_SR)                                        :: r_fac=.363610260832151995e-4_GRID_SR
	  REAL (KIND = GRID_SR)                                        :: r_tim
	  REAL (KIND = GRID_SR)					      :: r_dist
	  REAL (KIND = GRID_SR)					      :: r_phi, r_sin
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), PARAMETER  :: r_cntr=(/ 0._GRID_SR, 0._GRID_SR /)
	  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)             :: r_tmp

!---------- set time

	  IF(present(r_time)) THEN
	    r_tim= r_time
	  ELSE
	    r_tim= 0.0_GRID_SR
	  END IF

!---------- calculate the advection at (x,y) 
!---------- circulating wind in x,y-plane with increasing velocity 
!---------- wrt. the distance to r_cntr
!---------- this test case corresponds to the kaeser test case with slightly other 
!---------- wind data, so that the tracer is circulated in 0,75 days

	  r_tmp= r_coord- r_cntr
	  IF (r_tmp(1) > 0._GRID_SR) THEN
	    IF (r_tmp(2) >= 0._GRID_SR) THEN
	      r_phi = ATAN(r_tmp(2)/r_tmp(1))
	      r_sin = 1.5_GRID_SR*COS(2_GRID_SR*r_phi) + 2.5_GRID_SR
	      
	      r_field(1) = -r_tmp(2)* r_sin * r_fac
	      r_field(2) =  r_tmp(1)* r_sin * r_fac
	    ELSE 
	      r_phi = ATAN(-1*r_tmp(2)/r_tmp(1))
	      r_sin = 1.5_GRID_SR*COS(2_GRID_SR*r_phi) + 2.5_GRID_SR
	      
	      r_field(1) = -r_tmp(2)* r_sin * r_fac
	      r_field(2) =  r_tmp(1)* r_sin * r_fac
	    END IF
	  ELSE 
	    r_field(1)= -r_tmp(2)* r_fac
	    r_field(2)=  r_tmp(1)* r_fac
	  END IF

	
	  RETURN
	  END FUNCTION slm_windfield

!*****************************************************************
	  SUBROUTINE slm_windinit(p_control)

!---------- local declarations

	  IMPLICIT NONE
	  TYPE (control_struct)     :: p_control

	  RETURN
	  END SUBROUTINE slm_windinit

!*****************************************************************
	  SUBROUTINE slm_windquit

!---------- local declarations

	  IMPLICIT NONE

	  RETURN
	  END SUBROUTINE slm_windquit

!*****************************************************************
	END MODULE ADV_wind
