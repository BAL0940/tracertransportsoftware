#ifndef FLASHMENU_H
#define FLASHMENU_H

#include "M3VisNETView.h"
#include "FlashData.h"
//#include "M3TV_Menu.h"
#include <semaphore.h>
#include <stdio.h>

class FlashMenu: public M3TV_MenuProvider{
public:
    FlashMenu(M3VisNETView *view, int dimension,
	      FlashData *fd1, FlashData *fd2,
	      M3TV_DoubleTriangleList *dtl);
    virtual  ~FlashMenu();

    void buildMenu(M3TV_Menu* menuRoot);
    void removeMenu(M3TV_Menu* menuRoot);
    void menuCalled(int command);

    void wait();
    
    int getIsoValue(double &value);
private:
    M3VisNETView *view_;
    int dim_;
    FlashData *fd1_, *fd2_;
    M3TV_DoubleTriangleList *dtl_;
    double isoValue_;
    int generateIso_;

    M3TV_Menu *flashMenu_, *isoMenu_;
    M3TV_MenuItem *startstop_, *step_, *shGrid_, *shVecs_,
      *miT_, *miE_, *miN_, *miIsoT_, *miIsoE_, *miSetValue_;
    int paused_;
    sem_t wait_;
};

#endif
