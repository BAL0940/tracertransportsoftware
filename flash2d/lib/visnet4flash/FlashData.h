#ifndef FLASHDATA_H
#define FLASHDATA_H

#include "M3TV_StdDataSource.h"
//#include "M3TV_Menu.h"
#include "VectorList.h"

class FlashData : public M3TV_TriangleList{
public:
  //enum NODE_VALUES {TRACER = 0, UCOMP = 1, VCOMP = 2, WCOMP = 3, NORM=4, ZCOORD=5};
  const static int TRACER = 0;
  const static int UCOMP = 1;
  const static int VCOMP = 2;
  static int       WCOMP;
  static int       NORM;
  const static int ZCOORD = 5;

    FlashData(int dim);
    FlashData(int dim,
	      int nn, int ne, int nt,
	      float *coords,
	      float *vals,
	      int *edges, int *triangles);
    ~FlashData();
    /*
    void setAutoMinMax(int b);
    int getAutoMinMax();
    */
    void setData(int nn, int ne, int nt,
		 float *coords,
		 float *vals,
		 int *edges, int *triangles);

    /**
     * Choose which of the nodal values (tracer, ucomp or vcomp) to use as z-values
     */
    void chooseZValues(int z);

    void setGridVisibility(int b);
    int getGridVisibility();

    void setVectorsVisibility(int b);
    int getVectorsVisibility();

    void setVectorsHeight(double h);
    double  getVectorsHeight();

    int getNodeValueGroupCount();
    int chooseNodeValueGroup(int index);
    int getEdgeValueGroupCount(){return 1;}
    int chooseEdgeValueGroup(int index){return index == 0;}
    int getTriangleValueGroupCount(){return 1;}
    int chooseTriangleValueGroup(int index){return index == 0;}

    int getNodeValueGroupName(int index, char *name, int len);
    int getEdgeValueGroupName(int index, char *name, int len){return 0;}
    int getTriangleValueGroupName(int index, char *name, int len){return 0;}

    int hasChanged();
    void setChanged(int b);

    void firstNode();
    M3TV_Node* nextNode();

    void firstEdge();
    M3TV_Edge* nextEdge();

    void firstTriangle();
    M3TV_Triangle* nextTriangle();

    void setTriangleVisibility(int b);
    int getTriangleVisibility();

    void setEdgeVisibility(int b);
    int getEdgeVisibility();

    void setNodeVisibility(int b);
    int getNodeVisibility();

    void setIsoEdgeVisibility(int b);
    int getIsoEdgeVisibility();

    void setIsoTriangleVisibility(int b);
    int getIsoTriangleVisibility();

    void setIsoValue(double iso);
private:
    void init();
    int dim_;
    M3TV_ArrayTriangleList *grid_;
    M3TV_IsoSurfaceDataSource *iso_;
    M3TV_TriangleList *gridDS_;
    VectorList *vec_;
    int nn_, ne_, nt_;
    int *e_, *t_;
    float *coords_, *vals_;
    float valmin_[5], valmax_[5];
    int showGrid_, showVecs_; //, autoMinMax_;
    double vecHeight_;
    int zVal_;
    int nVal_;
};

#endif





