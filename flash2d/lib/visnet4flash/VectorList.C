#include "fortify.h"
#include "VectorList.h"
//#include "M3TV_DataSource.h"
#include "3dutil.h"
#include "stdio.h"

VectorList::VectorList():
  nn_(0), coords_(NULL), vals_(NULL)
{
    n_ = new M3TV_SimpleNode(0,0,0,0,M3TV_Data::REUSABLE);
    n1_ = new M3TV_SimpleNode(0,0,0,0,M3TV_Data::REUSABLE);
    n2_ = new M3TV_SimpleNode(0,0,0,0,M3TV_Data::REUSABLE);
    e_ = new M3TV_SimpleEdge(n1_, n2_);
}
VectorList::VectorList(int nn, int dim,
		       float *coords,
		       float *vals,
		       double height):
  nn_(0), coords_(NULL), vals_(NULL)
{
    n_ = new M3TV_SimpleNode(0,0,0,0,M3TV_Data::REUSABLE);
    n1_ = new M3TV_SimpleNode(0,0,0,0,M3TV_Data::REUSABLE);
    n2_ = new M3TV_SimpleNode(0,0,0,0,M3TV_Data::REUSABLE);
    e_ = new M3TV_SimpleEdge(n1_, n2_);

    setData(nn, dim, coords, vals);
    setHeight(height);
}

VectorList::~VectorList(){
    delete n_;
    delete e_;
    delete n1_;
    delete n2_;
}

void VectorList::setData(int nn, int dim, float *coords, float *vals){
  nn_ = nn;
  dim_ = dim;
  coords_ = coords;
  vals_ = vals;
  setChanged(1);
}

void VectorList::setHeight(double h){
    height_ = h;
    setChanged(1);
}

double VectorList::getHeight(){
    return height_;
}

void VectorList::firstNode(){
    ni_ = -1;
}

M3TV_Node *VectorList::nextNode(){
    if(++ni_ >= nn_){
	return NULL;
    }
    n_->setCoords(coords_[ni_*3], coords_[ni_*3+1], dim_==2?height_:coords_[ni_*3+2]);
    return n_;
}

void VectorList::firstEdge(){
    ei_ = -1;
    //e_ = new M3TV_SimpleEdge(NULL, NULL);
}

M3TV_Edge *VectorList::nextEdge(){
    if(++ei_ >= nn_){
	return NULL;
    }
    // vals[5*ei_+1] == ucomp
    // vals[5*ei_+2] == vcomp
    // vals[5*ei_+3] == 3d:wcomp
    n1_->setCoords(coords_[ei_*3],
		   coords_[ei_*3+1],
		   dim_==2?height_:coords_[ei_*3+2]);
    n2_->setCoords(coords_[ei_*3]+vals_[5*ei_+1],
		   coords_[ei_*3+1]+vals_[5*ei_+2],
		   dim_==2?height_:coords_[ei_*3+2]+vals_[5*ei_+3]);
    return e_;
}

void VectorList::initMinMax(){
    M3TV_Edge *e;
    double *c;
    
    firstEdge();
    e = nextEdge();
    if(e != NULL){
	c = e->getNode1()->getCoordsD();
	X(min_) = X(max_) = X(c);
	Y(min_) = Y(max_) = Y(c);
    }
    firstEdge();
    while((e = nextEdge()) != NULL){
	c = e->getNode1()->getCoordsD();
	if(X(c) < X(min_)) X(min_) = X(c);
	if(X(c) > X(max_)) X(max_) = X(c);
	if(Y(c) < Y(min_)) Y(min_) = Y(c);
	if(Y(c) > Y(max_)) Y(max_) = Y(c);
	
	c = e->getNode2()->getCoordsD();
	if(X(c) < X(min_)) X(min_) = X(c);
	if(X(c) > X(max_)) X(max_) = X(c);
	if(Y(c) < Y(min_)) Y(min_) = Y(c);
	if(Y(c) > Y(max_)) Y(max_) = Y(c);
    }
    
    Z(min_) = Z(max_) = height_;
    
    minnv_ = maxnv_ = 0;
    minev_ = maxev_ = 0;
    
    minMaxCoordsSet_ = 1;
    minMaxNodeValuesSet_ = 1;
    minMaxEdgeValuesSet_ = 1;
}
