#ifndef VECTROLIST_H
#define VECTROLIST_H

#include "M3TV_StdDataSource.h"

class VectorList : public M3TV_EdgeList{
 public:
    VectorList();
    VectorList(int nn, int dim,
	       float *coords,
	       float *vals,
	       double height = 0.0);
    ~VectorList();

    void setData(int nn, int dim, float *coords, float *vals);
    
    void setHeight(double h);
    double getHeight();
    
    int getNodeValueGroupCount(){return 1;}
    int chooseNodeValueGroup(int index){return index ==0;}
    int getEdgeValueGroupCount(){return 1;}
    int chooseEdgeValueGroup(int index){return index == 0;}

    int getNodeValueGroupName(int index, char *name, int len){return 0;}
    int getEdgeValueGroupName(int index, char *name, int len){return 0;}

    void firstNode();
    M3TV_Node *nextNode();
    
    void firstEdge();
    M3TV_Edge *nextEdge();

 protected:
    void initMinMax();

 private:
    int nn_, ni_, ei_;
    int dim_;
  float *coords_, *vals_;
    M3TV_SimpleEdge* e_;

    M3TV_SimpleNode *n_, *n1_, *n2_;
    double height_;
};

#endif
