#-----------------------------------------------------------
# Input file for batch mode input
# input is controlled via keywords
#
# new version with generic input metadata involved
#
# j. behrens 05/2016
#-----------------------------------------------------------
# 1. Section:
#    This section contains all standard parameters
#-----------------------------------------------------------
# the experiment no. (0 for a new experiment)     [integer]
EXPERIMENT_NUMBER
0
# switch diagnostics on (1) or off (0)            [integer]
SWITCH_ON_DIAGNOSTICS
0
# desired maximum level of refinements            [integer]
FINE_GRID_LEVEL
17
# minimum level of refinements                    [integer]
COARSE_GRID_LEVEL
9
# tolerance for refinement                        [real]
TOLERANCE_OF_REFINEMENT
0.2
# tolerance for coarsening                        [real]
TOLERANCE_OF_COARSENING
0.1
# watermark for refinement                        [real]
WATERMARK_OF_REFINEMENT
0.01
# watermark for coarsening                        [real]
WATERMARK_OF_COARSENING
0.01
# timestep length                                 [real]
TIMESTEP_LENGTH
1800.
# starting time                                   [real]
TIMESTEPPING_START_TIME
0.0
# final time                                      [real]
TIMESTEPPING_END_TIME
72000.
# plot in matlab style to file (no plot = 0)      [integer]
NETCDF_FILE_PLOTTING
0
# plot in gmv style to file (no plot = 0)         [integer]
VTU_FILE_PLOTTING
1
# timesteps between plots                         [integer]
STEPS_BTW_PLOTS
1
# timesteps between saves                         [integer]
STEPS_BTW_SAVES
500
# save last timestep for next experiment (no = 0) [integer]
SAVE_FINISH_CONFIGURATION
0
# file defining the domain                        [character]
DOMAIN_FILE_NAME
Domain.dat
# file defining the initial triangulation         [character]
TRIANG_FILE_NAME
Triang.dat
# number of iterations in trajectory estimation   [integer]
SLM_ITERATION_NUMBER
4
#-----------------------------------------------------------
# 2. Section:
#    This section contains parameters specific to the
#    test cases considered.
#
#    NOTE: the structure is always (more or less) the same:
#    If a parameter type is used, then the number of 
#    corresponding parameters needs to be given, immediately
#    followed by the keywords and values. So, if two params
#    of integer type are used, TST_INT_PARAMETERS is 2, and
#    four lines would follow with 
#      KEYWORD
#      i value
#      KEYWORD
#      k l values
#    entries. In fact, for integer and real types, there
#    also vector-valued parameters are allowed. So, the 
#    two lines preceeding the KEYWORD line above would read
#      TST_INT_STRUCTURE
#      1 2
#    The KEYWORD strings should be at least 14 characters
#    long.
#-----------------------------------------------------------
# do we use integer parameters? (0=no, how many?) [integer]
TST_INT_PARAMETERS
0
# do we use character parameters? (0=no, how many?)[integer]
TST_CHAR_PARAMETERS
1
WIND_FILE_NAME
Windparam.dat
# do we use real parameters? (0=no, how many?)    [integer]
# both are scalar values
# for advection diffusion test case: coefficient  [real]
# for scaling the wind strength                   [real]
TST_REAL_PARAMETERS
2
TST_REAL_STRUCTURE
1 1
DIFFUSION_COEFFICIENT
0.3
WIND_COEFFICIENT
1.0
#-----------------------------------------------------------
# END OF FILE
#-----------------------------------------------------------
