##################################################################
#   FLASH90                                                      #
#   FLexible Adaptive Semi-Lagrangian Hack                       #
#   written in Fortran 90                                        #
##################################################################
# makefile to build FLASH                                        #
# j. behrens 2/95, 3/96, 8/98                                    #
# l. mentrup 7/05                                                #
# --- this is for linux ---                                      #
##################################################################

# MACHINE
MACHINE = macosx_intel

# SYSTEM
SYSTEM = std-f90

# SET MAKETHING CORRESPONDING TO MACHINE:
MAKETHING= FLASH

# OPTIMIZATION SETTINGS [debug|norm|opt]
MODE := debug

# LIBRARY SETTINGS [yes|no]
# Useage: make NO_NETCDF=yes NO_VISNET=yes
NO_NETCDF := no
NO_VISNET := yes

# Use Method MPSLM [yes|no]
NO_MPSLM := yes

# SET MAIN DIRECTORY PATH
# !! This has to be alterd by user !!
ROOTDIR = $(HOME)/Documents/Development/amatos

# SET atlas/blas DIRECTORY PATH
# !! This has to be alterd by user !!
BLASDIR = $(MKLROOT)/lib
BLASLIB = 

# SET LAPACK DIRECTORY PATH/BLAS INCLUDED IN INTEL MKL
# !! This has to be alterd by user !!
LAPACKDIR =  $(MKLROOT)/lib #-L$($ROOTDIR)/amatos2d/trunk/3rdparty/LAPACK95
LAPACKLIB =  -I${MKLROOT}/include/lp64 -I${MKLROOT}/include ${MKLROOT}/lib/libmkl_blas95_lp64.a \
             ${MKLROOT}/lib/libmkl_lapack95_lp64.a ${MKLROOT}/lib/libmkl_intel_lp64.a \
             ${MKLROOT}/lib/libmkl_core.a ${MKLROOT}/lib/libmkl_sequential.a \
             -lpthread -lm # -llapack95

# SET C++ DIRECTORY PATH
# !! This has to be alterd by user !!
CCLIBDIR = /usr/lib/
CCLIB = -lstdc++

# SET MORE DIRECTORY PATHS
LIBDIR  = $(ROOTDIR)/amatos2d/trunk/lib/$(MACHINE)
INCDIR  = $(ROOTDIR)/amatos2d/trunk/include/$(MACHINE)
MODDIR  = $(ROOTDIR)/amatos2d/trunk/include/$(MACHINE)
MAINDIR = $(ROOTDIR)/flash2d/trunk

SRCDIR  = $(MAINDIR)/src/flash
OPTDIR  = $(MAINDIR)/src/options
SYSDIR  = $(MAINDIR)/src/system/$(SYSTEM)
TIMDIR  = $(MAINDIR)/src/timing
DATDIR  = $(MAINDIR)/data
BUILDIR = $(MAINDIR)/compile/$(MACHINE)

# SET NETCDF PATHS
LIBNETCDF = 
INCNETCDF =
ifneq ($(strip $(NO_NETCDF)), yes)
LIBNETCDF = -L$(LIBDIR) -lncugrid -L/usr/local/lib -lnetcdff -lnetcdf
INCNETCDF = -I/usr/local/include
endif

# SET VISNET DIRECTORY PATH
# !! This has to be alterd by user !!
VISNETDIR =
VISNETLIB = 
ifneq ($(strip $(NO_VISNET)),yes)
VISNETDIR = $(ROOTDIR)/lib/SINGLE
VISNETLIB = -lvisnet4flash -lm3tv -ldelaunay \
	    -ldetri -llia -lsos -lbasic \
	    $(VISNETDIR)/fortify.o \
	    -lglut -lGLU -lGL -ltiff
endif

# SET PATH TO GRID_API.mod
AMATOS    = $(MODDIR)/grid_api.mod

# SET THE ENDING OF MODULE FILES
MODEND  = mod

#----------------------------------------------------------------#
# FLAGS FOR LINUX / Intel Fortran Compiler                       #
#----------------------------------------------------------------#
F90 	= ifort
cc 	= gcc
CC 	= g++
LOADER  = ifort

# -------------- next for debugging -----------------------------#
ifeq ($(strip $(MODE)),debug)
  FFLAGS  = -g -fpic -Wl,-no_pie -DLAPACK95 -DUSE_MKL -check all #none # all
  CFLAGS  = -g -m64 -check
  LDFLAGS = -g -save-temps -static-intel -fpic -Wl,-no_pie -check all #none # all
endif
# -------------- next for normal compilation --------------------#
ifeq ($(strip $(MODE)),norm)
  FFLAGS  = -DLAPACK95 -DUSE_MKL
  CFLAGS  =
  LDFLAGS = -ifport
endif
# -------------- next with aggresive optimization (Pentium M) ---#
ifeq ($(strip $(MODE)),opt)
  FFLAGS  = -O -DLAPACK95 -DUSE_MKL
  CFLAGS  = -O
  LDFLAGS = -O -ifport
endif

# --------------------- include Visnet/NetCDF or not ------------#
ifeq ($(strip $(NO_VISNET)),yes)
FFLAGS += -DNO_VISNET
endif
ifeq ($(strip $(NO_NETCDF)),yes)
FFLAGS += -DNO_NETCDF
endif

# -------- next flag for using dummy graphics library -----------#
LIBS = -lamatos $(LIBNETCDF) -L$(LIBDIR) -I$(MODDIR) -L$(BLASDIR) $(BLASLIB) \
	           -L$(LAPACKDIR) $(LAPACKLIB) $(BLASLIB) $(VISNETLIB) \
	           $(CCLIB) -lm
		    
INCS = $(INCNETCDF) -I$(INCDIR) -I$(MODDIR) -I$(MKLROOT)/include/intel64/lp64 #\
#       -I$($ROOTDIR)/amatos2d/trunk/3rdparty/LAPACK95/lapack95_modules

#----------------------------------------------------------------#
# common stuff                                                   #
#----------------------------------------------------------------#

include $(MAINDIR)/compile/Makefiles/Makefile.common

#----------------------------------------------------------------#
# END of Makefile                                                #
#----------------------------------------------------------------#
