##################################################################
#   FLASH90                                                      #
#   FLexible Adaptive Semi-Lagrangian Hack                       #
#   written in Fortran 90                                        #
##################################################################
# makefile to build FLASH                                        #
# j. behrens 2/95, 3/96, 8/98                                    #
# --- this is for irix ---                                       #
##################################################################

# MACHINE
MACHINE = irix

# SET MAKETHING CORRESPONDING TO MACHINE:
MAKETHING= FLASH

# SET MAIN DIRECTORY PATH
# !! This has to be alterd by user !!
ROOTDIR = $(HOME)/Development

# SET atlas/blas DIRECTORY PATH
# !! This has to be alterd by user !!
BLASDIR = 
BLASLIB = 

# SET LAPACK DIRECTORY PATH
# !! This has to be alterd by user !!
LAPACKDIR = 
LAPACKLIB = -lscs

# SET VISNET DIRECTORY PATH
# !! This has to be alterd by user !!
VISNETDIR = $(ROOTDIR)/lib/SINGLE
VISNETLIB = -lvisnet4flash -lm3tv -ldelaunay \
	    -ldetri -llia -lsos -lbasic \
	    $(VISNETDIR)/fortify.o \
	    -lglut -lGLU -lGL -ltiff \
            -lXmu -lX11

# SET C++ DIRECTORY PATH
# !! This has to be alterd by user !!
CCLIBDIR = 
CCLIB = -lpthread -lC

# SET MORE DIRECTORY PATHS
LIBDIR  = $(ROOTDIR)/lib/SINGLE
INCDIR  = $(ROOTDIR)/include
MODDIR  = $(ROOTDIR)/include/SINGLE
MAINDIR = $(ROOTDIR)/flash2d

SRCDIR  = $(MAINDIR)/src/flash
OPTDIR  = $(MAINDIR)/src/options
SYSDIR  = $(MAINDIR)/src/system/std-f90
TIMDIR  = $(MAINDIR)/src/timing
DATDIR  = $(MAINDIR)/data
BUILDIR = $(MAINDIR)/$(MACHINE)

# SET LIBRARY PATHS
LIBNETCDF = -L/home/bornemann/m3/lib/IRIX -lnetcdf

# SET INCLUDE PATHS
INCNETCDF = /home/bornemann/m3/include
AMATOS    = $(MODDIR)/GRID_API.mod

# SET THE ENDING OF MODULE FILES
MODEND  = mod

#----------------------------------------------------------------#
# FLAGS FOR SGI                                                  #
#----------------------------------------------------------------#

F90    = f90
CC     = cc
LOADER = f90
# --------------------- next three for debugging ----------------#
# FFLAGS = -n32 -g -check_bounds -DEBUG:div_check=3:trap_uninitialized=ON:verbose_runtime=ON
# CFLAGS = -g
# LDFLAGS = -n32 -g
# --------------------- next three for optimized debugging ------#
FFLAGS = -n32 -O1 -OPT:Olimit=2069 -g3
CFLAGS = -O1 -g3
LDFLAGS = -n32 -O1 -OPT:Olimit=2069 -g3
# --------------------- next three with optimization ------------#
# FFLAGS = -O3 -n32
# CFLAGS = -O
# LDFLAGS = -O3 -n32
# -------- next flag for using dummy graphics library -----------#
LIBS = -L$(BLASDIR) -L$(LAPACKDIR) -L/usr/lib32/internal -L$(LIBDIR) \
	 -L$(VISNETDIR) -L$(CCLIBDIR) -rpath$(LIBDIR) \
       -lamatos $(LAPACKLIB) $(BLASLIB) $(LIBNETCDF) $(VISNETLIB) \
	$(CCLIB) -lm
INCS = -I$(INCNETCDF) -I$(INCDIR) -I$(MODDIR)

##################################################################
# AFTER THIS LINE, NO CHANGES SHOULD BE NECESSARY                #
##################################################################

#----------------------------------------------------------------#
# OBJECTS                                                        #
#----------------------------------------------------------------#
MAINOBJ = \
FLASH_parameters.o \
MISC_timing.o \
MISC_system.o \
IO_utils.o \
IO_matlabplot.o \
IO_gmvplot.o \
VisNET4Flash.o \
IO_visnetplot.o \
IO_netcdfplot.o \
SLM_initial.o \
SLM_errorestimate.o \
ADV_wind.o \
ADV_rhs.o \
SLM_simple.o \
SLM_advanced.o \
ADV_semilagrange.o \
Flash90.o

#----------------------------------------------------------------#
# COMPILE STEP                                                   #
#----------------------------------------------------------------#

.SUFFIXES: .F90 .f90 $(SUFFIXES)

.F90.o:
	@echo "make: Building object module from "$<
	$(F90) $(FFLAGS) $(INCS) -c $<

.f90.o:
	@echo "make: Building object module from "$<
	$(F90) $(FFLAGS) $(INCS) -c $<

.c.o:
	@echo "make: Building object module from "$<
	$(CC) $(CFLAGS) $(INCS) -D$(MACHINE) -c $<

clearsrc::
	@rm -f *.f90 *.h *.F90

clearex::
	@rm -f $(MAKETHING) Flash90* fort.*

cleardat::
	@rm -f *.dat

clean::
	@rm -f *.o *.$(MODEND) core

tidy::
	make clean
	make clearex
	make cleardat
	make clearsrc

#----------------------------------------------------------------#
# THIS COPIES REQUIRED DATA FILES                                #
#----------------------------------------------------------------#

datacopy::
	@cp $(DATDIR)/Domain.dat .
	@cp $(DATDIR)/Initial.dat .
	@cp $(DATDIR)/Land.dat .
	@cp $(DATDIR)/Triang.dat .
	@cp $(DATDIR)/Parameters.dat .

#----------------------------------------------------------------#
# THIS CREATES PREDEFINED OPTIONS                                #
#----------------------------------------------------------------#

maincopy::
	@cp $(SRCDIR)/*.f90 .
	@cp $(SRCDIR)/*.F90 .
	@cp $(SYSDIR)/*.f90 .
	@cp $(TIMDIR)/*.f90 .

Slot::
	@make maincopy
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90

Potsdam::
	@cp $(OPTDIR)/ADV_wind.file ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.file SLM_initial.f90
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.dual SLM_advanced.f90

Analytic::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.analytic SLM_advanced.f90

Bar::
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/ADV_wind.bar ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.bar SLM_initial.f90

Conservative::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.cons SLM_advanced.f90

Dual::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.dual SLM_advanced.f90

Cellintegrated::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.cellint SLM_advanced.f90

Quadrature::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.quadrature SLM_advanced.f90

Quadrature7::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.quadrature7 SLM_advanced.f90

MPSLM::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.mpslm SLM_advanced.f90

ConvWind::
	@cp $(OPTDIR)/ADV_wind.conv ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.conv SLM_initial.f90

DiagWind::
	@cp $(OPTDIR)/ADV_wind.diag ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.diag SLM_initial.f90

Kaeser::
	@cp $(OPTDIR)/ADV_wind.kaeser ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.kaeser SLM_initial.f90

CircWind::
	@cp $(OPTDIR)/ADV_wind.circ ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.circ SLM_initial.f90

#----------------------------------------------------------------#
# THIS COMPILES THE MAIN PROGRAM                                 #
#----------------------------------------------------------------#

executable: $(MAINOBJ)
	@echo "make: Linking object modules and libraries"
	$(LOADER) $(LDFLAGS) -o $(MAKETHING) $(MAINOBJ) $(LIBS)

$(MAKETHING)::
	@make maincopy
	@make executable

all::
	@make maincopy
	@make executable
	@make clearsrc
	@make clean

#----------------------------------------------------------------#
# DEPENDENCIES ON INCLUDE FILES                                  #
#----------------------------------------------------------------#
