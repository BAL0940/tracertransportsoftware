##################################################################
#   FLASH90                                                      #
#   FLexible Adaptive Semi-Lagrangian Hack                       #
#   written in Fortran 90                                        #
##################################################################
# makefile to build FLASH                                        #
# j. behrens 2/95, 3/96, 8/98                                    #
# --- this is for irix ---                                       #
##################################################################

# MACHINE
MACHINE = irix

# SET MAKETHING CORRESPONDING TO MACHINE:
MAKETHING= FLASH

# SET MAIN DIRECTORY PATH
MAINDIR = $(PWD)/..

# SET MORE DIRECTORY PATHS
SRCDIR  = $(MAINDIR)/src/flash-sphere
OPTDIR  = $(MAINDIR)/src/options
LIBDIR  = /home/bornemann/behrens/Development/amatos20/lib
# LIBDIR  = /home/bornemann/behrens/lib/IRIX_DEBUG/SAMATOS
SYSDIR  = $(MAINDIR)/src/system/std-f90
TIMDIR  = $(MAINDIR)/src/timing
DATDIR  = $(MAINDIR)/data
INCDIR  = /home/bornemann/behrens/include
MODDIR  = /home/bornemann/behrens/Development/amatos20/
# MODDIR  = /home/bornemann/behrens/lib/IRIX_DEBUG/SAMATOS
BUILDIR = $(MAINDIR)/$(MACHINE)

# SET LIBRARY PATHS
LIBNETCDF = -L/home/bornemann/m3/lib/IRIX -lnetcdf

# SET INCLUDE PATHS
INCNETCDF = /home/bornemann/m3/include
AMATOS    = $(MODDIR)/GRID_API.mod

# SET THE ENDING OF MODULE FILES
MODEND  = mod

#----------------------------------------------------------------#
# FLAGS FOR SUN                                                  #
#----------------------------------------------------------------#

F90    = f90
CC     = cc
LOADER = f90
# --------------------- next three for debugging ----------------#
FFLAGS = -n32 -g -DNO_NETCDF -check_bounds -DEBUG:div_check=3:trap_uninitialized=ON:verbose_runtime=ON
CFLAGS = -g
LDFLAGS = -n32 -g
# --------------------- next three for optimized debugging ------#
# FFLAGS = -n32 -O1 -OPT:Olimit=2069 -g3
# CFLAGS = -O1 -g3
# LDFLAGS = -n32 -O1 -OPT:Olimit=2069 -g3
# ARFLAGS = vru
# --------------------- next three for profiling ----------------#
# FFLAGS = -n32 -p
# CFLAGS = -p
# LDFLAGS = -n32 -p
# ARFLAGS = vru
# --------------------- next three with optimization ------------#
# FFLAGS = -O3 -n32
# CFLAGS = -O
# LDFLAGS = -O3 -n32
# ARFLAGS = vru
# -------- next flag for using dummy graphics library -----------#
#LIBS = -L$(LIBDIR) -rpath$(LIBDIR) -L/usr/lib32/internal \
#       -lBJUGL -lBUI -lamatos -lgpc $(LIBNETCDF)
LIBS = -L$(LIBDIR) -rpath$(LIBDIR) -L/usr/lib32/internal \
       -lamatos $(LIBNETCDF)
INCS = -I$(INCNETCDF) -I$(INCDIR) -I$(MODDIR)

##################################################################
# AFTER THIS LINE, NO CHANGES SHOULD BE NECESSARY                #
##################################################################

#----------------------------------------------------------------#
# OBJECTS                                                        #
#----------------------------------------------------------------#
MAINOBJ = \
FLASH_parameters.o \
MISC_timing.o \
MISC_system.o \
IO_plotdefine.o \
IO_utils.o \
IO_matlabplot.o \
IO_gmvplot.o \
IO_griddedplot.o \
SLM_initial.o \
SLM_errorestimate.o \
ADV_wind.o \
ADV_rhs.o \
SLM_simple.o \
SLM_advanced.o \
ADV_semilagrange.o \
Flash90.o

#----------------------------------------------------------------#
# COMPILE STEP                                                   #
#----------------------------------------------------------------#

.F90.o:
	@echo "make: Building object module from "$<
	@$(F90) $(FFLAGS) $(INCS) -c $<

.f90.o:
	@echo "make: Building object module from "$<
	@$(F90) $(FFLAGS) $(INCS) -c $<

.c.o:
	@echo "make: Building object module from "$<
	@$(CC) $(CFLAGS) $(INCS) -D$(MACHINE) -c $<

clearsrc::
	@rm -f *.f90 *.h *.F90

clearex::
	@rm -f $(MAKETHING) Flash90* fort.*

cleardat::
	@rm -f *.dat

clean::
	@rm -f *.o *.$(MODEND) core

tidy::
	make clean
	make clearex
	make cleardat
	make clearsrc

#----------------------------------------------------------------#
# THIS COPIES REQUIRED DATA FILES                                #
#----------------------------------------------------------------#

datacopy::
	@cp $(DATDIR)/*.dat .

#----------------------------------------------------------------#
# THIS CREATES THE MAIN PROGRAM                                  #
#----------------------------------------------------------------#

maincopy::
	@cp $(SRCDIR)/*.f90 .
	@cp $(SRCDIR)/*.F90 .
	@cp $(SYSDIR)/*.f90 .
	@cp $(TIMDIR)/*.f90 .
#	@cp $(INCDIR)/BJUGL.f90 .

Slot::
	@make maincopy
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90

Potsdam::
	@cp $(OPTDIR)/ADV_wind.file ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.file SLM_initial.f90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90

Gerrits::
	@cp $(OPTDIR)/ADV_wind.file ADV_wind.f90
	@cp $(OPTDIR)/ADV_rhs.gerrit ADV_rhs.f90
	@cp $(OPTDIR)/SLM_initial.gerrit SLM_initial.f90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90

Burgers::
	@cp $(OPTDIR)/ADV_rhs.burgers ADV_rhs.f90
	@cp $(OPTDIR)/ADV_semilagrange.burgers ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_initial.burgers SLM_initial.f90
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90

Bar::
	@cp $(OPTDIR)/SLM_simple.clip SLM_simple.f90
	@cp $(SRCDIR)/ADV_wind.bar ADV_wind.f90
	@cp $(SRCDIR)/SLM_initial.bar SLM_initial.f90
	
Conservative::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.plain SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.cons SLM_advanced.f90
	
Dual::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.plain SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.dual SLM_advanced.f90
	
Cellintegrated::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.plain SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.cellint SLM_advanced.f90
	
Quadrature::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.plain SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.quadrature SLM_advanced.f90
	
Quadrature7::
	@cp $(OPTDIR)/ADV_semilagrange.adv ADV_semilagrange.F90
	@cp $(OPTDIR)/SLM_simple.plain SLM_simple.f90
	@cp $(OPTDIR)/SLM_advanced.quadrature7 SLM_advanced.f90

ConvWind::
	@cp $(OPTDIR)/ADV_wind.convergent ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.divergent SLM_initial.f90

DivWind::
	@cp $(OPTDIR)/ADV_wind.divergent ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.divergent SLM_initial.f90

DiagonalWind::
	@cp $(OPTDIR)/ADV_wind.diagonal ADV_wind.f90
	@cp $(OPTDIR)/SLM_initial.divergent SLM_initial.f90

executable: $(MAINOBJ)
	@echo "make: Linking object modules and libraries"
	@$(LOADER) $(LDFLAGS) -o $(MAKETHING) $(MAINOBJ) $(LIBS)

$(MAKETHING)::
	@make maincopy
	@make executable

all::
	@make maincopy
	@make executable
	@make clearsrc
	@make clean

#----------------------------------------------------------------#
# DEPENDENCIES ON INCLUDE FILES                                  #
#----------------------------------------------------------------#
