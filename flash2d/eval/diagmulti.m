function diags(file)
% An M-File to postprocess data written by Flash90
% j. behrens 03/2002
%
% clear old data structures and initialize
%clear;

% set constants
LW=2;
TF=14;
AF=12;
%
% open file and read data
file1=sprintf('%s%s',file,'.clip');
disp(sprintf('%s%s\n','reading data from ',file1));
iou = fopen(file1);
RAW1 = fscanf(iou,'%d %d %d %d %d %d %f %f %f %f %f %f %f %f',[14,inf]);
stat = fclose(iou);
file1=sprintf('%s%s',file,'.dual');
disp(sprintf('%s%s\n','reading data from ',file1));
iou = fopen(file1);
RAW2 = fscanf(iou,'%d %d %d %d %d %d %f %f %f %f %f %f %f %f',[14,inf]);
stat = fclose(iou);
file1=sprintf('%s%s',file,'.cons');
disp(sprintf('%s%s\n','reading data from ',file1));
iou = fopen(file1);
RAW3 = fscanf(iou,'%d %d %d %d %d %d %f %f %f %f %f %f %f %f',[14,inf]);
stat = fclose(iou);
file1=sprintf('%s%s',file,'.quad');
disp(sprintf('%s%s\n','reading data from ',file1));
iou = fopen(file1);
RAW4 = fscanf(iou,'%d %d %d %d %d %d %f %f %f %f %f %f %f %f',[14,inf]);
stat = fclose(iou);
file1=sprintf('%s%s',file,'.quad7');
disp(sprintf('%s%s\n','reading data from ',file1));
iou = fopen(file1);
RAW5 = fscanf(iou,'%d %d %d %d %d %d %f %f %f %f %f %f %f %f',[14,inf]);
stat = fclose(iou);
file1=sprintf('%s%s',file,'.machenh');
disp(sprintf('%s%s\n','reading data from ',file1));
iou = fopen(file1);
RAW6 = fscanf(iou,'%d %d %d %d %d %d %f %f %f %f %f %f %f %f',[14,inf]);
stat = fclose(iou);
disp('... done, now processing grid ...');
%
% determine sizes
[n,m]= size(RAW1);
step=RAW1(1,1:m);
p1=plot(step,RAW1(2,1:m),'r-',step,RAW2(2,1:m),'b--',step,RAW3(2,1:m),'co-',step,RAW4(2,1:m),'y*-',step,RAW5(2,1:m),'gd-',step,RAW6(2,1:m),'m:');
t1=title('No. of total elements');
x1=xlabel('time step'); y1=ylabel('elements');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(3,1:m),'r-',step,RAW2(3,1:m),'b--',step,RAW3(3,1:m),'co-',step,RAW4(3,1:m),'y*-',step,RAW5(3,1:m),'gd-',step,RAW6(3,1:m),'m:');
t1=title('No. of fine grid elements');
x1=xlabel('time step'); y1=ylabel('elements');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(4,1:m),'r-',step,RAW2(4,1:m),'b--',step,RAW3(4,1:m),'co-',step,RAW4(4,1:m),'y*-',step,RAW5(4,1:m),'gd-',step,RAW6(4,1:m),'m:');
t1=title('No. of total edges');
x1=xlabel('time step'); y1=ylabel('edges');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(5,1:m),'r-',step,RAW2(5,1:m),'b--',step,RAW3(5,1:m),'co-',step,RAW4(5,1:m),'y*-',step,RAW5(5,1:m),'gd-',step,RAW6(5,1:m),'m:');
t1=title('No. of fine grid edges');
x1=xlabel('time step'); y1=ylabel('edges');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(6,1:m),'r-',step,RAW2(6,1:m),'b--',step,RAW3(6,1:m),'co-',step,RAW4(6,1:m),'y*-',step,RAW5(6,1:m),'gd-',step,RAW6(6,1:m),'m:');
t1=title('No. of nodes');
x1=xlabel('time step'); y1=ylabel('nodes');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(7,1:m),'r-',step,RAW2(7,1:m),'b--',step,RAW3(7,1:m),'co-',step,RAW4(7,1:m),'y*-',step,RAW5(7,1:m),'gd-',step,RAW6(7,1:m),'m:');
t1=title('Minimum tracer concentration');
x1=xlabel('time step'); y1=ylabel('minimum');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(8,1:m),'r-',step,RAW2(8,1:m),'b--',step,RAW3(8,1:m),'co-',step,RAW4(8,1:m),'y*-',step,RAW5(8,1:m),'gd-',step,RAW6(8,1:m),'m:');
t1=title('Maximum tracer concentration');
x1=xlabel('time step'); y1=ylabel('maximum');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(9,1:m),'r-',step,RAW2(9,1:m),'b--',step,RAW3(9,1:m),'co-',step,RAW4(9,1:m),'y*-',step,RAW5(9,1:m),'gd-',step,RAW6(9,1:m),'m:');
t1=title('Ratio of First Moments');
x1=xlabel('time step'); y1=ylabel('rfm');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(10,1:m),'r-',step,RAW2(10,1:m),'b--',step,RAW3(10,1:m),'co-',step,RAW4(10,1:m),'y*-',step,RAW5(10,1:m),'gd-',step,RAW6(10,1:m),'m:');
t1=title('Ratio of Second Moments');
x1=xlabel('time step'); y1=ylabel('rsm');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
pause

p1=plot(step,RAW1(12,1:m),'r-',step,RAW2(12,1:m),'b--',step,RAW3(12,1:m),'co-',step,RAW4(12,1:m),'y*-',step,RAW5(12,1:m),'gd-',step,RAW6(12,1:m),'m:');
t1=title('L2-Norm');
x1=xlabel('time step'); y1=ylabel('l2');
legend('Clipping    ','Dual Mesh   ','Stani.Gravel','Quadrature  ','Quadrature 7','Machenhauer ',0)
set(p1,'LineWidth',LW)
set(t1,'FontSize',TF)
set(x1,'FontSize',AF)
set(y1,'FontSize',AF)
%pause

return
