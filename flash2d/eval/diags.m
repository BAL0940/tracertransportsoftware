function diags(file)
% An M-File to postprocess data written by Flash90
% j. behrens 03/2002
%
% clear old data structures and initialize
%clear;
%
% open file and read data
disp('reading data ...');
iou = fopen(file);
RAW = fscanf(iou,'%d %d %d %d %d %d %f %f %f %f %f %f %f %f %f',[15,inf]);
stat = fclose(iou);
disp('... done, now processing grid ...');
%
% determine sizes
[n,m]= size(RAW);
step=RAW(1,1:m);
elements=RAW(2,1:m);
plot(step,elements)
title('No. of total elements')
xlabel('time step'); ylabel('elements')
pause

felements=RAW(3,1:m);
plot(step,felements)
title('No. of fine grid elements')
xlabel('time step'); ylabel('elements')
pause

edges=RAW(4,1:m);
plot(step,edges)
title('No. of total edges')
xlabel('time step'); ylabel('edges')
pause

fedges=RAW(5,1:m);
plot(step,fedges)
title('No. of fine grid edges')
xlabel('time step'); ylabel('edges')
pause

nodes=RAW(6,1:m);
plot(step,nodes)
title('No. of nodes')
xlabel('time step'); ylabel('nodes')
pause

mini=RAW(7,1:m);
plot(step,mini)
title('Minimum tracer concentration')
xlabel('time step'); ylabel('minimum')
pause

maxi=RAW(8,1:m);
plot(step,maxi)
title('Maximum tracer concentration')
xlabel('time step'); ylabel('maximum')
pause

rfm=RAW(9,1:m);
plot(step,rfm)
title('Ration of First Moments')
xlabel('time step'); ylabel('rfm')
pause

rsm=RAW(10,1:m);
plot(step,rsm)
title('Ration of Second Moments')
xlabel('time step'); ylabel('rsm')
pause

l2=RAW(12,1:m);
plot(step,l2)
title('L2-Norm')
xlabel('time step'); ylabel('l2')
pause


return
