% An M-File to postprocess data written by gpc DEBUG option
% Visualization in 2D
% j. behrens 12/2001
%
% clear old data structures and initialize
clear;
imax = 3000;
%  colormap(hsv)
%  brighten(0.8)
%
% pre loop file opening
hold on
iou = fopen('GPC_DEBUG_polyisec.dat');

% main loop, first read line
icnt=0;
while icnt<imax
%while 1
  tline = fgetl(iou);
  if ~ischar(tline)
    break
  else
    ipoly=sscanf(tline,'%d');
    for i=1:ipoly
      tline = fgetl(iou);
      if ~ischar(tline)
        break
      else
        ivert=sscanf(tline,'%d');
	xcoo=zeros(1,ivert); ycoo=zeros(1,ivert); zcoo=zeros(1,ivert);
        for j=1:ivert
          tline = fgetl(iou);
          if ~ischar(tline)
            break
          else
%            [xcoo(j) ycoo(j)]=sscanf(tline,'%f %f');
            a=sscanf(tline,'%f %f');
	    xcoo(j)=a(1); ycoo(j)=a(2);
          end
	end
	patch(xcoo,ycoo,zcoo,'w');
      end
%    disp(tline)
      icnt= icnt+1;
    end
  end
end

stat = fclose(iou);
hold off
