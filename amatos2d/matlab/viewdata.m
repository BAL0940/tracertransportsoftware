clear all

datapath = '~/rechnen/amatos/amatos2d/compile/linux_g64';
fileprefix = 'AMATOS_';

ncfiles = dir(fullfile(datapath, [fileprefix '*.nc']));

figure(1)
for istep = 1:length(ncfiles)

  % Open netCDF file.
  ncid = netcdf.open(fullfile(datapath, ncfiles(istep).name), 'NC_NOWRITE');

  % Get time step information
  % isteptime = netcdf.getAtt(ncid, netcdf.getConstant('NC_GLOBAL'), 'probtime');

  % Get information about the contents of the file.
  [numdims, numvars, numglobalatts, unlimdimID] = netcdf.inq(ncid);

  for ivar = 1:numvars
    % Get the names of variables.
    [varnames{ivar}, xtype, varDimIDs, varAtts] = netcdf.inqVar(ncid, ivar-1);

    % Get the values of the variables.
    data{ivar} = netcdf.getVar(ncid, ivar-1);
  end

  netcdf.close(ncid);

  % compute cell (triangle) centers
  clear eltcenter
  for ielt = 1:size(data{2},2)
    eltcenter(:,ielt) = mean(data{1}(:,data{2}(:,ielt)+1), 2);
  end

  tmp = zeros(size(data{1}, 2), 1);
  trdata = TriRep(double(data{2}+1)', [double(data{1}') tmp]);

  % plot refinement levels
  clf
  trisurf(trdata, 'CData', double(data{7}));
  view(0,90);
  axis equal
  set(gca, 'XLim', [0 1], 'YLim', [0 1])
  title('refinement level')

  if istep == 1
    pause()
  else
    pause(0.1)
  end
end
