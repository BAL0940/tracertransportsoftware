/**

@page ChapHowToUse How to use amatos

<tt><b>amatos</b></tt> is a mesh generator for adaptive algorithms. There are two
philosophies which seem to be the key to the understanding of <tt><b>amatos</b></tt>:
    -#  Think of the adaptive algorithm as a two phase procedure: In the first
      phase, the mesh is generated/adapted. Each mesh item keeps associated
      data. In the second phase, numerical calculations are performed. To
      achieve this, first gather all required data from mesh items into vectors,
      perform the calculations on vectors (utilizing consecutive storage positions
      for efficient pipelined or vectorized execution), and finally scatter
      the results back to mesh item storage positions. This is illustrated in a
      @ref FigGatherScatter "Figure".
    -#  Think of the program as a data-flow, with methods acting and manipulating
      the data. A data structure (called <tt>grid_handle</tt>) represents a specific
      instance of the mesh. Methods (routines in the GRID API) act on the
      instance, manipulating it. Different methods can be applied to the mesh
      more or less independently.

The GRID API provides routines that implement methods in both of the above
circumstances. Gathering (<tt>FEM_dataretrieve::grid_getinfo</tt>) and scattering (<tt>FEM_dataretrieve::grid_putinfo</tt>) accept
the mesh handle without manipulating the mesh topology. Other methods, like
<tt>GRID_api::grid_adapt</tt> alter the topology.

When performing numerical calculations on data gathered from the mesh into vectors,
there MUST NOT be any change to the mesh topology, before scattering the results
back to the mesh.

A new feature in Version 2.0 of <tt><b>amatos</b></tt> is the support of diverse
kinds of finite elements. In order to use user defined finite elements, the
element's characteristics have to be defined and implemented. This task cannot be
performed at runtime but has to be concluded before compiling the package. There
are two finite elements predefined in amatos, namely a linear and a quadratic
Lagrange element.

Once the library has been compiled and an application uses amatos, each variable
in the application has to be registered to a specific finite element type. A
detailed documentation on finite element support can be found in Chapter @ref ChapFEM.

\anchor FigGatherScatter
\image html gather-scatter.png "Figure: Two phases of calculation with amatos. Numerical calculations in vectors (after gathering data from mesh), and mesh manipulations (after scattering new values to mesh)."
\image latex gather-scatter.png "Two phases of calculation with amatos. Numerical calculations in vectors (after gathering data from mesh), and mesh manipulations (after scattering new values to mesh)." width = 10cm

*/

/**

@page ChapFEM FEM support

From version 2.0 <tt><b>amatos</b></tt> comes with a flexible support for finite
elements. Two types are predefined, a linear (unknowns defined in vertices) and
a quadratic (unknowns defined in vertices and edge centers) Lagrange element.

In order to provide a flexible interface to all kinds of finite elements,
<tt><b>amatos</b></tt> uses a signature data structure. The signature of a
specific finite element contains the essential information of that type of
element:
    - name, order, and total number of unknowns,
    - number of data items on the element's vertices,
    - number and position of data on element's edges,
    - number and position of data within the element.

The exact definition of this data structure is provided in the next section. For
defining a new FEM type, the following steps have to be taken:
    -# Edit module <tt>FEM_signature</tt> and define new FEM type in subroutine
       <tt>grid_signatureinit</tt> using the definitions of the default types
       as templates;
    -# Edit module <tt>FEM_signature</tt> and add a corresponding basis function
    calculation to subroutine <tt>grid_fembasis</tt>;
    -# recompile <tt><b>amatos</b></tt> and install it in your favorite directory;
    -# relink the application that uses the new FEM type with amatos.

This procedure involves alterations of <tt><b>amatos</b></tt>'s code and needs to
become a little involved with the structure of the sources. However, it is the
author's believe that code alterations of this kind are not necessary very often.
In any case, if users implement new element types, please send an e-mail with the
corresponding code fragments to Jörn Behrens (joern.behrens@uni-hamburg.de), I will try to
include them in future versions on <tt><b>amatos</b></tt>.

@section SecSignatureExamples Examples for the signatures
  -# For a linear element with values in each vertex, the signature is zero
     everywhere, except for
    - <tt>i_order = 1</tt>.
    - <tt>i_unknowns = 3</tt>.
    - <tt>i_npoints = 1</tt>.
  -# For a quadratic element with values in each vertex and in each edge center,
     the signature has following values:
    - <tt>i_order = 2</tt>.
    - <tt>i_unknowns = 6</tt>.
    - <tt>i_npoints = 1</tt>.
    - <tt>i_gpoints = 1</tt>.
    - <tt>r_gweights = (1/2, 1/2)</tt>.
    .
       This means, each nodal value is situated at position of the node
       (coordinates are known). Each edge value can be calculated by the
       following formula:
       \f[
        (x, y) = \mathtt{p\_edge\%i\_node(1)} \cdot \mathtt{r\_gweights(1)} + \mathtt{p\_edge\%i\_node(2)} \cdot \mathtt{r\_gweights(2)}
       \f]
  -# For a cubic element with one value in each vertex, two values in each edge,
     and one value in the element center, the signature has following values:
    - <tt>i_order = 3</tt>.
    - <tt>i_unknowns = 10</tt>.
    - <tt>i_npoints = 1</tt>.
    - <tt>i_gpoints = 2</tt>.
    - <tt>i_epoints = 1</tt>.
    - <tt>r_gweights = [(1/3, 2/3), (2/3, 1/3)]</tt>.
    - <tt>r_eweights = (1/3, 1/3, 1/3)</tt>.

The following table lists the allowed keywords in a signature (ftf) file,
together with their description, type and dimension and the corresponding item
name in the signature data structure:

Keyword                |Description                                                                                                   |Type and dimension             |Item name
-----------------------|--------------------------------------------------------------------------------------------------------------|-------------------------------|-------------
SIGNATURE_DESCRIPTION  |descriptive name of element type                                                                              |CHARACTER (len=DEF_fillen)     |c_sigdescr
ANALYTICBASIS_HANDLE   |entry handle for analytic basis function within amatos, if existent                                           |INTEGER                        |i_basishandle
POLY_DEGREE            |polynomial degree of element                                                                                  |INTEGER                        |i_degree
NODE_DOFS              |number of DOFs per node/vertex                                                                                |INTEGER                        |i_ndofs
EDGE_DOFS              |number of DOFs per edge                                                                                       |INTEGER                        |i_gdofs
ELMT_DOFS              |number of DOFs per element/face                                                                               |INTEGER                        |i_edofs
EDGE_QUADPOINTS        |number of quadrature points per edge (anpassen!)                                                              |INTEGER                        |i_gquadpts
ELMT_QUADPOINTS        |number of quadrature points per element/face                                                                  |INTEGER                        |i_equadpts
EDGE_PSINONZERO        |number of basis functions nonzero on a specific edge                                                          |INTEGER                        |i_gpsinonzero
EDGE_DOFCOORDINATES    |barycentric coordinates wrt edge of edge DOFs (i_gdofs many), coordinates based on local numbering of nodes   |REAL (DEF_egnodes, i_gdofs)    |r_gdofcoo
ELMT_DOFCOORDINATES    |barycentric coordinates of element/face DOFs (i_edofs many)                                                   |REAL (DEF_elnodes, i_edofs)    |r_edofcoo
EDGE_QUADCOORDINATES   |barycentric coordinates wrt edge of edge quadrature points, coordinates based on local numbering of nodes     |REAL(DEF_egnodes, i_gquadpts)  |r_gquadcoo
EDGE_QUADWEIGHTS       |weights for quadrature rule corresponding to edge quadrature points                                           |REAL(i_gquadpts)               |r_gquadwei
EDGE_PSIQUADPOINTS     |basis function evaluations at edge quad points (for one edge)                                                 |REAL (i_unknowns, i_gquadpts)  |r_gpsiquad
EDGE_PSIINDEX          |nonzero basis function indices for each edge                                                                  |INTEGER(i_eg_dofs, DEF_eledges)|i_gpsiidx
ELMT_QUADCOORDINATES   |barycentric coordinates of element/face quadrature points                                                     |REAL (DEF_elnodes, i_equadpts) |r_equadcoo
ELMT_QUADWEIGHTS       |weights for the quadrature rule corresponding to the element quadrature points                                |REAL (i_equadpts)              |r_equadwei
ELMT_PSIQUADPOINTS     |basis function evaluations at elmt quad points                                                                |REAL (i_unknowns, i_equadpts)  |r_epsiquad
DPSIDXI_MATRIX         |derivative matrix in \f$\xi\f$ direction for all basis functions on dof points (watch out for the correct DOF ordering!) |REAL (i_unknowns, i_unknowns)  |r_dpsidxi
DPSIDETA_MATRIX        |derivative matrix in \f$\eta\f$ direction for all basis functions on dof points (watch out for the correct DOF ordering!)|REAL (i_unknowns, i_unknowns)  |r_dpsideta
DOF_ROTATIONMATRIX     |permutations of dofs resulting from counterclockwise rotations of element                                     |INTEGER (i_unknowns, 3)        |i_rotation
DOF_REFLECTIONMATRIX   |permutations of dofs resulting from reflections on symmetry line through first node                           |INTEGER (i_unknowns, 2)        |i_reflection
ADAPT_PROLONGATION     |prolongation matrix for refinement (prolongate coarse grid solution to fine element)                          |REAL (i_unknowns, i_unknowns)  |r_prolong
ADAPT_RESTRICTION      |restriction matrix for coarsening (restrict fine grid solution to coarse element)                             |REAL (i_unknowns, i_unknowns)  |r_restrict

additionally in the signature structure:

- c_sigfile: name of signature file
- i_unknowns: total number of unknowns per element (computed during initialization)

@section SecVariablesReg Registering variables

In order to use different types of finite elements in an application, each
variable that is used, has to be registered with a specific FEM type. This has
to be done before any mesh items are created. The best point for registering
variables is right after the call to <tt>GRID_api::grid_initialize</tt>.

Once a variable has been registered, the appropriate memory will be allocated at
each unknown position corresponding to that element type. The type and order of
a finite element can be obtained by a call to
<tt>FEM_signature::grid_femtypequery</tt>. The FEM type identifier corresponding
to a registered variable is returned by <tt>FEM_signature::grid_femvarquery</tt>.
For a detailed description of these functions, see module <tt>GRID_api</tt>.

*/


/**

@page ChapConventions Conventions

There are not many conventions related to amatos. The few conventions are listed
here. Variables are named with a preceded character indicating the data type:
    - <tt>c_</tt> is a variable of type <tt>CHARACTER</tt>.
    - <tt>i_</tt> is a variable of type <tt>INTEGER</tt>.
    - <tt>l_</tt> is a variable of type <tt>LOGICAL</tt>.
    - <tt>p_</tt> is a variable of user declared type.
    - <tt>r_</tt> is a variable of type <tt>REAL</tt>.

In the following sections, when giving the syntax, we denote by the attribute
(<EM>opt.</EM>) an optional argument which can be omitted.

*/


/**

@page ChapIniTriang Initial Triangulation

The initial triangulation – usually consisting of only a few elements – has to be
given by the user. This definition is handed over to <tt><b>amatos</b></tt> by a
special input file. This section describes the input file (by default
<tt>Triang.dat</tt>).

The input file is constructed in a certain way: <var>Keywords</var> precede the
values for specified input parameters, <var>Comments</var> are marked by a ’ !’ or
’\#’ in the first column of a line. Each line contains either a <var>Keywords</var>
or a value, never both!

In <tt><b>amatos</b></tt> release 1.2 a new (alternative) file format has been
introduced. It is shorter and much more suitable for large initial triangulations.
Instead of giving <tt>NODE_*</tt>, <tt>EDGE_*</tt>, and <tt>ELEMENT_*</tt>
keywords (and data), new keywords <tt>NODES_DESCRIPTION</tt> and
<tt>ELEMENTS_DESCRIPTION</tt> are provided. Edges are calculated from the
information given in these two descriptions.

The following table lists the allowed keywords:

Keyword               |Description                                                                                                                      |Range of values [<b>default</b>]
----------------------|---------------------------------------------------------------------------------------------------------------------------------|--------------------------------
GRID_DIMENSION        |global parameter defining the grid’s space dimensions                                                                            |1 to 3 [<b>2</b>]
ELEMENT_VERTICES      |global parameter defining the shape of elements. In 2D for example, 3 is triangular mesh, 4 quadrilateral, etc.                  |3 to 4... [?]
NUMBER_OF_NODES       |this defines the total number of nodes in the inital mesh                                                                        |1 to integer range [<b>required</b>]
NUMBER_OF_EDGES       |this defines the total number of edges in the inital mesh                                                                        |1 to integer range [<b>required</b>]
NUMBER_OF_ELEMENTS    |this defines the total number of elements in the inital mesh                                                                     |1 to integer range [<b>required</b>]
DEF_INNERITEM         |this global value defines the attribute value for a grid item within the domain (no boundary)                                    |- integer range to 0 [<b>required</b>]
DEF_DIRICHLETBOUNDARY |this global value defines the attribute value for a grid item on a Dirichlet boundary section of the domain                      |- integer range to 0 [<b>required</b>]
DEF_NEUMANNBOUNDARY   |this global value defines the attribute value for a grid item on a Neumann boundary section of the domain. [Note: periodic boundary conditions are defined by the (positive) index number of the corresponding periodic partner item in the grid.]|- integer range to 0 [<b>required</b>]
NODE_INDEXNUMBER      |defines the (unique) index of a node. This keyword occurs as often as the value of NUMBER_OF_NODES indicates                     |1 to integer range [<b>required</b>]
NODE_COORDINATES      |defines a block of GRID_DIMENSION coordinates of the node                                                                        |- real range to + real range [<b>required</b>]
EDGE_INDEXNUMBER      |defines the (unique) index of an edge. This keyword occurs as often as the value of NUMBER_OF_EDGES indicates                    |1 to integer range [<b>required</b>]
EDGE_NODEINDICES      |two indices of nodes (defined in the previous section) which span the edge                                                       |1 to integer range [<b>required</b>]
EDGE_ELEMENTINDICES   |two indices of elements adjacent to the edge. 0 for one side if the edge is at a (non-periodic) boundary                         |1 to integer range [<b>required</b>]
EDGE_BOUNDARYCONDITION|defines the boundary condition according to the previosly defined values for Dirichlet, Neumann, or periodic boundary conditions |- integer range to + integer range [<b>required</b>]
ELEMENT_INDEXNUMBER   |defines the (unique) index of an element. This keyword occurs as often as the value of NUMBER_OF_ELEMENTS indicates|1 to integer range [<b>required</b>]
ELEMENT_NODEINDICES   |three indices of nodes which span the element                                                                                    |1 to integer range [<b>required</b>]
ELEMENT_EDGEINDICES   |three indices of edges which span the element                                                                                    |1 to integer range [<b>required</b>]
ELEMENT_MARKEDEDGE    |the local edge number that is marked for refinement (by bisection)                                                               |1 to 3 [<b>required</b>]
NODES_DESCRIPTION     |a list of rows with node descriptions for the short file format. The first entry is the index, the other two the (floating point) coordinates|1 to integer range, - real range to + real range [<b>alternative</b>]
ELEMENTS_DESCRIPTION  |a list of rows with the following integer values: index, node 1, node 2, node 3, marked edge, boundary condition for edge 1, edge 2, edge 3|1 to integer range (first 4 entries), 1 to 3, - integer range to + integer range (last 3 entries) [<b>alternative</b>]

A sample triangulation file can be found in the data subdirectory of the amatos
distribution.

*/


/**

@page ChapInstall Installation and Testing

@section SecDirStruct Directory Structure

This section describes installation of the <tt><b>amatos</b></tt> software
package. <tt><b>amatos</b></tt> is distributed as a source code
<tt>tar-file</tt> for Unix systems. The installation and execution has been
tested on IRIX, Solaris, and Linux Systems. A current list of tested operating
systems can be found in the docdirectory.

When unpacking the <tt>tar-archive</tt> with the following commands

        unix> gunzip amatos2d.tar.gz

        unix> tar xvf amatos2d.tar

a new directory is installed named <tt>amatos2d</tt> with a directory structure
as given in the directory structure list below. In <tt>data</tt> some example input files can be found,
while <tt>doc</tt> contains this documentation (as the reader might have
realized by now...). Subdirectories <tt>compile</tt> contains OS-specific files
for building the executables and libraries for <tt>aix</tt>, <tt>irix</tt>,
<tt>linux</tt>, and <tt>solaris</tt>. <tt>src</tt> contains the Fortran 90
sources. <tt>include</tt> and <tt>lib</tt> contain the libraries and module
files necessary for linking the executable.

The sources for <tt><b>amatos</b></tt> are all in subdirectory
<tt>src/gridgen</tt>. In <tt>src/test</tt> we put the sources for a test driver.
<tt>src/system</tt> contains some system dependent routines, mainly for command
line input (we provide NAGWare, Posix compliant and Standard variants).

Directory structure list:

- amatos2d
	- 3rdparty
		- poor_mans_lacpack
	- compile
		- Makefiles
		- aix_power
		- irix_mips
		- linux_ia32
		- linux_ia64
		- solaris_sparc

	- data
	- doc
	- eval
	- include
		- Makefiles
		- aix_power
		- irix_mips
		- linux_ia32
		- linux_ia64
		- solaris_sparc
	- lib
		- Makefiles
		- aix_power
		- irix_mips
		- linux_ia32
		- linux_ia64
		- solaris_sparc
	- src
		- gridgen
		- system
			- nag-f90
			- posix-f90
			- std-f90
		- test
		- timing

@section SecBuildLib Building Library and Test Driver

As a preliminary step, build supportive libraries in the <tt>3rdparty</tt>
directory:

-# If required (i.e. no optimized LAPACK interface is provided on your machine)
, build the “poor man’s LAPACKt’t’, provided for convenience here:

        unix> cd amatos2d/3rdparty/poor_mans_lapack/irix_mips

        unix> make all

The files <tt>libPMLAPACK.a</tt> and <tt>libPMLAPACK.so</tt> should be available
in <tt>lib/irix_mips</tt> after performing this step.

Now, the user has to decide whether the spherical version of amatos is required
or the planar version. There are two files called <tt>Makefile_*_amatos</tt>
and <tt>Makefile_*_samatos</tt> for building the planar and spherical versions
resp. in the <tt>compile/Makefiles</tt> directory (a file
<tt>Makefile_*PML_amatos</tt> is provided for illustrative reasons, when
linking with <tt>libPMLAPACLK.so</tt> is required). In Order to build the
software library containing the programming interface to <tt><b>amatos</b></tt>
the following easy procedure has to be taken.

-# Change into the directory corresponding to your target machine’s OS, e.g.

      unix> cd amatos2d/compile/irix_mips
-# Copy either <tt>Makefile_irix_amatos</tt> or
	<tt>Makefile_irix_samatos</tt> from the <tt>Makefiles</tt> directory to
	<tt>irix_mips</tt>, but as default there is already availabe
	<tt>Makefile</tt> for <tt>samatos</tt> in there.
-# Change location specific settings in the <tt>Makefile</tt>.
-# Type

      unix> make lib

If everything went right, there should be a files <tt>libamatos.a</tt> and
<tt>libamatos.so</tt> in the <tt>lib/irix_mips</tt> directory and some kind of
<tt>GRID_api.mod</tt> in the <tt>include/irix_mips</tt> directory.

Now, in order to build the test driver (an executable program, called
– guess – <tt>AMATOS</tt>) some similarly easy steps have to be taken, while still
residing in the <tt>compile/irix_mips</tt> directory.\n
Just type

      unix> make AMATOS

(for the spherical case type <tt>make SAMATOS</tt>).

All the above steps (except for building the support libraries) can also be
accomplished by just one command:

      unix> make all

@section SecRun Running the Test Driver

In order to test <tt><b>amatos</b></tt> the executable <tt>AMATOS</tt>, built
in the previous subsection, has to be executed. <tt>AMATOS</tt> can be called
with several command line options. A complete list of these options can be
obtained by calling

      unix> AMATOS -h

Before any useful output from <tt>AMATOS</tt> can be expected, however, some
data files have to be copied into the right position by typing

      unix> make datacopy

After that, a useful output should be obtained by calling

      unix> AMATOS -b -f Parameters.dat

@section SecInputParam Input Parameters for the Test Driver

Input for the executable <tt>AMATOS</tt> can be provided interactively or by
means of a <tt>Parameter</tt> file that can be read by the program in batch mode.
The program requires five different pieces of information:
	-# The finest level of refinement (Keyword <tt>FINE_GRID_LEVEL</tt>).
	-# The coarsest level of refinement (Keyword <tt>COARSE_GRID_LEVEL</tt>).
	-# NetCDF output option (Keyword <tt>NETCDF_FILE_PLOTTING</tt>).
	-# GMV output option (Keyword <tt>GMV_FILE_PLOTTING</tt>).
	-# Filename for initial triangulation (Keyword <tt>TRIANG_FILE_NAME</tt>).

The file is constructed in the same way as the file defining the initial
triangulation (see @ref ChapIniTriang).

*/


/**
@page ChapCopyright COPYRIGHT NOTICE

This software is provided for non-commercial use only. See the
license conditions in file LICENCE and the warranty conditions in
file WARRANTY.

Copyright (c) 1997-2003 Jörn Behrens


@page ChapLicence LICENSE

The use of amatos is hereby granted free of charge for an unlimited time,
provided the following rules are accepted and applied:
	-# You may use or modify this code for your own non commercial and non
violent purposes.
	-# The code may not be re-distributed without the consent of the authors.
	-# The copyright notice and statement of authorship must appear in all
copies.
	-# You accept the warranty conditions (see @ref ChapWarranty).
	-# In case you intend to use the code commercially, we oblige you
to sign an according licence agreement with the authors.



@page ChapWarranty WARRANTY

This code has been tested up to a certain level. Defects and weaknesses,
which may be included in the code, do not establish any warranties by
the authors.

The authors do not make any warranty, express or implied, or assume any
liability or responsibility for the use, acquisition or application of
this software.

*/
