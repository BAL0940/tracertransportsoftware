!*****************************************************************
!
!> @file FEM_param.F90
!> @brief includes module FEM_param
!
!*****************************************************************
!
! VERSION(S):
!  1. original version                      j. behrens  07/1997
!  2. block size f. hash table alloc added  j. behrens  07/1997
!  3. FEM support                           j. behrnes  01/2003
!  4. SFC support                           j. behrens  03/2004
!  5. initial values support                j. behrens  01/2006
!
!*****************************************************************
#if !defined(SGL_DBL) && !defined(DBL_DBL) && !defined(DBL_QUAD)
#define SGL_DBL
#endif

! MODULE DESCRIPTION:
!> defines parameters for the grid data structures
!
!> @note  use different affixes for different data types:
!>        n*_:  integer parameters,
!>        i*_:  integer variables,
!>        r*_:  real variables,
!>        p*_:  pointer variables,
!>        l*_:  logical variables,
!>
!>        *e_:  element associated variables,
!>        *g_:  edge associated variables,
!>        *n_:  node associated variables
!
MODULE FEM_param

  PUBLIC

!---------- real kind parameters: calculations in single,
!                                 some critical things in double precision
#ifdef SGL_DBL
  INTEGER, PARAMETER :: GRID_SR = selected_real_kind(6,20)
  INTEGER, PARAMETER :: GRID_DR = selected_real_kind(14,40)
#endif
!---------- real kind parameters: calculations in double,
!                                 some critical things in double as well
#ifdef DBL_DBL
  INTEGER, PARAMETER :: GRID_SR = selected_real_kind(14,40)
  INTEGER, PARAMETER :: GRID_DR = GRID_SR
#endif
!---------- real kind parameters: calculations in double,
!                                 some critical things in quad precision
#ifdef DBL_QUAD
  INTEGER, PARAMETER :: GRID_SR = selected_real_kind(14,40)
  INTEGER, PARAMETER :: GRID_DR = selected_real_kind(28,80)
#endif
!---------- integer kind parameters

  INTEGER, PARAMETER :: GRID_SI = selected_int_kind(8)
  INTEGER, PARAMETER :: GRID_DI = selected_int_kind(16)

!---------- size parameters

  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_elnodes= 3     !< no. of nodes
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_eledges= 3     !< no. of edges
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_elchild= 2     !< no. of children
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_egnodes= 2     !< no. of nodes
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_egelems= 2     !< no. of elements
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_egchild= 2     !< no. of elements
#ifdef SAMATOS
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_dimension= 3   !< no. of dimensions
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_dimspherical=2 !< no. of dimensions for lambda/phi
#elif AMATOS
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_dimension= 2   !< no. of dimensions
#endif
        INTEGER (KIND = GRID_SI), PARAMETER   :: DEF_dim2D= 2       !< no. of dimensions in plane
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_ndpatch= 16    !< elements in patch
!   INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_timesteps= 3   !< timesteps saved
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_timesteps= 2   !< timesteps saved
#ifdef SAMATOS
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_constno= 6     !< number of constituents
#elif AMATOS
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_constno= 5     !< number of constituents
#endif

!---------- size of value arrays parameters. Modify these, when modifiying FEM_siginit...

  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_evalsize= 1              !< size of real array (elmt)
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_gvalsize= DEF_constno    !< size of real array (edge)
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_nvalsize= 2*DEF_constno  !< size of real array (node)
  INTEGER (KIND = GRID_SI), PARAMETER         :: DEF_maxfemtypes=64           !< max no. of FEM types supported

!---------- definition parameters

  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_inner=  0       !< inner node/ edge
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_dirichlet= -1   !< Dirichlet boundary
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_neumann=   -2   !< Neumann boundary
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_unrefined=  0   !< Flag for unrefined
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_refined=  1     !< Flag for refined
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_pleasrefine=2   !< Flag for item to be refined
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_pleascoarse=3   !< Flag for item to be coarsened
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_erased=99       !< Flag for erased item

!---------- block size for hash table allocation

  INTEGER (KIND = GRID_SI), PARAMETER          :: DEF_hashblock= 512 !< Size of alloc blocks

!---------- pointer definitions for real arrays

  INTEGER (KIND = GRID_SI)               :: DEF_ucomp   !< Entry for u-component
  INTEGER (KIND = GRID_SI)               :: DEF_vcomp   !< Entry for v-component
#ifdef SAMATOS
  INTEGER (KIND = GRID_SI)               :: DEF_wcomp   !< Entry for v-component
#endif
  INTEGER (KIND = GRID_SI)               :: DEF_phi     !< Entry for geopot. height
  INTEGER (KIND = GRID_SI)               :: DEF_zeta    !< Entry for vorticity
  INTEGER (KIND = GRID_SI)               :: DEF_tracer  !< Entry for tracer

!---------- inernal item types

  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_nodetype= 1    !< Node
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_edgetype= 2    !< Edge
  INTEGER (KIND = GRID_SI), PARAMETER      :: DEF_elmttype= 3    !< Element

!---------- space-filling curve support

  INTEGER (KIND = GRID_SI)                     :: DEF_maxsfcbits     !< number of bits supported
  INTEGER (KIND = GRID_SI)                     :: DEF_sfcfrstbit     !< first significant bit
  INTEGER (KIND = GRID_SI)                     :: DEF_sfclvloffs     !< offset for levels

!---------- initial value computation in grid refinement

  LOGICAL                                      :: DEF_initvals       !< switch init. values

END MODULE FEM_param
