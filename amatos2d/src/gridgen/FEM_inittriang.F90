!*****************************************************************
!
!> @file FEM_inittriang.F90
!> @brief includes module FEM_inittriang
!
!*****************************************************************
!
! VERSION(S):
!  1. original version                j. behrens    08/96
!  2. added edges                     j. behrens    10/96
!  3. now with handles and time       j. behrens    10/96
!  4. FEM_handle added                j. behrens    07/97
!  5. hash table version              j. behrens    07/97
!  6. fem_create used                 j. behrens    10/97
!  7. now reading from file           j. behrens    12/97
!  8. updated for amatos version 1.0  j. behrens    11/2000
!  9. short input format              n. rakowsky   04/2001
!
!*****************************************************************
! MODULE DESCRIPTION:
!>  initializes a simple triangulation
!
MODULE FEM_inittriang
  USE MISC_globalparam
  USE MISC_error
  USE FEM_define
  USE FEM_handle
  USE FEM_create
  USE FEM_signature
  USE GRID_utils
  PRIVATE
  PUBLIC :: grid_create
  CONTAINS
!*****************************************************************
! DESCRIPTION of [SUBROUTINE grid_create]:
!> @brief creates a macro triangulation
!>
!> @param[out]      p_ghand     grid handle
!> @param[in]       c_name      input file name
!
  SUBROUTINE grid_create(p_ghand, c_name)

!---------- local declarations

    IMPLICIT NONE

    TYPE (grid_handle)                                        :: p_ghand
    CHARACTER (LEN=GRID_parameters%i_stringlength), OPTIONAL  :: c_name
    CHARACTER (LEN=GRID_parameters%i_stringlength)            :: c_nam
    TYPE (node), POINTER                                      :: p_ntmp
    TYPE (edge), POINTER                                      :: p_gtmp
    TYPE (elmt), POINTER                                      :: p_etmp
    INTEGER (KIND = GRID_SI)                                  :: i_tim, i_cnt, i_count, &
      i_iofil, i_iost, i_ioend, i_alct, i_tmp
    REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE        :: r_coo
    INTEGER (KIND = GRID_SI), DIMENSION(:,:), ALLOCATABLE     :: i_gnd, i_gel, i_end, i_eed
    INTEGER (KIND = GRID_SI), DIMENSION(:), ALLOCATABLE       :: i_gbn, i_eme
    INTEGER (KIND = GRID_SI), DIMENSION(:), ALLOCATABLE       :: i_pnd, i_peg
    CHARACTER (len=80)                                        :: c_filrow
    INTEGER (KIND = GRID_SI)                                  :: i_griddim, i_elvert, &
      i_nnodes, i_nedges, i_nelems, i_neumboun, i_innerboun, i_diriboun, &
      i_nodeno, i_edgeno, i_elmtno, i_per
#ifdef SAMATOS
    REAL (KIND = GRID_SR), DIMENSION(DEF_dimspherical)        :: r_scoo
#endif

!nr Variables needed for short input
    INTEGER (KIND = GRID_SI), DIMENSION(:,:), ALLOCATABLE :: i_eeb
    INTEGER (KIND = GRID_SI)                              :: i_node1, i_node2, i_eledge
    LOGICAL                                               :: l_short_input = .FALSE.
    LOGICAL                                               :: l_edge_registered, l_reorder
    CHARACTER (len=80)                                    :: a_message
    REAL (KIND = GRID_SR), DIMENSION(DEF_dimension)       :: r_1, r_2, r_3
#ifdef SAMATOS
    REAL (KIND = GRID_SR), DIMENSION(DEF_dimension)       :: r_1x2
#endif
    INTEGER (KIND = GRID_SI)                              :: i_help
    INTEGER (KIND = GRID_SI), DIMENSION(DEF_egnodes)      :: i_nm, i_np
    INTEGER (KIND = GRID_SI)                              :: i_bitpos
    INTEGER (KIND = GRID_DI)                              :: i_bitmap, i_dc
    INTEGER (KIND = GRID_SI)                              :: i_gbcoun
    INTEGER (KIND = GRID_SI)                              :: i_size

!---------- initialize grid handes

    p_grid(:)%i_etotal= 0
    p_grid(:)%i_gtotal= 0
    p_grid(:)%i_ntotal= 0
    p_ghand%i_gnumboun= 0

!---------- manipulate global time tag temporarily

    i_tim        = i_futuretime
    i_futuretime = p_ghand%i_timetag

!---------- allocate hashing tables

    CALL grid_initdatastruct

!---------- initialize file name

    file_present: IF(present(c_name)) THEN
      c_nam= c_name
    ELSE file_present
      c_nam= 'Triang.dat'
    END IF file_present

!---------- open file

    i_iofil= 16
    open(unit= i_iofil, file= c_nam, status= 'OLD', action= 'READ', iostat= i_iost)
    file_notopen: IF(i_iost /= 0) THEN
      IF(GRID_parameters%iolog > 0) &
        write(GRID_parameters%iolog,*) 'ERROR: [grid_create] Filename: ', c_nam
      CALL print_error(87)
    END IF file_notopen
    IF(GRID_parameters%iolog > 0) &
      write(GRID_parameters%iolog,*) 'INFO: [grid_create] Opened file ',c_nam,' on unit: ', i_iofil

!---------- loop to read all the lines of file

    read_loop: DO
      read(i_iofil,2000,iostat=i_ioend) c_filrow

!---------- if file ended

      file_end: IF(i_ioend /= 0) THEN
        close(i_iofil)
        IF(GRID_parameters%iolog > 0) &
          write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_iofil
        EXIT read_loop
      ELSE file_end

!---------- decide what to DO with line according to first character

        comment_line: IF(c_filrow(1:1) == '#' .OR. c_filrow(1:1) == '!') THEN
          CYCLE read_loop
        ENDIF comment_line

        data_type: select case (c_filrow(1:12))

        case ('GRID_DIMENSI')
          read(i_iofil,*) i_griddim
          check_dim: IF(i_griddim /= DEF_dimension) THEN
            CALL print_error(88)
          END IF check_dim

        case ('ELEMENT_VERT')
          read(i_iofil,*) i_elvert
          check_vert: IF(i_elvert /= DEF_elnodes) THEN
            CALL print_error(89)
          END IF check_vert

        case ('NUMBER_OF_NO')
          read(i_iofil,*) i_nnodes
          ALLOCATE(r_coo(DEF_dimension,i_nnodes), i_pnd(i_nnodes), stat=i_alct)
          IF(i_alct /= 0) THEN
            CALL print_error(90)
          END IF
          !-- resize node hash
          IF(i_nnodes > i_nhashmax) THEN
            i_size= CEILING(REAL(i_nnodes) / REAL(DEF_hashblock)) * DEF_hashblock
            CALL hash_realloc(i_nhashmax, i_size, c_action='nodehash')
          END IF
          !-- resize grid node index table
          IF(i_nnodes > i_ngridmax) THEN
            i_size= CEILING(REAL(i_nnodes) / REAL(DEF_hashblock)) * DEF_hashblock
            CALL hash_realloc(i_ngridmax, i_size, c_action='nodegrid')
          END IF

        case ('NUMBER_OF_ED')
          read(i_iofil,*) i_nedges
          ALLOCATE(i_gnd(DEF_egnodes,i_nedges), i_gel(DEF_egelems,i_nedges), &
          i_gbn(i_nedges), i_peg(i_nedges), stat=i_alct)
          IF(i_alct /= 0) THEN
            CALL print_error(91)
          END IF
          !-- edge hash
          IF(i_nedges > i_ghashmax) THEN
            i_size= CEILING(REAL(i_nedges) / REAL(DEF_hashblock)) * DEF_hashblock
            CALL hash_realloc(i_ghashmax, i_size, c_action='edgehash')
          END IF
          !-- resize grid edge index table
          IF(i_nedges > i_ggridmax) THEN
            i_size= CEILING(REAL(i_nedges) / REAL(DEF_hashblock)) * DEF_hashblock
            CALL hash_realloc(i_ggridmax, i_size, c_action='edgegrid')
          END IF

        case ('NUMBER_OF_EL')
          read(i_iofil,*) i_nelems
          ALLOCATE(i_end(DEF_elnodes,i_nelems), i_eed(DEF_eledges,i_nelems), &
          i_eme(i_nelems), stat=i_alct)
          IF(i_alct /= 0) THEN
            CALL print_error(92)
          END IF
          !-- resize element hash
          IF(i_nelems > i_ehashmax) THEN
            i_size= CEILING(REAL(i_nelems) / REAL(DEF_hashblock)) * DEF_hashblock
            CALL hash_realloc(i_ehashmax, i_size, c_action='elmthash')
          END IF
          !-- resize grid element index table
          IF(i_nelems > i_egridmax) THEN
            i_size= CEILING(REAL(i_nelems) / REAL(DEF_hashblock)) * DEF_hashblock
            CALL hash_realloc(i_egridmax, i_size, c_action='elmtgrid')
          END IF

        case ('DEF_INNERITE')
          read(i_iofil,*) i_innerboun

        case ('DEF_NEUMANNB')
          read(i_iofil,*) i_neumboun

        case ('DEF_DIRICHLE')
          read(i_iofil,*) i_diriboun

        case ('NODE_INDEXNU')
          read(i_iofil,*) i_nodeno
          check_nn: IF(i_nodeno > i_nnodes) THEN
            CALL print_error(93)
          END IF check_nn

        case ('EDGE_INDEXNU')
          read(i_iofil,*) i_edgeno
          check_gn: IF(i_edgeno > i_nedges) THEN
            CALL print_error(94)
          END IF check_gn

        case ('ELEMENT_INDE')
          read(i_iofil,*) i_elmtno
          check_en: IF(i_elmtno > i_nelems) THEN
            CALL print_error(95)
          END IF check_en

        case ('NODE_COORDIN')
          read(i_iofil,*) (r_coo(i_cnt,i_nodeno), i_cnt=1,DEF_dimension)

        case ('EDGE_NODEIND')
          read(i_iofil,*) (i_gnd(i_cnt,i_edgeno), i_cnt=1,DEF_egnodes)

        case ('EDGE_ELEMENT')
          read(i_iofil,*) (i_gel(i_cnt,i_edgeno), i_cnt=1,DEF_egelems)

        case ('EDGE_BOUNDAR')
          read(i_iofil,*) i_gbn(i_edgeno)

        case ('ELEMENT_NODE')
          read(i_iofil,*) (i_end(i_cnt,i_elmtno), i_cnt=1,DEF_elnodes)

        case ('ELEMENT_EDGE')
          read(i_iofil,*) (i_eed(i_cnt,i_elmtno), i_cnt=1,DEF_eledges)

        case ('ELEMENT_MARK')
          read(i_iofil,*) i_eme(i_elmtno)

!nr   And for a compressed format (alternative to the format used above)

!nr   Nodes: one line per node, containing
!nr          number of node (int), x-co-ord. (real), y-co-ord. (real)) [,z-co-ord,...]
        case ('NODES_DESCRI')
          DO i_cnt = 1, i_nnodes
            read(i_iofil,*) i_nodeno, r_coo(1:DEF_dimension,i_nodeno)

!nr  Some simple checks for consistency
            IF (i_nodeno > i_nnodes)  &
              CALL print_error(a_err='[grid_create] Node index is larger than number of nodes')
          END DO

!nr   Elements: one line per element, containing
!nr             number of element (int)
!nr             1. node (int), 2. node (int), 3. node (int),
!nr             relative number (i.e., 1, 2, or 3) of marked edge
!nr             boundary-condition of 1. edge (int), 2. edge (int), 3. edge (int)

!nr                     node 1
!nr                     /  \
!nr                    /    \
!nr            edge 3 /      \  edge 2
!nr                  /        \
!nr                 /          \
!nr                /            \
!nr               /              \
!nr          node 2 ----------- node 3
!nr                    edge 1

        case ('ELEMENTS_DES')

          IF (DEF_elnodes /= 3) THEN
            CALL print_error(a_err='[grid_create] Sorry, short input for triangular grids only')
          ENDIF

          l_short_input= .TRUE.
          ALLOCATE(i_eeb(DEF_eledges,i_nelems), stat=i_alct)
          IF (i_alct /= 0) CALL print_error(a_err='[grid_create] Could not allocate element array')

          DO i_cnt = 1,i_nelems
            read(i_iofil,*) i_elmtno,             &
                 i_end(1:DEF_elnodes,i_elmtno),   &
                 i_eme(i_elmtno),                 &
                 i_eeb(1:DEF_eledges,i_elmtno)

!nr  simple check for consistency
            IF (i_elmtno > i_nelems) CALL print_error   &
              (a_err='[grid_create] Element index is larger than number of elements')

          END DO


        END select data_type

      END IF file_end
    END DO read_loop

!nr Short input requires some work now.
!nr (As it has to be done only once per run, no effort was put
!nr  into optimization... never touch a running code ;-)
    short_input: IF (l_short_input) THEN
!nr   1. Construct edges
      i_gnd(:,:) = 0
      i_gel(:,:) = 0
      i_gbn(:) = 0
      i_edgeno = 0

!nr  loop through all elements
      elements: DO i_elmtno=1,i_nelems

!nr  loop through the 3 edges of the current element i_elmtno
        element_edges: DO i_eledge=1,DEF_eledges
          i_node1 = i_end(mod(i_eledge,3)+1,i_elmtno)
          i_node2 = i_end(mod(i_eledge+1,3)+1,i_elmtno)

          l_edge_registered = .FALSE.

!nr   An inner edge is shared with another element and might be registrated already.
!nr    => check!
          inner_edge: IF (i_eeb(i_eledge,i_elmtno) == i_innerboun) THEN

            registered_edges: DO i_cnt = 1,i_edgeno
              IF ((i_gnd(1,i_cnt)==i_node1 .AND. i_gnd(2,i_cnt)==i_node2)  .OR. &
                  (i_gnd(1,i_cnt)==i_node2 .AND. i_gnd(2,i_cnt)==i_node1)) THEN
!nr   Bingo, we've found the edge.
!nr   Update element-edge-number
                i_eed(i_eledge,i_elmtno) = i_cnt
!nr   The second neighbouring element of the edge is the current element
!nr   (or we made a mistake)
                IF (i_gel(2,i_cnt) == 0 .AND. i_gel(1,i_cnt) > 0 .AND. &
                    i_gbn(i_cnt) == i_innerboun) THEN
                  i_gel(2,i_cnt) = i_elmtno
                ELSE
!nr   error handling
                  IF (i_gel(2,i_cnt) /= 0) CALL print_error           &
                    (a_err='[grid_create] Edge has more than two neighbouring elements')
                  IF (i_gel(1,i_cnt) <= 0) CALL print_error           &
                    (a_err='[grid_create] Edge calculation: mystic internal error...')
                  IF (i_gbn(i_cnt) /= i_innerboun) CALL print_error   &
                    (a_err='[grid_create] Boundary edge has more than one neighbouring element')
                ENDIF
                l_edge_registered = .TRUE.
                exit registered_edges
              ENDIF
            END DO registered_edges

          ENDIF inner_edge

!nr   Not registered? Ok, let's do it now
          register_edge: IF (.NOT. l_edge_registered) THEN
            i_edgeno = i_edgeno+1

!nr   Check!
            IF (i_edgeno > i_nedges) CALL print_error    &
              (a_err='[grid_create] Number of edges is greater than indicated')

!nr   properties of the edge
            i_gnd(1,i_edgeno) = i_node1
            i_gnd(2,i_edgeno) = i_node2
            i_gel(1,i_edgeno) = i_elmtno
            i_gbn(i_edgeno)   = i_eeb(i_eledge,i_elmtno)
!nr   updating element
            i_eed(i_eledge,i_elmtno) = i_edgeno
          ENDIF register_edge

        END DO element_edges
      END DO elements

      IF (i_edgeno < i_nedges) THEN
        write(a_message,1010) i_edgeno
1010            format('[grid_create] Number of egdes less than indicated: #edges=',i8)
        CALL print_error(a_err=a_message)
      ENDIF

      DEALLOCATE(i_eeb)

    ENDIF short_input

!nr  check for counter-clockwise order of the elements' nodes

!nr  in 3D, this check makes only sense for surfaces with known orientation, e.g., the sphere.
!nr  for other surfaces, we have to trust the user...

    check_elems: DO i_cnt = 1,i_nelems

      r_1 = r_coo(:,i_end(1,i_cnt))
      r_2 = r_coo(:,i_end(2,i_cnt))
      r_3 = r_coo(:,i_end(3,i_cnt))

      dimension: IF (DEF_dimension==3) THEN
!nr  in 3D, this check makes only sense for surfaces with known orientation, e.g., the sphere.
!nr  for other surfaces, we have to trust the user...
#ifdef SAMATOS
        r_1x2 = cross_product(r_1,r_2)
        l_reorder = (dot_product(r_1x2,r_3) <= 0)
#elif AMATOS
        l_reorder = .FALSE.
#endif
      ELSEIF (DEF_dimension==2) THEN
        r_1 = r_1 - r_3
        r_2 = r_2 - r_3
        l_reorder = (r_1(1)*r_2(2) - r_1(2)*r_2(1) <= 0)
      ENDIF dimension

!nr  clockwise order? => submit a warning and
!nr  reorder by interchanging 2. and 3. node
      reorder: IF (l_reorder) THEN

        write(a_message,1020) i_cnt
1020            format('[grid_create] Nodes of element No. ',i8,' are reorderd counterclockwise')
        CALL print_error(10, a_err= a_message)

        i_help         = i_end(3,i_cnt)
        i_end(3,i_cnt) = i_end(2,i_cnt)
        i_end(2,i_cnt) = i_help

        IF (i_eme(i_cnt) == 2) THEN
          i_eme(i_cnt) = 3
        ELSEIF (i_eme(i_cnt) == 3) THEN
          i_eme(i_cnt) = 2
        ENDIF

        i_help         = i_eed(3,i_cnt)
        i_eed(3,i_cnt) = i_eed(2,i_cnt)
        i_eed(2,i_cnt) = i_help

      ENDIF reorder
    END DO check_elems

!---------- find (and treat) periodic boundaries (indicated by edge boundary cond > 0)

    i_peg= 0
    i_pnd= 0

    DO i_count= 1,i_nedges
      periodic: IF((i_gbn(i_count) > DEF_inner) .AND. (i_peg(i_count) == 0)) THEN

        i_per = i_gbn(i_count)

!---------- identify edges: i_count is the master

        i_peg(i_count)= i_per
        i_peg(i_per)  = i_count
!         i_gbn(i_count)= i_count

!---------- find corresp. nodes

        i_nm(:)= i_gnd(:,i_per)
        i_np(:)= i_gnd(:,i_count)

!---------- sort them in the same way

        IF(r_coo(1,i_nm(1)) > r_coo(1,i_nm(2))) THEN
          IF(r_coo(2,i_nm(1)) <= r_coo(2,i_nm(2))) THEN
            i_tmp= i_nm(1); i_nm(1)= i_nm(2); i_nm(2)= i_tmp
          END IF
        ELSE
          IF(r_coo(2,i_nm(1)) > r_coo(2,i_nm(2))) THEN
            i_tmp= i_nm(1); i_nm(1)= i_nm(2); i_nm(2)= i_tmp
          END IF
        END IF
        IF(r_coo(1,i_np(1)) > r_coo(1,i_np(2))) THEN
          IF(r_coo(2,i_np(1)) <= r_coo(2,i_np(2))) THEN
            i_tmp= i_np(1); i_np(1)= i_np(2); i_np(2)= i_tmp
          END IF
        ELSE
          IF(r_coo(2,i_np(1)) > r_coo(2,i_np(2))) THEN
            i_tmp= i_np(1); i_np(1)= i_np(2); i_np(2)= i_tmp
          END IF
        END IF

!---------- identify nodes

        IF(i_pnd(i_nm(1)) /= 0) THEN
          i_pnd(i_np(1)) = i_pnd(i_nm(1))
        ELSE
          i_pnd(i_nm(1)) = i_nm(1)
          i_pnd(i_np(1)) = i_nm(1)
        END IF

        IF(i_pnd(i_nm(2)) /= 0) THEN
          i_pnd(i_np(2)) = i_pnd(i_nm(2))
        ELSE
          i_pnd(i_nm(2)) = i_nm(2)
          i_pnd(i_np(2)) = i_nm(2)
        END IF

      END IF periodic
    END DO

!---------- create nodes

    node_create: DO i_count=1,i_nnodes
#ifdef SAMATOS
      r_scoo=  kart_geo(r_coo(:,i_count))
      r_coo(:,i_count)= geo_kart(r_scoo)
#endif
      p_ntmp             => create_node(r_coo(:,i_count), p_periodic=i_pnd(i_count))
      p_nhash(i_count)%np=> p_ntmp
      i_ngrid(i_count,i_futuretime)= i_count
    END DO node_create

!---------- ok, nodes created, yeah! updating handles

    p_ghand%i_ntotal  = i_nnodes
    p_ghand%i_nnumber = p_ghand%i_ntotal

!---------- create edges

    i_gbcoun = 0
    edge_create: DO i_count=1,i_nedges
      boundary: IF(i_gbn(i_count) == i_innerboun) THEN
        i_tmp= DEF_inner
      ELSE IF(i_gbn(i_count) == i_diriboun) THEN boundary
        i_tmp= DEF_dirichlet
      ELSE IF(i_gbn(i_count) == i_neumboun) THEN boundary
        i_tmp= DEF_neumann
      ELSE boundary
        i_tmp= i_gbn(i_count)
      END IF boundary
      IF(i_gbn(i_count) < i_innerboun) THEN
        i_gbcoun = i_gbcoun + 1
      END IF
      p_gtmp => create_edge(i_gnd(:,i_count), p_elements= i_gel(:,i_count), &
                            i_boundary=i_tmp, i_stat=DEF_unrefined, &
                            p_periodic= i_peg(i_count))
      p_ghash(i_count)%gp   => p_gtmp
      i_ggrid(i_count,i_futuretime)=  i_count
!!$     i_gfine(i_count,i_futuretime)=  i_count
    END DO edge_create

!---------- update handle

    p_ghand%i_gtotal   = i_nedges
    p_ghand%i_gnumber  = p_ghand%i_gtotal
    p_ghand%i_gnumboun = i_gbcoun
    p_ghand%i_gnumfine = p_ghand%i_gtotal

!fk-------- save coarse level edge count for search initialisation
    p_ghand%i_gnumcrs     = p_ghand%i_gtotal
    p_ghand%i_gnumbouncrs = p_ghand%i_gnumboun

!---------- create elements

    elmt_create: DO i_count=1,i_nelems
      p_etmp => create_elmt(i_end(:,i_count), i_eed(:,i_count), &
                            i_mark = i_eme(i_count), i_level = 1, i_stat = 0)
      p_ehash(i_count)%ep   => p_etmp
      i_egrid(i_count,i_futuretime)=  i_count
!!$     i_efine(i_count,i_futuretime)=  i_count
    END DO elmt_create

!---------- uff, elements created as well! update handles

    p_ghand%i_etotal  = i_nelems
    p_ghand%i_enumber = p_ghand%i_etotal
    p_ghand%i_enumfine= p_ghand%i_etotal

!fk-------- save coarse level element count for search initialisation
    p_ghand%i_enumcrs = p_ghand%i_etotal

!---------- calculate sfc offsets for initial triangles, first check for bitsize

    DEF_sfclvloffs= p_ghand%i_reflvlbnd+ 1 ! leave one level for closures
    i_tmp= floor(log(REAL(i_nelems))/log(REAL(2)))     ! calculate length of necessary bits
    IF((DEF_maxsfcbits-DEF_sfclvloffs) < i_tmp) &
      CALL print_error(a_err='[grid_create]: sfc indexing not possible, too many initial triangles or levels!')
    i_bitpos= DEF_sfclvloffs
    i_bitmap= 0_GRID_DI
    sfc_ind: DO i_count=1,i_nelems
      i_dc= INT(i_count-1,GRID_DI)
      i_bitmap=ishft(i_dc,DEF_sfclvloffs)
      p_ehash(i_count)%ep%att%i_sfcb= i_bitmap
    END DO sfc_ind
    sfc_maxtest: DO i_count=DEF_maxsfcbits-1,0,-1
      IF(btest(i_bitmap,i_count)) THEN
        DEF_sfcfrstbit= i_count
        EXIT sfc_maxtest
      END IF
    END DO sfc_maxtest

!---------- set global level info

    p_ghand%i_minlvl=1
    p_ghand%i_maxlvl=1

!---------- set global number of DOFs

    DO i_cnt=1,GRID_femtypes%i_number
      p_ghand%i_unknowns(i_cnt)= &
        p_ghand%i_nnumber* GRID_femtypes%p_type(i_cnt)%sig%i_ndofs+ &
        p_ghand%i_gnumfine* GRID_femtypes%p_type(i_cnt)%sig%i_gdofs+ &
        p_ghand%i_enumfine* GRID_femtypes%p_type(i_cnt)%sig%i_edofs
    END DO

!---------- deallocate temporary workspace

    DEALLOCATE(r_coo)
    DEALLOCATE(i_gnd, i_gel, i_gbn, i_end, i_eed, i_eme, i_pnd, i_peg)

!---------- recover global time tag

    i_futuretime= i_tim

    RETURN
 2000   FORMAT(a80)
  END SUBROUTINE grid_create

END MODULE FEM_inittriang
