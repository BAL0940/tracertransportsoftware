!*****************************************************************
!
!> @file MISC_datasph.f90
!> @brief includes module MISC_dataph
!
!*****************************************************************
!
! VERSION(S):
!  1. original version              j. behrens    03/2001
!
!*****************************************************************
! MODULE DESCRIPTION:
!> provides some dummy functions for testing amatos
!
MODULE MISC_datasph
  USE MAIN_parameters
  USE GRID_api
  PRIVATE
  PUBLIC :: calc_trac, calc_uv
  PUBLIC :: calc_gradfunc, calc_gradgrad
  PUBLIC :: calc_femsol, calc_femrhs
  CONTAINS
!*****************************************************************
! DESCRIPTION of [FUNCTION calc_trac]:
!> @brief calculates a tracer (scalar) function
!>
!> @param[in]       r_coo     coordinate
!> @return          r_res     result
!
  FUNCTION calc_trac(r_coo) RESULT (r_res)

    IMPLICIT NONE

!---------- local declarations

    REAL, DIMENSION(GRID_dimension), INTENT(in)    :: r_coo
    REAL                                           :: r_res
    REAL                                           :: r_tmp
    REAL, DIMENSION(GRID_dimension)                :: r_zero

    r_res  = 0.0
    r_zero = 0.5
    r_tmp  = veclen((r_zero-r_coo))
    IF(r_tmp < 1.0) THEN
      r_res = cos(r_tmp)
    END IF

    RETURN
  END FUNCTION calc_trac

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_uv]:
!> @brief calculates a velocity (vector) function
!>
!> @param[in]       r_coo     coordinate
!> @return          r_res     velocity vector
!
  FUNCTION calc_uv(r_coo) RESULT (r_res)

    IMPLICIT NONE

!---------- local declarations

    REAL, DIMENSION(GRID_dimension), INTENT(in)    :: r_coo
    REAL, DIMENSION(GRID_dimension)                :: r_res

    r_res    = 0.0
    r_res(1) = r_coo(2)
    r_res(2) = -r_coo(1)

    RETURN
  END FUNCTION calc_uv

!*****************************************************************
! DESCRIPTION of [FUNCTION veclen]:
!> @brief short description
!>
!> @param[in]       r_vec
!> @return          r_rst
!
  FUNCTION veclen(r_vec) RESULT(r_rst)

    IMPLICIT NONE

!---------- local declarations

    REAL, DIMENSION(GRID_dimension)   :: r_vec
    REAL                              :: r_rst, r_tmp

!---------- calculate vector crossproduct, 3D only

    r_tmp = dot_product(r_vec, r_vec)
    r_rst = sqrt(r_tmp)

  END FUNCTION veclen

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_gradfunc]:
!> @brief short description
!>
!> @param[in]       r_coord
!> @return          r_val
!
  FUNCTION calc_gradfunc(r_coord) RESULT (r_val)

    IMPLICIT NONE

!---------- local declarations

    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR)                                         :: r_val

!---------- f= (1+ x* y)* e^z

    r_val = (1._GRID_SR + r_coord(1)* r_coord(2)) * exp(r_coord(3))

    RETURN
  END FUNCTION calc_gradfunc

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_gradgrad]:
!> @brief short description
!>
!> @param[in]       r_coord
!> @return          r_val
!
  FUNCTION calc_gradgrad(r_coord) RESULT (r_val)

    IMPLICIT NONE

!---------- local declarations

    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)              :: r_val

!---------- not yet implemented...

    r_val    = 0.0_GRID_SR
    r_val(1) = r_coord(2)*exp(r_coord(3))
    r_val(2) = r_coord(1)*exp(r_coord(3))
    r_val(3) = (1._GRID_SR + r_coord(1)*r_coord(2)) * exp(r_coord(3))

    RETURN
  END FUNCTION calc_gradgrad

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_femrhs]:
!> @brief short description
!>
!> @param[in]       r_coord
!> @return          r_val
!
  FUNCTION calc_femrhs(r_coord) RESULT (r_val)

    IMPLICIT NONE

!---------- local declarations


    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR)                                         :: r_val

    REAL (KIND = GRID_SR)                                         :: r_x, &
      r_y, r_z

!---------- f = -(x* y* (z* z+ 6* (z+1))+ z* (z+2))* e^z

    r_x   = r_coord(1)
    r_y   = r_coord(2)
    r_z   = r_coord(3)
    r_val = -(r_x*r_y * (r_z*r_z + 6._GRID_SR*(r_z + 1._GRID_SR)) + &
              r_z * (r_z + 2._GRID_SR)) * exp(r_z)

    RETURN
  END FUNCTION calc_femrhs

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_femsol]:
!> @brief short description
!>
!> @param[in]     r_coord
!> @return        r_val
!
  FUNCTION calc_femsol(r_coord) RESULT (r_val)

!---------- local declarations

    IMPLICIT NONE

    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR)                                         :: r_val

!---------- f= (1+ x* y)* e^z

    r_val = (1._GRID_SR + r_coord(1)*r_coord(2)) * exp(r_coord(3))

    RETURN
  END FUNCTION calc_femsol

!*****************************************************************
END MODULE MISC_datasph
