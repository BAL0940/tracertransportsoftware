!*****************************************************************
!
!> @file MISC_mw.f90
!> @brief includes module MISC_mw
!
!*****************************************************************
!
! VERSION(S):
!  1. original version              j. behrens    03/2001
!
!*****************************************************************
! MODULE DESCRIPTION:
!> short description
!
MODULE MISC_mw
  USE MAIN_parameters
  USE GRID_api
  CONTAINS
!*****************************************************************
! DESCRIPTION of [FUNCTION mesh_width]:
!> @brief mesh width defining function
!>
!> @param[in]       r_coos      spherical coordinate
!> @return          r_wid       width value
!
  FUNCTION mesh_width(r_coos) RESULT (r_wid)

!---------- local declarations

    IMPLICIT NONE
    REAL, DIMENSION(GRID_dimspherical), INTENT(in)  :: r_coos
    REAL                                            :: r_wid

    r_wid = r_coos(1) * r_coos(2)
!   r_wid = 1.

    RETURN
  END FUNCTION mesh_width

!*****************************************************************
END MODULE MISC_mw
