!*****************************************************************
!
!> @file Amatos.F90
!> @brief includes program gridtest
!
!*****************************************************************
!
! NAME:
!   partest
! FUNCTION:
!   test the grid generation routines from AMATOS
! SYNTAX:
!   gridtest ...
! ON INPUT:
!   try AMATOS -h in order to get a list of command line arguments
! ON OUTPUT:
!   several files with some results, format to be chosen with parameter file
! CALLS:
!
! COMMENTS:
!
! LIBRARIES:
!   uses the amatos library
! REFERENCES:
!   J. Behrens: "Adaptive Mesh generator for Atmospheric and Ocean
!   Simulations", Technical Report TUM M0409, Technische Universitaet
!   Muenchen, 2004
!   (URL: http://www-lit.ma.tum.de/veroeff/html/040.65008.html)
! VERSION(S):
!  1. original version                j. behrens    08/1997
!  2. now with new grid generator     j. behrens    05/1998
!  3. amatos-1.0 compliant            j. behrens    11/2000
!  4. consolidations and fem support  j. behrens    07/2005
!
!*****************************************************************
! PROGRAM DESCRIPTION:
!> short description
!
PROGRAM gridtest

!---------- uses

  USE MAIN_parameters
  USE IO_utils
#ifndef NO_NETCDF
  USE IO_netcdfplot
#endif
  USE IO_gmvplot
  USE MISC_testgrad
  USE MISC_testfem
  USE MISC_datafun
  USE GRID_api
  USE MISC_outputdata
  USE IO_vtuplot

!---------- local declarations

  IMPLICIT NONE

  INTEGER (KIND = GRID_SI)                                  :: i_tmp, i_err, i_cnt, i_alct
  INTEGER (KIND = GRID_SI), PARAMETER                       :: i_vertnum=4
  INTEGER (KIND = GRID_SI), PARAMETER                       :: i_redirout=8
  INTEGER (KIND = GRID_SI), PARAMETER                       :: i_redirlog=7
  INTEGER (KIND = GRID_SI), PARAMETER                       :: i_maxiter=25
  REAL (KIND= GRID_SR), DIMENSION(GRID_dimension,i_vertnum) :: r_vertinit
  INTEGER (KIND = GRID_SI), DIMENSION(:), ALLOCATABLE       :: i_mark
  INTEGER (KIND = GRID_SI), DIMENSION(:,:), ALLOCATABLE     :: i_nodes
  REAL (KIND= GRID_SR), DIMENSION(:), ALLOCATABLE           :: r_diams
  REAL (KIND= GRID_SR), DIMENSION(:,:), ALLOCATABLE         :: r_coords
  REAL (KIND= GRID_SR), DIMENSION(:,:), ALLOCATABLE         :: r_velocity
  REAL (KIND= GRID_SR), DIMENSION(GRID_dimension)           :: r_xy
  REAL (KIND= GRID_SR)                                      :: r_third=1./3.
  LOGICAL                                                   :: l_grd, l_adapgo
  CHARACTER (len=32)                                        :: c_mfile
  CHARACTER (len=28)                                        :: c_tmp
  CHARACTER                                                 :: c_dummy
  INTEGER (KIND = GRID_SI)                                  :: i_fst, i_hightype
  REAL (KIND= GRID_SR)                                      :: r_rad=0.15, r_rfc=0.6
  REAL (KIND= GRID_SR)                                      :: r_long, r_short, &
    r_mindiam, r_maxdiam, r_meandia, r_diaratio
  INTEGER (KIND = GRID_SI)                                  :: i_newitm, i_adapt, i_unknow
  INTEGER (KIND = GRID_SI), PARAMETER                       :: i_vallen=3
  INTEGER (KIND = GRID_SI), DIMENSION(i_vallen)             :: i_valind
  INTEGER (KIND = GRID_SI), DIMENSION(:), ALLOCATABLE       :: i_femtyp

!---------- read command line options

  CALL io_getcmdline(p_contr)

!---------- read user input

  interactive_input: IF(p_contr%cmd%l_interactive) THEN
    CALL io_getinterinput(p_contr)
  ELSE interactive_input
    CALL io_getbatchinput(p_contr)
  END IF interactive_input

!---------- initialize grid generator

  IF(p_contr%io%i_numsigs > 0) THEN
    ALLOCATE(i_femtyp(p_contr%io%i_numsigs+1), stat=i_alct)
    IF(i_alct /= 0) CALL grid_error(c_error='[MAIN]: Could not allocate i_femtyp')
    i_femtyp(1)= grid_registerfemtype('default.ftf')
    DO i_cnt=1,p_contr%io%i_numsigs
      i_femtyp(i_cnt+1)= grid_registerfemtype(p_contr%io%c_sigfiles(i_cnt))
      i_hightype= i_femtyp(i_cnt+1)
    END DO
  END IF

  IF(p_contr%cmd%l_output) THEN
    IF(p_contr%cmd%l_logging) THEN
      CALL grid_initialize(i_output=i_redirout, i_logging=i_redirlog)
    ELSE
      CALL grid_initialize(i_output=i_redirout)
    END IF
  ELSE
    IF(p_contr%cmd%l_logging) THEN
      CALL grid_initialize(i_logging=i_redirlog)
    ELSE
      CALL grid_initialize
    END IF
  END IF

!---------- register new variables first: low order, then high order

  IF(p_contr%io%i_numsigs > 0) THEN
    i_tmp=i_femtyp(1)
  ELSE
    i_tmp= 1
    i_hightype= 1
  END IF
  AMAT_var1= grid_registerfemvar(i_tmp,i_length=2)
  AMAT_var2= AMAT_var1+1
  AMAT_var3= grid_registerfemvar(i_tmp)
  AMAT_var4= grid_registerfemvar(i_tmp)

  AMAT_hi1= grid_registerfemvar(i_hightype,i_length=2)
  AMAT_hi2= AMAT_hi1+1
  AMAT_hi3= grid_registerfemvar(i_hightype)
  AMAT_hi4= grid_registerfemvar(i_hightype)

!---------- FEM definition

  CALL grid_femtypequery(1, i_unknowns=i_unknow)
!   IF(i_unknow /= 3) THEN
!     CALL grid_error(c_error='ERROR: Unknowns of FEM type 1 .ne. 3, stopped ...')
!   END IF

  WRITE(*,2050)

!---------- initialize grid parameters

  CALL grid_setparameter(p_grid, i_coarselevel= p_contr%phy%i_crslevel, &
                                 i_finelevel= p_contr%phy%i_reflevel)

!---------- define domain

  r_vertinit= RESHAPE((/ 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0 /),(/ 2, 4 /))
  CALL grid_definegeometry(i_vertnum, r_vertexarr= r_vertinit)

!---------- create initial triangulation

  CALL grid_createinitial(p_grid, c_filename=p_contr%io%c_triangfile)

  WRITE(*,2051)


!---------- plot grid

  ncd1_plot: IF(p_contr%io%l_netcdf) THEN
#ifndef NO_NETCDF
    CALL plot_netcdf(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
#else
    CALL grid_error(c_error='[AMATOS]: NetCDF library not installed - cannot write data!')
#endif
  END IF ncd1_plot
  gmv1_plot: IF(p_contr%io%l_gmv) THEN
    CALL plot_gmv(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
  END IF gmv1_plot
  vtu1_plot: IF(p_contr%io%l_vtu) THEN
    CALL generate_vtu(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
  END IF vtu1_plot

  WRITE(*,2052)

!---------- adaptation loop

  WRITE(*,2070) p_grid(i_timeplus)%i_nnumber
  i_adapt=0
  l_adapgo=.TRUE.
  r_diaratio= 0.0_GRID_SR

  adapt_loop: DO WHILE(l_adapgo)

    diam_control: IF(p_contr%phy%l_diamcontr) THEN

!---------- mark some elements for refinement

      ALLOCATE(r_diams(p_grid(i_timeplus)%i_enumfine), stat= i_alct)
      IF(i_alct /= 0) THEN
        CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
      END IF
      ALLOCATE(i_mark(p_grid(i_timeplus)%i_enumfine), stat= i_alct)
      IF(i_alct /= 0) THEN
        CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
      END IF
      i_mark= 0

      CALL grid_getinfo(p_grid(i_timeplus), &
        l_finelevel= .TRUE., r_elementwidth= r_diams)

      r_maxdiam= maxval(r_diams)
      r_mindiam= minval(r_diams)
      r_diaratio= r_maxdiam/r_mindiam
      r_meandia= (r_mindiam+ r_maxdiam)* 0.5_GRID_SR
      IF(r_diaratio > p_contr%phy%r_diam) THEN
        DO i_cnt=1, p_grid(i_timeplus)%i_enumfine
          IF(r_diams(i_cnt) > r_meandia) THEN
            i_mark(i_cnt)= GRID_pleaserefine
          END IF
        END DO
      ELSE
        l_adapgo=.FALSE.
      END IF

    ELSE diam_control

!---------- mark some elements for refinement

      ALLOCATE(i_nodes(GRID_elementnodes, p_grid(i_timeplus)%i_enumfine), &
               stat= i_alct)
      IF(i_alct /= 0) &
        CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
      ALLOCATE(r_coords(GRID_dimension, p_grid(i_timeplus)%i_nnumber), &
               stat= i_alct)
      IF(i_alct /= 0) &
        CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
      ALLOCATE(i_mark(p_grid(i_timeplus)%i_enumfine), stat= i_alct)
      IF(i_alct /= 0) &
        CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
      i_mark= 0

      CALL grid_getinfo(p_grid(i_timeplus), &
        l_finelevel= .TRUE., i_elementnodes= i_nodes, &
        r_nodecoordinates=r_coords)

      DO i_cnt=1, p_grid(i_timeplus)%i_enumfine
        r_xy(1)= SUM(r_coords(1,i_nodes(:,i_cnt)))* r_third- 0.5
        r_xy(2)= SUM(r_coords(2,i_nodes(:,i_cnt)))* r_third- 0.5
        IF(DOT_PRODUCT(r_xy,r_xy) <= r_rad) THEN
          i_mark(i_cnt)= GRID_pleaserefine
        END IF
      END DO
      r_rad= r_rad* r_rfc
    END IF diam_control

    CALL grid_putinfo(p_grid(i_timeplus), &
      l_finelevel= .TRUE., i_elementstatus= i_mark)

    IF(ALLOCATED(r_coords)) DEALLOCATE(r_coords)
    IF(ALLOCATED(i_nodes))  DEALLOCATE(i_nodes)
    IF(ALLOCATED(i_mark))   DEALLOCATE(i_mark)
    IF(ALLOCATED(r_diams))  DEALLOCATE(r_diams)

!---------- adapt grid

    CALL grid_adapt(p_grid(i_timeplus), l_grd)
    IF(p_grid(i_timeplus)%i_maxlvl >= p_grid(i_timeplus)%i_reflvlbnd) &
      l_adapgo=.FALSE.
    i_adapt= i_adapt+1
    IF(i_adapt > i_maxiter) l_adapgo=.FALSE.

  END DO adapt_loop

  WRITE(*,2053)
  WRITE(*,2054) i_adapt
  WRITE(*,2071) p_grid(i_timeplus)%i_nnumber
  IF(i_adapt >= i_maxiter) WRITE(*,2072) r_diaratio
  IF(p_contr%phy%l_diamcontr) WRITE(*,2073) r_diaratio

!---------- solve Poisson's equation for test reasons on highest type of element

  CALL poisson_test(p_grid(i_timeplus),i_hightype)
  WRITE(*,2060)

!---------- do some calculations on the grid
!           WARNING: THIS IS ONLY VALID FOR GRID_dimension = {2,3} !!!

  ALLOCATE(r_coords(GRID_dimension,p_grid(i_timeplus)%i_unknowns(i_femtyp(1))), &
    r_velocity(i_vallen,p_grid(i_timeplus)%i_unknowns(i_femtyp(1))), stat= i_alct)
  IF(i_alct /= 0) &
    CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
  i_valind= (/GRID_ucomp, GRID_vcomp, GRID_tracer/)

!---------- compute velocity like function values and a tracer

  CALL grid_getinfo(p_grid(i_timeplus), i_femtype=i_femtyp(1), r_dofcoordinates= r_coords)
  DO i_cnt=1,p_grid(i_timeplus)%i_unknowns(i_femtyp(1))
    r_velocity(1:GRID_dimension,i_cnt)= calc_uv(REAL(r_coords(:,i_cnt)))
    r_velocity(i_vallen,i_cnt)        = calc_trac(REAL(r_coords(:,i_cnt)))
  END DO
  CALL grid_putinfo(p_grid(i_timeplus), r_dofvalues=r_velocity, &
    i_arraypoint= i_valind)
  WRITE(*,2059)

  ALLOCATE(i_mark(p_grid(i_timeplus)%i_nnumber), stat= i_alct)
  IF(i_alct /= 0) &
    CALL grid_error(c_error='ERROR: Temporary array i_mark not allocated, stopped ...')
  CALL grid_getinfo(p_grid(i_timeplus),i_newsdepth=(i_adapt-1), &
                    i_nlength=i_newitm, i_nodeindex=i_mark)
  DO i_cnt=1,i_newitm
    r_velocity(i_vallen,i_mark(i_cnt))= &
    r_velocity(i_vallen,i_mark(i_cnt))+ 1.0_GRID_SR
  END DO
  CALL grid_putinfo(p_grid(i_timeplus),i_newsdepth=(i_adapt-1), &
       i_arraypoint=(/GRID_zeta/), r_nodevalues=r_velocity(i_vallen:i_vallen,:))
  DEALLOCATE(r_coords, r_velocity, i_mark)
  WRITE(*,20591)

!---------- do some calculations on the grid, test higher order elements
!           WARNING: THIS IS ONLY VALID FOR GRID_dimension = {2,3} !!!

  ALLOCATE(r_coords(GRID_dimension,p_grid(i_timeplus)%i_unknowns(i_hightype)), &
    r_velocity(1,p_grid(i_timeplus)%i_unknowns(i_hightype)), stat= i_alct)
  IF(i_alct /= 0) &
    CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')

  CALL grid_getinfo(p_grid(i_timeplus), i_femtype=i_hightype, r_dofcoordinates= r_coords)
  DO i_cnt=1,p_grid(i_timeplus)%i_unknowns(i_hightype)
    r_velocity(1,i_cnt)        = calc_trac(REAL(r_coords(:,i_cnt)))
  END DO
  CALL grid_putinfo(p_grid(i_timeplus), r_dofvalues=r_velocity, &
    i_arraypoint= (/ AMAT_hi3 /))
  DEALLOCATE(r_coords, r_velocity)
  WRITE(*,20592)

!---------- test gradient calculation, store x-comp. of grad in zeta...

  CALL gradient_test(p_grid(i_timeplus))
  WRITE(*,2061)
!
!   r_xy= (/0.2443, 0.233 /)
!   r_short= grid_coordvalue(p_grid(i_timeplus), r_xy, i_index=i_fst)
!   write(*,*) 'INFO: Interpolation at coordinate: (0.2443,0.233)'
!   write(*,*) '      value: ',r_short
!   write(*,*) '      index: ',i_fst

!---------- plot grid

  ncd2_plot: IF(p_contr%io%l_netcdf) THEN
#ifndef NO_NETCDF
    CALL plot_netcdf(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
#else
    CALL grid_error(c_error='[AMATOS]: NetCDF library not installed - cannot write data!')
#endif
  END IF ncd2_plot
  gmv2_plot: IF(p_contr%io%l_gmv) THEN
    CALL plot_gmv(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
  END IF gmv2_plot
  vtu2_plot: IF(p_contr%io%l_vtu) THEN
    CALL generate_vtu(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
  END IF vtu2_plot

!---------- get edge length

  CALL grid_edgelength(p_grid(i_timeplus), r_max=r_long, r_min=r_short)
  WRITE(*,2058)
  WRITE(*,2020)
  WRITE(*,2021) r_long
  WRITE(*,2022) r_short
  WRITE(*,2020)

!---------- write a save set

  write(c_tmp,*) trim(GRID_parameters%program_name(1:22)), '_save.'
  write(c_mfile,2010) trim(c_tmp), p_grid(i_timeplus)%i_maxlvl
  c_mfile= adjustl(c_mfile)
  CALL grid_writesaveset(c_mfile,p_grid)

!---------- finish grid generator

  CALL grid_terminate

!---------- clean up memory

  IF(ALLOCATED(i_femtyp)) DEALLOCATE(i_femtyp)

  STOP

 2000  FORMAT(a)
 2010  FORMAT(a28,i4.4)
 2020  FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****')
 2021  FORMAT(1x,'***** Longest edge in triangulation: ',e16.5,' *****')
 2022  FORMAT(1x,'***** Shortest edge in triangulation:',e16.5,' *****')
 2050  FORMAT(1x,'----- Grid initialization  .  .  .  .  .  .  . passed -----')
 2051  FORMAT(1x,'----- Grid creation  .  .  .  .  .  .  .  .  . passed -----')
 2052  FORMAT(1x,'----- Plotting and data retrieval   .  .  .  . passed -----')
 2053  FORMAT(1x,'----- Grid adaptation   .  .  .  .  .  .  .  . passed -----')
 2054  FORMAT(1x,'----- Number of adaption iterations:',9x,i8,' -----')
 2058  FORMAT(1x,'----- Test of grid_edgelength .  .  .  .  .  . passed -----')
 2059  FORMAT(1x,'----- Test of grid_putinfo .  .  .  .  .  .  . passed -----')
 20591 FORMAT(1x,'----- Test of newslevel .  .  .  .  .  .  .  . passed -----')
 20592 FORMAT(1x,'----- Test of higher order computation .  .  . passed -----')
 2060  FORMAT(1x,'----- Test of FEM support  .  .  .  .  .  .  . passed -----')
 2061  FORMAT(1x,'----- Test of gradient calculation  .  .  .  . passed -----')
 2070  FORMAT(1x,'----- Nodes before adaptation:',15x,i8,' -----')
 2071  FORMAT(1x,'----- Nodes after adaptation:',16x,i8,' -----')
 2072  FORMAT(1x,'----- Could not achieve to leverage diameters, ',7x,'-----',/ &
              1x,'----- The present ratio is:',10x,e16.5,' -----')
 2073  FORMAT(1x,'----- The present diam. ratio is:',4x,e16.5,' -----')

END PROGRAM gridtest
