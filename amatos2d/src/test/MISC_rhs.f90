!*****************************************************************
!
!> @file MISC_rhs.f90
!> @brief includes module MISC_rhs
!
!*****************************************************************
!
! VERSION(S):
!  1. original version              j. behrens    01/2005
!
!*****************************************************************
! MODULE DESCRIPTION:
!> calculates a tracer (RHS) for samatos
!
MODULE MISC_rhs
  USE MAIN_parameters
  USE GRID_api
  PRIVATE

!---------- global constant

  LOGICAL :: l_rhsinit=.FALSE.
  INTEGER (KIND = GRID_SI), PARAMETER :: i_etop= 4321_GRID_SI
  INTEGER (KIND = GRID_SI), PARAMETER :: j_etop= 2161_GRID_SI
  REAL (KIND = GRID_SR), DIMENSION(i_etop,j_etop) :: r_etopo5
  INTEGER*2, DIMENSION(:,:), ALLOCATABLE :: i_rawetopo5
  CHARACTER (len=10) :: c_etopo5file='ETOPO5.ASC'

  PUBLIC :: tracer_init, writeETOPOtoPGM, getETOPOHeightmap

  CONTAINS
!*****************************************************************
! DESCRIPTION of [SUBROUTINE writeETOPOtoPGM]:
!> @brief short description
!
  SUBROUTINE writeETOPOtoPGM()
    IMPLICIT NONE
    INTEGER (KIND = GRID_SI)                   :: i_unit, i_cnt, j_cnt, i_fst
    INTEGER (KIND = GRID_SI)                   :: ii_cnt, jj_cnt
    REAL                                       :: r_min, r_max, r_dif, r_scal

    IF(.NOT. l_rhsinit) THEN
        !CALL read_etopo
        CALL read_etopoform
        l_rhsinit= .TRUE.
    END IF

    i_unit=22
    open(i_unit, file= 'etopo.pgm', iostat= i_fst)
    IF(i_fst /= 0) &
          CALL grid_error(c_error='[read_etopo]: could open file bitmap file')
    WRITE(i_unit, '(A)') 'P2'
    WRITE(i_unit, '(A)') '4321 2161'
    WRITE(i_unit, '(A)') '255'

    !r_min = r_etopo5(1,1)
    !r_max = r_etopo5(1,1)
    r_min = -10376
    r_max =   7833
    r_dif = r_max-r_min
    r_scal = 255/r_dif

    DO j_cnt=1,j_etop
      jj_cnt = j_cnt
      !read(i_unit,1000) (i_rawetopo5(i_cnt,j_cnt), i_cnt=1,i_etop)
      DO i_cnt=1,i_etop
        !-- rotate 180 degrees
        ii_cnt = MOD(i_cnt + 2160, 4321)
        !-- rotate for the nicer look
        !ii_cnt = MOD(i_cnt + 2580, 4321)
        !-- find min max
        !if(r_etopo5(ii_cnt,jj_cnt) < r_min) r_min = r_etopo5(ii_cnt,jj_cnt)
        !if(r_etopo5(ii_cnt,jj_cnt) > r_max) r_max = r_etopo5(ii_cnt,jj_cnt)
        !-- normal scale
        !WRITE(i_unit, '(I3)') floor((r_etopo5(ii_cnt,jj_cnt)-r_min)*r_scal)
        !-- scale to (min=0, nn=128, max = 255) or black 'n' white
        IF(r_etopo5(ii_cnt,jj_cnt)<0) THEN
          !WRITE(i_unit, '(I3)') floor(127 - (r_etopo5(ii_cnt,jj_cnt)/r_min)*127)
          WRITE(i_unit, '(I3)') 255
        ELSE
          !WRITE(i_unit, '(I3)') floor(127 + (r_etopo5(ii_cnt,jj_cnt)/r_max)*128)
          WRITE(i_unit, '(I3)') 0
        END IF
      END DO
    END DO

    WRITE(*, '(A, F20.6)') 'etopo5 min value = ', r_min
    WRITE(*, '(A, F20.6)') 'etopo5 max value = ', r_max

    close(i_unit)

  END SUBROUTINE writeETOPOtoPGM

!*****************************************************************
! DESCRIPTION of [SUBROUTINE getETOPOHeightmap]:
!> @brief short description
!>
!> @param           i_heightmap
!
  SUBROUTINE getETOPOHeightmap(i_heightmap)

    IMPLICIT NONE

    INTEGER (KIND = GRID_SI)                   :: i_unit, i_cnt, j_cnt, ii_cnt, jj_cnt

    INTEGER, DIMENSION(:,:), POINTER           :: i_heightmap

    IF(.NOT. l_rhsinit) THEN
      !    CALL read_etopo
      CALL read_etopoform
      l_rhsinit= .TRUE.
    END IF

    DO j_cnt=1,j_etop
      jj_cnt = j_cnt
      DO i_cnt=1,i_etop
        !-- rotate 180 degrees
        ii_cnt = MOD(i_cnt + 2160, 4321)
        IF(r_etopo5(ii_cnt,jj_cnt) < 0) THEN
            i_heightmap(i_cnt,j_cnt) = 0
        ELSE
            i_heightmap(i_cnt,j_cnt) = 1
        END IF
      END DO
    END DO
  END SUBROUTINE getETOPOHeightmap

!*****************************************************************
! DESCRIPTION of [FUNCTION tracer_init]:
!> @brief calculates a tracer/constituent initial function
!>
!> @param[in]     r_coos      coordinate array
!> @return        r_wid       value at postition r_coo
!
  FUNCTION tracer_init(r_coos) RESULT (r_val)

!---------- local declarations

  IMPLICIT NONE
  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)           :: r_coos
  REAL (KIND = GRID_SR)                                      :: r_val

  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)        :: r_lamp
  INTEGER (KIND = GRID_SI) :: i_pos, j_pos, i_tmp, j_tmp

!---------- load topographical data, if not loaded

  IF(.NOT. l_rhsinit) THEN
!     CALL read_etopo
    CALL read_etopoform
    l_rhsinit= .TRUE.
  END IF

!---------- calculate spherical coordinate and derive the nearest
!           integer position from that

  r_lamp= grid_kartgeo(r_coos)

  i_tmp = INT(((r_lamp(1)+GRID_PI)/(2._GRID_SR* GRID_PI))* &
              360._GRID_SR* 12._GRID_SR, GRID_SI) + 1_GRID_SI
  j_tmp = INT(((-r_lamp(2)+0.5_GRID_SR*GRID_PI)/(GRID_PI))* &
              180._GRID_SR* 12._GRID_SR, GRID_SI) + 1_GRID_SI
  j_pos = min(j_tmp, j_etop)
  i_pos = min(i_tmp, i_etop)

!---------- now the value

  r_val= r_etopo5(i_pos,j_pos)
  IF(r_val < 0.0_GRID_SR) r_val= -2000.0_GRID_SR

  END FUNCTION tracer_init

!*****************************************************************
! DESCRIPTION of [SUBROUTINE read_etopo]:
!> @brief short description
!
  SUBROUTINE read_etopo

!---------- local declarations

  IMPLICIT NONE
  INTEGER (KIND = GRID_SI) :: i_alct, i_unit, i_cnt, j_cnt, i_fst

!---------- allocate raw data array

  ALLOCATE(i_rawetopo5(i_etop,j_etop), stat= i_alct)
  IF(i_alct /= 0) &
    CALL grid_error(c_error='[read_etopo]: could not allocate raw data field')

!---------- open topography file

  i_unit=21
  open(i_unit, file= c_etopo5file, form= 'unformatted', iostat= i_fst)
  IF(i_fst /= 0) &
    CALL grid_error(c_error='[read_etopo]: could open file topography file')
  IF(GRID_parameters%iolog > 0) &
    write(GRID_parameters%iolog,*) 'INFO: Filename: ', c_etopo5file, ' opened on unit: ', i_unit

!---------- read raw data

  read(i_unit) ((i_rawetopo5(i_cnt,j_cnt), i_cnt=1,i_etop), j_cnt=1,j_etop)

!---------- close topography file

  close(i_unit)
  IF(GRID_parameters%iolog > 0) &
    write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_unit

!---------- convert data

  r_etopo5 = REAL(i_rawetopo5, GRID_SR)

!---------- deallocate raw array

  DEALLOCATE(i_rawetopo5)

  END SUBROUTINE read_etopo

!*****************************************************************
! DESCRIPTION of [SUBROUTINE read_etopoform]:
!> @brief short description
!
  SUBROUTINE read_etopoform

!---------- local declarations

  IMPLICIT NONE
  INTEGER (KIND = GRID_SI) :: i_alct, i_unit, i_cnt, j_cnt, i_fst
  CHARACTER (len = 80)     :: c_dummy

!---------- allocate raw data array

  ALLOCATE(i_rawetopo5(i_etop,j_etop), stat= i_alct)
  IF(i_alct /= 0) &
    CALL grid_error(c_error='[read_etopo]: could not allocate raw data field')

!---------- open topography file

  i_unit=21
  open(i_unit, file= c_etopo5file, form= 'formatted', iostat= i_fst)
  IF(i_fst /= 0) &
    CALL grid_error(c_error='[read_etopo]: could open file topography file')
  IF(GRID_parameters%iolog > 0) &
    write(GRID_parameters%iolog,*) 'INFO: Filename: ', c_etopo5file, ' opened on unit: ', i_unit

!---------- read raw data

  DO j_cnt=1,8
    read(i_unit,*) c_dummy
  END DO
  DO j_cnt=1,j_etop
    read(i_unit,1000) (i_rawetopo5(i_cnt,j_cnt), i_cnt=1,i_etop)
  END DO
 1000 FORMAT(4321I6)

!---------- close topography file

  close(i_unit)
  IF(GRID_parameters%iolog > 0) &
    write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_unit

!---------- convert data

  r_etopo5 = REAL(i_rawetopo5, GRID_SR)

!---------- deallocate raw array

  DEALLOCATE(i_rawetopo5)

  END SUBROUTINE read_etopoform

!*****************************************************************
END MODULE MISC_rhs
