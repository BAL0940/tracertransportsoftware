!*******************************************************************************
!
!> @file IO_netcdfplot.f90
!> @brief contains module IO_netcdfplot
!
!*******************************************************************************
!
! REFERENCES:
!   this module is based on the original implementations in the
!   splash/fe project (fortran 77 version), but wildly modified!
!
! VERSION(S):
!  1. original version                    j. behrens      01/2000
!  2. compliant to amatos 1.0             j. behrens      12/2000
!  3. compliant to amatos 1.2             j. behrens      03/2002
!  3. compliant to amatos 2.0             j. behrens      07/2003
!  4. rewritten to get more data          s. vater        05/2012
!  5. rewritten to use IO_ncugrid module  s. vater        03/2014
!
!*******************************************************************************
! MODULE DESCRIPTION:
!> creates output in NetCDF file format
!
MODULE IO_netcdfplot
  USE IO_ncugrid
  USE MAIN_parameters
  USE GRID_api

  PRIVATE
  PUBLIC :: plot_netcdf

  CONTAINS
!*******************************************************************************
! DESCRIPTION of [SUBROUTINE plot_netcdf]:
!> @brief creates output in netcdf file format
!>
!> @param[in]       p_ghand     grid handle for the linked lists
!> @param[in]       i_time      time stamp for file naming (opt.)
!
  SUBROUTINE plot_netcdf(p_ghand, i_time)

    IMPLICIT NONE

!---------- local declarations

    TYPE (grid_handle), INTENT(in)                              :: p_ghand
    INTEGER, OPTIONAL, INTENT(in)                               :: i_time
    INTEGER                                                     :: i_cnt, &
      i_numelmt, i_numnode, i_alct, i_tcnt, i_dimvel, i_unkvel
    INTEGER, SAVE                                               :: i_timecount = 0
    CHARACTER (len=32)                                          :: c_file, c_mesh
    CHARACTER (len=64)                                          :: c_title, c_tmp
    REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE          :: r_nodexy
    REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE, TARGET  :: r_val, r_aux
    INTEGER, DIMENSION(:,:), ALLOCATABLE                        :: i_elmtnodes, i_eltdofs
    INTEGER (KIND = GRID_SI), PARAMETER                         :: i_vallen = 4
    INTEGER (KIND = GRID_SI), DIMENSION(:), ALLOCATABLE, TARGET :: i_eltlev, i_eltsta
    INTEGER (KIND = GRID_SI), DIMENSION(i_vallen)               :: i_valind
    INTEGER (KIND = GRID_SI), DIMENSION(GRID_dimension)         :: i_valuv
    INTEGER (KIND = GRID_SI)                                    :: i_hityp, i_lotyp
    TYPE (ncugrid_vardatatype), DIMENSION(i_vallen+1)           :: p_vdataarr

!---------- check input for optional parameter

    IF(PRESENT(i_time)) THEN
      i_tcnt = i_time
    ELSE
      i_tcnt = i_timecount
    END IF
    i_timecount = i_timecount + 1

!---------- check for FEM types

    i_lotyp = grid_femvarquery(AMAT_var1)
    i_hityp = grid_femvarquery(AMAT_hi1)

!---------- create the title

    WRITE(c_title,*) 'netCDF output from ',TRIM(GRID_parameters%program_name)
    c_title = ADJUSTL(c_title)

!---------- create generic file name

    c_mesh = 'Mesh2'

    WRITE(c_file, '(A, A1, I4.4, A3)') TRIM(GRID_parameters%program_name), '_', i_tcnt, '.nc'

!---------- get array with node coordinates, and variable values

    i_numnode = p_ghand%i_nnumber
    i_numelmt = p_ghand%i_enumfine
    i_dimvel = GRID_femtypes%p_type(i_hityp)%sig%i_unknowns
    i_unkvel = p_ghand%i_unknowns(i_hityp)

    ALLOCATE(r_nodexy(GRID_dimension, i_numnode), &
             i_elmtnodes(GRID_elementnodes, i_numelmt), i_eltdofs(i_dimvel, i_numelmt), &
             r_val(i_vallen, i_unkvel), r_aux(i_vallen, i_numelmt), &
             i_eltlev(i_numelmt), i_eltsta(i_numelmt), stat=i_alct)
    IF (i_alct /= 0) CALL grid_error(c_error='[plot_netcdf]: could not allocate aux. arrays')

    i_valind= (/ AMAT_hi1, AMAT_hi2, AMAT_hi3, AMAT_hi4 /)

    CALL grid_getinfo(p_ghand, l_finelevel=.TRUE., l_relative=.TRUE., &
                      i_femtype=i_hityp, i_arraypoint=i_valind, r_nodecoordinates=r_nodexy, &
                      i_elementnodes=i_elmtnodes, r_dofvalues=r_val, i_elementdofs=i_eltdofs, &
                      i_elementlevel=i_eltlev, i_elementstatus=i_eltsta)

!---------- create the file for timestep data

    CALL ncugrid_createmesh(c_file, c_mesh, i_numnode, i_numelmt, &
                            i_elmtnodes, r_nodexy, c_title, TRIM(GRID_parameters%author_affil2), &
                            c_contact=TRIM(GRID_parameters%author_email))

!---------- define variables (data)

    WRITE(c_tmp, '(A, A4)') TRIM(c_mesh), '_ssh'
    p_vdataarr(1)%c_varname       = TRIM(c_tmp)
    p_vdataarr(1)%c_long_name     = 'sea surface height'
    p_vdataarr(1)%c_standard_name = 'sea_surface_height' ! not CF conform!
    p_vdataarr(1)%c_units         = 'm'
    p_vdataarr(1)%c_location      = 'face'
    p_vdataarr(1)%i_datatype      = 1

    WRITE(c_tmp, '(A, A6)') TRIM(c_mesh), '_depth'
    p_vdataarr(2)%c_varname       = TRIM(c_tmp)
    p_vdataarr(2)%c_long_name     = 'depth from reference height'
    p_vdataarr(2)%c_standard_name = 'depth_from_reference_height' ! not CF conform!
    p_vdataarr(2)%c_units         = 'm'
    p_vdataarr(2)%c_location      = 'face'
    p_vdataarr(2)%i_datatype      = 1

    WRITE(c_tmp, '(A, A4)') TRIM(c_mesh), '_v_x'
    p_vdataarr(3)%c_varname       = TRIM(c_tmp)
    p_vdataarr(3)%c_long_name     = 'velocity in x-direction'
    p_vdataarr(3)%c_standard_name = 'velocity_in_x_direction' ! not CF conform!
    p_vdataarr(3)%c_units         = 'm/s'
    p_vdataarr(3)%c_location      = 'face'
    p_vdataarr(3)%i_datatype      = 1

    WRITE(c_tmp, '(A, A4)') TRIM(c_mesh), '_v_y'
    p_vdataarr(4)%c_varname       = TRIM(c_tmp)
    p_vdataarr(4)%c_long_name     = 'velocity in y-direction'
    p_vdataarr(4)%c_standard_name = 'velocity_in_y_direction' ! not CF conform!
    p_vdataarr(4)%c_units         = 'm/s'
    p_vdataarr(4)%c_location      = 'face'
    p_vdataarr(4)%i_datatype      = 1

    WRITE(c_tmp, '(A, A6)') TRIM(c_mesh), '_level'
    p_vdataarr(5)%c_varname       = TRIM(c_tmp)
    p_vdataarr(5)%c_long_name     = 'grid level'
    p_vdataarr(5)%c_standard_name = 'grid_level' ! not CF conform!
    p_vdataarr(5)%c_units         = 'none'
    p_vdataarr(5)%c_location      = 'face'
    p_vdataarr(5)%i_datatype      = 0

!---------- average over each cell and write into netCDF file

    DO i_cnt=1,i_numelmt
      r_aux(1, i_cnt) = SUM(r_val(1, i_eltdofs(:, i_cnt))) / REAL(i_dimvel, GRID_SR)
      r_aux(2, i_cnt) = SUM(r_val(2, i_eltdofs(:, i_cnt))) / REAL(i_dimvel, GRID_SR)
      r_aux(3, i_cnt) = SUM(r_val(3, i_eltdofs(:, i_cnt))) / REAL(i_dimvel, GRID_SR)
      r_aux(4, i_cnt) = SUM(r_val(4, i_eltdofs(:, i_cnt))) / REAL(i_dimvel, GRID_SR)
    END DO

!---------- write the nodal grid data

    p_vdataarr(1)%p_rvardata => r_aux(1,:)
    p_vdataarr(2)%p_rvardata => r_aux(2,:)
    p_vdataarr(3)%p_rvardata => r_aux(3,:)
    p_vdataarr(4)%p_rvardata => r_aux(4,:)
    p_vdataarr(5)%p_ivardata => i_eltlev

!--- write variables

    CALL ncugrid_putvariablearray(c_file, c_mesh, p_vdataarr, REAL(i_tcnt, GRID_SR))

!--- deallocate data arrays

    DO i_cnt=1,SIZE(p_vdataarr)
      NULLIFY(p_vdataarr(i_cnt)%p_ivardata)
      NULLIFY(p_vdataarr(i_cnt)%p_rvardata)
    END DO
    DEALLOCATE(r_nodexy, i_elmtnodes, r_val, r_aux, i_eltdofs, &
               i_eltlev, i_eltsta)

  END SUBROUTINE plot_netcdf

!*******************************************************************************
END MODULE IO_netcdfplot
