!*****************************************************************
!
!> @file MISC_datafun.f90
!> @brief includes module MISC_datafun
!
!*****************************************************************
!
! VERSION(S):
!  1. original version              j. behrens      03/2001
!
!*****************************************************************
! MODULE DESCRIPTION:
!> provides some dummy functions for testing amatos
!
MODULE MISC_datafun
  USE MAIN_parameters
  USE GRID_api
  PRIVATE
  PUBLIC :: calc_trac, calc_uv
  PUBLIC :: calc_gradfunc, calc_gradgrad
  PUBLIC :: calc_femsol, calc_femrhs

  CONTAINS

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_trac]:
!> @brief calculates a tracer (scalar) function
!>
!> @param[in]       r_coo     coordinate
!> @return          r_res     result
!
  FUNCTION calc_trac(r_coo) RESULT (r_res)

    IMPLICIT NONE

!---------- local declarations

    REAL, DIMENSION(GRID_dimension), INTENT(in)     :: r_coo
    REAL                                            :: r_res
    REAL                                            :: r_tmp
    REAL, DIMENSION(GRID_dimension)                 :: r_zero

    r_res  = 0.0
    r_zero = 0.5
    r_tmp  = veclen((r_zero-r_coo))
    IF(r_tmp < 1.0) THEN
      r_res = cos(r_tmp)
    END IF

    RETURN
  END FUNCTION calc_trac

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_uv]:
!> @brief  calculates a velocity (vector) function
!>
!> @param[in]       r_coo     coordinate
!> @return          r_res     velocity vector
!
  FUNCTION calc_uv(r_coo) RESULT (r_res)

    IMPLICIT NONE

!---------- local declarations

    REAL, DIMENSION(GRID_dimension), INTENT(in)     :: r_coo
    REAL, DIMENSION(GRID_dimension)                 :: r_res

    r_res    = 0.0
    r_res(1) =  r_coo(2)
    r_res(2) = -r_coo(1)

    RETURN
  END FUNCTION calc_uv

!*****************************************************************
! DESCRIPTION of [FUNCTION veclen]:
!> @brief short description
!>
!> @param[in]       r_vec
!> @return          r_rst
!
  FUNCTION veclen(r_vec) RESULT(r_rst)

    IMPLICIT NONE

!---------- local declarations

    REAL, DIMENSION(GRID_dimension)                 :: r_vec
    REAL                                            :: r_rst, r_tmp

!---------- calculate vector crossproduct, 3D only

    r_tmp = dot_product(r_vec, r_vec)
    r_rst = sqrt(r_tmp)

  END FUNCTION veclen

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_gradfunc]:
!> @brief short description
!>
!> @param[in]       r_coord
!> @return          r_val
!
  FUNCTION calc_gradfunc(r_coord) RESULT (r_val)

    IMPLICIT NONE

!---------- local declarations

    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR)                                         :: r_val

!---------- uniform right hand side function f=1

!   r_val= 1.0_GRID_SR

!---------- f= cos(2 pi (x-1/4)) cos (2 pi (y-1/4)) !!! ONLY 2D in unit square !!!

    r_val = cos(2.0_GRID_SR*GRID_PI * (r_coord(1) - 0.25_GRID_SR)) * &
            cos(2.0_GRID_SR*GRID_PI * (r_coord(2) - 0.25_GRID_SR))

    RETURN
  END FUNCTION calc_gradfunc

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_gradgrad]:
!> @brief short description
!>
!> @param[in]       r_coord
!> @return          r_val
!
  FUNCTION calc_gradgrad(r_coord) RESULT (r_val)

    IMPLICIT NONE

!---------- local declarations

    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)              :: r_val

!---------- no solution available

!   r_val= 0.0_GRID_SR

!---------- gradient corresponding to
!           f= cos(2 pi (x-1/4)) cos (2 pi (y-1/4)) !!! ONLY 2D in unit square !!!

    r_val(1) = -2.0_GRID_SR*GRID_PI * &
               sin(2.0_GRID_SR*GRID_PI * (r_coord(1) - 0.25_GRID_SR)) * &
               cos(2.0_GRID_SR*GRID_PI * (r_coord(2) - 0.25_GRID_SR))
    r_val(2) = -2.0_GRID_SR*GRID_PI * &
               cos(2.0_GRID_SR*GRID_PI * (r_coord(1) - 0.25_GRID_SR)) * &
               sin(2.0_GRID_SR*GRID_PI * (r_coord(2) - 0.25_GRID_SR))

    RETURN
  END FUNCTION calc_gradgrad

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_femrhs]:
!> @brief short description
!>
!> @param[in]       r_coord
!> @return          r_val
!
  FUNCTION calc_femrhs(r_coord) RESULT (r_val)

    IMPLICIT NONE

!---------- local declarations

    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR)                                         :: r_val
    REAL (KIND = GRID_SR)                                         :: r_x, r_y

!---------- uniform right hand side function f=1

!   r_val= 1.0_GRID_SR

!---------- f=2 (x + y - x*x - y*y) !!! ONLY 2D !!!

    r_x   = r_coord(1)
    r_y   = r_coord(2)
    r_val = 2.0_GRID_SR * (r_x + r_y - r_x*r_x - r_y*r_y)

    RETURN
  END FUNCTION calc_femrhs

!*****************************************************************
! DESCRIPTION of [FUNCTION calc_femsol]:
!> @brief short description
!>
!> @param[in]       r_coord
!> @return          r_val
!
  FUNCTION calc_femsol(r_coord) RESULT (r_val)

    IMPLICIT NONE

!---------- local declarations

    REAL (KIND = GRID_SR), DIMENSION(GRID_dimension), INTENT(in)  :: r_coord
    REAL (KIND = GRID_SR)                                         :: r_val
    REAL (KIND = GRID_SR)                                         :: r_x, r_y

!---------- no solution available

!   r_val= 0.0_GRID_SR

!---------- Solution corresponding to rhs: f=2 (x + y - x*x - y*y) !!! ONLY 2D !!!

    r_x   = r_coord(1)
    r_y   = r_coord(2)
    r_val = r_x * r_y * (1.0_GRID_SR-r_x) * (1.0_GRID_SR-r_y)

    RETURN
  END FUNCTION calc_femsol

END MODULE MISC_datafun
