!*****************************************************************
!
!> @file Samatos.f90
!> @brief includes program gridtest
!
!*****************************************************************
!
! SYNTAX:
!   SAMATOS ...
! ON INPUT:
!
! ON OUTPUT:
!
! COMMENTS:
!
! LIBRARIES:
!   SAMATOS
!
! VERSION(S):
!  1. original version                j. behrens    08/1997
!  2. now with new grid generator     j. behrens    05/1998
!  3. amatos-1.0 compliant            j. behrens    11/2000
!  4. samatos version                 j. behrens    02/2001
!
!*****************************************************************
! PROGRAM DESCRIPTION:
!> test the grid generation routines from SAMATOS
!
PROGRAM gridtest

!---------- uses

  USE MAIN_parameters
  USE IO_utils
  USE IO_gmvplot
  USE MISC_testgrad
!   USE MISC_testfem
  USE MISC_datasph
  USE GRID_api
  USE SLM_errorestimate

!---------- local declarations

  IMPLICIT NONE

  INTEGER (KIND = GRID_SI)                                   :: i_tmp, i_err, i_cnt, i_alct
  INTEGER (KIND = GRID_SI), PARAMETER                        :: i_vertnum=4
  INTEGER (KIND = GRID_SI), PARAMETER                        :: i_redirout=8
  INTEGER (KIND = GRID_SI), PARAMETER                        :: i_redirlog=7
  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension,i_vertnum) :: r_vertinit
  INTEGER (KIND = GRID_SI), DIMENSION(:), ALLOCATABLE        :: i_mark
  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE         :: r_trac
  REAL (KIND = GRID_SR), DIMENSION(:,:,:), ALLOCATABLE       :: r_nodes
  REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE         :: r_cords
  REAL (KIND = GRID_SR), DIMENSION(GRID_dimension)           :: r_xy, r_kcentr, r_grad
  REAL (KIND = GRID_SR), DIMENSION(GRID_dimspherical)        :: r_lcentr
  INTEGER (KIND = GRID_SI), DIMENSION(1)                     :: i_valpoint
  REAL (KIND = GRID_SR)                                      :: r_third=1./3.
  LOGICAL                                                    :: l_grd
  CHARACTER (len=32)                                         :: c_mfile
  CHARACTER (len=28)                                         :: c_tmp
  CHARACTER                                                  :: c_dummy
  INTEGER (KIND = GRID_SI)                                   :: i_fst, i_hightype
  REAL (KIND = GRID_SR)                                      :: r_rad=0.15, r_rfc=0.6
  REAL (KIND = GRID_SR)                                      :: r_long, r_short, r_tmp
  REAL (KIND = GRID_SR)                                      :: r_trsh=0.15
  INTEGER (KIND = GRID_SI), DIMENSION(:), ALLOCATABLE        :: i_femtyp

!---------- read command line options

  CALL io_getcmdline(p_contr)

!---------- read user input

  interactive_input: IF(p_contr%cmd%l_interactive) THEN
    CALL io_getinterinput(p_contr)
  ELSE interactive_input
    CALL io_getbatchinput(p_contr)
  END IF interactive_input

!---------- initialize grid generator

  IF(p_contr%io%i_numsigs == 2) THEN
    ALLOCATE(i_femtyp(p_contr%io%i_numsigs+1), stat=i_alct)
    IF(i_alct /= 0) CALL grid_error(c_error='[MAIN]: Could not allocate i_femtyp')
    i_femtyp(1)= grid_registerfemtype('default.ftf')
    i_femtyp(2)= grid_registerfemtype(p_contr%io%c_sigfiles(1))
    i_hightype= i_femtyp(2)
    i_femtyp(3)= grid_registerfemtype(p_contr%io%c_sigfiles(2))
  END IF

  IF(p_contr%cmd%l_output) THEN
    IF(p_contr%cmd%l_logging) THEN
      CALL grid_initialize(i_output=i_redirout, i_logging=i_redirlog)
    ELSE
      CALL grid_initialize(i_output=i_redirout)
    END IF
  ELSE
    IF(p_contr%cmd%l_logging) THEN
      CALL grid_initialize(i_logging=i_redirlog)
    ELSE
      CALL grid_initialize
    END IF
  END IF

!---------- register new variables

  IF(p_contr%io%i_numsigs > 0) THEN
    i_tmp=i_femtyp(1)
  ELSE
    i_tmp= 1
    i_hightype= 1
  END IF
  AMAT_var1= grid_registerfemvar(i_tmp,i_length=2)
  AMAT_var2= AMAT_var1+1
  AMAT_var3= grid_registerfemvar(i_tmp)
  AMAT_var4= grid_registerfemvar(i_tmp)

  AMAT_hi1= grid_registerfemvar(i_hightype,i_length=2)
  AMAT_hi2= AMAT_hi1+1
  AMAT_hi3= grid_registerfemvar(i_hightype)
  AMAT_hi4= grid_registerfemvar(i_hightype)

  AMAT_lo1= grid_registerfemvar(i_femtyp(3))
  AMAT_lo2= grid_registerfemvar(i_femtyp(3))
  AMAT_lo3= grid_registerfemvar(i_femtyp(3))

  WRITE(*,2050)

!---------- initialize grid parameters

  CALL grid_setparameter(p_grid, i_coarselevel= p_contr%phy%i_crslevel, &
                                 i_finelevel= p_contr%phy%i_reflevel)

!---------- define domain

  r_vertinit= RESHAPE((/ 0.0_GRID_SR, 0.0_GRID_SR, 0.0_GRID_SR, &
                         1.0_GRID_SR, 0.0_GRID_SR, 0.0_GRID_SR, &
             1.0_GRID_SR, 1.0_GRID_SR, 0.0_GRID_SR, &
             0.0_GRID_SR, 1.0_GRID_SR, 0.0_GRID_SR/),(/ 3, 4 /))
  CALL grid_definegeometry(i_vertnum, r_vertexarr= r_vertinit)

!---------- create initial triangulation

  CALL grid_createinitial(p_grid, c_filename=p_contr%io%c_triangfile)

! CONTINUE HERE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!---------- initialize a tracer (cosine hill)

  r_lcentr= (/ -(GRID_PI*0.5_GRID_SR), 0._GRID_SR /) ! the center in sph. coordinates
  r_kcentr= grid_geokart(r_lcentr)    ! the center in kart. coordinates

  ALLOCATE(r_cords(GRID_dimension, p_grid(i_timeplus)%i_nnumber), &
           stat= i_alct)
  IF(i_alct /= 0) THEN
    CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
  END IF
  ALLOCATE(r_trac(1,p_grid(i_timeplus)%i_nnumber), stat= i_alct)
  IF(i_alct /= 0) THEN
    CALL grid_error(c_error='ERROR: Temporary array not allocated, stopped ...')
  END IF
  CALL grid_getinfo(p_grid(i_timeplus), r_nodecoordinates= r_cords)
  DO i_cnt=1,p_grid(i_timeplus)%i_nnumber
    r_xy(:)= r_cords(:,i_cnt) - r_kcentr(:)
    r_tmp= DOT_PRODUCT(r_xy,r_xy)
    IF(r_tmp < r_rad) THEN
      r_trac(1,i_cnt)= 400._GRID_SR*(1._GRID_SR+ cos( ((GRID_PI* r_tmp)/ r_rad) ))
    ELSE
      r_trac(1,i_cnt)= 0._GRID_SR
    END IF
  END DO
  DEALLOCATE(r_cords)

  i_valpoint= (/ GRID_tracer /)
  CALL grid_putinfo(p_grid(i_timeplus), i_arraypoint=i_valpoint, r_nodevalues= r_trac)
  DEALLOCATE(r_trac)

!---------- plot grid

  ncd1_plot: IF(p_contr%io%l_netcdf) THEN
    ! plot into NetCDF file (to be implemented)
  END IF ncd1_plot
  gmv1_plot: IF(p_contr%io%l_gmv) THEN
    CALL plot_gmv(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
  END IF gmv1_plot

!---------- adaptation loop

  adapt_loop: DO WHILE(p_grid(i_timeplus)%i_maxlvl < p_grid(i_timeplus)%i_reflvlbnd)

!---------- esmimate error... reuse r_trac here

  ALLOCATE(r_trac(1,p_grid(i_timeplus)%i_enumfine), stat= i_alct)
  IF(i_alct /= 0) THEN
    WRITE(*,*) 'ERROR: Temporary array not allocated, stopped ...'
    STOP
  END IF
  CALL slm_errorest(p_grid(i_timeplus), p_grid(i_timeplus)%i_enumfine, r_trac(1,:))

!---------- mark some elements for refinement

  ALLOCATE(i_mark(p_grid(i_timeplus)%i_enumfine), stat= i_alct)
  IF(i_alct /= 0) THEN
    WRITE(*,*) 'ERROR: Temporary array not allocated, stopped ...'
    STOP
  END IF
  i_mark= 0

  DO i_cnt=1, p_grid(i_timeplus)%i_enumfine
    IF(r_trac(1,i_cnt) > r_trsh) THEN
      i_mark(i_cnt)= GRID_pleaserefine
    END IF
  END DO

  CALL grid_putinfo(p_grid(i_timeplus), &
    l_finelevel= .TRUE., i_elementstatus= i_mark)

  IF(ALLOCATED(r_trac)) DEALLOCATE(r_trac)
  IF(ALLOCATED(i_mark)) DEALLOCATE(i_mark)

!---------- adapt grid

  CALL grid_adapt(p_grid(i_timeplus), l_grd)

!---------- initialize a tracer (cosine hill)

  ALLOCATE(r_cords(GRID_dimension, p_grid(i_timeplus)%i_nnumber), &
           stat= i_alct)
  IF(i_alct /= 0) THEN
    WRITE(*,*) 'ERROR: Temporary array not allocated, stopped ...'
    STOP
  END IF
  ALLOCATE(r_trac(1,p_grid(i_timeplus)%i_nnumber), stat= i_alct)
  IF(i_alct /= 0) THEN
    WRITE(*,*) 'ERROR: Temporary array not allocated, stopped ...'
    STOP
  END IF
  CALL grid_getinfo(p_grid(i_timeplus), r_nodecoordinates= r_cords)
  DO i_cnt=1,p_grid(i_timeplus)%i_nnumber
    r_xy(:)= r_cords(:,i_cnt) - r_kcentr(:)
    r_tmp= DOT_PRODUCT(r_xy,r_xy)
    IF(r_tmp < r_rad) THEN
      r_trac(1,i_cnt)= 400._GRID_SR*(1._GRID_SR+ cos( ((GRID_PI* r_tmp)/ r_rad) ))
    ELSE
      r_trac(1,i_cnt)= 0._GRID_SR
    END IF
  END DO
  DEALLOCATE(r_cords)

  i_valpoint= (/ GRID_tracer /)
  CALL grid_putinfo(p_grid(i_timeplus), i_arraypoint=i_valpoint, r_nodevalues= r_trac)
  DEALLOCATE(r_trac)

!---------- plot grid

!   ncd2_plot: IF(p_contr%io%l_netcdf) THEN
!     ! plot into NetCDF file (to be implemented)
!   END IF ncd2_plot
!   gmv2_plot: IF(p_contr%io%l_gmv) THEN
!     CALL plot_gmv(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
!   END IF gmv2_plot

  END DO adapt_loop

!---------- test gradient calculation, store x-comp. of grad in zeta...

  CALL gradient_test(p_grid(i_timeplus))
  WRITE(*,2061)

!---------- plot grid

  ncd2_plot: IF(p_contr%io%l_netcdf) THEN
    ! plot into NetCDF file (to be implemented)
  END IF ncd2_plot
  gmv2_plot: IF(p_contr%io%l_gmv) THEN
    CALL plot_gmv(p_grid(i_timeplus), i_time=p_grid(i_timeplus)%i_maxlvl)
  END IF gmv2_plot

!---------- get edge length

  CALL grid_edgelength(p_grid(i_timeplus), r_max=r_long, r_min=r_short)
  WRITE(*,2058)
  WRITE(*,2020)
  WRITE(*,2021) r_long
  WRITE(*,2022) r_short
  WRITE(*,2020)

!---------- write a save set

  write(c_tmp,*) trim(GRID_parameters%program_name(1:22)), '_save.'
  write(c_mfile,2010) trim(c_tmp), p_grid(i_timeplus)%i_maxlvl
  c_mfile= adjustl(c_mfile)
  CALL grid_writesaveset(c_mfile,p_grid)

!---------- finish grid generator

  CALL grid_terminate

 2000 FORMAT(a)
 2010 FORMAT(a28,i4.4)
 2020 FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****')
 2021 FORMAT(1x,'***** Longest edge in triangulation: ',e16.5,' *****')
 2022 FORMAT(1x,'***** Shortest edge in triangulation:',e16.5,' *****')
 2050 FORMAT(1x,'----- Grid initialization  .  .  .  .  .  .  . passed -----')
 2051 FORMAT(1x,'----- Grid creation  .  .  .  .  .  .  .  .  . passed -----')
 2052 FORMAT(1x,'----- Plotting and data retrieval   .  .  .  . passed -----')
 2053 FORMAT(1x,'----- Grid adaptation   .  .  .  .  .  .  .  . passed -----')
 2054 FORMAT(1x,'----- Number of adaption iterations:',9x,i8,' -----')
 2055 FORMAT(1x,'----- Test of grid_newitems   .  .  .  .  .  . passed -----')
 2056 FORMAT(1x,'----- New nodes in last iteration:',11x,i8,' -----')
 2057 FORMAT(1x,'----- New nodes in total adaptation:',9x,i8,' -----')
 2058 FORMAT(1x,'----- Test of grid_edgelength .  .  .  .  .  . passed -----')
 2059 FORMAT(1x,'----- Test of grid_putinfo .  .  .  .  .  .  . passed -----')
20591 FORMAT(1x,'----- Test of newslevel .  .  .  .  .  .  .  . passed -----')
 2060 FORMAT(1x,'----- Test of FEM support  .  .  .  .  .  .  . passed -----')
 2061 FORMAT(1x,'----- Test of gradient calculation  .  .  .  . passed -----')
 2070 FORMAT(1x,'----- Nodes before adaptation:',15x,i8,' -----')
 2071 FORMAT(1x,'----- Nodes after adaptation:',16x,i8,' -----')
 2072 FORMAT(1x,'----- Could not achieve to leverage diameters, ',7x,'-----',/ &
             1x,'----- The present ratio is:',10x,e16.5,' -----')
 2073 FORMAT(1x,'----- The present diam. ratio is:',4x,e16.5,' -----')

END PROGRAM gridtest
