!*****************************************************************
!
!> @file IO_utils.f90
!> @brief includes module IO_utils
!
!*****************************************************************
!
! VERSION(S):
!  1. original version                        j. behrens    07/96
!  2. largely extended                        j. behrens    11/96
!  3. changed command line io                 j. behrens    01/97
!  4. changed runtime output (adaptations)    j. behrens    01/97
!  5. control data struct, less command io    j. behrens    12/97
!  6. tiff file plotting included             j. behrens    01/98
!  7. updated for amatos version 1.0          j. behrens    11/2000
!
!*****************************************************************
! MODULE DESCRIPTION:
!> input/output routines (io_get...something, io_put...something)
!
MODULE IO_utils
  USE MAIN_parameters
  USE MISC_timing
  USE MISC_system
  USE GRID_api
  PRIVATE
  PUBLIC :: io_getcmdline, io_getinterinput, io_getbatchinput, &
            io_putparameters, io_putinputfile
  INTEGER, PARAMETER :: i_ioin= 5
  CONTAINS

!*****************************************************************
! DESCRIPTION of [SUBROUTINE io_getcmdline]:
!> @brief reads options from command line
!>
!> @param[out]      p_cmd       control parameters
!
  SUBROUTINE io_getcmdline(p_cmd)

  IMPLICIT NONE

!---------- local declarations

  TYPE (control_struct), INTENT(out) :: p_cmd

  LOGICAL                            :: l_ict
  LOGICAL                            :: l_mtl
  LOGICAL                            :: l_dia
  LOGICAL                            :: l_log
  LOGICAL                            :: l_red
  CHARACTER (len=io_fillen)          :: a_infln
  INTEGER                            :: numargs
  INTEGER                            :: i= 1
  INTEGER                            :: i_fst
  LOGICAL                            :: help= .TRUE.
  LOGICAL                            :: shoversion= .FALSE.
  CHARACTER (len=2)                  :: option
  CHARACTER (len=15)                 :: comdnam
  CHARACTER (len=32)                 :: c_file
  CHARACTER (len=80)                 :: c_dummy
  INTEGER, PARAMETER                 :: i_ioout=6

!---------- initialize output variables

  a_infln= 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
  l_ict= .FALSE.
  l_mtl= .FALSE.
  l_dia= .FALSE.
  l_log= .FALSE.
  l_red= .FALSE.

!---------- read number commandline arguments
!           this is possibly non standard f90, but definitely quasi standard

  numargs= sys_numcmdargs()
  CALL sys_getcmdargs(0,c_dummy,i_len=len(c_dummy))
  comdnam= c_dummy(1:15)
  check_args: IF(numargs < 1) THEN
    GOTO 100 ! print_help
  ELSE check_args

!---------- read command line arguments one by one

    DO WHILE (i <= numargs)
      CALL sys_getcmdargs(i,c_dummy,i_len=len(c_dummy))
      option= c_dummy(1:2)

!---------- select the CASEs for command line options

      eval_option: SELECT CASE (option)
        CASE('-h') eval_option !--- request for help ---
          help= .TRUE.
          i= i+1
        CASE('-?') eval_option !--- request for help ---
          help= .TRUE.
          i= i+1
        CASE('-r') eval_option !--- print release information ---
          shoversion= .TRUE.
          i= i+1
        CASE('-i') eval_option !--- select interactive input mode ---
          help= .FALSE.
          l_ict= .TRUE.
          i= i+1
        CASE('-d') eval_option !--- switch on diagnostics ---
          help= .FALSE.
          l_dia= .TRUE.
          i= i+1
        CASE('-l') eval_option !--- switch on logging ---
          help= .FALSE.
          p_cmd%cmd%l_logging= .TRUE.
          i= i+1
        CASE('-o') eval_option !--- redirect output into file ---
          help= .FALSE.
          p_cmd%cmd%l_output= .TRUE.
          i= i+1
        CASE('-b') eval_option !--- select batch input mode ---
          IF(l_ict) THEN
            help= .TRUE.
            GOTO 100 ! print_help
          ELSE
            help= .FALSE.
            IF(a_infln == 'xxxxxxxxxxxxxxxxxxxx') &
              a_infln= 'Parameters.in' !--- default input file name ---
            i= i+1
          END IF
        CASE('-f') eval_option !--- supply input file name ---
          i= i+1
          CALL sys_getcmdargs(i,c_dummy,i_len=len(c_dummy))
          a_infln= c_dummy(1:io_fillen)
          IF(a_infln(1:1) == '-') THEN  !--- check correctnes of file name ---
            help= .TRUE.
            GOTO 100 ! print_help
          ELSE
            i= i+1
          END IF
        CASE DEFAULT eval_option !--- default CASE: show help ---
          help= .TRUE.
          GOTO 100 ! print_help
      END SELECT eval_option
    END DO
  END IF check_args

!---------- update output structure

  p_cmd%cmd%c_batchfile = a_infln
  p_cmd%cmd%l_interactive= l_ict
  p_cmd%cmd%l_diagnostics= l_dia

!---------- print help information

 100  print_help: IF(help) THEN
    IF(shoversion) THEN
      write(i_ioout,1001) GRID_parameters%program_name, GRID_parameters%version, GRID_parameters%subversion, &
                          GRID_parameters%patchversion, GRID_parameters%datemonth, GRID_parameters%dateyear, &
                          GRID_parameters%author_name, GRID_parameters%author_email, GRID_parameters%author_affil1, &
                          GRID_parameters%author_affil2, GRID_parameters%author_affil3
      write(i_ioout,1002) comdnam
      write(i_ioout,*) 'STOPPED ... this is all I can say'
      STOP
    ELSE
      write(i_ioout,1010) comdnam
      write(i_ioout,1011) GRID_parameters%dateyear, GRID_parameters%author_name
      write(i_ioout,*) 'STOPPED ... hope this made it clear'
      STOP
    END IF
  END IF print_help

!---------- print version information

  print_version: IF(shoversion) THEN
    write(i_ioout,1001) GRID_parameters%program_name, GRID_parameters%version, GRID_parameters%subversion, &
                        GRID_parameters%patchversion, GRID_parameters%datemonth, GRID_parameters%dateyear, &
                        GRID_parameters%author_name, GRID_parameters%author_email, GRID_parameters%author_affil1, &
                        GRID_parameters%author_affil2, GRID_parameters%author_affil3
    IF(GRID_parameters%iolog > 0) &
      write(GRID_parameters%iolog,1001) GRID_parameters%program_name, GRID_parameters%version, GRID_parameters%subversion, &
                          GRID_parameters%patchversion, GRID_parameters%datemonth, GRID_parameters%dateyear, &
                          GRID_parameters%author_name, GRID_parameters%author_email, GRID_parameters%author_affil1, &
                          GRID_parameters%author_affil2, GRID_parameters%author_affil3
  END IF print_version

  RETURN

 1001 FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/ &
         1x,'***** PROGRAM: ',a15,24x,'*****',/ &
         1x,'***** VERSION: ',i2.2,'.',i2.2,'.',i2.2,31x,'*****',/ &
         1x,'***** DATE:    ',i2.2,'/',i4.4,32x,'*****',/ &
         1x,'***** AUTHOR:  ',a15,24x,'*****',/ &
         1x,'***** E-MAIL:  ',a39,'*****',/ &
         1x,'***** ADDRESS: ',a39,'*****',/ &
         1x,'*****          ',a39,'*****',/ &
         1x,'*****          ',a39,'*****',/ &
         1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****')
 1002 FORMAT(1x,'***** TYPE ',a15,' -h, to get help',12x,'*****',/ &
         1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****')
 1010 FORMAT(1x,'USAGE: ',a15,' [-i|-b] {-d} {-f name} {-h} {-l} {-o} {-r}',/ &
         1x,'       -i: interactive input mode',/ &
         1x,'       -b: batch input mode (read from file)',/ &
         1x,'       -d: switch on diagnostics',/ &
         1x,'       -f: input filename is << name >>',/ &
         1x,'       -h: help information (this output)',/ &
         1x,'       -l: switch on log file output',/ &
         1x,'       -o: redirect standard output to a file',/ &
         1x,'       -r: release information')
 1011 FORMAT(1x,'Copyright (c) 1996-',i4.4,', ',a13)
  END SUBROUTINE io_getcmdline

!*****************************************************************
! DESCRIPTION of [SUBROUTINE io_getinterinput]:
!> @brief  gets user input interactively
!>
!> @param[out]      p_param     control parameters
!
  SUBROUTINE io_getinterinput(p_param)

  IMPLICIT NONE

!---------- local declarations

  TYPE (control_struct), INTENT(out) :: p_param
  INTEGER                            :: i_iost, i_tmp, i_cln, i_alct, &
    i_cnt
  CHARACTER (len=io_fillen)          :: c_tmp

!---------- initialize

  p_param%phy%i_crslevel      = -1
  p_param%phy%i_reflevel      = -1
  p_param%io%l_netcdf         = .FALSE.
  p_param%io%l_gmv            = .FALSE.
  p_param%io%l_vtu            = .FALSE.
  p_param%io%c_triangfile     = 'Triang.dat'

  i_cln= MAX(GRID_parameters%i_stringlength,io_fillen)

!---------- prompt user for input (loop in case of wrong input)

  write(GRID_parameters%ioout,1000) GRID_parameters%program_name, GRID_parameters%version, &
                      GRID_parameters%subversion, GRID_parameters%patchversion

!---------- Adaptivity Control

  write(GRID_parameters%ioout,1001)
  write(GRID_parameters%ioout,1020)
  write(GRID_parameters%ioout,1021,advance='NO')
  read(i_ioin,*) p_param%phy%i_reflevel
  write(GRID_parameters%ioout,1022,advance='NO')
  read(i_ioin,*) p_param%phy%i_crslevel
  write(GRID_parameters%ioout,1027,advance='NO')
  read(i_ioin,*) p_param%phy%r_diam
  IF(p_param%phy%r_diam < 0.0) THEN
    p_param%phy%l_diamcontr= .FALSE.
  ELSE
    p_param%phy%l_diamcontr= .TRUE.
  END IF

!---------- Output Control

  write(GRID_parameters%ioout,1001)
  write(GRID_parameters%ioout,1040)
  write(GRID_parameters%ioout,1049,advance='NO')
  read(i_ioin,*) i_tmp
  IF(i_tmp /= 0) THEN
    p_param%io%l_netcdf = .TRUE.
  END IF
  write(GRID_parameters%ioout,10491,advance='NO')
  read(i_ioin,*) i_tmp
  IF(i_tmp /= 0) THEN
    p_param%io%l_gmv= .TRUE.
  END IF
  write(GRID_parameters%ioout,10492,advance='NO')
  read(i_ioin,*) i_tmp
  IF(i_tmp /= 0) THEN
    p_param%io%l_vtu= .TRUE.
  END IF

!---------- Initial triangulation

  write(GRID_parameters%ioout,1001)
  write(GRID_parameters%ioout,1030)
  write(GRID_parameters%ioout,1047,advance='NO')
  read(i_ioin,2000,iostat=i_iost) c_tmp
  IF(i_iost == 0) THEN
    p_param%io%c_triangfile(1:i_cln)= c_tmp(1:i_cln)
  END IF

!---------- FEM support

  write(GRID_parameters%ioout,1001)
  write(GRID_parameters%ioout,1050)
  write(GRID_parameters%ioout,1051,advance='NO')
  read(i_ioin,*) p_param%io%i_numsigs
  ALLOCATE(p_param%io%c_sigfiles(p_param%io%i_numsigs), stat=i_alct)
  IF(i_alct /= 0) CALL grid_error(c_error='[io_getbatchinput] Could not allocate c_sigfiles')
  DO i_cnt=1,p_param%io%i_numsigs
    write(GRID_parameters%ioout,1052,advance='NO')
    read(i_ioin,2000,iostat=i_iost) c_tmp
    IF(i_iost == 0) p_param%io%c_sigfiles(i_cnt)(1:i_cln)= c_tmp(1:i_cln)
  END DO

!---------- error handling

  no_value: IF((p_param%phy%i_crslevel      < 0)   .OR. &
               (p_param%phy%i_reflevel      < 0)) THEN
    CALL grid_error(c_error='[get_interinput]: incompatible coarse and fine levels given')
  END IF no_value
  RETURN

 1000 FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/ &
         1x,'***** PROGRAM: ',a15,24x,'*****',/ &
         1x,'***** VERSION: ',i2.2,'.',i2.2,'.',i2.2,31x,'*****',/ &
         1x,'*****          Started in INTERACTIVE input mode      *****',/ &
         1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/)
 1001 FORMAT(1x,'-----------------------------------------------------------',/)
 1002 FORMAT(1x,'---------------------- end of input -----------------------',/)
 1010 FORMAT(1x,'                        Experiment')
 1020 FORMAT(1x,'                    Adaptivity Control')
 1030 FORMAT(1x,'                   Initial Triangulation')
 1040 FORMAT(1x,'                   Input/Output Control')
 1050 FORMAT(1x,'                        FEM Support')
 1011 FORMAT(1x,'INPUT: Experiment No. (first exp. = 0)              > ')
 1021 FORMAT(1x,'INPUT: Finest level of refinement                   > ')
 1022 FORMAT(1x,'INPUT: Coarsest level of refinement                 > ')
 1023 FORMAT(1x,'INPUT: Tolerance for refinement (|t_r| < 1)         > ')
 1024 FORMAT(1x,'INPUT: Tolerance for Coarsening (t_c < t_r)         > ')
 1025 FORMAT(1x,'INPUT: Watermark for refinement (|w_r| < 1)         > ')
 1026 FORMAT(1x,'INPUT: Watermark for coarsening (|w_c| < 1)         > ')
 1027 FORMAT(1x,'INPUT: Refine until diam reached? (no = negative)   > ')
 1031 FORMAT(1x,'INPUT: Timestep length (delta t)                    > ')
 1032 FORMAT(1x,'INPUT: First timestep number                        > ')
 1033 FORMAT(1x,'INPUT: Last timestep number                         > ')
 1041 FORMAT(1x,'INPUT: Number of timesteps between saves            > ')
 1042 FORMAT(1x,'INPUT: Number of timesteps between plots            > ')
 1043 FORMAT(1x,'INPUT: Save last step for next experiment (no = 0)  > ')
 1044 FORMAT(1x,'INPUT: Plotting desired? I need the type of plot',/ &
         1x,'        0: no plot desired',/ &
         1x,'        7: plot the plain grid',/ &
         1x,'        8: plot the wind vector field',/ &
         1x,'       10: plot tracer scalar field                 > ')
 1045 FORMAT(1x,'INPUT: Plot polygonal outline (no = 0)              > ')
10451 FORMAT(1x,'INPUT: Filename for polygon data    (Polyline.dat)  > ')
 1046 FORMAT(1x,'INPUT: Read wind data from file (no = 0)            > ')
10461 FORMAT(1x,'INPUT: Filename for wind data       (Flow.dat)      > ')
 1047 FORMAT(1x,'INPUT: Filename for triangulation   (Triang.dat)    > ')
 1049 FORMAT(1x,'INPUT: Write NetCDF compatible output file (no = 0) > ')
10491 FORMAT(1x,'INPUT: Write gmv compatible output file (no = 0)    > ')
10492 FORMAT(1x,'INPUT: Write vtu compatible output file (no = 0)    > ')
 1051 FORMAT(1x,'INPUT: Number of FEM types supported/given in .ftf  > ')
 1052 FORMAT(1x,'INPUT: Filename for FEM signature .ftf file         > ')

 2000 FORMAT(a32)

  END SUBROUTINE io_getinterinput

!*****************************************************************
! DESCRIPTION of [SUBROUTINE io_getbatchinput]:
!> @brief reads user input from file
!>
!> @param[in]       p_param     control parameters
!
  SUBROUTINE io_getbatchinput(p_param)

  IMPLICIT NONE

!---------- local declarations

  TYPE (control_struct), INTENT(out)         :: p_param
  INTEGER, PARAMETER                         :: i_iofil= 10
  CHARACTER (len=80)                         :: a_filrow
  CHARACTER (len=io_fillen)                  :: c_tmp
  INTEGER                                    :: i_iost, i_ioend, &
    i_tmp, i_cln, i_cnt, i_alct

!---------- initialize

  p_param%phy%i_crslevel      = -1
  p_param%phy%i_reflevel      = -1
  p_param%phy%r_diam          = -1.
  p_param%io%l_netcdf         = .FALSE.
  p_param%io%l_gmv            = .FALSE.
  p_param%io%l_vtu            = .FALSE.
  p_param%io%c_triangfile     = 'Triang.dat'

  i_cln= MAX(GRID_parameters%i_stringlength,io_fillen)

!---------- open input file

  open(unit= i_iofil, file= p_param%cmd%c_batchfile, status= 'OLD', &
       action= 'READ', iostat= i_iost)
  file_notopen: IF(i_iost /= 0) THEN
    IF(GRID_parameters%iolog > 0) &
      write(GRID_parameters%iolog,*) 'ERROR: Filename: ', p_param%cmd%c_batchfile
    CALL grid_error(c_error='[get_batchinput]: could not read input file')
  ELSE file_notopen
    write(GRID_parameters%ioout,1000) GRID_parameters%program_name, &
      GRID_parameters%version,GRID_parameters%subversion, GRID_parameters%patchversion, &
      p_param%cmd%c_batchfile
    IF(GRID_parameters%iolog > 0) THEN
      write(GRID_parameters%iolog,*) 'INFO: Filename: ', p_param%cmd%c_batchfile, ' opened on unit: ', i_iofil
      write(GRID_parameters%iolog,1000) GRID_parameters%program_name, &
        GRID_parameters%version,GRID_parameters%subversion, GRID_parameters%patchversion, &
        p_param%cmd%c_batchfile
    END IF
  END IF file_notopen

!---------- read line by line

  read_loop: DO
    read(i_iofil,2000,iostat=i_ioend) a_filrow

!---------- if file ended

    file_end: IF(i_ioend /= 0) THEN
      close(i_iofil)
      IF(GRID_parameters%iolog > 0) &
        write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_iofil
      EXIT
    ELSE file_end

!---------- decide what to DO with line according to first character

      comment_line: IF(a_filrow(1:1) == '#' .or. a_filrow(1:1) == '!') THEN
        CYCLE read_loop
      ELSE IF(a_filrow(1:14) == 'FINE_GRID_LEVE') THEN comment_line
        read(i_iofil,*) p_param%phy%i_reflevel
      ELSE IF(a_filrow(1:14) == 'COARSE_GRID_LE') THEN comment_line
        read(i_iofil,*) p_param%phy%i_crslevel
      ELSE IF(a_filrow(1:14) == 'LARGEST_RATIO_') THEN comment_line
        read(i_iofil,*) p_param%phy%r_diam
      ELSE IF(a_filrow(1:14) == 'NETCDF_FILE_PL') THEN comment_line
        read(i_iofil,*) i_tmp
        IF(i_tmp /= 0) p_param%io%l_netcdf = .TRUE.
      ELSE IF(a_filrow(1:14) == 'GMV_FILE_PLOTT') THEN comment_line
        read(i_iofil,*) i_tmp
        IF(i_tmp /= 0) p_param%io%l_gmv= .TRUE.
      ELSE IF(a_filrow(1:14) == 'VTU_FILE_PLOTT') THEN comment_line
        read(i_iofil,*) i_tmp
        IF(i_tmp /= 0) p_param%io%l_vtu= .TRUE.
      ELSE IF(a_filrow(1:14) == 'TRIANG_FILE_NA') THEN comment_line
        read(i_iofil,2010,iostat=i_tmp) c_tmp
        IF(i_tmp == 0) p_param%io%c_triangfile(1:i_cln)= c_tmp(1:i_cln)
      ELSE IF(a_filrow(1:14) == 'NUM_SIGNATURE_') THEN comment_line
        read(i_iofil,*) p_param%io%i_numsigs
      ELSE IF(a_filrow(1:14) == 'SIGNATURE_FILE') THEN comment_line
        ALLOCATE(p_param%io%c_sigfiles(p_param%io%i_numsigs), stat=i_alct)
        IF(i_alct /= 0) CALL grid_error(c_error='[io_getbatchinput] Could not allocate c_sigfiles')
        DO i_cnt=1,p_param%io%i_numsigs
          read(i_iofil,2010,iostat=i_tmp) c_tmp
    IF(i_tmp == 0) p_param%io%c_sigfiles(i_cnt)(1:i_cln)= c_tmp(1:i_cln)
        END DO
      END IF comment_line
    END IF file_end
  END DO read_loop

!---------- diameter control activated?

  IF(p_param%phy%r_diam < 0.0) THEN
    p_param%phy%l_diamcontr=.FALSE.
  ELSE
    p_param%phy%l_diamcontr=.TRUE.
  END IF

!---------- error handling

  no_value: IF((p_param%phy%i_crslevel < 0) .OR. &
               (p_param%phy%i_reflevel < 0)) THEN
    CALL grid_error(c_error='[get_batchinput]: incompatible coarse and fine levels given')
  END IF no_value

  RETURN

 1000 FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/ &
         1x,'***** PROGRAM:   ',a15,22x,'*****',/ &
         1x,'***** VERSION:   ',i2.2,'.',i2.2,'.',i2.2,29x,'*****',/ &
         1x,'*****            Started in BATCH input mode',10x,'*****',/ &
         1x,'***** INPUTFILE: ',a20,17x,'*****',/ &
         1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****')
 2000 FORMAT(a80)
 2010 FORMAT(a32)

  END SUBROUTINE io_getbatchinput

!*****************************************************************
! DESCRIPTION of [SUBROUTINE  io_putparameters]:
!> @brief writes out parameters in a nice way
!>
!> @param[in]       p_param     data structure containing parameters
!
  SUBROUTINE io_putparameters(p_param)

  IMPLICIT NONE

!---------- local declarations

  TYPE (control_struct), INTENT(in) :: p_param
  INTEGER                           :: i1, i2

!---------- temporary store

  i1 = p_param%phy%i_reflevel
  i2 = p_param%phy%i_crslevel

!---------- write statement

  write(GRID_parameters%ioout,1000) i1, i2
  IF(GRID_parameters%iolog > 0) &
    write(GRID_parameters%iolog,1000) i1, i2

  RETURN

 1000 FORMAT(1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****',/ &
         1x,'*****                Global Parameters',16x,'*****',/ &
         1x,'***** ----- ----- ----- ----- ----- ----- ----- ----- *****',/ &
         1x,'***** Finest grid level',22x,i8,' *****',/ &
         1x,'***** Coarsest grid level',20x,i8,' *****',/ &
         1x,'***** ***** ***** ***** ***** ***** ***** ***** ***** *****')

  END SUBROUTINE io_putparameters

!*****************************************************************
! DESCRIPTION of [SUBROUTINE io_putinputfile]:
!> @brief prints an input file conforming to io_getbatchinput
!>
!> @param[in]       p_param     global parameter data structure
!
  SUBROUTINE io_putinputfile(p_param)

  IMPLICIT NONE

!---------- local declarations

  TYPE (control_struct), INTENT(in) :: p_param
  INTEGER                           :: i1, i2, i3, i4, i5
  INTEGER                           :: i_unit=15, i_fst
  CHARACTER (len=32)                :: c_file
  CHARACTER (len=28)                :: c_tmp

!---------- temporary store

  i1 = p_param%phy%i_reflevel
  i2 = p_param%phy%i_crslevel
  i3 = 0
  IF(p_param%io%l_netcdf) i3 = 1
  i4 = 0
  IF(p_param%io%l_gmv) i4 = 1
  i5 = 0
  IF(p_param%io%l_vtu) i5 = 1

!---------- open file

    write(c_tmp,*) trim(GRID_parameters%program_name), '_input.'
    write(c_file,1010) trim(c_tmp), i1
    c_file= adjustl(c_file)
    open(i_unit, file= c_file, action= 'write', form= 'formatted', &
         status='replace', iostat= i_fst)
    not_opened: IF(i_fst /= 0) THEN
      CALL grid_error(c_error='[io_putinputfile]: could not open output file')
    END IF not_opened
    IF(GRID_parameters%iolog > 0) &
      write(GRID_parameters%iolog,*) 'INFO: Filename: ', c_file, ' opened on unit: ', i_unit

!---------- write statement

  write(i_unit,1001) c_file, GRID_parameters%program_name
  write(i_unit,1002) i1, i2, i3, i4, i5
  write(i_unit,1003) p_param%io%c_triangfile

!---------- open file

  close(i_unit)
  IF(GRID_parameters%iolog > 0) &
    write(GRID_parameters%iolog,*) 'INFO: Closed file on unit: ', i_unit

  RETURN

 1001 FORMAT('# --- --- --- --- --- --- --- --- --- --- --- --- ---',/ &
         '# Parameter file ',a32,/ &
         '# created automatically by program ',a15,/ &
         '# --- --- --- --- --- --- --- --- --- --- --- --- ---')
 1002 FORMAT('FINE_GRID_LEVEL',/ &
         i8,/ &
         'COARSE_GRID_LEVEL',/ &
         i8,/ &
         'NETCDF_FILE_PLOTTING',/ &
         i8,/ &
         'GMV_FILE_PLOTTING',/ &
         i8)
 1003 FORMAT('TRIANG_FILE_NAME',/ &
         a32,/ &
         '# --- ---  End of parameter file  --- --- --- --- ---')
 1010 FORMAT(a28,i4.4)

  END SUBROUTINE io_putinputfile

END MODULE IO_utils
