!*****************************************************************
!
!> @file SLM_errorestimate.f90
!> @brief includes MODULE SLM_errorestimate
!
!*****************************************************************
!
! VERSION(S):
!  1. original version              j. behrens  11/96
!  2. nodal values time depend.     j. behrens  01/97
!  3. FEM_handle added              j. behrens  07/97
!  4. version with hashing          j. behrens  07/97
!  5. changed to use GRID_api       j. behrens  11/97
!
!*****************************************************************
! MODULE DESCRIPTION:
!> provides an error estimator for the adaptive SLM scheme
!
MODULE SLM_errorestimate
  USE GRID_api
  PRIVATE
  PUBLIC :: slm_errorest
  CONTAINS
!*****************************************************************
! DESCRIPTION of [FUNCTION slm_error]:
!> @brief estimate the error elementwise
!>
!> @param[in]       i_siz     coordinate
!> @param[in]       r_v       values at element nodes
!> @return          r_esterr  estimated error
!>
!> @note  this is only a simple estimator based on local gradients
!
  FUNCTION slm_error(i_siz,r_v) RESULT (r_esterr)

!---------- local declarations

    IMPLICIT NONE
    REAL (KIND = GRID_SR), DIMENSION(:)     :: r_v
    INTEGER                                 :: i_siz, i_ind1, i_cnt
    REAL (KIND = GRID_SR)                   :: r_esterr
    REAL (KIND = GRID_SR), DIMENSION(i_siz) :: r_d

!---------- calculate differences

    DO i_cnt=1,i_siz
      i_ind1= mod(i_cnt,i_siz)+ 1
      r_d   = abs(r_v(i_cnt)- r_v(i_ind1))
    END DO

!---------- this is the estimated error

    r_esterr= SUM(r_d)/ REAL(i_siz,GRID_SR)

    RETURN
  END FUNCTION slm_error

!*****************************************************************
! DESCRIPTION of [SUBROUTINE slm_errorest]:
!> @brief this hides the loop structure from the advection part
!>
!> @param[in]       p_ghand   handle for the grid
!> @param[in]       i_siz     size of array for local errors
!> @param[out]      r_arr     estimated error
!
  SUBROUTINE slm_errorest(p_ghand, i_siz, r_arr)

!---------- local declarations

    IMPLICIT NONE
    TYPE (grid_handle), INTENT(in)                     :: p_ghand
    INTEGER, INTENT(in)                                :: i_siz
    REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_xytmp
    REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE :: r_val
    INTEGER, DIMENSION(:,:), ALLOCATABLE               :: i_edofs
    REAL (KIND = GRID_SR), DIMENSION(:), INTENT(out)   :: r_arr
    INTEGER                                            :: i_cnt, i_size, &
      i_alct, i_ftyp, i_unknow, i_dofnum
    INTEGER, DIMENSION(1)                              :: i_valpoint

!---------- determine FEM type of tracer variable

    i_ftyp= grid_femvarquery(GRID_tracer)
!     IF(i_ftyp /= 1) THEN
!       WRITE(*,*) 'ERROR: FEM type inconsistent, stopped ...'
!       STOP
!     END IF
    CALL grid_femtypequery(i_ftyp, i_unknowns=i_unknow)
    i_dofnum= p_ghand%i_unknowns(i_ftyp)

!---------- allocate auxilliary array

    i_size= p_ghand%i_enumfine
    IF(i_siz /= i_size) THEN
      WRITE(*,*) 'ERROR: array size inconsistent, stopped ...'
      STOP
    END IF

    i_valpoint= (/ GRID_tracer /)
    ALLOCATE(r_val(1,i_dofnum), i_edofs(i_unknow,i_size), stat=i_alct)
    IF(i_alct /= 0) THEN
      WRITE(*,*) 'ERROR: SLM_ERROREST could not allocate arrays ...'
      STOP
    END IF

!---------- get values

    CALL grid_getinfo(p_ghand, i_arraypoint=i_valpoint, r_dofvalues= r_val, &
                      i_femtype=i_ftyp, i_elementdofs=i_edofs)

!---------- loop through all elements of finest loop

    elmt_loop: DO i_cnt=1,i_siz
      r_arr(i_cnt)= slm_error(i_unknow,r_val(1,i_edofs(:,i_cnt)))
    END DO elmt_loop

!---------- deallocate and exit

    DEALLOCATE(r_val, i_edofs)

    RETURN
  END SUBROUTINE slm_errorest

END MODULE SLM_errorestimate
