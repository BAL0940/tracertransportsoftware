/*!***************************************************************
*
* @file IO_pgm_c.c
* @brief text
*
*****************************************************************/

#include <stdio.h>
#define TRUE 1
#define FALSE 0
#include <pgm.h>
#include <string.h>
#include <math.h>

FILE *pgmFile;
int pgmCols, pgmRows, pgmFormat;
gray pgmMaxVal, **pgmImage;

/*!***************************************************************
* DESCRIPTION of [FUNCTION c_openpgm]:
* @brief brief description
*
* @param[in]        c_filename
* @param[in]        i_fn_len
* @param[in]        i_width
* @param[in]        i_height
* @return                       return value
*/
/*
  FUNCTION c_openpgm(c_filename, i_fn_len, i_width, i_height) RESULT(i_ret)
    IMPLICIT NONE
    CHARACTER (len=256)          :: c_filename
    INTEGER                      :: i_fn_len, i_width, i_height
    INTEGER                      :: i_ret
  END FUNCTION c_openpgm
*/
int c_openpgm_(char* c_filename, int* i_fn_len, int* i_width, int* i_height){
  char filename[1025];
  char appName[265];
  char *argv[1];
  int argc;

  //printf("c_openpgm_() ...\n");

#ifdef SAMATOS
  strcpy(appName, "Samatos");
#else
  strcpy(appName, "Amatos");
#endif
  argv[0] = appName;
  argc = 1;
  pgm_init(&argc, argv);

  strncpy(filename, c_filename, *i_fn_len);
  filename[*i_fn_len] = 0;

  pgmFile = fopen(filename, "r");
  if(pgmFile == NULL){
    perror("c_openpgm_");
    return 0;
  }

  pgm_readpgminit(pgmFile, &pgmCols, &pgmRows, &pgmMaxVal, &pgmFormat);
  *i_width = pgmCols;
  *i_height = pgmRows;

  pgmImage = pgm_allocarray(pgmCols, pgmRows);
  //printf("pgmImage=%p\n", pgmImage);

  return 1;
}

/*!***************************************************************
* DESCRIPTION of [FUNCTION c_readpgm]:
* @brief brief description
*
* @param[in]        i_bitmap
* @return                       return value
*/
/*
  FUNCTION c_readpgm(i_bitmap) RESULT(i_ret)
    IMPLICIT NONE
    INTEGER, DIMENSION(:,:)      :: i_imageBuffer
    INTEGER                      :: i_ret
  END FUNCTION c_readpgm
*/
int c_readpgm_(int **i_bitmap){
  int row, col;

  //printf("c code:\n");
  //printf("pgmRows = %d\n", pgmRows);
  //printf("pgmCols = %d\n", pgmCols);

  rewind(pgmFile);
  pgmImage = pgm_readpgm(pgmFile, &pgmCols, &pgmRows, &pgmMaxVal);


#ifdef f90
#define ROW_OFFSET 0
#define COL_OFFSET 0
#elif ifc
#define ROW_OFFSET 1
#define COL_OFFSET 1
#else
#define ROW_OFFSET 0
#define COL_OFFSET 0
#endif

  for(row = 0; row < pgmRows; ++row){
    for(col = 0; col < pgmCols; ++col){
      if(pgmImage[row][col] != 0){
  (*i_bitmap)[(ROW_OFFSET+row)*pgmCols+(COL_OFFSET+col)] = 1;
  //printf("0");
      }else{
  (*i_bitmap)[(ROW_OFFSET+row)*pgmCols+(COL_OFFSET+col)] = 0;
  //printf("*");
      }
      //printf("\n");
    }
    //printf("\n");
  }

  return 1;
}

/*!***************************************************************
* DESCRIPTION of [FUNCTION c_closepgm]:
* @brief brief description
*
* @return                       return value
*/
/*
  FUNCTION c_closepgm() RESULT(i_ret)
    IMPLICIT NONE
    INTEGER                      :: i_ret
  END FUNCTION c_closepgm
*/
int c_closepgm_(){
  pgm_freearray(pgmImage, pgmRows);
  fclose(pgmFile);
  return 1;
}
