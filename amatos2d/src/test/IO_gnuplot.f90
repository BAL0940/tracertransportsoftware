!*****************************************************************
!
!> @file IO_gnuplot.f90
!> @brief includes module IO_gnuplot
!
!*****************************************************************
!
! VERSION(S):
!  1. original version        n. rakowsky       03/2001
!
!*****************************************************************
! MODULE DESCRIPTION:
!> performs rendering into a gnuplot compatible file
!
MODULE IO_gnuplot
  USE GRID_api
  PRIVATE
  INTEGER, SAVE :: i_timecounter= 0
  PUBLIC :: plot_gnuplot
  CONTAINS
!*****************************************************************
! DESCRIPTION of [SUBROUTINE plot_gnuplot]:
!> @brief create output for visualization by gnuplot
!>
!> @param[in]       p_handle    grid handle for the linked lists
!> @param[in]       i_time      time stamp for file naming
!
  SUBROUTINE plot_gnuplot(p_handle, i_time)

    IMPLICIT NONE

!---------- local declarations

    TYPE (grid_handle), INTENT(in)       :: p_handle
    INTEGER, OPTIONAL, INTENT(in)        :: i_time
    INTEGER                              :: i_io1, i_cnt, i_tim, i_alct, i_enum
    INTEGER                              :: i_tcnt, i_fst, i_nnum
    REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_xytmp
    REAL (KIND = GRID_SR), DIMENSION(:,:), ALLOCATABLE  :: r_val
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: i_tets
    CHARACTER (len=32)                   :: c_gnufile
    CHARACTER (len=28)                   :: c_tmp
#ifdef SAMATOS
    INTEGER, PARAMETER                   :: i_vallen=6
#elif AMATOS
    INTEGER, PARAMETER                   :: i_vallen=5
#endif
    INTEGER, DIMENSION(i_vallen)         :: i_valind

!---------- file handling (open)

    IF(present(i_time)) THEN
      i_tcnt= i_time
      i_timecounter= i_timecounter+1
    ELSE
      i_tcnt= i_timecounter
      i_timecounter= i_timecounter+1
    END IF
    write(c_tmp,*) trim(GRID_parameters%program_name(1:23)), '_gnuplot.'
    write(c_gnufile,1010) trim(c_tmp), i_tcnt
    c_gnufile= adjustl(c_gnufile)
    i_io1= 15
    OPEN(i_io1, file= c_gnufile, form= 'formatted', iostat= i_fst)
    IF(i_fst /= 0) THEN
      RETURN
    END IF

!---------- set time tag

    i_tim= p_handle%i_timetag
    i_enum= p_handle%i_enumfine
    i_nnum= p_handle%i_nnumber

!---------- allocate arrays and extract data

    ALLOCATE(r_xytmp(GRID_dimension, i_nnum), &
             r_val(i_vallen, i_nnum), &
             i_tets(GRID_elementnodes, i_enum), &
             stat=i_alct)
    IF (i_alct /= 0) CALL grid_error(c_error='[plot_gnuplot]: Could not allocate arrays')

#ifdef SAMATOS
    i_valind= (/GRID_ucomp, GRID_vcomp, GRID_wcomp, GRID_phi, GRID_zeta, GRID_tracer/)
#elif AMATOS
    i_valind= (/GRID_ucomp, GRID_vcomp, GRID_phi, GRID_zeta, GRID_tracer/)
#endif
    CALL grid_getinfo(p_handle, l_finelevel=.TRUE., &
                      r_nodecoordinates= r_xytmp,        &
                      r_nodevalues= r_val, i_arraypoint=i_valind, &
          i_elementnodes= i_tets)

!---------- print values

    DO i_cnt=1,i_enum
             write(i_io1,1000) r_xytmp(:,i_tets(1,i_cnt)), r_val(GRID_tracer,i_tets(1,i_cnt))
             write(i_io1,1000) r_xytmp(:,i_tets(2,i_cnt)), r_val(GRID_tracer,i_tets(2,i_cnt))
             write(i_io1,1000) r_xytmp(:,i_tets(3,i_cnt)), r_val(GRID_tracer,i_tets(3,i_cnt))
             write(i_io1,1000) r_xytmp(:,i_tets(1,i_cnt)), r_val(GRID_tracer,i_tets(1,i_cnt))
             write(i_io1,'(a)') ""
    END DO

!---------- close file

    CLOSE(i_io1)

    RETURN
 1000   FORMAT(4f15.6)
 1010   FORMAT(a28,i4.4)

  END SUBROUTINE plot_gnuplot

END MODULE IO_gnuplot
