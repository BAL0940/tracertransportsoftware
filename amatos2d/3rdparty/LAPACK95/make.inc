#
#  -- LAPACK95 interface driver routine (version 2.0) --
#     UNI-C, Denmark; Univ. of Tennessee, USA; NAG Ltd., UK
#     August 5, 2000
#
FC       = gfortran -ffree-form
FC1      = gfortran -ffixed-form
# -dcfuns  Enable recognition of non-standard double
#          precision  complex intrinsic functions
# -dusty   Allows the compilation and execution of "legacy"
#          software by downgrading the category  of  common
#          errors  found  in  such software from "Error" to
# -ieee=full enables all IEEE arithmetic facilities
#          including non-stop arithmetic.

# SET MAIN DIRECTORY PATH
# !! This has to be alterd by user !!
ROOTDIR     = $(HOME)/Transport-20/tracertransportsoftware/amatos2d
LAPACK95DIR = $(ROOTDIR)/3rdparty/LAPACK95

OPTS0    = -O3 -u -V
LAPACK95MOD = $(LAPACK95DIR)/lapack95_modules
OPTS1    = -c $(OPTS0)
OPTS3    = $(OPTS1) -I$(LAPACK95MOD)
OPTL     = -o
OPTLIB   =

# SET atlas/blas DIRECTORY PATH
# !! This has to be alterd by user !!
BLASLIB = -L/usr/lib -lblas

# SET LAPACK DIRECTORY PATH
# !! This has to be alterd by user !!
LAPACKLIB = -L/usr/lib -llapack -lpthread -lc

LAPACK95LIB = -L$(LAPACK95DIR) -llapack95
LAPACK95    = liblapack95.a

TMG77       = /usr/lib64/tmglib.a

# LIBS     = $(LAPACK95LIB) $(TMG77) $(LAPACKLIB) $(BLASLIB)
LIBS     = $(LAPACK95LIB) $(LAPACKLIB) $(BLASLIB)
SUF      = f90

XX = 'rm' -f $@; \
        'rm' -f $@.res; \
	$(FC) $(OPTS0) -o $@ -I$(LAPACK95MOD) $@.$(SUF) $(OPTLIB) $(LIBS); \
        $@ < $@.dat > $@.res; \
        'rm' -f $@

YY = $(FC) $(OPTS0) -o $@ -I$(LAPACK95MOD) $@.$(SUF) $(OPTLIB) $(LIBS)

.SUFFIXES: .f90 .f .o

.$(SUF).o:
	$(FC) $(OPTS3) $<

.f.o:
	$(FC1) $(OPTS3) $<
