##################################################################
#   AMATOS                                                       #
#   Adaptive Mesh generator for                                  #
#   ATmospheric and Oceanic Simulation                           #
##################################################################
# makefile to build AMATOS / library objects of amatos API       #
# j. behrens 2/95, 3/96, 5/97, 12/2003                           #
# l. mentrup 1/2005                                              #
# --- this is for Mac OS X on Intel architecture ---             #
# --- using the intel Fortran compiler ---                       #
##################################################################

# MACHINE
MACHINE = macosx_intel

# SYSTEM
SYSTEM = std-f90

# SET MAKETHING CORRESPONDING TO MACHINE:
MAKETHING= AMATOS
AMATFLAG = $(MF)$(MAKETHING)

SAVETHING= SAVETEST

# OPTIMIZATION SETTINGS [debug|opt|norm|opt]
MODE := debug

# CREATE SHARED LIBRARIES
CREATE_SHARED := yes

# FURTHER OPTIONS
DBL_DBL    := yes

# PARALLELISM SETTINGS
OMP := no
MPI := no

# LIBRARY SETTINGS [yes|no]
# Usage: $(MAKE) NO_NETCDF=yes
NO_NETCDF := no

# LIBRARY SETTINGS [yes|no]
# Usage: make IO_EMIT=yes IO_PGM=yes
IO_EMIT := no
IO_PGM := yes

# ADDITIONAL MACRO DEFS
MACROS =

# SET MAIN DIRECTORY PATH
# !! This has to be alterd by user !!
ROOTDIR = $(HOME)/Documents/Teaching/WS2021/Transport-20/TracerTransportSoftware

# SET atlas/blas DIRECTORY PATH
# !! This has to be alterd by user !!
BLASLIB = #-L$(MKLROOT)/lib -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm

# SET LAPACK DIRECTORY PATH
# !! This has to be alterd by user !!
#LAPACKLIB = -L$(MKLROOT)/lib -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm
LAPACKLIB =  -I${MKLROOT}/include/lp64 -I${MKLROOT}/include ${MKLROOT}/lib/libmkl_blas95_lp64.a ${MKLROOT}/lib/libmkl_lapack95_lp64.a ${MKLROOT}/lib/libmkl_intel_lp64.a ${MKLROOT}/lib/libmkl_core.a ${MKLROOT}/lib/libmkl_sequential.a -lpthread -lm

# SET IO_EMIT LIBRARY PATH
# !! This has to be alterd by user !!
EMITLIB =
ifeq ($(strip $(IO_EMIT)),yes)
  EMITLIB = -lstdc++ #-lresolv -lsocket -lnsl -lCstd
endif

# SET BITMAP LIBRARY PATH
# !! This has to be alterd by user !!
BITMAPLIB =
BITMAPINC =
ifeq ($(strip $(IO_PGM)),yes)
  BITMAPLIB = -L/opt/local/lib -lnetpbm
  BITMAPINC = -I/opt/local/include
endif

# SET NETCDF PATHS
# !! This has to be alterd by user !!
NETCDFLIB =
NETCDFINC =
NCUGRIDLIB =
ifneq ($(strip $(NO_NETCDF)), yes)
  NETCDFLIB = -L/usr/local/lib -lnetcdff -lnetcdf
  NETCDFINC = -I/usr/local/include
  NCUGRIDLIB = -lncugrid
endif

# SET MORE DIRECTORY PATHS
MAINDIR = $(ROOTDIR)/amatos2d

BUILDIR = $(MAINDIR)/compile/$(MACHINE)
LIBDIR  = $(ROOTDIR)/lib/$(MACHINE)
INCDIR  = $(ROOTDIR)/include/$(MACHINE)
DATDIR  = $(MAINDIR)/data
SRCDIR  = $(MAINDIR)/src
#SRCDIR = $(ROOTDIR)/amatos2d/branches/FixPeriodicBC/src/

TSTDIR  = $(SRCDIR)/test
GRIDDIR = $(SRCDIR)/gridgen
SYSDIR  = $(SRCDIR)/system/$(SYSTEM)
TIMDIR  = $(SRCDIR)/timing

GRIDLIB = libamatos.a
GRDSLIB = libamatos.dylib
NCULIB  = libncugrid.a
NCUSLIB = libncugrid.dylib
MODEND  = mod
GRDMOD  = *.$(MODEND)

#----------------------------------------------------------------#
# library and include paths                                      #
#----------------------------------------------------------------#
LIB_SH     = $(LAPACKLIB) $(EMITLIB)
LIBS       = $(NCUGRIDLIB) $(NETCDFLIB) \
             $(LAPACKLIB) $(BLASLIB) $(BITMAPLIB) -L$(LIBDIR) -lamatos
INCPATH    = $(NETCDFINC) -I$(INCDIR) -I.
INCPATH_CC = $(BITMAPINC)

#----------------------------------------------------------------#
# FLAGS FOR Mac OS X/ Intel Fortran                              #
#----------------------------------------------------------------#
F90     = ifort
cc      = gcc
CC      = g++
LOADER  = ifort
AR      = ar
CP      = cp
CPFLAGS =
ARFLAGS = vru
# Compiler flag for macros
MF = -D

# --------------------- next are for debugging ------------------#
ifeq ($(strip $(MODE)),debug)
  FFLAGS = -g -fpic -Wl,-no_pie -check all# pointers #all # -save-temps -C
  CFLAGS = -g -m64
  LDFLAGS = -g -save-temps -static-intel -fpic -Wl,-no_pie
  SHFLAGS = -nofor_main -dynamiclib -single_module -check all #pointers #all #-lifport
endif
# --------------------- next are for optimized debugging --------#
ifeq ($(strip $(MODE)),optdebug)
  FFLAGS = -g -save-temps -fpic -Wl,-no_pie # -C
  CFLAGS = -g -m64
  LDFLAGS = -g -save-temps -fpic -Wl,-no_pie
  SHFLAGS = -nofor_main -dynamiclib -single_module #-lifport
endif
# --------------------- next are for normal compilation ---------#
ifeq ($(strip $(MODE)),norm)
  FFLAGS = -fpic # -C
  CFLAGS =
  LDFLAGS =
  SHFLAGS = -nofor_main -dynamiclib -single_module #-lifport
endif
# --------------------- next are for optimization ---------------#
ifeq ($(strip $(MODE)),opt)
  FFLAGS = -O3 -fpic # -C
  CFLAGS = -O3
  LDFLAGS = -O3
  SHFLAGS = -nofor_main -dynamiclib -single_module #-lifport
endif

# -------- next flag for using dummy graphics library -----------#

# ----- next flags identify the fortran compiler ----------------#
FFLAGS += $(MF)$(F90)
CFLAGS += $(MF)$(F90)

# --------------------- next are for Module IO_EMIT -------------#
ifeq ($(strip $(IO_EMIT)),yes)
  LIB_SH  += $(EMITLIB)
  FFLAGS += $(MF)IO_EMIT
  CFLAGS += $(MF)IO_EMIT
endif
# --------------------- next are for Module IO_PGM --------------#
ifeq ($(strip $(IO_PGM)),yes)
  LIBS  += $(BITMAPLIB)
  FFLAGS += $(MF)IO_PGM
  CFLAGS += $(MF)IO_PGM
endif
# --------------------- next are for NetCDF support -------------#
ifeq ($(strip $(NO_NETCDF)),yes)
  FFLAGS += $(MF)NO_NETCDF
endif
# --------------------- next are for OpenMP ---------------------#
ifeq ($(strip $(OMP)),yes)
  #LIBS   +=
  #FFLAGS +=
  #CFLAGS +=
endif
# --------------------- next are for MPI ------------------------#
ifeq ($(strip $(MPI)),yes)
  #LIBS   +=
  #FFLAGS +=
  #CFLAGS +=
endif
# --------------------- next are for double precision -----------#
ifeq ($(strip $(DBL_DBL)), yes)
  FFLAGS += -DDBL_DBL
endif

#----------------------------------------------------------------#
# additional clear/clean-Items                                   #
#----------------------------------------------------------------#
CLEARSRC_EXT  =
CLEAREX_EXT   = $(SAVETHING) *.dylib
CLEARDAT_EXT  = $(SAVETHING:%=%_*)
CLEARLIB_EXT  = *.dylib
CLEAN_EXT     = *.vtu 
TIDY_EXT      = *.i90 *.s

#----------------------------------------------------------------#
# common stuff                                                   #
#----------------------------------------------------------------#

include $(MAINDIR)/compile/Makefiles/Makefile.common

#----------------------------------------------------------------#
# copy source files                                              #
#----------------------------------------------------------------#

include $(MAINDIR)/compile/Makefiles/Makefile.cpsrc

#----------------------------------------------------------------#
# test programs                                                  #
#----------------------------------------------------------------#

include $(MAINDIR)/compile/Makefiles/Makefile.TestsAmatos

#----------------------------------------------------------------#
# DEPENDENCIES ON INCLUDE FILES                                  #
#----------------------------------------------------------------#
