# Tracer Transport Software Repo

This repository hosts the software, we will be using in the Tracer Transport simulation lab.

It will have two major sub-directories (with corresponding sources):
1. amatos2d
2. flash2d

Note, this software is open, but a lincense grants its usage. The license is available in the doc directory and using the software means accepting the license. The software is copyrighted and the corresponding copyright statement is also to be found in the doc directory of each of the two subdirectories.
